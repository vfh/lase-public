package lase_obsolete.textsearch;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lase.template.QueryToken;
import lase.template.SearchPattern;
import lase.template.Token;
import lase.util.StringsUtils;


public class SimpleTextSearch implements TextSearch {

	@Override
	public String findAnswer(String webPageText, SearchPattern searchPattern) {
		String searchStrings[] = searchPattern.getSearchStrings();
		String regex = StringsUtils.concat(searchStrings, "(.*?)");
		QueryToken qt = searchPattern.get(searchPattern.size()-1);
		if (qt.getToken().hasModifier(Token.Modifier.ANSWER))
			regex += "[\\s,.;:]*([^\\s]+)";
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		Matcher matcher = pattern.matcher(webPageText);
		String answer = "";
		if (matcher.find()) {
			answer = matcher.group(1);
		}
		return answer;
	}

}
