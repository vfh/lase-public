package lase_obsolete.textsearch;

import lase.template.SearchPattern;

/**
 * Rozhraní třídy sloužící k vyhledávání odpovědi v texteh staženích z internetu.  
 * 
 * @author Rudolf Šíma
 *
 */
public interface TextSearch {

	/**
	 * Pokusí se v textu extrahovaném z webové stránky najít odpověď skládající
	 * se za začátku odpovědi (parametr <code>answerOutset</code>) a dalšího
	 * textu, k jehož rozpoznání slouží pomocné informace {@link SearchHints}
	 * (parametr <code>hints</code>).
	 * 
	 * @param webPageText
	 *            - text extrahovaný z webové stránky
	 * @param answerOutset
	 *            - začátek odpovědi
	 * @param hints
	 *            - pomocné informace k rozpoznání textu odpovědi.
	 * 
	 * @return Vrací nalezený text odpovědi nebo prázný řetězec nebo
	 *         </code>null</code> v případě neúspěchu.
	 */
	public String findAnswer(String webPageText, SearchPattern pattern);
	
}
