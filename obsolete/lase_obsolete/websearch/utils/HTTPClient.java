package lase_obsolete.websearch.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lase.util.ByteVector;

/**
 * HTTP Klient sloužící ke stažení webové stránky do řetězce. Z jejího obsahu se
 * pokusí zjistit a nastavit kódování.
 * 
 * @author Rudolf Šíma
 */
public class HTTPClient {
	/** regulární výraz pro vyhledání meta tagu s kódováním */ 
	private static final Pattern encodingMetaPattern = Pattern.compile("<[mM][eE][tT][aA] .* [Cc][hH][aA][rR][sS][eE][tT]=(.*?)[\",\'].*>");
	
	/**
	 * Stáhne stránku určenou jejím URL.
	 * 
	 * @param url
	 *            - adresa stránky
	 * @param timeout
	 *            - maximální čas v milsekundách, po kterém je stahování
	 *            ukončeno.
	 *            
	 * @return Vrací řetězec s obsahem stránky nebo <code>null</code>, pokud se
	 *         stahování nestihlo ve stanoveném čase.
	 *         
	 * @throws MalformedURLException
	 *             Vyhozena v případě, že parametr <code>url</code> není platný
	 *             identifikátor adresy.
	 * @throws IOException
	 *             Vyhozena v případě, že při stahování došlo k chybě čtení.
	 */
	public static String download(String url, int timeout) throws MalformedURLException, IOException {
		return download(new URL(url), timeout);
	}
	
	/**
	 * Stáhne stránku určenou jejím URL.
	 * 
	 * @param url
	 *            - identifikátor adresy stránky
	 * @param timeout
	 *            - maximální čas v milsekundách, po kterém je stahování
	 *            ukončeno.
	 *            
	 * @return Vrací řetězec s obsahem stránky nebo <code>null</code> pokus se
	 *         stahování nestihlo ve stanoveném čase.
	 *         
	 * @throws IOException
	 *             Vyhozena v případě, že při stahování došlo k chybě čtení.
	 */
	public static String download(URL url, int timeout) throws SocketTimeoutException, IOException{
		long startTime = System.currentTimeMillis(); 
		URLConnection connection = url.openConnection();
		connection.setReadTimeout(timeout);

		// identifikace jako Firefox.
		connection.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.6) Gecko/20091216 Fedora/3.5.6-1.fc11 Firefox/3.5.6");
		connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		connection.setRequestProperty("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		
		InputStream iStream = connection.getInputStream();
		
		int bytesRead; 
		byte[] buffer = new byte[1024];
		ByteVector byteContent = new ByteVector(buffer.length); 
		
		Charset detectedCharset = null;
		boolean timedOut = false;
		while (!timedOut && (bytesRead = iStream.read(buffer)) > 0) {
			
			// stahování trvá moc dlouho, ukončit
			if (System.currentTimeMillis() > startTime + timeout)
				timedOut = true;
			
			byteContent.add(buffer, 0, bytesRead);
			// Vyhledávání meta tagu se znakovou sadou, pokud ještě nebyla
			// v dosud stažených datech nalezena.
			if (detectedCharset == null) {
				String chunk = new String(byteContent.toByteArray());
				detectedCharset = detectCharset(chunk);
			}
		}
		iStream.close();
		
		if (detectedCharset == null)
			detectedCharset = Charset.defaultCharset();
		
		return new String(byteContent.toByteArray(), detectedCharset);
	}
	
	
	/**
	 * Pokus o nalezení meta tagu s kódováním ve staženém úseku dokumentu.
	 * 
	 * @param content
	 *            - část dokumentu
	 *            
	 * @return Vrací nalezenou znakovou sadu nebo <code>null</code>, pokud úsek
	 *         dokumentu <code>content</code> neobsahuje hledaný meta tag.
	 */
	private static Charset detectCharset(String content) {		
		Matcher matcher = encodingMetaPattern.matcher(content);
		if (matcher.find()) {
			String charsetName = matcher.group(1).trim();
			if (Charset.isSupported(charsetName));
				return Charset.forName(charsetName);
		}
		return null;
	}
}
