package lase.websearch;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lase.util.Corpus;
import lase.util.Html2Text;
import lase.util.LaseException;
import lase.util.MulticharDelimiterStringTokenizer;

import org.apache.commons.httpclient.Header;


public class GoogleSnippetWebSearchByRegex extends AbstractGoogleWebSearch {
	private static final int numResults = 50;
	private static final Pattern snippetPattern = Pattern.compile("<h3 class=\"r\"><a href=\"(.+?)\".*?>(.+?)</a></h3><div class=\"s\">(.+?)<b>...</b><br>");
	
	public GoogleSnippetWebSearchByRegex() {
	}
	 
	public Corpus search(String[] query) throws LaseException {
		String searchUrl = getSearchUrl(quoteQuery(query), numResults);
		String body = downloadBodyAsString(searchUrl, (Header)null);
		Corpus corpus = new Corpus();
		Matcher snippetMatcher = snippetPattern.matcher(body);

		/* volání find() se někdy zasekne na hrozně dlouho */
		while(snippetMatcher.find()) {
			String url = snippetMatcher.group(1);
			String title = snippetMatcher.group(2);
			String snippetHtml = snippetMatcher.group(snippetMatcher.groupCount());
			snippetHtml = removeSnippetInfo(snippetHtml);
			String snippetText = Html2Text.convert(snippetHtml);
			corpus.addPart(snippetText, title, url);
		}
		return corpus;
	}
	private String removeSnippetInfo(String snippetHtml) {
		final String divStart = "<div";
		final String divEnd = "</div>";
		final Pattern spanPattern = Pattern.compile("(<span class=f>.*?<br>)+(.*)");
		Matcher matcher = spanPattern.matcher(snippetHtml);
		if (matcher.find()) {
			return matcher.group(matcher.groupCount());
		}
		return removeTextSection(snippetHtml, divStart, divEnd);
	}
	
	private String removeTextSection(String text, String startsWith, String endsWith) {
		if (!text.startsWith(startsWith))
			return text;
		MulticharDelimiterStringTokenizer tokenizer = new MulticharDelimiterStringTokenizer(text, startsWith, endsWith);
		int level = 0;
		while(tokenizer.hasNext()) {
			String token = tokenizer.nextToken();
			if (startsWith.equals(token))
				level++;
			else if (endsWith.equals(token)) {
				level--;
				if (level == 0) {
					if (tokenizer.hasNext())
						return tokenizer.nextToken();
					else
						break;
				}
			}
		}
		return text;
	}
}
