package lase.websearch;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.HTML.Tag;
import javax.swing.text.html.parser.ParserDelegator;

import lase.util.Corpus;
import lase.util.LaseException;

import org.apache.commons.httpclient.Header;

/**
 * Parser snippetů na vyhledávací stránce Googlu založený na počítání úrovně
 * zanoření některých tagů. Zdá se, že funguje výrazně spolehlivěji a je
 * odolnější vůči změnám, než varianta s regulárními výrazy.
 * 
 * @author Rudolf Šíma
 * 
 */
public class GoogleSnippetWebSearchByTagNesting extends AbstractGoogleWebSearch {
	private static final int numResults = 50;
	
	@Override
	public Corpus search(String[] query) throws LaseException {
		String searchUrl = getSearchUrl(quoteQuery(query), numResults);
		String body = downloadBodyAsString(searchUrl, (Header)null);
		ParserDelegator delegator = new ParserDelegator();
		InputStream is = new ByteArrayInputStream(body.getBytes());
		Reader reader = new InputStreamReader(is);
		ParserCallback callback = new ParserCallback();
		try {
			delegator.parse(reader, callback, true);
		} catch (IOException e) {
			assert false;
			throw new LaseException(e);
		}
		return callback.corpus;
	}
	
	private class ParserCallback extends HTMLEditorKit.ParserCallback {
		private boolean debug = false;
		int divLevel = 0;
		int spanLevel = 0;
		int citeLevel = 0;
		int aLevel = 0;
		int h3Level = 0;
		StringBuilder snippetBuilder = new StringBuilder(0xFF);
		StringBuilder titleBuilder = new StringBuilder(0x40);
		StringBuilder urlBuilder = new StringBuilder(0x40);
		Corpus corpus = new Corpus();
		
		@Override
		public void handleStartTag(Tag t, MutableAttributeSet a, int pos) {
			if (t.equals(Tag.DIV)) {
				divLevel++;
			} else if (t.equals(Tag.SPAN)) {
				spanLevel++;
			}  else if (t.equals(Tag.CITE)) {
				citeLevel++;
			} else if (t.equals(Tag.A)) {
				aLevel++;
			} else if (t.equals(Tag.H3)) {
				h3Level++;
			}  
			if (debug)
				System.err.println("+" + t);
		}

		@Override
		public void handleEndTag(Tag t, int pos) {
			if (t.equals(Tag.DIV)) {
				divLevel--;
			} else if (t.equals(Tag.SPAN)) {
				spanLevel--;
			} else if (t.equals(Tag.CITE)) {
				citeLevel--;
			} else if (t.equals(Tag.A)) {
				aLevel--;
			} else if (t.equals(Tag.H3)) {
				h3Level--;
			}   
			if (divLevel == 5)
				flushSnippetBuilder();
			if (debug)
				System.err.println("-" + t + divLevel + " " + citeLevel);
		}

		@Override
		public void handleText(char[] data, int pos) {
			// bez optimalizace podmínek z důvodu snadného provádění změn
			if (divLevel == 6 && spanLevel == 0 && citeLevel == 0 && aLevel == 0) {
				snippetBuilder.append(data);
			} else if (h3Level == 1 && aLevel == 1) {
				titleBuilder.append(data);
			} else if (divLevel == 6 && citeLevel == 1) {
				urlBuilder.append(data);
			}
			if (debug)
				System.err.println("=" + new String(data));
		}
		
		void flushSnippetBuilder() {
			String snippetText = snippetBuilder.toString();
			if (snippetText.length() > 0) {
				snippetBuilder.setLength(0);
				String title = titleBuilder.toString();
				titleBuilder.setLength(0);
				String url = urlBuilder.toString();
				urlBuilder.setLength(0);
				corpus.addPart(snippetText, title, url);
			}
		}
	}
}
