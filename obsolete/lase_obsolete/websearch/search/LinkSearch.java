package lase_obsolete.websearch.search;

import java.io.IOException;
import java.util.List;

/**
 * Rozhraní třídy implementující hledání odkazů internetovým vyhledávačem.
 * 
 * @author Rudolf Šíma
 * 
 */
public interface LinkSearch {

	/**
	 * Vyhledání odkazů internetovým vyhledávačem.
	 * 
	 * @param query
	 *            - dotaz pro vyhledávač.
	 * 
	 * @return Vrací seznam nalezených odkazů ve formě řetězců (včetně
	 *         specifikace protokolu).
	 * 
	 * @throws IOException
	 *             Vyhozena, pokud při komunikaci s vyhledávačem nastala chyba.
	 */
	public List<String> findLinks(String query) throws IOException;

}
