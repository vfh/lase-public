package lase_obsolete.websearch.search;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lase_obsolete.websearch.utils.HTTPClient;


/**
 * Hledání odkazů vyhledávačem google.cz
 * 
 * @author Rudolf Šíma
 * 
 */
public class GoogleLinkSearch implements LinkSearch {
	/** formát adresy vyhledávače */
	private static String urlFormat = "http://www.google.cz/search?hl=cs&"
			+ "num=%d&q=%s&btnG=Hledat&lr=lang_cs";
	/** regulární výraz pro nalezení odkazů ve staženém html */
	private static String linkRegEx = "<h3 class=r><a href=\"(.*?)\"";
	/** zkompilovaný regulární výraz <code>linkRegEx</code> */
	private static Pattern linkPattern = Pattern.compile(linkRegEx);
	/** maximální počet hledaných odkazů */
	private int limit;
	/** maximální doba stahování stahování stránky před ukončením pokusu */
	private int timeout;

	/**
	 * Vytvoří novou instanci <code>GoogleSearch</code>, kterou lze opakovaně
	 * používat k vyhledávání odkazů na google.cz.
	 * 
	 * @param limit
	 *            - maximální počet hledaných odkazů
	 * 
	 * @param timeout
	 *            - maximální doba stahování stránky před ukončením pokusu
	 */
	public GoogleLinkSearch(int limit, int timeout) {
		this.limit = limit;
		this.timeout = timeout;
	}

	/**
	 * Vyhledání odkazů internetovým na google.cz.
	 * 
	 * @param query
	 *            - dotaz pro vyhledávač.
	 * 
	 * @return Vrací seznam nalezených odkazů ve formě řetězců (včetně
	 *         specifikace protokolu).
	 * 
	 * @throws IOException
	 *             Vyhozena, pokud při komunikaci s vyhledávačem nastala chyba.
	 */
	public List<String> findLinks(String query) throws SocketTimeoutException,
			IOException {
		List<String> links = new ArrayList<String>(limit);
		String url = getUrl(query);
		String resultPageContent = HTTPClient.download(url, timeout);

		Matcher linkMatcher = linkPattern.matcher(resultPageContent);
		while (linkMatcher.find()) {
			links.add(linkMatcher.group(1));
		}
		return links;
	}

	/**
	 * Vrací řetězec URL v URL kódování odpovídající hledanému dotazu
	 * <code>query</code>
	 * 
	 * @param query
	 *            - hledaný řetězec
	 *            
	 * @return Vrací URL.
	 *           
	 */
	private String getUrl(String query) {

		try {
			query = URLEncoder.encode(query, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String url = String.format(urlFormat, limit, query);
		return url;
	}
}
