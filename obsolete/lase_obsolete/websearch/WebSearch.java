package lase_obsolete.websearch;

import java.io.IOException;

/**
 * Rozhraní třídy implementující vyhledávání a stažení stránek obsahujících
 * danou frázi.
 * 
 * @author Rudolf Šíma
 * 
 */
public interface WebSearch {

	/**
	 * Vrací obsah další stažené stránky. Blokuje volající vlákno do doby, než
	 * je nějaká stránka k dispozici.
	 * 
	 * @return Vrací obsah další stažené stránky nebo <code>null</code>, pokud
	 *         již byly při předchozích voláních vráceny všechny stránky.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při stahování došlo k chybě.
	 */
	public String nextWebPage() throws IOException;

	/**
	 * Uvolní zdroje použité při stahování stránek.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při uvolnění zdrojů došlo k chybě.
	 */
	public void close() throws IOException;

}
