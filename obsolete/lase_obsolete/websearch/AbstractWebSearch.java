package lase_obsolete.websearch;

import java.io.IOException;
import java.util.List;

import lase_obsolete.websearch.download.DownloadManager;
import lase_obsolete.websearch.search.LinkSearch;


/**
 * 
 * 
 * @author Rudolf Šíma
 * 
 * @see {@link WebSearch}
 *
 */
public abstract class AbstractWebSearch implements WebSearch {
	/** Manažer stahování */
	private DownloadManager downloadManager;
	/** Rozhraní internetového vyhledávače sloužící pro získání odkazů na stránky
	 *  obsahující hledanou frázi. */
	private LinkSearch search;
	/** Začátek odpovědi, který se vyhledává */
	private String answerOutset;

	/**
	 * Vytvoří nový stahovač sloužící pro stažení stránek obsahující frázi
	 * <code>answerOutset</code>
	 * 
	 * @param answerOutset
	 *            - hledaná fráze
	 * @param limit
	 *            - maximální počet stažených stránek
	 * @param timeout
	 *            - maximální doba čekání
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při stahování nastala chyba.
	 */
	public AbstractWebSearch(LinkSearch search, String answerOutset, int limit, int timeout) throws IOException {
		this.answerOutset = answerOutset;
		downloadManager = new DownloadManager(timeout);
		this.search = search; 
		search();
	}
	
	/**
	 * Provede vyhledání odkazů a nalezené odkazy dá ke stažení manažeru
	 * stahování.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při stahování nastala chyba.
	 */
	private void search() throws IOException {
		List<String> queryResults = search.findLinks(answerOutset);
		for (String res : queryResults) {
			downloadManager.addLink(res);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public String nextWebPage() throws IOException {
		return downloadManager.getNextPageContent();
	}

	/**
	 * Neprovádí žádnou akci. Uvolnění zdrojů není potřeba.
	 */
	public void close() throws IOException {		
	}
}
