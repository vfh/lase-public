package lase.websearch;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import lase.util.Corpus;
import lase.util.LaseException;
import lase.util.StringUtils;

public abstract class AbstractGoogleWebSearch extends AbstractWebSearch {
	private static final String urlFormat = "http://www.google.cz/search?hl=cs&num=%d&q=%s&btnG=Hledat&lr=lang_cs";

	@Override
	public abstract Corpus search(String[] query) throws LaseException;
	
	protected String getSearchUrl(String query, int numResults) {
		String encodedQuery;
		try {
			encodedQuery = URLEncoder.encode(query, "utf-8");
		} catch (UnsupportedEncodingException e) {
			assert false;
			e.printStackTrace();
			encodedQuery = query;
		}
		String url = String.format(urlFormat, numResults, encodedQuery);
		return url;
	}
	
	protected String quoteQuery(String[] query) {
		return String.format("\"%s\"", StringUtils.concat(query, "\" \""));
	}
}
