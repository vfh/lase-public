package lase_obsolete.websearch;

import java.io.IOException;

import lase_obsolete.websearch.search.GoogleLinkSearch;


public class GoogleWebSearch extends AbstractWebSearch {

	public GoogleWebSearch(String answerOutset, int limit, int timeout)
			throws IOException {
		super(new GoogleLinkSearch(limit, timeout), answerOutset, limit, timeout);
	}
	
}
