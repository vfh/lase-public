package lase_obsolete.websearch.download;

/**
 * Rozhraní třídy implementující reakci na zpětné volání vlákna, provádějícího
 * stažení stránky z Internetu.
 * 
 * @author Rudolf Šíma
 * 
 */
interface DownloadThreadCallback {

	/**
	 * Metoda <code>downloadFinished(String)</code> je volána vláknem
	 * provádějícím stažení stránky z internetu po dokončení stahování a je-jí
	 * předán stažený obsah střánky.
	 * 
	 * @param pageContent
	 *            - řetězec obsahující stažený obsah střánky nebo
	 *            <code>null</code>, bylo-li stahování neúspěšné.
	 */
	public void downloadFinished(String pageContent);

}
