package lase.answersearch;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lase.template.NamedEntitySearch;
import lase.template.QueryToken;
import lase.template.SearchPattern;
import lase.template.TemplateMatchResult;
import lase.template.Token;
import lase.template.NamedEntityToken.Subtype;
import lase.template.Token.Modifier;
import lase.util.Corpus;
import lase.util.LaseException;
import lase.util.MulticharDelimiterStringTokenizer;

// TODO oddědit od abstraktního matcheru
// ... možná ne
/** Toto je pokusná implementace s pokusným nastavením parametrů přímo v kódu. */
public class RegexAnswerMatcher extends AbstractAnswerMatcher {
	@SuppressWarnings("unused")
	private Corpus corpus; 
	private Iterator<Corpus.Entry> corpusIterator;
	private SearchPattern searchPattern;
	private Pattern regexPattern;
	private Queue<Answer> answerQueue = new LinkedList<Answer>();
	
	
	public RegexAnswerMatcher(Corpus corpus, SearchPattern pattern) {
		this.corpus = corpus;
		this.corpusIterator = corpus.iterator();
		this.searchPattern = pattern;
		this.regexPattern = getRegexPattern(pattern);
	}
	

	private static String insertSpaces(String orig) {
		final String[] delimiters = {"-", ".", "!", "?", "(", ")"};
		//@OPT
		//final Set<String> delimSet = new HashSet<String>(Arrays.asList(delimiters));
		// TODO - nepřidávat mezeru, pokud byla předchozí také mezera
		MulticharDelimiterStringTokenizer tokenizer = new MulticharDelimiterStringTokenizer(
				orig, delimiters);
		StringBuilder builder = new StringBuilder();
		while(tokenizer.hasNext()) {
			String token = tokenizer.nextToken();
			//if (!delimSet.contains(token)) {
				builder.append(token);
				builder.append(" ");
			//}
		}
		return builder.toString().trim();
	}
	
	private static Pattern getRegexPattern(SearchPattern searchPattern) {
		//final String separators = "\\s,.\\-!\\?\\):\\(";
		final String separators  = "\\s"; 
		final String wordChars = "\\S";
		final String spaces = "[" + separators + "]*";
		
		StringBuilder regex = new StringBuilder();
		for (QueryToken qt : searchPattern) {
			Token token = qt.getToken();
			// TODO - zjistit, jestli WORD staci
			if ((token.hasModifier(Token.Modifier.ANSWER) && token.voidValue()) || !isGooglable(qt)) {
				switch(token.getType())
				{
				case WORD:
				case NAMED_ENTITY:
					// TODO vyřešit dobrovolné token v otázce
					regex.append("(([" + wordChars + "]+" + separators + "*){1,7})" + spaces);
					break;
				default:
					// SearchPattern smí obsahovat pouze tokeny typu WORD a NAMED_ENTITY
					assert false; 
				}
			} else {
				String word = insertSpaces(qt.getOriginalWord());
				regex.append(escapeRegexReservedChars(word) + spaces);
			}
		}
		return Pattern.compile(regex.toString(), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
	}
	
	private static boolean isGooglable(QueryToken  qt) {
		if (qt.getOriginalWord() == null)
			return false;
		Token token = qt.getToken();
		// TODO vyřešit opt v odpovědi.
		if (token.hasModifier(Modifier.OPT))
			return false;
		switch (token.getType()) {
		case NAMED_ENTITY:
		case WORD:
			return true;
		}
		return false;
	}

	protected Answer findNextAnswer() {
		if (!answerQueue.isEmpty()) 
			return answerQueue.poll();
		
		while(corpusIterator.hasNext()) {
			Corpus.Entry entry = corpusIterator.next();
			String matchedString = insertSpaces(entry.getData());
			Matcher matcher = regexPattern.matcher(matchedString);
			if (matcher.find()) {
				String group = matcher.group(0);
				for (Answer answer : createAnswers(group, entry.getSource())) {
					answerQueue.offer(answer);	
				}
				if (!answerQueue.isEmpty())
					return answerQueue.poll();
			}
		}
		return null;
	}
	
	private Answer[] createAnswers(String group, Corpus.SourcePage source) {
		NamedEntitySearch nes;
		try {
			nes = new NamedEntitySearch(group);
		} catch (LaseException e) {
			e.printStackTrace();
			return new Answer[0];
		}

		TemplateMatchResult[] matchResults = searchPattern.matchQueries(nes);
		List<Answer> result = new ArrayList<Answer>();
		for (int i = 0; i < matchResults.length; i++) {
			assert matchResults[i].matches();
			Map<Token, QueryToken> map = matchResults[i].getTokenMap();
			// @OPT - optimalizace průchodů searchPatternem
			Answer answer = new Answer(source);
			for (QueryToken qt : searchPattern) {
				Token t = qt.getToken();
				if (t.hasModifier(Modifier.ANSWER)) {
					Token patternToken = qt.getToken();
					QueryToken answerToken = map.get(patternToken);
					assert answerToken != null;
					switch(patternToken.getType()) {
					case WORD:
						answer.add(new TextAnswerToken(answerToken.getOriginalWord()));
						break;
					case NAMED_ENTITY:
						//NamedEntityToken ned = (NamedEntityToken) answerToken.getToken();
						answer.add(new NamedEntityAnswerToken((Subtype) patternToken.getSubtype(), 
								answerToken.getOriginalWord(), answerToken.getSemanticValue()));
						break;
					default:
						assert false;
					}
				}
			}
			result.add(answer);
		}
		return result.toArray(new Answer[0]);
	}

	// TODO - použít mapu
	private static String escapeRegexReservedChars(String aRegexFragment){
	    final StringBuilder result = new StringBuilder();

	    final StringCharacterIterator iterator = 
	      new StringCharacterIterator(aRegexFragment)
	    ;
	    char character =  iterator.current();
	    while (character != CharacterIterator.DONE ){
	      /*
	       All literals need to have backslashes doubled.
	      */
	      if (character == '.') {
	        result.append("\\.");
	      }
	      else if (character == '\\') {
	        result.append("\\\\");
	      }
	      else if (character == '?') {
	        result.append("\\?");
	      }
	      else if (character == '*') {
	        result.append("\\*");
	      }
	      else if (character == '+') {
	        result.append("\\+");
	      }
	      else if (character == '&') {
	        result.append("\\&");
	      }
	      else if (character == ':') {
	        result.append("\\:");
	      }
	      else if (character == '{') {
	        result.append("\\{");
	      }
	      else if (character == '}') {
	        result.append("\\}");
	      }
	      else if (character == '[') {
	        result.append("\\[");
	      }
	      else if (character == ']') {
	        result.append("\\]");
	      }
	      else if (character == '(') {
	        result.append("\\(");
	      }
	      else if (character == ')') {
	        result.append("\\)");
	      }
	      else if (character == '^') {
	        result.append("\\^");
	      }
	      else if (character == '$') {
	        result.append("\\$");
	      }
	      else {
	        //the char is not a special one
	        //add it to the result as is
	        result.append(character);
	      }
	      character = iterator.next();
	    }
	    return result.toString();
	  }

}
