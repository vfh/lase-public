package lase.testing;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.TransformerException;

import lase.template.ReferenceToken;
import lase.template.Template;
import lase.template.TemplateCreator;
import lase.template.Token;
import lase.template.Token.Modifier;
import lase.util.Debug;
import lase.util.LaseException;


public class TemplateGenerator {
	
	public TemplateGenerator() {
		
	}
	
	public Template createTemplate(String queryInput, String searchPatternInput) throws LaseException { 
		String searchPatterns[] = searchPatternInput.split("\n");
		
		TemplateCreator creator = new TemplateCreator();
		parseTokens(creator, queryInput);
		for (String searchPattern : searchPatterns) {
			creator.newSearchPattern();
			parseTokens(creator, searchPattern);
		}
		
		return creator.getTemplate();
	}
	
	public void parseTokens(TemplateCreator creator, String input) throws LaseException {
		Pattern pattern  = Pattern.compile("(\"(.*)\")|([^\\s]+)");
		Matcher matcher = pattern.matcher(input);
		while(matcher.find()) {
			String g = matcher.group(2) == null ? matcher.group(0) : matcher.group(2);
			ParsedToken pt = new ParsedToken(g);
			Token t;
			if (pt.tag.equalsIgnoreCase("REF")) {
				t = new ReferenceToken(Integer.parseInt(pt.word));
			} else {
				if (pt.tag.equals("")) {
					pt.tag = "word";
				}
				t = Token.newToken(0, pt.tag, pt.word);
			}
			if (pt.opt)
				t.addModifier(Modifier.OPT);
			if (pt.answer)
				t.addModifier(Modifier.ANSWER);
			t.makeImmutable();
			creator.addToken(t);
		}
	}
	
	private class ParsedToken {
		
		public ParsedToken(String unparsedToken) {
			String[] p  = unparsedToken.split("_");
			if (p.length > 0) {
				word = p[0];
				if (p.length > 1) {
					tag = p[1];
				}
				int k = 1;
				while (++k < p.length) {
					if ("ANSWER".equals(p[k])) {
						answer = true;
					} else if ("OPT".equals(p[k])) {
						opt = true;
					}
				}
			}
			if (word == null)
				word = "";
			if (tag == null)
				tag = "";
		}
		
		String word;
		String tag;
		boolean answer;
		boolean opt;
	}
	
	public String[] groupByQuotes(String[] split) {
		StringBuilder sb = new StringBuilder();
		List<String> results = new ArrayList<String>();
		boolean quotes = false;
		for (String s : split) {
			sb.append(s);
			if (s.contains("\""))
				quotes = !quotes;
			if (quotes) {
				sb.append(" ");
			}
			else {
				results.add(sb.toString().replace("\"", ""));
				sb.setLength(0);
			}
		}
		return results.toArray(new String[0]);
	}
	
	public void storeTemplate(Template t) throws TransformerException {
		final File dir = new File("./data/templates/");
		final Pattern pattern = Pattern.compile(".+([0-9]+)\\.xml");
		File[] tempFiles = dir.listFiles();
		int maxNum = -1;
		for (File tf : tempFiles) {
			Matcher matcher = pattern.matcher(tf.getName());
			if (matcher.matches()) {
				int num = Integer.parseInt(matcher.group(1));
				if (num > maxNum)
					maxNum = num;
			}
		}
		String newName = "template" + String.format("%04d", (maxNum + 1)) + ".xml";
		t.saveToXmlFile(dir.getPath() + "/" + newName);
	}

	public static void main(String[] argv) throws Exception {
		String input;
		try {
			input = Debug.fileToString(argv[0]);
		} catch(Exception e) {
			System.err.println("Chyba argumentu.");
			return;
		}
		String paragraphs[] = input.split("\n\n");
		for(String paragraph : paragraphs) {
			TemplateGenerator gen = new TemplateGenerator();
			Template t;
			try {
				String[] spl = paragraph.split("\n", 2);
				t = gen.createTemplate(spl[0], spl[1]);
			} catch (Exception e) {
				System.err.println("Chyba formátu.");
				e.printStackTrace();
				return;
			}
			gen.storeTemplate(t);
		}
	}
}
