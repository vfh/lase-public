package lase.template;

import java.util.regex.Pattern;

import lase.util.ReadOnlyVector;

@Deprecated
public class TokenPattern {
	private Pattern regex;
	private ReadOnlyVector<String> enumeration;
	
	public TokenPattern(String regex, ReadOnlyVector<String> enumeration) {
		this.regex = Pattern.compile(regex);
		this.enumeration = enumeration;
	}

	public Pattern getRegex() {
		return regex;
	}

	public void setRegex(Pattern regex) {
		this.regex = regex;
	}

	public ReadOnlyVector<String> getEnumeration() {
		return enumeration;
	}

	public void setEnumeration(ReadOnlyVector<String> enumeration) {
		this.enumeration = enumeration;
	}
}
