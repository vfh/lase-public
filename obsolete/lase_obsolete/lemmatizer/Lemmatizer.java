package lase_obsolete.lemmatizer;

import java.io.IOException;

/**
 * Rozhraní lematizátoru, který slouží k převodu všech slov v textu na základní
 * tvar. ("Tento být příklad text po správný lematizace")
 * 
 * @author Rudolf Šíma
 * 
 */
public interface Lemmatizer {

	/**
	 * Provede lematizaci všech slov textu a vrátí výsledný text.
	 * 
	 * @param text
	 *            - Text k lematizaci
	 * 
	 * @return Vrací výsledný text po lematizaci se všemi slovy v základním
	 *         tvaru, proběhla-li lematizace zcela úpspěšně.
	 * 
	 * @throws LemmatizerException
	 *             Tato výjimka by měla být vyhozena v případě, že lematizace
	 *             skončí chybou. Výjimka, který tuto chybu způsobila by měla
	 *             být nastavena jako příčina (<code>cause</code>) výjimky
	 *             <code>LematizeException</code>.
	 */
	public String lematize(String text) throws LemmatizerException;

	/**
	 * Ukončí lematizátor. Instance se od volání metody <code>close</code> stane
	 * nepoužitelnot.
	 * 
	 * @throws IOException
	 */
	public void close() throws IOException;
}
