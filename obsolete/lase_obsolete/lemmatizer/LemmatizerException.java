package lase_obsolete.lemmatizer;


/**
 * Výjimka <code>LematizerException</code> je vyhozena v případě, že
 * lematizátor, který implementuje rozhraní {@link Lemmatizer}, vyhodil
 * výjimku. Tuto původní výjimku by mělo být možné získa voláním
 * metody <code>getCause()</code>, pokud byl lematizátor implementován
 * správně.
 * 
 * @author Rudolf Šíma
 *
 */
public class LemmatizerException extends Exception {
	private static final long serialVersionUID = 5897993260149900613L;

	public LemmatizerException() {
		super();
	}

	public LemmatizerException(String message, Throwable cause) {
		super(message, cause);
	}

	public LemmatizerException(String message) {
		super(message);
	}

	public LemmatizerException(Throwable cause) {
		super(cause);
	}
}
