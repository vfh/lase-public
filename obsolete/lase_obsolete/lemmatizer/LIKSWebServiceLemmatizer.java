package lase_obsolete.lemmatizer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import cz.zcu.fav.liks.morphology.client.MorphologyRPCWebServiceClient;
import cz.zcu.fav.liks.morphology.client.MorphologyRPCWebServiceClient2Beta;
import cz.zcu.fav.liks.morphology.data.MorphologicAnalyser;
import cz.zcu.fav.liks.morphology.data.MorphologyAnalysisException;

/**
 * Implementace lematizátoru využívající webovou službu, ke které je
 * přistupováno pomocí metod třídy {@link MorphologyRPCWebServiceClient2Beta}.
 * 
 * @author Rudolf Šíma
 * 
 */
public class LIKSWebServiceLemmatizer implements Lemmatizer {
	private MorphologicAnalyser analyser;
	private Map<String, String> cache;
	private String cacheFile;
	private boolean useCache;
	private boolean cacheFileUpToDate;

	/**
	 * Vytvoří novou instanci lematizátoru připravenou pro připojení k webové
	 * službě.
	 * 
	 * @param url
	 *            - URL služby
	 * @param useCache
	 *            - Udává, zda má být použita cache. Pro opakovaný dotaz bude
	 *            odpověď nalezena lokálně a webová služba nebude použita.
	 *            Volání metody <code>finalize()</code> nebo
	 *            <code>serialize()</code> NEzpůsobí uložení cache na disk. Toho
	 *            může být dosaženo použitím konstruktoru
	 *            <code>(String, boolean, String)</code>
	 * 
	 * @throws IOException
	 */
	public LIKSWebServiceLemmatizer(String url, boolean useCache)
			throws IOException {
		this(url, useCache, null);
	}

	/**
	 * Vytvoří novou instanci lematizátoru připravenou pro připojení k webové
	 * službě.
	 * 
	 * @param url
	 *            - URL služby
	 * @param useCache
	 *            - Udává, zda má být použita cache. Pro opakovaný dotaz bude
	 *            odpověď nalezena lokálně a webová služba nebude použita.
	 * 
	 * @param cacheFile
	 *            - Pokud je použita cache, volání metody
	 *            <code>finalize()</code> nebo <code>serialize()</code> způsobí
	 *            uložení cache na disk do souboru <code>cacheFile</code>.
	 * 
	 * @throws IOException
	 */
	public LIKSWebServiceLemmatizer(String url, boolean useCache,
			String cacheFile) throws IOException {
		this.useCache = useCache;
		this.cacheFile = cacheFile;
		analyser = new MorphologyRPCWebServiceClient(url);
		if (useCache) {
			if (cacheFile != null) {
				try {
					loadCache();
				} catch (Exception e) {
					cacheFile = null;
				}
			}
			if (cacheFile == null)
				cache = new HashMap<String, String>();
		}
	}

	/**
	 * Vrací zlematizovaný text získaný vyhledáním v cache, a pokud je neúspěšné,
	 * získaný použitím webové služby.
	 */
	public String lematize(String text) throws LemmatizerException {
		String lematizedText = cache == null ? null : cache.get(text);
		if (lematizedText == null) {
			try {
				lematizedText = analyser.lematize(CharFilter.filter(text));
				if (cache != null) {
					cache.put(text, lematizedText);
					cacheFileUpToDate = false;
				}
			} catch (MorphologyAnalysisException e) {
				throw new LemmatizerException(e.getMessage(), e);
			}
		}
		return lematizedText;
	}

	/**
	 * Načte cache ze souboru <code>cacheFile</code> na disku.
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private void loadCache() throws IOException, ClassNotFoundException {
		InputStream fis = new FileInputStream(cacheFile);
		ObjectInputStream ois = new ObjectInputStream(fis);
		cache = (Map<String, String>) ois.readObject();
		cacheFileUpToDate = true;
	}

	/**
	 * Uloží cache do souboru <code>cacheFile</code> na disku.
	 * 
	 * @throws IOException
	 */
	public void serialize() throws IOException {
		if (!useCache || cacheFileUpToDate)
			return;
		OutputStream fos = new FileOutputStream(cacheFile);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(cache);
	}

	/**
	 * Volá metodu <code>serialize()</code>.
	 */
	public void close() throws IOException {
		serialize();
	}
}
