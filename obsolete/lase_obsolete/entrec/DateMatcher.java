package lase.entrec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lase.morphology.MorphWord;
import lase.template.NamedEntityToken.Subtype;
import lase.util.LaseCalendar;

@SuppressWarnings("unused")
@Deprecated
// použít NewDateMatcher
public class DateMatcher extends NumberMatcher {
	private static final Map<String, Integer> months = new HashMap<String, Integer>();
	private static String endToken = "\0";
	static {
		int i = 0;
		months.put("leden", ++i);
		months.put("únor", ++i);
		months.put("březen", ++i);
		months.put("duben", ++i);
		months.put("květen", ++i);
		months.put("červen", ++i);
		months.put("červenec", ++i);
		months.put("srpen", ++i);
		months.put("září", ++i);
		months.put("říjen", ++i);
		months.put("listopad", ++i);
		months.put("prosinec", ++i);
	}
	
	private List<MorphWord> tokens;
	private String curToken;
	private String curTokenOrig;
	private DateBuilder dateBuilder = new DateBuilder();
	private List<PositionedEntity> entityList = new ArrayList<PositionedEntity>();
	private int curPos;
	private int mark;
	private DateBuilder markedBuilder;
	private boolean finished;
	
	// TODO - nahradit DateBuilder přímo lase.util.LaseCalendar
	private static class DateBuilder implements Cloneable{
		private int day, month, year;
		private int hour, minute, second;
		private boolean timeSet;
		private boolean dateSet;
		private StringBuilder words = new StringBuilder();
		private int wordsCount;
		
		public DateBuilder() {
			reset();
		}
		
		public LaseCalendar getDate() {
			//Calendar cal = new GregorianCalendar(year, month-1, day, hour, minute, second);
			return new LaseCalendar(year, month, day, hour, minute, second);
		}
		
		public String toString() {
			return String.format("%d.%d.%d %02d:%02d:%02d", day, month, year, hour, minute, second);
		}
		
		public boolean isTimeSet() {
			return timeSet;
		}

		public boolean isDateSet() {
			return dateSet;
		}
		
		public void setDay(int day) {
			this.day = day;
			this.dateSet = true;
		}

		public void setMonth(int month) {
			this.month = month;
			this.dateSet = true;
		}

		public void setYear(int year) {
			this.year = year;
			this.dateSet = true;
		}

		public void setHour(int hour) {
			this.hour = hour;
			this.timeSet = true;
		}

		public void setMinute(int minute) {
			this.minute = minute;
			this.timeSet = true;
		}

		public void setSecond(int second) {
			this.second = second;
			this.timeSet = true;
		}
		
		public void addWord(String w) {
			wordsCount++;
			words.append(w);
			words.append(' ');
		}
		
		public String getWords() {
			if (words.length() == 0)
				return "";
			int newLen = words.length() - 1;
			if (words.charAt(newLen) == ' ')
				words.setLength(newLen);
			return words.toString();
		}
		
		public int getWordsCount() {
			return wordsCount;
		}
		
		public void reset() {
			day = -1;
			month = -1;
			year = -1;
			hour = -1;
			minute = -1;
			second = -1;
			timeSet = false;
			dateSet = false;
			words.setLength(0);
			wordsCount = 0;
		}
		
		public DateBuilder clone() {
			DateBuilder clone = new DateBuilder();
			clone.dateSet = dateSet;
			clone.timeSet = timeSet;
			clone.day = day;
			clone.month = month;
			clone.year = year;
			clone.hour = hour;
			clone.minute = minute;
			clone.second = second;
			clone.words = new StringBuilder(words);
			clone.wordsCount = wordsCount;
			return clone;
		}
	}

	private boolean readToken()  {
		if (curPos < tokens.size()) {
			MorphWord mw = tokens.get(curPos++);
			curToken = mw.getLemma();
			curTokenOrig = mw.getWord(); 
			return true;
		}
		finished = true;
		curToken = endToken;
		return false;
	}

	private boolean accept(String token) {
		if (curToken == null)
			return false;
		if (token.equals(curToken)) {
			dateBuilder.addWord(curTokenOrig);
			readToken();
			return true;
		}
		return false;
	}

	private boolean acceptNumber(int min, int max) {
		if (curToken == null)
			return false;
		if (!isInteger(curToken))
			return false;
		int val = Integer.parseInt(curToken);
		if (min < max) {
			if (val < min || val > max)
				return false;
		}
		dateBuilder.addWord(curTokenOrig);
		readToken();
		return true;
	}
	
	private boolean acceptDay() {
		String prev = curToken;
		if (acceptNumber(1, 31)) {
			dateBuilder.setDay(Integer.parseInt(prev)); 
			return true;
		}
		return false;
	}
	
	private boolean acceptMonthName() {
		if (months.containsKey(curToken)) {
			dateBuilder.setMonth(months.get(curToken));
			dateBuilder.addWord(curTokenOrig);
			readToken();
			return true;
		}
		return false;
	}
	
	private boolean acceptMonth() {
		String prev = curToken;
		if (acceptNumber(1, 12)) {
			dateBuilder.setMonth(Integer.parseInt(prev)); 
			return true;
		}
		return false;
	}
	
	private boolean acceptYear() {
		mark();
		accept("v");
		if (!accept("rok"))
			rollback();
		String prev = curToken;
		if (acceptNumber(0, 3000)) {
			dateBuilder.setYear(Integer.parseInt(prev)); 
			return true;
		}
		return false;
	}
	
	private boolean acceptHour() {
		String prev = curToken;
		if (acceptNumber(0, 23)) {
			dateBuilder.setHour(Integer.parseInt(prev)); 
			return true;
		}
		return false;
	}
	
	private boolean acceptMinute() {
		String prev = curToken;
		if (acceptNumber(0, 60)) {
			dateBuilder.setMinute(Integer.parseInt(prev)); 
			return true;
		}
		return false;
	}
	
// TODO - otestovat rozpoznávání časových údajů
//	private boolean acceptSecond() {
//		String prev = curToken;
//		if (acceptNumber(0, 60)) {
//			dateBuilder.setSecond(Integer.parseInt(prev)); 
//			return true;
//		}
//		return false;
//	}
	
	private boolean acceptDate() {
		if (acceptDay()) {
			if (accept(".")) {
				if (acceptMonth()) {
					if (accept(".")) {
						if (acceptYear()) {
							return true;
						}
					}
				} else if (acceptMonthName()){
					if (acceptYear()) {
						return true;
					}
				}
			}
		}
		if (acceptMonthName()) {
			if (acceptYear()) {
				return true;
			}
		}
		if (acceptYear())
			return true;
		return false;
	}
	
	private boolean acceptTime() {
		accept("v");
		String prev = curToken;
		if (acceptHour()) {
			if (accept("hodina")) {
				dateBuilder.setHour(Integer.parseInt(prev));
				return true;
			} else if (accept(":")) {
				prev = curToken;
				if (acceptMinute()) {
					dateBuilder.setMinute(Integer.parseInt(prev));
				}
			}
		}
		return false;
	}

	private void reset(List<MorphWord> tokens) {
		this.tokens = tokens;
		this.entityList.clear();
		this.curPos = 0;
		this.mark = -1;
		this.finished = false;
	}
	
	private void parse() {
		readToken();
		int tokenCount = tokens.size();
		while(curPos < tokenCount) {
			if (acceptDate()) {
				mark();
				if (acceptTime()) {
					storeDate();
				} else {
					rollback();
					storeDate();
				}
			} else if (acceptTime()) {
				storeDate();
			} else {
				readToken();
			}
			dateBuilder.reset();
		}
	}
	
	private void mark() {
		mark = curPos;
		markedBuilder = dateBuilder.clone();
	}
	
	private void rollback() {
		if (mark < 0)
			return;
//		if (mark >= tokens.size()) {
//			curToken = endToken;
//			curPos = mark;
//		} else {
			curPos = mark - 1;
			readToken();
//		}
		dateBuilder = markedBuilder;
	}
	
	private void storeDate() {
		int wCnt = dateBuilder.getWordsCount();;
		int startPos = curPos - wCnt;
		if (!finished)
			startPos--;
		String words = dateBuilder.getWords();
		LaseCalendar date = dateBuilder.getDate();
		
		Subtype subType;
		if (dateBuilder.isDateSet()) {
			if (dateBuilder.isTimeSet()) {
				subType = Subtype.DATE_TIME;
			} else {
				subType = Subtype.DATE;
			} 
		} else if (dateBuilder.isTimeSet()) {
				subType = Subtype.TIME;
		} else {
			subType = null;
		}
		
		assert subType != null;
		
		PositionedEntity newPE = new PositionedEntity(startPos, wCnt, words, date, subType);
		entityList.add(newPE);
	}
	
	@Override
	public List<PositionedEntity> search(List<MorphWord> tokens) {
		reset(tokens);
		parse();
		return entityList;
	}
}
