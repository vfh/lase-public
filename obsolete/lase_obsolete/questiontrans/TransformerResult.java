package lase_obsolete.questiontrans;

import java.util.Set;

/**
 * Návratová hodnota metody {@link QuestionTransformer#transform(String)}.
 * Obsahuje začátek odpovědi odpovádající otázce předané této metodě a další
 * informace, kterými je typ odpovědi {@link AnswerType} a seznam důležitých
 * slov z otázky, které je možné využít při hledání odpovědi na Internetu.
 * 
 * @author Rudolf Šíma
 * 
 */
public class TransformerResult {
	/** začátek odpovědi */
	private String answerOutset;
	/** typ odpovědi */
	private AnswerType expectedAnswerType;
	/** seznam důležitých slov z otázky */
	private Set<String> importantWords;

	/**
	 * Vytvoří nový <code>TransformerResult</code> a nastaví začátek odpovědi.
	 * 
	 * @param answerOutset
	 *            - začátek odpovědi
	 */
	public TransformerResult(String answerOutset) {
		this.answerOutset = answerOutset;
	}

	/**
	 * Vytvoří nový <code>TransformerResult</code> a nastaví všechny atributy.
	 * 
	 * @param answerOutset
	 *            - začátek odpovědi
	 * @param importantWords
	 *            - množina důležitých slov z otázky
	 * @param expectedAnswerType
	 *            - druh odpovědi
	 */
	public TransformerResult(String answerOutset, Set<String> importantWords,
			AnswerType expectedAnswerType) {
		this.answerOutset = answerOutset;
		this.importantWords = importantWords;
		this.expectedAnswerType = expectedAnswerType;
	}

	/**
	 * Vrací začátek odpovědi.
	 * @return Vrací začátek odpovědi.
	 */
	public String getAnswerOutset() {
		return answerOutset;
	}

	/**
	 * Vrací množinu důležitých slov z otázky, kterou lze využít při hledání
	 * odpovědi na Internetu.
	 * 
	 * @return Vrací množinu důležitých slov z otázky.
	 */
	public Set<String> getImportantWords() {
		return importantWords;
	}

	/**
	 * Vrací druh odpovědi.
	 * @return Vrací druh odpovědi.
	 */
	public AnswerType getAnswerType() {
		return expectedAnswerType;
	}

	/**
	 * Možné druhy odpovědi.
	 */
	public enum AnswerType {
		PERSON, DATE, CITY, NOUN, PRONOUN
	}
}
