package lase_obsolete.questiontrans;

import java.io.IOException;

/**
 * <p>
 * Rozhraní třídy sloužící k převodu otázky na začátek odpovědi.
 * </p>
 * 
 * <p>
 * Například na otázku: "Kudy vede nejlepší cesta z Plzně do Prahy?" by v
 * ideálním případě měla dát metoda <code>transform()</code> odpověď:
 * "nejlepší cesta z Plzně do Prahy vede".
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public interface QuestionTransformer {

	/**
	 * Pokusí se převést otázku na začátek odpovědi.
	 * 
	 * @param question
	 *            - otázka (včetně interpunkce)
	 * 
	 * @return V případě úspěchu vrací začátek odpovědi odpovídajíčí zadané
	 *         otázce a další informace o otázce. Více informací v dokumentaci
	 *         {@link TransformerResult}.
	 * 
	 * @throws TransformerException
	 *             - Vyhozena v případě, že z nějakého důvodu nebylo možné
	 *             převod provést.
	 */
	public TransformerResult transform(String question) throws TransformerException;

	/**
	 * Uvolní prostředky, které byly pro funkci převodníku použity.
	 * 
	 * @throws IOException
	 *             - Vyhozena, pokud při uvolnění prostředků došlo k chybě.
	 */
	public void close() throws IOException;
}
