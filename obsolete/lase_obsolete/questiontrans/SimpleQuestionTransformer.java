package lase_obsolete.questiontrans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;

import lase.morphology.MorphWord;
import lase.morphology.MorphologicalAnalyser;
import lase.morphology.MorphologyException;
import lase.util.Common;

/**
 * Velmi primitivní převod otázky na částo odpovědi. Pouze k testování, v praxi
 * nepoužitelné.
 * 
 * @author Rudolf Šíma
 * 
 */
public class SimpleQuestionTransformer implements QuestionTransformer {
	private static Set<Character> prefixStopTypes = new TreeSet<Character>();
	private static Set<Character> suffixTypes = new TreeSet<Character>();
	static {
		prefixStopTypes.add('V'); // sloveso
		suffixTypes.add('N');     // podstatné jméno
		suffixTypes.add('A');     // přídavné jméno
		suffixTypes.add('P');     // zájmeno
		suffixTypes.add('C');     // spojka
		suffixTypes.add('R');     // předložka
	}
	MorphologicalAnalyser analyzer;

	public SimpleQuestionTransformer() throws IOException {
		analyzer = Common.getMorphologicalAnalyser();
	}

	public TransformerResult transform(String question) throws TransformerException {
		List<MorphWord> morphWords;
		try {
			morphWords = analyzer.analyse(question);
		} catch (MorphologyException e) {
			throw new TransformerException(e);
		}
		List<MorphWord> outset = new ArrayList<MorphWord>();
		ListIterator<MorphWord> revIt = morphWords.listIterator(morphWords
				.size());
		MorphWord prev = null;
		// prohledávání odzadu, dokud jsou suffix type
		while (revIt.hasPrevious()
				&& suffixTypes.contains((prev = revIt.previous()).getWordType())) {
			outset.add(prev);
		}
		LinkedList<MorphWord> resultList = new LinkedList<MorphWord>();
		MorphWord next = null;
		if (revIt.hasNext() && revIt.next() != null && revIt.hasNext()) {
			prev = revIt.next();
		} else {
			throw new TransformerException("Malformed question ;)");
		}
		ListIterator<MorphWord> it;
		// přeházení pořadí
		for (it = outset.listIterator(outset.size()); it.hasPrevious();) {
			resultList.add(it.previous());
		}
		it = morphWords.listIterator();
		while (it.hasNext()
				&& !prefixStopTypes.contains((next = it.next()).getWordType())
				&& !"se".equals(next.getLemma())
				&& !"si".equals(next.getLemma()))
			;
		if (it.hasPrevious())
			it.previous();
		// zkopírování slovesa
		while (it.hasNext() && (next = it.next()) != prev) {
			resultList.add(next);
		}
		char firstWordType = morphWords.get(0).getWordType();
		if (firstWordType == 'R')
			resultList.add(morphWords.get(0));
		if (resultList.get(0).getWordType() == 'P')
			resultList.remove(0);
		StringBuilder sb = new StringBuilder();
		for (it = resultList.listIterator(); it.hasNext();) {
			sb.append(it.next().getWord());
			if (it.hasNext())
				sb.append(" ");
		}
		return new TransformerResult(sb.toString());
	}

	public void close() throws IOException {
		analyzer.close();
	}
	
	public static void main(String[] argv) {
		QuestionTransformer qt = null;
		try {
			qt = new SimpleQuestionTransformer();
			String s = "Přes které obce bych se také mohl dostat  autobusem  z Plzně do Prahy?";
			TransformerResult ret = qt.transform(s);
			System.out.println(s);
			System.out.println(ret.getAnswerOutset());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (qt != null) {
				try {
					qt.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
