package lase.morphology;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.zcu.fav.liks.morphology.client.MorphologyRPCWebServiceClient;
import cz.zcu.fav.liks.morphology.client.MorphologyRPCWebServiceClient2Beta;
import cz.zcu.fav.liks.morphology.data.MorphologicAnalyser;
import cz.zcu.fav.liks.morphology.data.MorphologyAnalysisException;

/**
 * 
 * Implementace morfologicého analyzátoru využívající webovou službu, ke které
 * je přistupováno pomocí metod třídy {@link MorphologyRPCWebServiceClient2Beta}
 * 
 * @author Rudolf Šíma
 * 
 */
public class LiksWebServiceMorphologicalAnalyser implements MorphologicalAnalyser {
	/** Morfologický analyzátor (z knihovny) */
	private MorphologicAnalyser analyser;
	/** Zda používat cache (RAM a případně serializace)*/
	private boolean useCache;
	/** Soubor pro serializaci cache nebo null pro žádný */
	private File cacheFile;
	/** Cache - Mapa zlematicovaných seznamů slov na hledané řetězce */ 
	private Map<String, List<MorphWord>> cache = new HashMap<String, List<MorphWord>>();
	/** Zda byla cache od posledního čtení změněna */
	private boolean cacheUpdated;

	/**
	 * Vytvoří novou instanci lematizátoru připravenou pro připojení k webové
	 * službě. (Konstruktor žádné spojení nevytvoří.)
	 * 
	 * @param url
	 *            - adresa webové služby
	 * @param cacheFile
	 *            - soubor s cache (nemusí existovat) nebo <code>null</code> pro
	 *            vyřazení serializace do souboru.
	 * @throws IOException
	 *             - Vyhozena, pokud při načítání cache ze souboru byla vyhozena
	 *             výjimka.
	 */
	public LiksWebServiceMorphologicalAnalyser(String url, String cacheFile)
			throws IOException {
		this(url);
		useCache = true;
		this.cacheFile = new File(cacheFile);
		loadCache();
	}

	/**
	 * Vytvoří novou instanci lematizátoru připravenou pro připojení k webové
	 * službě. (Konstruktor žádné spojení nevytvoří.)
	 * 
	 * @param url
	 *            - adresa webové služby
	 * @param useNonPersistentCache
	 *            - udává, zda má být použita cache (pouze v RAM). Načtení cache
	 *            ze souboru a serializace do souboru při volání tohoto
	 *            konstruktoru použita nebude.
	 */
	public LiksWebServiceMorphologicalAnalyser(String url,
			boolean useNonPersistentCache) throws IOException {
		this(url);
		useCache = useNonPersistentCache;
	}

	/**
	 * Vytvoří novou instanci lematizátoru připravenou pro připojení k webové
	 * službě. (Konstruktor žádné spojení nevytvoří.) Při použití tohoto
	 * konstruktoru se nepoužívá cache.
	 * 
	 * @param url - adresa webové služby
	 */
	public LiksWebServiceMorphologicalAnalyser(String url) throws IOException {
		//analyser = new MorphologyRPCWebServiceClient2Beta(url);
		analyser = new MorphologyRPCWebServiceClient(url);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<MorphWord> analyse(String text) throws MorphologyException {
		if (useCache) {
			List<MorphWord> cacheEntry = cache.get(text);
			if (cacheEntry != null)
				return cacheEntry;
		}
		List<cz.zcu.fav.liks.morphology.data.MorphWord> words;
		try {
			words = analyser.analyseSentence(text);//CharFilter.filter(text));
		} catch (MorphologyAnalysisException e) {
			throw new MorphologyException(e);
		}
		List<MorphWord> result = new ArrayList<MorphWord>(words.size());
		for (cz.zcu.fav.liks.morphology.data.MorphWord w : words) {
			// Slovní druh určuje první písmeno v tagu
			char wordType = w.getTag().charAt(0);
			MorphWord resWord = new MorphWord(w.getWord(), w.getLemma(),
					wordType);
			result.add(resWord);
		}
		if (useCache) {
			cache.put(text, result);
			cacheUpdated = true;
		}
		return result;
	}

	/**
	 * Uloží cache do souboru, byla-li změněna a je-li cache vůbec použita.
	 */
	public void close() throws IOException {
		if (cacheUpdated)
			storeCache();
	}

	/**
	 * Načte cache ze souboru <code>cacheFile</code>.
	 * 
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private void loadCache() throws IOException {
		if (cacheFile == null)
			return;
		try {
			InputStream fis = new FileInputStream(cacheFile);
			ObjectInputStream ois = new ObjectInputStream(fis);
			cache = (Map<String, List<MorphWord>>) ois.readObject();
		} catch (FileNotFoundException e) {
			// prázdná cache
		} catch (ClassNotFoundException e) {
			throw new IOException(e);
		}
	}

	/**
	 * Uloží cache do souboru <code>chacheFile</code>.
	 * @throws IOException
	 */
	private void storeCache() throws IOException {
		if (cacheFile == null)
			return;
		OutputStream fos = new FileOutputStream(cacheFile);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(cache);
	}
}
