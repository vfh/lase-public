package lase.morphology;

/**
 * Třída <code>CharFilter</code> obsahuje statickou metodu <code>filter()</code>,
 * která z textu odstraní všechny znaky kromě písmen, číslic a bílých znaků.
 * Využívá se k úpravě vstupního textu pro lematizátor.
 * 
 * @author Rudolf Šíma
 * 
 */
@Deprecated
public class CharFilter {

	private CharFilter() {
	}

	/**
	 * Odstraní z textu všechny znaky kromě písmen, číslic a bílých znaků.
	 * Využívá se k úpravě vstupního textu pro lematizátor.
	 * 
	 * @param text - text, ze kterého mají být ostraněny nežádoucí znaky
	 * 
	 * @return Vrací text po odstranění nežádoucích znaků.
	 */
	public static String filter(String text) {
		int length = text.length();
		char[] output = new char[length];
		int cnt = 0;
		for (int i = 0; i < length; i++) {
			char c = text.charAt(i);
			if (Character.isLetterOrDigit(c) || Character.isWhitespace(c))
				output[cnt++] = c;
		}
		return new String(output, 0, cnt);
	}
}
