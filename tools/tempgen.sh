#!/bin/bash

LASE_ROOT=.

cd $LASE_ROOT;
LASE_ROOT=`pwd`

# překlad šablon ve standardním adresáři
cd data/templates/
rm -f template????.xml

java -cp $LASE_ROOT/build/classes lase.tools.tempgen.TemplateCompiler $LASE_ROOT/data/templates/templates.src

# překlad šablon s otázkami pro učení
cd $LASE_ROOT/data/templates/learning
rm -f template????.xml

java -cp $LASE_ROOT/build/classes lase.tools.tempgen.TemplateCompiler $LASE_ROOT/data/templates/learning/questions.src
