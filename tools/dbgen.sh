#!/bin/bash

LASE_ROOT=.

cd $LASE_ROOT
java -classpath build/classes:lib/* lase.tools.dbgen.DatabaseGenerator data/pndb/$1 $2
