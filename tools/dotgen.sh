#!/bin/bash

LASE_ROOT=.

subdir=pdf
format=pdf

cd $LASE_ROOT/graphviz/
mkdir -p "$subdir"

for f in `ls *.gv`; do
	name=`echo $f | sed -e "s/.gv$//"`.$format
	dot -T$format $f -o $subdir/$name
done
