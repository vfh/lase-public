<%@page session="true"  import='java.util.*'%>
<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<jsp:include page="header.jsp" />

  <div class="text">
  <h1>Contact</h1>

  <h2>ZČU</h2><br/>
  Project LASE is developed by the <a href="http://www.kiv.zcu.cz/en/" alt="KIV ZCU" target="_blank">Department of Computer Science and Engineering at University of West Bohemia</a>.
  <p/>
  <h2>Team members</h2><p/>

  <h2 class="blue">Táriq Saleh Salem</h2><br/>
  <a href="mailto:tariq[at]students.zcu.cz">tariq[at]students.zcu.cz</a><p/>

  <h2 class="blue">Jan Rybák</h2><br/>
  <a href="mailto:rybak[at]students.zcu.cz">rybak[at]students.zcu.cz</a><p/>

  <h2 class="blue">Matyáš Latner</h2><br/>
  <a href="mailto:malatner[at]students.zcu.cz">malatner[at]students.zcu.cz</a><p/>
  <p/>
  <h2>Credits</h2><p/>

  <h2 class="blue">Jiří Fatka</h2><br/>
  <a href="mailto:fatkaj[at]students.zcu.cz">fatkaj[at]students.zcu.cz</a><p/>

  <h2 class="blue">Jaroslav Kohout</h2><br/>
  <a href="mailto:kohout[at]students.zcu.cz">kohout[at]students.zcu.cz</a><p/>
  <p/>
  <h2>Scientific supervision</h2><p/>

  <h2 class="blue">Miloslav Konopík</h2><br/>
  <a href="mailto:konopik[at]kiv.zcu.cz">konopik[at]kiv.zcu.cz</a><p/>

  <h2 class="blue">Ondřej Rohlík</h2><br/>
  <a href="mailto:rohlik[at]kiv.zcu.cz">rohlik[at]kiv.zcu.cz</a><p/>


  </div>
</body>
</html>