<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
<head>
     <meta name="robots" content="all, follow" />
     <meta http-equiv="content-type" content="text/html; charset=utf-8" />

     <meta name="author" content="El Goog Team, West Bohemian University in Pilsen" />
     <meta name="description" content="The application for finding answers to questions made in natural language on the Internet" />
     <meta name="keywords" content="LASE, natural language, question, search engine, askwiki" />

     <style type="text/css" media="screen">   @import url("style/style.css"); </style>
     <style type="text/css" media="screen">   @import url("style/toggle.css"); </style>
     <style type="text/css" media="handheld"> @import url("style/pda.css");   </style>
     <style type="text/css" media="print">    @import url("style/print.css"); </style>

	<script type="text/javascript" src="js/jquery-latest.pack.js"></script>
	<script type="text/javascript" src="js/jquery.toggleElements.pack.js"></script>

     <title>LASE - Looking for answers to questions in natural language"</title>

	 <script type="text/javascript">
		$(document).ready(function(){
			$('div.toggler').toggleElements( { fxAnimation:'show', fxSpeed:'slow', className:'toggler' } );
			$('div.toggler_grey').toggleElements( { fxAnimation:'show', fxSpeed:'slow', className:'toggler_grey' } );
			$('div.searchers').toggleElements( { fxAnimation:'show', fxSpeed:'slow', className:'searchers' } );
		});
		</script>
	 </script>
</head>

<body>
<div id="top">
	<div id="navi">
	<a href="./about.jsp">Co je LASE</a> | <a href="./help.jsp">Nápověda</a> | <a href="./contact.jsp">Kontakt</a>
	</div>
	<a href="./" alt="Home" title="Úvodní strana"><img src="img/lase_logo.gif" alt="LASE" title="LASE - Lingvo Answer Search Engine" id="lase_logo"></a>
</div>

<div id="search">
	<form action="./" name="questionForm" method="get" accept-charset="utf-8">
		<input type="text" name="q">
		<input type="image" id="submit" src="img/search_button.gif" value="SEARCH">
	</form>
</div>