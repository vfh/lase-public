package lase.util;

import java.util.Iterator;

/**
 * Utility sloužící pro zjednodušení práce s iterátory a instancemi rozhraní
 * {@link Iterable}.
 * 
 * @author Rudolf Šíma
 * 
 */
public class IterationUtils {

	/**
	 * Vytvoří instanci rozhraní {@link Iterable} obalující daný iterátor.
	 * 
	 * @param iterator
	 *            Iterátor která má být základem vytvořené instance.
	 *            
	 * @return Vytvoří instanci rozhraní {@link Iterable} obalující daný
	 *         iterátor.
	 */
	public static <T> Iterable<T> newIterable(final Iterator<T> iterator) {
		return new Iterable<T>() {

			@Override
			public Iterator<T> iterator() {
				return iterator;
			}
			
		};
	}

	/**
	 * Vytvoří nový iterátor ochuzený o možnost odebírání prvků voláním metody
	 * {@link Iterator#remove()} využívájící původní iterátor získaný z daného
	 * rozhraní {@link Iterable}. Pokus o odebrání prvku skončí vyhozením
	 * výjimky {@link UnsupportedOperationException}.
	 * 
	 * @param iterable
	 *            Rozhraní, ze kterého se získá původní iterátor.
	 * 
	 * @return Vrací read-only iterátor.
	 */
	public static <T> Iterator<T> readOnlyIterator(final Iterable<T> iterable) {
		return new ReadOnlyIterator<T>(iterable.iterator());
	}
	
	/**
	 * Vytvoří nový iterátor ochuzený o možnost odebírání prvků voláním metody
	 * {@link Iterator#remove()} využívájící daný původní iterátor. Pokus o
	 * odebrání prvku skončí vyhozením výjimky
	 * {@link UnsupportedOperationException}.
	 * 
	 * @param originalIterator
	 *            Původní iterátor ze kterého má být vytvořen read-only
	 *            iterátor.
	 *            
	 * @return Vrací read-only iterátor.
	 */
	public static <T> Iterator<T> readOnlyIterator(final Iterator<T> originalIterator) {
		return new ReadOnlyIterator<T>(originalIterator);
	}
	
	/**
	 * Iterátor, který neumožňuje odebírání prvků. Pokus odebrání prvku způsobí
	 * vyhození výjimky {@link UnsupportedOperationException}.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	private static final class ReadOnlyIterator<T> implements Iterator<T> {

		/**
		 * Původní iterátor, ze kterého je v této třídě odebrána možno odebírat
		 * prvky.
		 */
		private Iterator<T> originalIterator;

		/**
		 * Vytvoří nový read-only iterátor využívající původní iterátor.
		 * 
		 * @param originalIterator
		 *            Původní iterátor.
		 */
		public ReadOnlyIterator(Iterator<T> originalIterator) {
			this.originalIterator = originalIterator;
		}
		
		/* (non-Javadoc)
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			return originalIterator.hasNext();
		}

		/* (non-Javadoc)
		 * @see java.util.Iterator#next()
		 */
		@Override
		public T next() {
			return originalIterator.next();
		}

		/**
		 * Volání této metody způsobí vyhození výjimky
		 * {@link UnsupportedOperationException}.
		 */
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
	}
}
