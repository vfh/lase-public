package lase.util;

import java.io.IOException;

import lase.entrec.NamedEntityGraph;

public class DebugGraphWriter {
	private static int counter = 0;
	
	private static String nextFilename() {
		return String.format("graphviz/Graph%05d.gv", counter++);
	}
	
	public static boolean write(NamedEntityGraph g) {
		try {
			g.outputGraphViz(nextFilename());
		} catch (IOException e) {
			return false;
		}
		return true;
	}
}