package lase.util;

import java.util.Arrays;

/**
 * Implementace automaticky se zvětšujícího pole bytů.
 * 
 * @author Rudolf Šíma
 * 
 * @deprecated Použít {@link java.io.ByteArrayOutputStream}.
 * 
 */
@Deprecated
public class ByteVector {
	private byte[] data;
	private int size = 0;
	
	/**
	 * Vytvoří nový <code>ByteVector</code> s počáteční velikostí
	 * <code>initialCapacity</code>.
	 * 
	 * @param initialCapacity
	 *            - počáteční velikost
	 */
	public ByteVector(int initialCapacity) {
		data = new byte[initialCapacity];
	}

	/**
	 * Připojí k poli nový úsek. V případě, že kapacita pole nestačí, provede se
	 * realokace.
	 * 
	 * @param chunk
	 *            - nový úsek pole
	 * @param offset
	 *            - počáteční index nového úseku
	 * @param length
	 *            - délka nového úseku
	 *            
	 * @throws ArrayIndexOutOfBoundsException
	 *             Vyhozena v případě, že <code>offset</code> je menší než 0
	 *             nebo že <code>length</code> je větší než
	 *             <code>chunk.length</code>.
	 */
	public void add(byte[] chunk, int offset, int length) {
		ensureCapacity(size + length);
		System.arraycopy(chunk, offset, data, size, length);
		size += length;
	}

	/**
	 * Vrací počet prvků v poli, tj. počet bytů použitý pro platná data.
	 * 
	 * @return Vrací počet prvků v poli.
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Zkopíruje všechny platné byty do pole, které vrací.
	 * 
	 * @return Vrací kopii pole bytů.
	 */
	public byte[] toByteArray() {
		return Arrays.copyOf(data, size);
	}

	/**
	 * Zajistí, aby mělo vnitřní pole bytů velikost minimálně <code>minCapacity</code>.
	 * Provede realokaci, je-li potřeba.
	 * 
	 * @param minCapacity - minimální požadovaná kapacita pole.
	 */
	private void ensureCapacity(int minCapacity) {
		int oldCapacity = data.length;
		if (minCapacity > oldCapacity) {
			int newCapacity = (oldCapacity * 3) / 2 + 1;
			if (newCapacity < minCapacity)
				newCapacity = minCapacity;
			data = Arrays.copyOf(data, newCapacity);
		}
	}
}
