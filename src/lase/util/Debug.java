package lase.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Utility, které lze využít pro účely vývoje a ladění.
 * 
 * @author Rudolf Šíma
 * 
 */
public class Debug {
	
	/**
	 * Privátní konstruktor, všechny metody jsou statické.
	 */
	private Debug() {}

	/**
	 * <p>
	 * Vždy vrací {@code true}. Způsob jak oklamat překladač Javy, aby nepoznal, že
	 * podmínka bude splněna vždy a překlad skončil chybou "Unreachable code".
	 * </p>
	 * 
	 * <p>
	 * Místo
	 * <pre> if (true) ... </pre>
	 * se použije
	 * <pre> if (Debug.always()) ... </pre>
	 * </p>
	 * 
	 * @return Vrací {@code true}.
	 */
	public static boolean always() {
		return true;
	}
	
	/**
	 * Uloží obsah řetězce do souboru v kódování, které používá JVM.
	 * 
	 * @param filename
	 *            Cesta k souboru, do kterého bude řetězec zapsán.
	 * @param string
	 *            Řetězec, který má být zapsán do souboru.
	 */
	public static void stringToFile(String filename, String string) {
		try {
			FileOutputStream fos = new FileOutputStream(filename);
			try {
				fos.write(string.getBytes());
			} finally {
				fos.close();
			}
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}
	
	/**
	 * Načte obsah souboru v kódováním UTF-8 do řetězce.
	 * 
	 * @param filename
	 *            Cesta k souboru, ze kterého se má řetězec načíst.
	 *            
	 * @return Vrací obsah souboru jako řetězec.
	 */
	public static String fileToString(String filename) {
		return fileToString(filename, "UTF-8");
	}

	/**
	 * Načte obsah souboru v daném kódování do řetězece
	 * 
	 * @param filename
	 *            Cesta k souboru, ze kterého se má řetězec načíst.
	 * @param charset
	 *            Znaková sada, ve které je soubor kódován.
	 * 
	 * @return Vrací obsah souboru jako řetězec.
	 */
	public static String fileToString(String filename, String charset) {
		File file = new File(filename);
		byte[] buffer = new byte[(int)file.length()];
		try {
			FileInputStream fis = new FileInputStream(file);
			int offset = 0;
			int read;
			try {
				while ((read = fis.read(buffer, offset, buffer.length - offset)) > 0) {
					offset += read;
				}
			} finally {
				fis.close();
			}
		}catch(IOException e) {
			throw new RuntimeIOException(e);
		}
		return new String(buffer); 
	}
	
	
	/**
	 * Obdoba výjimky {@link IOException}, která je ovšem potomkem výjimky
	 * {@link RuntimeException}, tudíž není nutné ji explicitně ošetřit.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	public static class RuntimeIOException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		/**
		 * Vytvoří novou výjimku bez podrobné zprávy a nastavení příčiny.
		 */
		public RuntimeIOException() {
		}
		
		/**
		 * Vytvoří novou výjimku, nastaví podrobnou zprávu a příčinu.
		 */
		public RuntimeIOException(String message, Throwable cause) {
			super(message, cause);
		}

		/**
		 * Vytvoří novou výjimku a nastaví podrobnou zprávu.
		 */
		public RuntimeIOException(String message) {
			super(message);
		}

		/**
		 * Vytvoří novou výjimku a nastaví příčinu.
		 */
		public RuntimeIOException(Throwable cause) {
			super(cause);
		}
	}
}
