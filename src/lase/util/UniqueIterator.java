package lase.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Iterátor přes unikátní prvky. Při iteraci ukládá jednotlivé prvky, u kterých
 * se předpokládá správná implementace {@link Object#hashCode()} a
 * {@link Object#equals(Object)}), do {@link HashSet}, a případné opakující se
 * prvky ignoruje.
 * 
 * @author Rudolf Šíma
 * 
 */
public class UniqueIterator<T> implements Iterator<T>{
	
	/** Množina unikátních prvků, které již byly při iteraci nalezeny */
	private Set<T> set = new HashSet<T>();
	
	/** Iterátor množiny s duplicitními prvky */
	private Iterator<T> iterator;

	/** Následující prvek iterace */
	private T next;
	
	/**
	 * Vytvoří nový iterátor.
	 * 
	 * @param iterator
	 *            - Iterátor množiny s duplicitními prvky
	 */
	public UniqueIterator(Iterator<T> iterator) {
		this.iterator = iterator;
		findNext();
	}
	
	/**
	 *  Nalezne další unikátní prvek a nastaví proměnnou <code>next</code>.
	 */
	public void findNext() {
		while(iterator.hasNext()) {
			next = iterator.next();
			if (!set.contains(next)) {
				set.add(next);
				return;
			}
		}
		next = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		boolean result = next != null;
		if (!result)
			set = null;
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next() {
		T result = next;
		findNext();
		return result;
	}

	/**
	 * Vždy způsobí vyhození výjimky {@link UnsupportedOperationException}.
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}