package lase.util;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;


/**
 * <p>
 * Korpus textů stažených z internetu.
 * </p>
 * 
 * <p>
 * Zapouzdřuje seznam znáznamů.<br/>
 * Záznam korpusu ({@link Entry}) se skládá z textu webové stránku (resp. snippetu vyhledávače),
 * URL stránky, ze které text pochází a titulku této stránky.
 * </p>
 * 
 * @author Rudolf Šíma
 *
 */
public class Corpus implements Iterable<Corpus.Entry>, RandomAccess, Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Seznam záznamů korpusu.
	 */
	private List<Entry> corpus;
	
	/**
	 * Vytvoří nový prázdný korpus.
	 */
	public Corpus() {
		corpus = new ArrayList<Entry>();
	}
	
	/**
	 * Vytvoří nový prázdný korpus a prealokuje seznam pro {@code initialSize}
	 * záznamů.
	 * 
	 * @param initialSize
	 *            - Počáteční kapacita korpusu.
	 */
	public Corpus(int initialSize) {
		corpus = new ArrayList<Entry>(initialSize);
	}
	
	/**
	 * Přidá záznam do korpusu.
	 * 
	 * @param text
	 *            - Test webové stránky resp. snippetu webového vyhledávače,
	 *            který bude přidán do korpusu.
	 * @param pageTitle
	 *            - Titulek stránky, ze které text korpusu pochází.
	 * @param url
	 *            - URL stránky, ze které přidávaný text pochází.
	 */
	public void addEntry(String text, String pageTitle, String url) {
		corpus.add(new Entry(text, new SourcePage(pageTitle, url)));
	}

	/**
	 * Vrací read-only iterátor záznamů korpusu.
	 */
	@Override
	public Iterator<Entry> iterator() {
		final Iterator<Entry> it = corpus.iterator(); 
		return IterationUtils.readOnlyIterator(it);
	}

	/**
	 * Vrací instanci rozhraní {@link Iterable}, která umožňuje iteraci přes
	 * texty záznamů tohoto korpusu. Lze tedy například jednoduše spojit texty
	 * určitým oddělovačem voláním metody
	 * {@link StringUtils#concat(Iterable, String)}.
	 * 
	 * @return Vrací instanci rozhraní {@link Iterable} sloužící k iteraci přes
	 *         text záznamů korpusu.
	 */
	public Iterable<String> getStringIterable() {
		return new Iterable<String>() {
			@Override
			public Iterator<String> iterator() {
				final Iterator<Entry> it = corpus.iterator();
				return new Iterator<String>() {
					public boolean hasNext() {
						return it.hasNext();
					}
					@Override
					public String next() {
						return it.next().text;
					}
					@Override
					public void remove() {
						throw new UnsupportedOperationException();
					}
				};
			}
		};
	}
	
	/**
	 * Vrací záznam korpusu na zadaném indexu.
	 * 
	 * @param index - Index záznamu.
	 * 
	 * @return Vrací záznam korpusu.
	 */
	public Entry getEntry(int index) {
		return corpus.get(index);
	}

	/**
	 * Vrací velikost korpusu, tj. počet záznamů v něm umístěných.
	 * 
	 * @return Vrací velikost korpusu.
	 */
	public int size() {
		return corpus.size();
	}
	
	/**
	 * <p>
	 * Záznam korpusu.
	 * </p>
	 * 
	 * <p>
	 * Skládá se z textu webové stránku (resp. snippetu vyhledávače),
	 * URL stránky, ze které text pochází a titulku této stránky.
	 * </p>
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	public static class Entry implements Serializable {
		private static final long serialVersionUID = 1L;
		
		/**
		 *	Text webové stránky resp. text snippet z webového vyhledávače.
		 */
		private String text;
		
		/**
		 *	Zdrojová stránka, ze které text pochází.
		 */
		private SourcePage source;
		
		/**
		 * Vytvoří a inicializuje nový záznam korpusu.
		 * 
		 * @param text
		 *            Text webové stránky resp. text snippet z webového
		 *            vyhledávače.
		 * @param source
		 *            Zdrojová stránka, ze které text pochází.
		 */
		public Entry(String text, SourcePage source) {
			this.text = text;
			this.source = source;
		}

		/**
		 * Vrací text webové stránky resp. text snippet z webového
		 * 
		 * @return Vrací text záznamu.
		 */
		public String getText() {
			return text;
		}

		/**
		 * Vrací informace o zdrojové stránce, ze které text pochází.
		 * 
		 * @return Vrací zdrojovou stránku.
		 */
		public SourcePage getSource() {
			return source;
		}
		
		/**
		 * @see #getText()
		 */
		public String toString() {
			return text;
		}
	}
	
	/**
	 * <p>
	 * Informace o zdrojové stránce, ze které pochází text jednoho záznamu korpusu.
	 * </p>
	 * 
	 * <p>
	 * Skládá se URL stránky, ze které text pochází a její titulku.
	 * </p>
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	public static class SourcePage implements Serializable {
		private static final long serialVersionUID = 1L;

		/**
		 *	Žádné resp. prázdné informace o zdrojé stránce.
		 */
		public static SourcePage NONE = new SourcePage("", "");
		
		/**
		 * Titulek stránky, ze které text záznamu korpusu pochází.
		 */
		private String title;
		
		/**
		 * URL stránky, ze které text záznamu korpusu pochází.
		 */
		private URL url;

		/**
		 * Vytvoří a inicializuje novou instanci informací o zdrojové stránkce.
		 * 
		 * @param title
		 *            Titulek stránky, ze které text záznamu korpusu pochází.
		 * @param url
		 *            URL stránky, ze které text záznamu korpusu pochází.
		 */
		public SourcePage(String title, URL url) {
			this.title = title;
			this.url = url;
		}
		
		/**
		 * Vytvoří a inicializuje novou instanci informací o zdrojové stránkce.
		 * 
		 * @param title
		 *            Titulek stránky, ze které text záznamu korpusu pochází.
		 * @param url
		 *            URL stránky, ze které text záznamu korpusu pochází.
		 */
		public SourcePage(String title, String url) {
			this.title = title;
			if (!url.contains("://"))
				url = "http://" + url;
			try {
				this.url = new URL(url);
			} catch (MalformedURLException e) {
				// URL není k dispozici: this.url = null;
			}
		}

		/**
		 * Vrací titulek stránky, ze které text záznamu korpusu pochází.
		 * 
		 * @return Vrací titulek stránky, ze které text záznamu korpusu pochází.
		 */
		public String getTitle() {
			return title;
		}

		/**
		 * Vrací URL stránky, ze které text záznamu korpusu pochází.
		 * 
		 * @return URL titulek stránky, ze které text záznamu korpusu pochází.
		 */
		public URL getUrl() {
			return url;
		}
		
		/**
		 * Vrací řetězcovou reprezentaci informací o zdrojové stránce pro ladicí
		 * účely.
		 */
		public String toString() {
			return String.format("%s [%s]", title, url == null ? "null" : url.toString());
		}
	}
}
