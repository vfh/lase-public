package lase.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * <p>
 * Implementace iterátoru ({@link java.util.Iterator}), která navíc přidává
 * metody {@link #peek()} umožnující získat hodnotu následujícího prvku bez
 * toho, aby se změnila pozice iterátoru.
 * </p>
 * 
 * <p>
 * Ve skutečnosti je {@code PeekIterator} implementován tak, že původním
 * iterátorem čte vždy o jeden prvek napřed, který uchovává. Místo následujícího
 * prvku tedy vrací prvek současný a posune se opět o jeden prvek vpřed, je-li
 * takový k dispozici.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class PeekIterator<T> implements Iterator<T>{

	/**
	 * Původní iterátor, který slouží jako zdroj prvků, přes které se iteruje.
	 */
	private Iterator<T> iterator;

	/**
	 * <p>
	 * Současný prvek iterace (prvek na aktuální pozici). Je to prvek, který byl
	 * vrácen předchozím voláním metody {@link Iterator#next()} původního
	 * iterátoru a v iterátor {@code PeekIterator} je vydáván za následující
	 * prvek, který je vrácen metodami {@link #peek()} a {@link #next()}.
	 * </p>
	 * 
	 * <p>
	 * Má-li hodnotu {@code null}, znamená to, již není k dispozici žádný prvek,
	 * všechny již byly vráceny.
	 * </p>
	 */
	private T currentElement;

	/**
	 * Vytvoří nový {@code Peek}, který získává prvky iterace z původního
	 * iterátoru {@code iterator}.
	 * 
	 * @param iterator
	 *            Původní iterátor, ze kterého se získávají prvky.
	 */
	public PeekIterator(Iterator<T> iterator) {
		this.iterator = iterator;
		readAhead();
	}
	
	/* (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		return currentElement != null;
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public T next() {
		T next = peek();
		readAhead();
		return next;
	}

	/**
	 * Vrátí následující prvek iterace stejně jako metody {@link #next()} s tím
	 * rozdílem, že se neposune v iteraci o jeden prvek dopředu. Opakované
	 * volání této metody tedy vrací stále jeden a ten samý prvek.
	 * 
	 * @return Vrací následující prvek.
	 * 
	 * @throws NoSuchElementException
	 *             Vyhozena v případě, že již není k dispozici další prvek, tj.
	 *             metoda {@link #hasNext()} vrací {@code false}.
	 */
	public T peek() {
		if (currentElement == null)
			throw new NoSuchElementException();
		return currentElement;
	}

	/**
	 * Tento iterátor je read-only. Neumožňuje odebírání prvků. Volání této meto
	 * metody vždy vede k vyhození výjimky {@link UnsupportedOperationException}
	 * .
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Původním iterátorem přečte následující prvek a nastaví atribut
	 * {@link #currentElement}.
	 */
	private void readAhead() {
		if (iterator.hasNext()) {
			currentElement = iterator.next();
		} else {
			currentElement = null;
		}
	}
}
