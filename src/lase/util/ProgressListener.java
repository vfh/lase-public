package lase.util;

/**
 * Rozhraní posluchače průběhu implementované třídamy, který mají zájem být
 * informovány a průběhu nějaké operace, u které je možné vyjádřit, jak velká
 * část již byla dokončena resp. jak veklá část ještě zbývá.
 * 
 * @author Rudolf Šíma
 * 
 */
public interface ProgressListener {

	/**
	 * Voláním této metody dává proces vykonávající nějakou činnost najevo, že
	 * provedl další část plánované práce a informuje posluchače o tom, jaké
	 * část již byla celkem dokončena. Může jít přesnou hodnotu určující díl
	 * splněné činnosti nebo pouze o její odhad.
	 * 
	 * @param value
	 *            Poměrná část činnosti, která již byla provedena vyjádřená v
	 *            rozmezí <tt>0.0</tt> až <tt>1.0</tt>. Zbývající část činnost lze tedy
	 *            vyjádřit jako <tt>1.0 - value</tt>.
	 */
	public void progressChanged(double value);
	
}
