package lase.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * Ladicí utitlita sloužící k zobrazení textu zapsaného na standardní výstup a
 * chybový výstup na webové stránce.
 * </p>
 * 
 * <p>
 * Voláním metody {@link #grabOutput()} dojde k přesměrování výstupu do bufferu
 * v paměti. Metoda {@link #display(HttpServletResponse)} potom slouží k zapsání
 * obsahu bufferu na přímo na webovou stránku. Metodou {@link #restoreOutput()}
 * lze obnovit předchozí nastavení standardního a chybového výstupu.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class WebLogger {

	/**
	 * {@code PrintStream} směřující do bufferu v paměti sloužící k uchovávání
	 * textů zapsaných na standardní a chybový výstup.
	 */
	private static PrintStream memoryPrintStream;

	/**
	 * Buffer v paměti sloužící k uchovávání textů zapsaných na standardní a
	 * chybový výstup.
	 */
	private static ByteArrayOutputStream memoryOutputStream;

	/**
	 * Původní standardní výstup nastavený při volání {@link #grabOutput()}
	 * sloužící k obnovení standardního výstupu při volání
	 * {@link #restoreOutput()}.
	 */
	private static PrintStream originalOut;

	/**
	 * Původní chybový výstup nastavený při volání {@link #grabOutput()}
	 * sloužící k obnovení standardního výstupu při volání
	 * {@link #restoreOutput()}.
	 */
	private static PrintStream originalErr;

	/**
	 * {@code OutputStream} sloužící k zapsání obsahu bufferu přímo do webové
	 * stránky.
	 */
	private static OutputStream responseOutputStream;

	/**
	 * Přesměruje standardní výstup a standardní chybový výstup do bufferu v
	 * paměti. Obsah tohoto bufferu lze zobrazit na webové stránce voláním
	 * metody {@link #display(HttpServletResponse)}. Původní nastavení
	 * standardního a chybového výstupu lze obnovit metodou
	 * {@link #restoreOutput()}.
	 */
	public static void grabOutput() {
		if (memoryPrintStream != null)
			memoryPrintStream.close();
		memoryOutputStream = new ByteArrayOutputStream();
		memoryPrintStream = new PrintStream(memoryOutputStream, true);
		restoreOutput();
		originalOut = System.out;
		originalErr = System.err;
		System.setOut(memoryPrintStream);
		System.setErr(memoryPrintStream);
	}

	/**
	 * Obnoví původní nastavení standardního a chybového výstupu přesměrovaného
	 * předchozím voláním {@link #grabOutput()}.
	 */
	public static void restoreOutput() {
		if (originalOut != null) {
			System.setOut(originalOut);
			originalOut = null;
		}
		if (originalErr != null) {
			System.setOut(originalErr);
			originalErr = null;
		}
	}

	/**
	 * Zapíše na webovou stránku obsah bufferu, do kterého se ukládá text
	 * zapsaný na standardní a chybový výstup od předchozího volání
	 * {@link #grabOutput()}.
	 * 
	 * @param response
	 *            Objekt {@link HttpServletResponse}, do kterého bude zapsán
	 *            obsah textového bufferu.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při zápisu textu do {@code Response}
	 *             nastala chyba.
	 */
	public static void display(HttpServletResponse response) throws IOException {
		if (response != null) {
			if (responseOutputStream == null)
				responseOutputStream = response.getOutputStream();
			responseOutputStream.write(memoryOutputStream.toByteArray());
		}
		memoryOutputStream.reset();
	}
}
