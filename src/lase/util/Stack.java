package lase.util;


public interface Stack<T> {
	
	public T pop();
	
	public void push(T e);
	
	public boolean isEmpty();

}
