package lase.util;

import java.util.Iterator;

/**
 * Utility pro usnadnění nebo standardizaci některých často prováděných akcí
 * s řetězci.
 * 
 * @author Rudolf Šíma
 * 
 */
public class StringUtils {

	/**
	 * Nelze vytvořit instanci této třídy, všechny metody jsou statické.
	 */
	private StringUtils() {
	}

	/**
	 * Spojí řetězce nebo obecně libovolné objekty správně implementující metodu
	 * {@code toString()} do jednoho řetězce. Mezi jednotlivé spojované řetězce
	 * vkládá oddělovat {@code delimiter}.
	 * 
	 * @param array
	 *            Pole prvků, jejichž řetězcové reprezentace mají být spojeny do
	 *            jednoho řetězce.
	 * 
	 * @param delimiter
	 *            Oddělovač, který bude vložen mezi všechny spojované řetězce.
	 * 
	 * @return Vrací řetězce vzniklý spojením přetězců s daným oddělovačem.
	 */
	public static String concat(Object[] array, String delimiter) {
		return concat(new ArrayIterator<Object>(array).getIterable(), delimiter);
	}

	/**
	 * Spojí řetězce nebo obecně libovolné objekty správně implementující metodu
	 * {@code toString()} do jednoho řetězce. Mezi jednotlivé spojované řetězce
	 * vkládá oddělovat {@code delimiter}.
	 * 
	 * @param iterable
	 *            Instance rozhraní {@link Iterable} umožňující iteraci přes
	 *            objekty, jejichž řetězcové reprezentace mají být spojeny do
	 *            jednoho řetězce.
	 * 
	 * @param delimiter
	 *            Oddělovač, který bude vložen mezi všechny spojované řetězce.
	 * 
	 * @return Vrací řetězce vzniklý spojením přetězců s daným oddělovačem.
	 */
	public static <T> String concat(Iterable<T> iterable, String delimiter) {
		Iterator<T> iterator = iterable.iterator();
		return concat(delimiter, iterator);
	}

	/**
	 * Spojí řetězce nebo obecně libovolné objekty správně implementující metodu
	 * {@code toString()} do jednoho řetězce. Mezi jednotlivé spojované řetězce
	 * vkládá oddělovat {@code delimiter}.
	 * 
	 * @param iterable
	 *            Iterátor objektů, jejichž řetězcové reprezentace mají být
	 *            spojeny do jednoho řetězce.
	 * 
	 * @param delimiter
	 *            Oddělovač, který bude vložen mezi všechny spojované řetězce.
	 * 
	 * @return Vrací řetězce vzniklý spojením přetězců s daným oddělovačem.
	 */
	private static <T> String concat(String delimiter, Iterator<T> iterator) {
		if (!iterator.hasNext())
			return "";
		StringBuilder sb = new StringBuilder();
		T next = iterator.next();
		if (next != null)
			sb.append(next);
		while (iterator.hasNext()) {
			next = iterator.next();
			if (next != null) {
				sb.append(delimiter);
				sb.append(next);
			}
		}
		return sb.toString();
	}

	/**
	 * Workaround sloužící k částečnému napravení současné nedokonalosti systému
	 * LASE, díky které není přesně zrekonstruována přesná původní podoba textu
	 * zpracováného tokenizátorem nebo morfologickým analyzátorem. Vrácené
	 * tokeny jsou pouze spojovány mezerami. Tato metoda slouží k odstranění
	 * všech mezer před tečkou, což je téměř jediný pozorovaný negativní efekt.
	 * 
	 * @param s
	 *            Řetězec, ve které bude provedeno nahrazování.
	 * 
	 * @return Vrací opravený řetězec.
	 */
	public static String workaroundExtraSpaces(String s) {
		return s.replace(" .", ".");
	}

	/**
	 * Odstraní jeden nebo více otazníků na konci retězce.
	 * 
	 * @param question
	 *            Řetězek, na jehož konci se vyskytují nebo mohou vyskytovat
	 *            otazníky.
	 * 
	 * @return Vrací řetězec bez ukončujících otazníků.
	 */
	public static String stripQuestionMarks(String question) {
		while (question.endsWith("?")) {
			question = question.substring(0, question.length() - 1);
		}
		return question;
	}

	// TODO - comment
	public static String trimChar(String original, char c) {
		int beginIndex;
		int origLen = original.length();
		for (beginIndex = 0; beginIndex < origLen
				&& original.charAt(beginIndex) == c; beginIndex++)
			;
		int endIndex;
		for (endIndex = origLen; endIndex > beginIndex
				&& original.charAt(endIndex - 1) == c; endIndex--)
			;
		return original.substring(beginIndex, endIndex);
	}
}
