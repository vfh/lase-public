package lase.util;

/**
 * <p>
 * Základní výjimka systému LASE. Její vyhození vyjadřuje, že při práci systému
 * nastala chyba. Tato chyba je zpravidla způsobena předchozím vyhozením jiné
 * výjimky, která je nastavena jako příčina (<code>cause</code>) výjimky {@code
 * LaseException}.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class LaseException extends Exception {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Vytvoří novou výjimku bez podrobné zprávy a nastavení příčiny.
	 */
	public LaseException() {
		super();
	}

	/**
	 * Vytvoří novou výjimku, nastaví podrobnou zprávu a příčinu.
	 */
	public LaseException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Vytvoří novou výjimku a nastaví podrobnou zprávu.
	 */
	public LaseException(String message) {
		super(message);
	}

	/**
	 * Vytvoří novou výjimku a nastaví příčinu.
	 */
	public LaseException(Throwable cause) {
		super(cause);
	}
}
