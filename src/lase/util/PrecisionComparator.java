package lase.util;

/**
 * <p>
 * Komparátor přesnosti dat. Metoda {@link #compare(Object, Object)} vrací
 * výčtový typ {@link Precision}, tedy jednu z hodnot:
 * <ul>
 * <li>neporovnatelné</li>
 * <li>přesnější (první údaj přesnější)</li>
 * <li>méně přesný" (druhý údaj přesnější)</li>
 * <li>rovné</li>
 * </ul>
 * </p>
 * <p>
 * Například výsledkem porovnání dvojice ("leden 1993", "1.1.1993") bude verdikt
 * "méně přesný".
 * 
 * @author Rudolf Šíma
 * 
 * @param <T>
 *            - Typ porovnývaných objektů.
 */
public interface PrecisionComparator<T> {
	
	
	/**
	 * Provede porovnání paramterů a vrací jednu z hodnot:
	 * <ul>
	 * <li>neporovnatelné</li>
	 * <li>přesnější (první údaj přesnější)</li>
	 * <li>méně přesný" (druhý údaj přesnější)</li>
	 * <li>rovné</li>
	 * </ul>
	 * 
	 * @param o1
	 *            - první operand porovnání
	 * @param o2
	 *            - druhý operand porovnáí
	 *            
	 * @return Vrací výsledek porovnání operandů podle přesnosti. 
	 */
	public Precision compare(T o1, T o2); 
	
	/**
	 * Výsledek porovnání dvou údajů podle přesnosti. 
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	public enum Precision {
		/** Neporovnatelné údaje. */
		INCOMPARABLE,
		/** První z údajů je přesnější. */
		MORE_PRECISE,
		/** Druhý z údajů je přesnější. */
		LESS_PRECISE,
		/** Údaje jsou si z hlediska přesnosti rovny. */
		EQUAL
	}
}
