package lase.util;

import java.util.HashMap;
import java.util.Map;

import lase.morphology.MorphologicalAnalyser;
import lase.nametagsearch.TimeItem;

public class DateMaker {
	
	public static final transient int fieldNotSet = -1;
	
	LaseCalendar curDate;
	
	/**
	 * Mapa čísel měsíců podle jejich názvu.
	 */
	private static final Map<String, Integer> months = new HashMap<String, Integer>();
	static {
		int i = 0;
		months.put("leden", ++i);
		months.put("únor", ++i);
		months.put("březen", ++i);
		months.put("duben", ++i);
		months.put("květen", ++i);
		months.put("červen", ++i);
		months.put("červenec", ++i);
		months.put("srpen", ++i);
		months.put("září", ++i);
		months.put("říjen", ++i);
		months.put("listopad", ++i);
		months.put("prosinec", ++i);
	}
	
	public LaseCalendar createCalendar(TimeItem item,Object value){
		curDate = new LaseCalendar(fieldNotSet,fieldNotSet,fieldNotSet,fieldNotSet,fieldNotSet,fieldNotSet);
				
		acceptDay(item.getDay());
		if(!acceptMonth(item.getMonth())){
		acceptMonthName(value);
		}
		acceptYear(item.getYear());
		
		return curDate;	
		
	}
	
	public LaseCalendar createCalendar(String year){
		curDate = new LaseCalendar(fieldNotSet,fieldNotSet,fieldNotSet,fieldNotSet,fieldNotSet,fieldNotSet);
		
		acceptYear(year);
		
		return curDate;	
		
	}
	
	private boolean acceptDay(String ct) {
		if(ct == null){
			return false;
		}
		if (acceptNumber(1, 31, ct)) {
			curDate.set(LaseCalendar.DAY, Integer.parseInt(ct)); 
			return true;
		}
		return false;
	}
	
	private boolean acceptMonthName(Object value) {
		if(value == null){
			return false;
		}
		String lemma = (String)value;
		String[] lemmas = lemma.split(" ");
		for(String ct:lemmas){
		if (months.containsKey(ct)) {
			curDate.set(LaseCalendar.MONTH, months.get(ct));
			return true;
		}
		}
		return false;
	}
	
	private boolean acceptMonth(String ct) {
		
		if(ct == null){
			return false;
		}
		if (acceptNumber(1, 12, ct)) {
			curDate.set(LaseCalendar.MONTH, Integer.parseInt(ct));
			return true;
		}
		return false;
	}
	
	private boolean acceptYear(String ct) {
		if(ct == null){
			return false;
		}
		if (acceptNumber(0, 3000, ct)) {
			curDate.set(LaseCalendar.YEAR, Integer.parseInt(ct)); 
			return true;
		}
		return false;
	}
	
	/**
	 * Akceptuje token, pokud je číslem.
	 * 
	 * @return Vrací {@code true}, je-li token akceptován. Jinak vrací {@code
	 *         false}.
	 */
	private boolean acceptNumber(int min, int max, String ct) {
		if (!isInteger(ct))
			return false;
		int val = Integer.parseInt(ct);
		if (min < max) {
			if (val < min || val > max) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * <p>
	 * Zjistí, zda je znaková posloupnost číslovkou. Tato verze pracuje pouze z
	 * čísly zapsanými číslicemi. Zkoumá tedy, zda jsou všechny znaky
	 * posloupnosti číslice.
	 * </p>
	 * 
	 * @param token
	 *            Zkoumaná posloupnost znaků.
	 * 
	 * @return Vrací {@code true}, je-li daná posloupnost znaků číslovkou. Jinak
	 *         vrací {@code false}.
	 */
	public boolean isInteger(String token) {
		int len = token.length();
		if (len > 9) // Větší se do Integeru nemusí vejít.
			return false;
		for (int i = 0; i < len; i++) {
			if (!Character.isDigit(token.charAt(i)))
				return false;
		}
		return len > 0;
	}

}
