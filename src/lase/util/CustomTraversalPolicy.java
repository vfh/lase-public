package lase.util;

import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.util.ArrayList;
import java.util.List;

/**
 * Politika přechodu fokusu, která umožňuje ručně stanovit pořadá, ve které bude fokus
 * přecházet mezi jednotlivými komponentami. Toto pořadí je běžné známé jako TabOrder.
 * 
 * @author Rudolf Šíma
 * 
 * @see FocusTraversalPolicy
 * @see Container#setFocusTraversalPolicy(FocusTraversalPolicy)
 */
public class CustomTraversalPolicy extends FocusTraversalPolicy {
	/**
	 * Seznam komponent určující pořadí, ve kterém mezi nimi bude
	 * přecházet fokus.
	 */
	private List<Component> order = new ArrayList<Component>();

	/**
	 * Přidá kompomentu do seznamu, který určuje pořadí, ve kterém mezi
	 * jednotlivými komponentami bude přecházet fokus. Pořadí přechodu odpovídá
	 * pořadí, ve kterém jsou komponenty přidány voláním této metody.
	 * 
	 * @param component
	 *            - Přidávaná komponenta.
	 */
	public void addComponent(Component component) {
		order.add(component);
	}

	/* (non-Javadoc)
	 * @see java.awt.FocusTraversalPolicy#getComponentAfter(java.awt.Container, java.awt.Component)
	 */
	@Override
	public Component getComponentAfter(Container focusCycleRoot, Component component) {
		int index = (order.indexOf(component) + 1) % order.size();
		return order.get(index);
	}

	/* (non-Javadoc)
	 * @see java.awt.FocusTraversalPolicy#getComponentBefore(java.awt.Container, java.awt.Component)
	 */
	@Override
	public Component getComponentBefore(Container focusCycleRoot, Component component) {
		int index = order.indexOf(component) - 1;
		if (index < 0);
			index = order.size() - 1;
		return order.get(index);
	}

	/* (non-Javadoc)
	 * @see java.awt.FocusTraversalPolicy#getDefaultComponent(java.awt.Container)
	 */
	@Override
	public Component getDefaultComponent(Container focusCycleRoot) {
		return order.get(0);
	}

	/* (non-Javadoc)
	 * @see java.awt.FocusTraversalPolicy#getLastComponent(java.awt.Container)
	 */
	@Override
	public Component getLastComponent(Container focusCycleRoot) {
		return order.get(order.size()-1);
	}

	/* (non-Javadoc)
	 * @see java.awt.FocusTraversalPolicy#getFirstComponent(java.awt.Container)
	 */
	@Override
	public Component getFirstComponent(Container focusCycleRoot) {
		return order.get(0);
	}
}