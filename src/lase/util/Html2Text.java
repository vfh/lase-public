package lase.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

/**
 * Konvertor HTML na prostý text, který vznikne jednoduchým odstaněním všech
 * HTML tagů.
 * 
 * @author Rudolf Šíma
 * 
 */
public class Html2Text {
	
	/**
	 * Nelze vytvářet instance této třídy.
	 */
	private Html2Text() {	
	}

	/**
	 * Odstraní HTML tady z kódu stránky.
	 * 
	 * @param html
	 *            - HTML kód, ze kterého budou odstraněny tagy.
	 * 
	 * @return Vrací text, který zbyde po odstanění všech HTML tagů.
	 */
	public static String convert(String html) {
		final StringBuilder sb = new StringBuilder(html.length());
		HTMLEditorKit.ParserCallback callback = new HTMLEditorKit.ParserCallback() {
			public void handleText(char[] text, int pos) {
				sb.append(text);
			}
		};
		ParserDelegator delegator = new ParserDelegator();
		InputStream is = new ByteArrayInputStream(html.getBytes());
		Reader reader = new InputStreamReader(is);
		try {
			delegator.parse(reader, callback, true);
		} catch (IOException e) {
			assert false;
			e.printStackTrace();
		}
		return sb.toString();
	}
}
