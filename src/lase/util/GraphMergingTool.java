package lase.util;

import lase.entrec.NamedEntityGraph;
import lase.entrec.NamedEntityGraph.Node;
import lase.learning.MergedGraph;
import lase.template.Query;

public class GraphMergingTool {
	
	private MergedGraph mergedGraph;
	
	public GraphMergingTool(Query question, Query answer) {
		mergedGraph = new MergedGraph(question, answer);
	}
	
	public void addGraph(NamedEntityGraph newGraph) {
		Node mergedRoot = mergedGraph.getRootNode();
		Node newGraphRoot = newGraph.getRootNode();
		for (Node successor : newGraphRoot.getSuccessors()) {
			mergedGraph.addEdge(mergedRoot, successor);
		}
	}
	
	public MergedGraph getMergedGraph() {
		return mergedGraph;
	}
}
