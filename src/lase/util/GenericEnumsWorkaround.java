package lase.util;

/**
 * <p>
 * Workaround umožňující porovnání a přetypování generických výčtů, které na
 * některých překladačích vede k chybě překladu.
 * </p>
 * 
 * <p>
 * Problém se týká přinejmenším překladačů <code>javac</code> do verze 1.6.0,
 * které <em>nejsou</em> dodávány s vývojovým prostředím Eclipse. Projevuje se
 * chybami překladu při manipulaci generickými výčtovými typy, jak je vysvětleno
 * v následujících dvou příkladech.
 * </p>
 * 
 * <pre>
 *	enum TestEnum {
 * 		A;
 * 	}
 * 
 *	Enum<?> enum1 = TestEnum.A;
 *	TestEnum enum2 = TestEnum.A;
 * 
 *   
 *	// Příklad 1 – porovnání
 *	if (enum1 == enum2) ...     // Nepřeloží se: "Incomparable types"
 *   
 *	// Řešení příkladu 1
 *	if (GenericEnumsWorkaround.compare(enum1, enum2)) ...
 *   
 *   
 *	// Příklad 2 – přetypování
 *	enum2 = (TestEnum) enum1;   // Nepřeloží se: "Incompatible types"
 *   
 *	// Řešení příkladu 1
 *	enum2 = GenericEnumsWorkaround.cast(enum1, TestEnum.class);
 *	// nebo
 *	enum2 = (TestEnum)(Object)enum1;
 * </pre>
 * 
 * 
 * @author Rudolf Šíma
 * 
 */
public class GenericEnumsWorkaround {
	
	/**
	 * Provede porovnání generických výčtů. Výsledek je shodný s výsledkem
	 * porovnání operátorem "==" (včetně případů, kdy jsou argumenty
	 * <code>null</code>), které ovšem z důvodu chyby v některých překladačích
	 * není možné provést.
	 * 
	 * @param e1
	 *            - první operand porovnání
	 *            
	 * @param e2
	 *            - druhý operand porovnání
	 * 
	 * @return Vrací <code>true</code>, platí-li <code>e1 == e2</code>. V
	 *         opačném případě vrací <code>false</code>.
	 */
	public static boolean compare(Enum<?> e1, Enum<?> e2) {
		if (e1 == null)
			return e2 == null;
		return e1.equals(e2);
	}

	/**
	 * Provede přetypování argumentu <code>e</code> na výčtový typ daný
	 * argumentem <code>type</code>. Výsledek je stejný jako při použití
	 * operátoru přetypování, který ovšem na některých překladačích není možné
	 * provést.
	 * 
	 * @see GenericEnumsWorkaround
	 * 
	 * @param e
	 *            - Generický výčet, který bude přetypován.
	 * 
	 * @param type
	 *            - Typ, na který má být argument <code>e</code> přetypován.
	 * 
	 * @return Vrací argument <code>e</code>, který je typu </code>T</code>.
	 * 
	 * @throws ClassCastException
	 *             Pokud <code>e</code> není výčtem typu <code>T</code>.
	 */
	public static <T extends Enum<T>> T cast(Enum<?> e, Class<T> type) {
		return type.cast(e);
	}
}
