package lase.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * Utilita schopná vytvořit jednoznačné mapování řetězců na znaky ze seznamu
 * daných řetězců. Výsledkem je vytvoření řetězce skládajícího se ze znaků,
 * které nahrazují původní řetězce.
 * <p>
 * 
 * <p>
 * <strong>Příklad:</strong><br>
 * Nahrazením seznamu řetězců ["pes", "kočka", "kočka", "pes"] vznikne například
 * řetězec "ABBA".
 * </p>
 * 
 * <p>
 * <strong>Poznámka:</strong><br>
 * Není důvod, aby byl vstupem seznam řetězců. Mohlo by jít o libovolné objekty správně
 * implementující metodu {@code equals(Object)}. Třída {@code CharsForStringsSubstitution}
 * ale není již v systému LASE použita, proto jsem se touto úpravou nezabýval.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class CharsForStringsSubstitution {
	private static final char firstCharacter = 'A';
	
	private Iterator<String> iterator;
	private Map<String, Character> map;
	private Map<Character, String> inverseMap;
	private char nextCharacter = firstCharacter;
	private char[] charArrayResult;
	private String stringResult;
	private StringBuilder builder;

	/**
	 * Vytvoří nový nahrazovač a ihned provede nahrazení.
	 * 
	 * @param tokens
	 *            Pole řetězců, které mají být nahrazeny znaky.
	 */
	public CharsForStringsSubstitution(String[] tokens) {
		this(new ArrayIterator<String>(tokens), tokens.length);
	}

	/**
	 * Vytvoří nový nahrazovač a ihned provede nahrazení.
	 * 
	 * @param tokens
	 *            Seznam řetězců, které mají být nahrazeny znaky.
	 */
	public CharsForStringsSubstitution(List<String> tokens) {
		this(tokens.iterator(), tokens.size());
	}

	/**
	 * Vytvoří nový nahrazovač a ihned provede nahrazení. Tento privátní
	 * konstruktor je použit z důvodu nezávislosti na kontejneru, ve kterém jsou
	 * řetězce předány.
	 * 
	 * @see CharsForStringsSubstitution#CharsForStringsSubstitution(List)
	 * @see CharsForStringsSubstitution#CharsForStringsSubstitution(String[])
	 * 
	 * @param iterator
	 *            Iterátor seznamu řetězců, které mají být nahrazeny znaky
	 * @param size
	 *            Velikost seznamu řetězců (počet řetězců).
	 */
	private CharsForStringsSubstitution(Iterator<String> iterator, int size) {
		this.iterator = iterator;
		map = new HashMap<String, Character>((int)(size * 1.34) + 1, 0.75f);
		builder = new StringBuilder(size); 
		substitute();
	}

	/**
	 * Provádí vlastní nahrazení řetězců znaky. Algoritmus je jednoprůchodový a
	 * průběžně vytváří mapu řetězců na znaky, na základě které které potom při
	 * zpracování každého dalšího řetězce zjišťuje, zda se shoduje s nějakým
	 * dříve nahrazeným.
	 */
	private void substitute() {
		while(iterator.hasNext()) {
			String str = iterator.next();
			Character mapResult = map.get(str);
			char mapping;
			if (mapResult == null) {
				mapping = nextCharacter;
				map.put(str, nextCharacter++);
			} else {
				mapping = mapResult;
			}
			builder.append(mapping);
		}
	}

	/**
	 * Vrací výsledek nahrazení jako pole znaků.
	 * 
	 * @return Vrací výsledek nahrazení jako pole znaků.
	 */
	public char[] getCharArrayResult() {
		if (charArrayResult == null) {
			int len = builder.length();
			charArrayResult = new char[len];
			builder.getChars(0, len, charArrayResult, 0);
		}
		return charArrayResult;
	}

	/**
	 * Vrací výsledek nahrazení jako řetězce.
	 * 
	 * @return Vrací výsledek nahrazení jako řetězce.
	 */
	public String getStringResult() {
		if (stringResult == null) {
			stringResult = builder.toString();
		}
		return stringResult;
	}

	/**
	 * Vrací mapu řetězců na znaky vytvořenou při nahrazení.
	 * 
	 * @return Vrací mapu řetězců na znaky vytvořenou při nahrazení.
	 */
	public Map<String, Character> getMapping() {
		return Collections.unmodifiableMap(map);
	}

	/**
	 * Vrací map inverzní k té, kterou vrací metoda {@link #getMapping()}, tedy
	 * mapu znaků, kterými byly řetězce nahrazeny, na nahrazené řetězce.
	 * 
	 * @return Vrací mapu řetězců na znaky.
	 */
	public Map<Character, String> getInverseMapping() {
		if (inverseMap == null) {
			inverseMap = new HashMap<Character, String>();
			for (Map.Entry<String, Character> e : map.entrySet()) {
				inverseMap.put(e.getValue(), e.getKey());
			}
		}
		return inverseMap;
	}
}

