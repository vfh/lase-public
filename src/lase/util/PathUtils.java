package lase.util;

import java.io.File;

/**
 * <p>
 * Utilita umožňující jednotné použití relativní relativních cest k datovým
 * souborů v desktopové i webové aplikaci. V případě webové aplikace je možné
 * cestu k adresáři <tt>WebContent</tt> zjistit pouze z kontextu servletu. V
 * kódu servletu je tedy možné voláním {@link #setParentDirectory(String)}
 * nastavit kořenový adresář s datovými soubory (např.: <tt>WebContent</tt>).
 * Metoda {@link #convertRelativePath(String)} potom umožní zjistit správnou
 * cestu z relativní cesty vzhledem ke kořenovému adresáři bez ohledu na to, zda
 * je systém spuštěn jako desktopová nebo webová aplikace.
 * </p>
 * 
 * <p>
 * <strong>Příklad:</strong><br>
 * 
 * <pre>
 * String dataFilePath = &quot;data/dataFile&quot;;
 * 
 * // desktopová aplikace
 * PathUtils.setParentDirectory(&quot;.&quot;);
 * String absolutePath = PathUtils.convertRelativePath(dataFilePath);
 * 
 * // webová aplikace
 * PathUtils.setParentDirectory(getServletContext().getRealPath(&quot;/&quot;));
 * String absolutePath = PathUtils.convertAbsolutePath(dataFilePath);
 * 
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class PathUtils {
	/**
	 * Kořenový adresář. Implicitně nastaven na aktuální adresář.
	 */
	public static String parentDirectory = ".";

	/**
	 * Nastaví cestu ke kořenovámu adresáři.
	 * 
	 * @param parentDirectory
	 *            Cesta ke kořenovému adresáři.
	 */
	public static void setParentDirectory(String parentDirectory) {
		PathUtils.parentDirectory = parentDirectory;
	}

	/**
	 * Vrací cestu ke kořenovému adresáři, která mohla být nastavena voláním
	 * {@link #setParentDirectory(String)}.
	 * 
	 * @return Vrací cestu ke kořenovému adresáři
	 */
	public static String getParentDirectory() {
		return parentDirectory;
	}

	/**
	 * Zjistí absolutní cestu, která odpovídá spojení cestu kořenového adresáře,
	 * kterou lze nastavi voláním {@link #setParentDirectory(String)} a dané
	 * cestu {@code relativePath}.
	 * 
	 * @param relativePath
	 *            Relativní cesta k souboru nebo adresáři, jehož absolutní cestu
	 *            chceme zjistit.
	 * 
	 * @return Vrací absolutní cestu vytvořenou z kořenového adresáře a
	 *         relativní cesty {@code relative path}.
	 */
	public static String convertRelativePath(String relativePath) {
		return new File(parentDirectory, relativePath).getAbsolutePath();
	}
}
