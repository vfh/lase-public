package lase.websearch;

import lase.morphology.CharFilter;
import lase.util.Corpus;
import lase.util.LaseException;


/**
 * Hledání na falešném webovém vyhledávači. Bez ohledu na podobu vyhledávací fráze
 * vrací vždy stejné výsledky.
 * 
 * @author Rudolf Šíma
 *
 */
public class FakeWebSearch implements WebSearch {
	
	/**
	 *	Pole falešných snipetů, ze kterých se sestaví falešný korpus.
	 */
	public static final String[] fakeSnippets = {
 		//"Pes je pes.",
 		//"Pes je zřejmě pes.",
 		//"Pes je pravděpodobně pes.",
 		//"Pes je chlupatý pes vybavený ocasem.",
 		//"Takový pes je Pes a nic jiného.",
 		//"Pes chlupatý psem jest.",
		//"Bla bla bla. Rudolf Šíma se narodil v lese 21. 5. 1968 v Plzni. Bla bla.",
		//"Rudolf Šíma se narodil v Plzni.",
		//"Rudolf Šíma (21.5.1968—20.9.2011) je taký týpek, co neudělal státnice."
		//"Nezajímavé pindy, a teď to přijde: Karel IV. se narodil 13. dubna 2010."
		"Josef Čapek se narodil 5.1. 1890",
		"Petr Parléř se narodil v roce 1332 v Kolíně nad Rýnem. Byl to německý architekt, stavitel, kameník, ....." 
	};
	
	/**
	 * Znakový filtr sloužící k nahrazení sémanticky ekvivalentních znaků jedním
	 * zástupcem dané třídy.
	 * 
	 * @see CharFilter
	 */
	private static final CharFilter charFilter = CharFilter.getPreferredFilter();
	
	/**
	 * Falešný korpus, který je vrácen při každém pokusu o hledání na webu.
	 */
	private static final Corpus fakeCorpus = new Corpus();
	static {
		for (String s : fakeSnippets)
			fakeCorpus.addEntry(charFilter.substitute(s), "Falešná stránka", "http://liks.fav.zcu.cz/");
	}
	
	/**
	 * Vrací korpus z pevně daných snippetů bez ohledu na vyhledávací řetězce.
	 */
	@Override
	public Corpus search(String[] exactPhrase, String[] otherWords) throws LaseException {
		return fakeCorpus;
	}
}
