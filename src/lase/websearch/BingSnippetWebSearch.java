package lase.websearch;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.swing.text.AttributeSet;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.HTML.Tag;
import javax.swing.text.html.parser.ParserDelegator;

import lase.util.Corpus;
import lase.util.LaseException;
import lase.websearch.AbstractWebSearch;

import org.apache.commons.httpclient.Header;

/**
 * <p>
 * Rozhraní webového vyhledávače Jyxo.cz.
 * </p>
 * 
 * @author Matyáš Latner <malatner@students.zcu.cz>
 * 
 * @see WebSearch
 *
 */
public class BingSnippetWebSearch extends AbstractWebSearch {

        /*
         * Řetězec rozdělující ukázky ve snippetu.
         */
        private String previewSeparator = "...";

	/**
	 * Předloha URL, ze kterého se stahují výsledky hledání.
	 */
	private static final String urlPattern = "http://www.bing.com/search?q=%s&go=&form=QBLH&qs=n&sk=";
	
	/**
	 * Hlavička s cookie sloužící k nastavení počtu odkazů na stránce. Ve
	 * výchozím stavu je to pouze 10.
	 */
	private static final Header cookieHeader = new Header("Cookie", "SRCHHPGUSR=NEWWND=0&ADLT=DEMOTE&NRSLT=50&NRSPH=2&SRCHLANG=&AS=1");

        /* (non-Javadoc)
	 * @see lase.websearch.WebSearch#getPreviewSeparator()
	 */
        
        public String getPreviewSeparator() {
            return this.previewSeparator;
        }
	
	/**
	 * Stáhne korpus textů ze snippetů, které se získají vyhledáním daných frázi
	 * na vyhledávači Jyxo.
	 * 
	 * @see lase.websearch.AbstractWebSearch#search(java.lang.String[],
	 *      java.lang.String[])
	 */
	@Override
	public Corpus search(String[] exactPhrases, String[] otherWords) throws LaseException {
		String url = getSearchUrl(exactPhrases, otherWords);
		System.out.println("URL: " + url);
		String body = downloadBody(url);

		ParserDelegator delegator = new ParserDelegator();
		InputStream is = new ByteArrayInputStream(body.getBytes());
		Reader reader = new InputStreamReader(is);
		ParserCallback callback = new ParserCallback();
		try {
			delegator.parse(reader, callback, true);
		} catch (IOException e) {
			assert false;
			throw new LaseException(e);
		}
		System.out.println("Počet snippetů: " + callback.corpus.size());
		/*System.out.println(body);
		 for (String s : callback.corpus.getStringIterable()) {
		 		System.err.println(s);
		 }*/
		return callback.corpus;
	}
	
	/**
	 * Callback HTML parseru, kde se zpracovává vyhledávací stránka z Bingu
	 * a vytváří se z ní korpus. 
	 *
         * @author Matyáš Latner
	 *
	 */
	private class ParserCallback extends HTMLEditorKit.ParserCallback {
		
		/**
		 * <code>true</code>, pokud se právě načítá text snippetu.
		 */
		private boolean captureSnippet;

                /**
		 * <code>true</code>, pokud se právě parsuje div se snipettem.
		 */
		private boolean captureDiv;
		
		
		/**
		 * <code>true</code>, pokud se právě načítá nadpis stránky
		 * (ze které snippet pochází). 
		 */
		private boolean captureTitle;
		
		/**
		 * StringBuilder, do kterého se ukládá text snippetu.
		 */
		StringBuilder snippetBuilder = new StringBuilder(0xFF);
		
		/**
		 * StringBuilder, do které se ukládá nadpis stránky
		 */
		StringBuilder titleBuilder = new StringBuilder(0x40);
		
		/**
		 * Řetězec, do kterého se uloží URL stránky, ze které snippet pochází.
		 */
		String url;
		
		/**
		 * Korpus, který je výsledkem zpracování vyhledávací stránky.
		 */
		Corpus corpus = new Corpus();

		/**
		 * Zjistí, zda množina atributů obsahuje atribut daného jména a zároveň
		 * má danou hodnotu.
		 * 
		 * @param set
		 *            - Množina atributů HTML tagu.
		 * @param name
		 *            - Jméno a atributu.
		 * @param value
		 *            - Požadovaná hodnota atributu.
		 * 
		 * @return Vrací <code>true</code>, pokud množina atribudů obsahuje
		 *         atribut daného jména a hodnoty. Jinak vrací
		 *         <code>false</code>
		 */
		private boolean containsAttribute(AttributeSet set, String name, String value) {			
			Object attr = set.getAttribute(HTML.Attribute.CLASS);
			return attr != null && value.equals(attr.toString());
		}
		
		/**
		 * Nastaví příslušné vlajky při nalezení určitého počátečního tagu.
		 */
		@Override
		public void handleStartTag(Tag t, MutableAttributeSet a, int pos) {
			if (t.equals(Tag.DIV)) {
				if (containsAttribute(a, "class", "sa_cc")){
                                        captureDiv = true;
                                } else if (containsAttribute(a, "class", "sb_tlst")){
                                        if(captureDiv){
                                            captureTitle = true;
                                        }
                                }
			} else if (t.equals(Tag.A)) {
				if (captureDiv) {
					url = a.getAttribute(HTML.Attribute.HREF).toString();
				}
			} else if (t.equals(Tag.P)) {
                            //if(captureDiv){
                                captureSnippet = true;
                            //}
                        }
		}

		/**
		 * Při nalezení příslušného koncového tagu uloží vytvořený snippet do korpusu.
		 */
		@Override
		public void handleEndTag(Tag t, int pos) {
			
			if (t.equals(Tag.DIV)) {
                            if(captureTitle){
				captureTitle = false;
                            }
			}
                        if (t.equals(Tag.P)) {
                            if(captureSnippet){
                                flushSnippetBuilder();
                                captureSnippet = false;
                                captureDiv = false;
                            }
			}
		}

		/**
		 * Přidá text snippetu nebo nadpisu stránky do přislušného StringBuilderu.
		 */
		@Override
		public void handleText(char[] data, int pos) {
			if (captureSnippet){
				snippetBuilder.append(data);
                        } else if (captureTitle){
				titleBuilder.append(data);
                        }
		}

		/**
		 * Uloží obsah snippetu do korpusu, pokud v příslušném StringBuilderu
		 * nějaký text je.
		 */
		private void flushSnippetBuilder() {
			String snippetText = snippetBuilder.toString();
			if (snippetText.length() > 0) {
				snippetBuilder.setLength(0);
				String title = titleBuilder.toString();
				titleBuilder.setLength(0);
				if (url == null)
					url = "";
				corpus.addEntry(snippetText, title, url);
			}
		}
	}

	/**
	 * Sestaví vyhledávací URL pro konkrétní řetězce, které se budou hledat.
	 * 
	 * @param exactPhrase
	 *            - Pole přesných frází, které budou zadány do vyhledávače Jyxo
	 *            v uvozovkách.
	 * @param otherWords
	 *            - Pole ostatních slov, které budou zadány bez uvozovek.
	 * 
	 * @return Vrací URL, na které Jyxo zobrazuje výsledky hledání.
	 */
	private String getSearchUrl(String[] exactPhrase, String[] otherWords) {
		String query = getSearchString(exactPhrase, otherWords);
		return String.format(urlPattern, urlEncodeUtf8(query));
	}

	/**
	 * Stáhne obsah webové stránky jako řetězec. V HTTP požadavku odešle
	 * hlavičku {@link #cookieHeader}.
	 * 
	 * @param url
	 *            - URL webové stránky.
	 *            
	 * @return Vrací webovou stránku jako řetězec.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že stažení stránky z nějakého důvodu
	 *             selhalo.
	 */
	private String downloadBody(String url) throws LaseException {
		return downloadBodyAsString(url, cookieHeader);
	}
}
