package lase.websearch;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import lase.util.Corpus;
import lase.util.LaseException;
import lase.util.StringUtils;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * <p>
 * Abstraktní předek tříd implementujících získávání korpusu z internetu webovým
 * vyhledávačem.
 * </p>
 * 
 * <p>
 * Obsahuje metody pro stažení stánky z internetu nebo pro sestavení
 * vyhledávacího řetězce, které jsou pro standardní typy vyhledávačů (typu
 * Google) společné.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 * @see WebSearch
 * 
 */
public abstract class AbstractWebSearch implements WebSearch {
	
	/**
	 * Prázdné pole pro nahrazení parametrů, které jsou <code>null</code> 
	 */
	private static final String[] emptyStringArray = new String[0];
	
	/* (non-Javadoc)
	 * @see lase.websearch.WebSearch#search(java.lang.String[], java.lang.String[])
	 */
	@Override
	public abstract Corpus search(String[] exactPhrases, String[] otherWords) throws LaseException;	
	
	
	// TODO - comment
	public AbstractWebSearch() {
		Logger.getLogger(HttpMethodBase.class).setLevel(Level.ERROR);
	}
	
	/**
	 * Stáhne webovou stránku umístěnou na zadaném URL jako řetězec, přičemž je
	 * automaticky zajištěno nastavení správný převod znaků do interního
	 * kódování javy podle hlavičky <code>ContentType</code> webové stránky.
	 * 
	 * @param url
	 *            - URL webové stránky.
	 * @param headers
	 *            - Proměnný počet hlaviček (např. cookies), které se mají v
	 *            HTTP požadavku odeslat webovému serveru.
	 * 
	 * @return Vrací obsah webové stránky jako řetězec.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že z nějakého důvodu není možné provést
	 *             nebo stažení obsahu webové stránky.
	 */
	protected String downloadBodyAsString(String url, Header... headers) throws LaseException {
		HttpMethod method = downloadBody(url, headers);
		try {
			return method.getResponseBodyAsString();
		} catch (IOException e) {
			throw new LaseException(e); 
		}
	}

	/**
	 * Stáhne webovou stránku (resp. libovolný soubor) umístěnou na zadaném URL
	 * jako pole bytů. Narozdíl od metody
	 * {@link #downloadBodyAsString(String, Header...)} zde není možné zajistit
	 * jakékoli úpravy kódování na základě obsahu stránky (protože to není
	 * řetězec, ale jen byty).
	 * 
	 * @param url
	 *            - URL webové stránky
	 * @param headers
	 *            - Proměnný počet hlaviček (např. cookies), které se mají v
	 *            HTTP požadavku odeslat webovému serveru.
	 * @return Vrací obsah webové stránky jako pole bytů.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že z nějakého důvodu není možné provést
	 *             nebo stažení obsahu webové stránky.
	 */
	protected byte[] downloadBodyAsByteArray(String url, Header... headers) throws LaseException {
		HttpMethod method = downloadBody(url, headers);
		try {
			return method.getResponseBody();
		} catch (IOException e) {
			throw new LaseException(e);
		}
	}
	
	/**
	 * <p>
	 * Z parametrů <code>extactPhrase</code> a <code>otherWords</code> sestaví
	 * vyhledávací frázi.
	 * <p>
	 * 
	 * <p>
	 * Sestavení fráze pro běžné typy vyhledávačů (např.: Google) se provede
	 * tak, že se nejprve všechny prvky <code>exactPhrases</code> vloží do
	 * uvozovek a poté se spojí mezerami do jednoho řetězce. Stejně tak
	 * <code>otherWords</code> se oddělené mezerami spojí do jednoho řetězce.
	 * Oba vzniklé řetězce se spojí do jednoho odděleného mezerou.
	 * </p>
	 * 
	 * <h3>Příklad:</h3>
	 * <p>
	 * V případě následujících hodnot parametrů"
	 * 
	 * <pre>
	 * exactPhrases[] = {"aa", "bb", "cc"}
	 * otherWords[]   = {"a", "b", "c"}
	 * </pre>
	 * 
	 * bude mít vyhledávací fráze například vypadat
	 * 
	 * <pre>
	 * "aa" "bb" "cc" a b c
	 * </pre>
	 * 
	 * </p>
	 * 
	 * @param exactPhrases
	 *            - Pole přesných frází, které budou uvedeny v uvozovkách.
	 * @param otherWords
	 *            - Slova, která budou uvedena bez uvozovek
	 * 
	 * @return Vrací vytvořenou vyhledávací frázi.
	 */
	protected static String getSearchString(String[] exactPhrase, String[] otherWords) {
		if (exactPhrase == null)
			exactPhrase = emptyStringArray;
		if (otherWords == null)
			otherWords = emptyStringArray;
		
		String exactPhrasePart;
		if (exactPhrase.length > 0)
			exactPhrasePart = "\"" + StringUtils.concat(exactPhrase, "\" \"") + "\"";
		else
			exactPhrasePart = "";
		
		String otherWordsPart;
		if (otherWords.length > 0)
			otherWordsPart = " " + StringUtils.concat(otherWords, " ");
		else
			otherWordsPart = "";

		String query = exactPhrasePart + otherWordsPart;
		
		// TODO odstranit nežádoucí uvozovky zadané uživatelem
		return query;
	}

	/**
	 * Zákóduje řetězec URL kódováním, aby mohl být použit jako součást URL i v
	 * případě, že obsahuje jiné než ASCII znaky mezery a podobně. Využívá se
	 * kódování UTF-8.
	 * 
	 * @param str
	 *            - Řetězec, který bude zakódován URL kódováním
	 *            
	 * @return Vrací řetězec v URL kódování.
	 */
	protected String urlEncodeUtf8(String str) {
		try {
			return URLEncoder.encode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new AssertionError();
		}
	}

	/**
	 * Využití třídy {@link HttpClient} odešle HTTP požadavek webovému serveru.
	 * Když se vrátí odpověď, vrací {@link HttpMethod}, která umožněje čtení dat
	 * různými způsoby (viz {@link #downloadBodyAsString(String, Header...)} a
	 * {@link #downloadBodyAsByteArray(String, Header...)}).
	 * 
	 * @param url
	 *            - URL webové stránky
	 * @param headers
	 *            - Pole hlaviček (např. cookies), které se mají v HTTP
	 *            požadavku odeslat webovému serveru.
	 * @return Vrací {@link HttpMethod} s odpovědí vrácenou webovým serverem.
	 * 
	 * @throws LaseException
	 *             Vyhozena vpřípadě, že nastala chyba při odeslání požadavku
	 *             nebo že odpověď serveru nebyla "200 OK".
	 */
	private HttpMethod downloadBody(String url, Header[] headers) throws LaseException {
		HttpClient client = new HttpClient();
		HttpMethod method = new GetMethod(url);
		if (headers != null) {
			for (Header h : headers)
				method.setRequestHeader(h);
		}
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
		int status;
		try {
			status = client.executeMethod(method);
		} catch (IOException e) {
			throw new LaseException(e);
		}
		if (status != HttpStatus.SC_OK)
			throw new LaseException("HTTP request failed. The HTTP status is not OK.");
		return method;
	}
}
