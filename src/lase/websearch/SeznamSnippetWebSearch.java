/**
 * 
 */
package lase.websearch;

import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import lase.util.Corpus;
import lase.util.Html2Text;
import lase.util.LaseException;

/**
 * realizuje vyhledávání odpovedí přes API Seznamu
 * @author Ondřej Pražák
 * @version 1.00
 * @since 2.0
 */
public class SeznamSnippetWebSearch extends AbstractWebSearch {

	private static final String URL_PATTERN = "http://searchapi.seznam.cz/api2/search?client=zcu&source=web&format=xml&query=";
	
	/**
	 * zpoždění přístupu k API, maximálně jednou za vteřinu
	 */
	private static final int DELAY = 1000;
	
	/**
	 * čas posledního přístupu k API
	 */
	private long lastAccessTime = 0;
	
	

	/* (non-Javadoc)
	 * @see lase.websearch.AbstractWebSearch#search(java.lang.String[], java.lang.String[])
	 */
	@Override
	public synchronized Corpus search(String[] exactPhrases, String[] otherWords)
			throws LaseException {
		
		String url = getSearchUrl(exactPhrases, otherWords);
		System.out.println("URL: " + url);
		
		// pokud od posledního dotazu neuplynula celá vteřina, vlákno čeká
		// počet dotazá na Seznamu je totiž omezen na jeden za vteřinu
		long currentTime = System.currentTimeMillis();
		while(currentTime-lastAccessTime<DELAY)
		{
			try {
				Thread.sleep(DELAY-(currentTime-lastAccessTime));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			currentTime = System.currentTimeMillis();
		}
		
		String body = downloadBodyAsString(url);
		lastAccessTime = System.currentTimeMillis();
		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser sp;
		XMLReader parser;
		try {
			sp = spf.newSAXParser();
			parser = sp.getXMLReader();
			SearchXMLHandler handler = new SearchXMLHandler();
			parser.setContentHandler(handler);
			parser.parse(new InputSource(new StringReader(body)));
			while(!handler.isDone())
			{
				Thread.sleep(10);
			}
			return handler.corpus;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
	}
		
		return null;
	}
	
	
	/**
	 * Sestaví vyhledávací URL pro konkrétní řetězce, které se budou hledat.
	 * 
	 * @param exactPhrase
	 *            - Pole přesných frází, které budou zadány do vyhledávače
	 * @param otherWords
	 *            - Pole ostatních slov, které budou zadány bez uvozovek.
	 * 
	 * @return Vrací URL, pro zobrazeni výsledků hledání.
	 */
	private String getSearchUrl(String[] exactPhrase, String[] otherWords) {
		String query = getSearchString(exactPhrase, otherWords);
		return "\""+URL_PATTERN+urlEncodeUtf8(query)+"\"";
	}

	/**
	 * zpracovává data ve formátu xml, která vrátí vyhledávač a vytváří z nich {@code Corpus}
	 * @author Ondřej Pražák
	 * @version 1.00
	 * @since 2.0
	 */
	private static class SearchXMLHandler extends DefaultHandler
	{
		/**
		 * příznak dokončení zpracování
		 */
		private boolean done = false;
		
		/**
		 * {@code StringBuilder} pro sbírání dat z těla elementu
		 */
		private StringBuilder data = new StringBuilder();
		
		private Corpus corpus;
		
		private String url;
		
		private String title;
		
		private String text;
		
		private SearchXMLHandler()
		{
			corpus = new Corpus();
		}
		
		

		public synchronized boolean isDone() {
			return done;
		}

		public synchronized void setDone(boolean done) {
			this.done = done;
		}


		@Override
		public void characters(char[] ch, int start, int length)
				throws SAXException {
			data.append(String.valueOf(ch, start, length));
		}

		@Override
		public void endDocument() throws SAXException {
			setDone(true);
		}

		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			if(qName.equals("description"))
				text = Html2Text.convert(data.toString());
			else if(qName.equals("title"))
				title = Html2Text.convert(data.toString());
			else if(qName.equals("url"))
				url = data.toString();
			else if(qName.equals("result"))
			{
				corpus.addEntry(text, title, url);
			}
		}

		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			data.delete(0, data.length());
			if(qName.equals("result"))
			{
				text = title = url = null;
			}
		}
		
		
	}
}
