package lase.morphology;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * Morfologický analyzátor textu.
 * </p>
 * 
 * <p>
 * Cílem morfologické analýzy v systému LASE pro všechny tokeny vstupího textu
 * určit jejich lemma (slovníkový tvar slova) a slovní druh.
 * </p>
 * 
 * 
 * @author Rudolf Šíma
 * 
 */
public interface MorphologicalAnalyser {

	/**
	 * Provede morfologickou analýzu textu <code>text</code> a vrátí seznam slov
	 * obsažených v textu s morfologickými informacemi.
	 * 
	 * @param text
	 *            Slova (tokeny) textu, který je předmětem lemmatizace.
	 * 
	 * @return Vrací seznam morfologických slov, které odpovídají prvkům seznamu
	 *         <code>text</code>. Vrácený výsledný seznam musí tedy být délkou
	 *         shodný s počtem tokenů vstupního textu.
	 * 
	 * @throws MorphologyException
	 *             - Vyhozena v případě, že při morfologické analýze došlo k
	 *             chybě. Výjimka, které způsobí tuto chybu je nastavena jako
	 *             příčina výjimky <code>MorphologyException</code>.
	 */
	@Deprecated
	public List<MorphWord> analyse(String[] text) throws MorphologyException;
	
	/**
	 * Provede morfologickou analýzu textu <code>text</code> a vrátí seznam slov
	 * obsažených v textu s morfologickými informacemi.
	 * 
	 * @param text
	 *            Slova (tokeny) textu, který je předmětem lemmatizace.
	 * 
	 * @return Vrací seznam morfologických slov, které odpovídají prvkům seznamu
	 *         <code>text</code>. Vrácený výsledný seznam musí tedy být délkou
	 *         shodný s počtem tokenů vstupního textu.
	 * 
	 * @throws MorphologyException
	 *             - Vyhozena v případě, že při morfologické analýze došlo k
	 *             chybě. Výjimka, které způsobí tuto chybu je nastavena jako
	 *             příčina výjimky <code>MorphologyException</code>.
	 */
	public List<MorphWord> analyse(List<String> text) throws MorphologyException;
	
	/**
	 * Provede morfologickou analýzu textu <code>text</code> a vrátí seznam slov
	 * obsažených v textu s morfologickými informacemi.
	 * 
	 * @param text
	 *            text, který je předmětem lemmatizace.
	 * 
	 * @return Vrací seznam morfologických slov.
	 * 
	 * @throws MorphologyException
	 *             - Vyhozena v případě, že při morfologické analýze došlo k
	 *             chybě. Výjimka, které způsobí tuto chybu je nastavena jako
	 *             příčina výjimky <code>MorphologyException</code>.
	 */
	public List<MorphWord> analyse(String text) throws MorphologyException;

	/**
	 * Zavře morfologický analyzátor.
	 */ 
	public void close() throws IOException;
}
