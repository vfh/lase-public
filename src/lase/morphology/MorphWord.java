package lase.morphology;

import java.io.Serializable;

/**
 * Morfologické slovo, tj. jedno slovo řetězce slov vráceného morfologickým
 * analyzátorem. Obsahuje původní slovo a další morfologické informace.
 * 
 * @author Rudolf Šíma
 * 
 */
public class MorphWord implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * původní slovo z analyzovaného textu
	 */
	private String word;
	
	/**
	 *  lemma (slovníkový tvar) původního slova
	 */
	private String lemma;
	
	/**
	 * anglická zkratka slovního druhu
	 */
	private char posTag;
	
	/**
	 * Vytvoří novou instanci </code>MorphWord</code> a nastaví atributy podle
	 * následujících parametrů.
	 * 
	 * @param word
	 *            - původní slovo z analyzovaného textu
	 * @param lemma
	 *            - lemma (slovníkový tvar) původního slova
	 * @param posTag
	 *            - anglická zkratka slovního druhu:<br/>
	 *              N - podstatné jméno<br/>
	 *              V - sloveso<br/>
	 *              P - zájmeno<br/>
	 *              R - předložka<br/>
	 *              A - přídavné jméno<br/>
	 *              C - číslovka<br/>
	 */
	public MorphWord(String word, String lemma, char posTag) {
		this.word = word;
		this.lemma = lemma;
		this.posTag = posTag;
	}

	/**
	 * Vrací původní slovo ve tvaru, jak je v analyzovaném textu.
	 * 
	 * @return Vrací původní slvo z analyzovaného textu.
	 */
	public String getWord() {
		return word;
	}

	/**
	 * Vrací lema (slovníkový tvar) původního slova z analyzovaného textu.
	 * 
	 * @return Vrací lema (slovníkový tvar) původního slova z analyzovaného
	 *         textu.
	 */
	public String getLemma() {
		return lemma;
	}

	/**
	 * Vrací anglickou zkratku slovního druhu:<br/>
	 *              N - podstatné jméno<br/>
	 *              V - sloveso<br/>
	 *              P - zájmeno<br/>
	 *              R - předložka<br/>
	 *              A - přídavné jméno<br/>
	 *              C - číslovka<br/>
	 *              D - příslovce<br/>
	 * @return Vrací anglickou zkratku svlovního druhu.
	 * 
	 */
	public char getPosTag() {
		return posTag;
	}	
	
	/**
	 * Vrací původní slovo ve tvaru, jak je v analyzovaném textu.
	 * 
	 * @return Vrací původní slvo z analyzovaného textu.
	 */
	public String toString() {
		return word; 
	}
}
