package lase.morphology;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lase.cache.CacheEntryNotFoundException;
import lase.cache.CachedMethod;
import lase.template.NamedEntityToken;
import lase.template.Query;
import lase.template.QueryToken;
import lase.template.Token;
import lase.template.WordToken;
import lase.template.NamedEntityToken.Subtype;
import cz.zcu.fav.liks.morphology.client.MorphologyRPCWebServiceClient;
import cz.zcu.fav.liks.semantics.AnalysisException;
import cz.zcu.fav.liks.semantics.lexical.AbstractLexicalAnalysis;
import cz.zcu.fav.liks.semantics.lexical.LexicalAnalysisImpl;
import cz.zcu.fav.liks.semantics.lexical.LexicalClass;
import cz.zcu.fav.liks.semantics.lexical.LexicalClassAnalysisException;
import cz.zcu.fav.liks.semantics.lexical.LexicalToken;
import cz.zcu.fav.liks.semantics.lexical.Sentence;
import cz.zcu.fav.liks.semantics.lexical.scripting.DateResult;

@Deprecated
public class LiksNamedEntityRecognizer implements NamedEntityRecognizer {
	private static Map<String, Subtype> classNamesMap = new HashMap<String, Subtype>();
	static {
		// inicializace mapy jmen lexikálních tříd
		classNamesMap.put("date", Subtype.DATE);
		classNamesMap.put("time", Subtype.TIME);
		classNamesMap.put("number", Subtype.NUMBER);
		classNamesMap.put("geography", Subtype.GEOGRAPHY); // kdo ví, jestli existuje
		classNamesMap.put("city", Subtype.GEOGRAPHY);
		classNamesMap.put("persons", Subtype.PERSON);
	}
	private AbstractLexicalAnalysis analysis;
	
	public LiksNamedEntityRecognizer(String url, boolean useCache) throws LexicalClassAnalysisException, IOException {
		if (useCache) {
			analysis = new CachedLexicalAnalysis(url);
		} else {
			MorphologyRPCWebServiceClient client = new MorphologyRPCWebServiceClient(url);
			analysis = new LexicalAnalysisImpl(client);
		}
	}
	
	@Override
	public List<Query> recognize(String sentence) throws MorphologyException {
		List<Sentence> sentences;
		try {
			sentences = analysis.analyseAll(sentence);
		} catch (AnalysisException e) {
			throw new MorphologyException(e.getMessage(), e.getCause());
		}
		
		List<Query> recognizedQueries = new ArrayList<Query>();
		for (Sentence sen : sentences) {
			List<QueryToken> tokenList = new ArrayList<QueryToken>(sen.getLength());
			for (LexicalToken lexicalToken : sen.getTokenIterbale()) {
				String lexicalTokenText = lexicalToken.getText();
				Token token = null;
				Object semanticValue;
				if (lexicalToken instanceof LexicalClass) {
					LexicalClass lexicalClass = (LexicalClass) lexicalToken;
					semanticValue = convertSemanticContent(lexicalClass.getSemanticContent());
					token = createToken(lexicalClass);
				} else {
					semanticValue = lexicalToken.getLematizedText();
					if (acceptWordToken(lexicalTokenText))
						token = new WordToken(lexicalTokenText);
				}
				if (token != null) {
					QueryToken newQueryToken = new QueryToken(token, lexicalTokenText, semanticValue);
					tokenList.add(newQueryToken);
				}
			}
			Query newQuery = new Query(tokenList);
			recognizedQueries.add(newQuery);
		}
		return recognizedQueries;		
	}

	private Object convertSemanticContent(Object semanticContent) {
		if (semanticContent instanceof DateResult)
			return ((DateResult)semanticContent).getCalendar();
		return semanticContent;
	}

	private boolean acceptWordToken(String text) {
		return !"?".equals(text);
	}
	
	private static Token createToken(LexicalClass lc) {		
		Subtype type = classNamesMap.get(lc.getName());
		if (type == null) {
			assert false : "Unknown class: " + lc.getName();
			return new WordToken(0, "");
		}
		
		NamedEntityToken result = new NamedEntityToken(0, type);
		return result;
	}

	@Override
	public void close() throws IOException {
		if (analysis instanceof CachedLexicalAnalysis)
			((CachedLexicalAnalysis)analysis).close();
	}

	public static class CachedLexicalAnalysis extends AbstractLexicalAnalysis {
		private CachedMethod<List<Sentence>> analyseAllMethod = new CachedMethod<List<Sentence>>(this, "analyseAllUncached", String.class);
		private AbstractLexicalAnalysis impl;
		private String webServiceUrl;

		
		public CachedLexicalAnalysis() throws LexicalClassAnalysisException, IllegalArgumentException, IOException {
		}
		
		public CachedLexicalAnalysis(String webServiceUrl) throws LexicalClassAnalysisException, IOException {
			this.webServiceUrl = webServiceUrl;		
		}
		
		public List<Sentence> analyseAll(String sentence) throws AnalysisException {
			try {
				List<Sentence> result = analyseAllMethod.invoke(sentence);
				return result;
			} catch (InvocationTargetException e) {
				Throwable cause = e.getCause();
				if (cause instanceof RuntimeException)
					throw (RuntimeException) cause;
				throw new AnalysisException(cause.getMessage(), cause);
			}
		}

		public List<Sentence> analyseAllUncached(String sentence) throws AnalysisException, IOException {
			if (impl == null) {
				if (webServiceUrl == null)
					throw new CacheEntryNotFoundException();
				else
					impl = new LexicalAnalysisImpl(new MorphologyRPCWebServiceClient(webServiceUrl));
			}
			return impl.analyseAll(sentence);
		}
		
		public void close() throws IOException {
			if (!analyseAllMethod.storeCache())
				throw analyseAllMethod.getIOException();
		}
	}
}
