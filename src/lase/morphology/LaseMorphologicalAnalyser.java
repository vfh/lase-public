package lase.morphology;

import edu.emory.mathcs.backport.java.util.Arrays;
import gate.Annotation;
import gate.AnnotationSet;
import gate.Gate;
import gate.creole.ResourceInstantiationException;
import gate.util.GateException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import lase.app.SharedObjects;
import lase.util.PathUtils;
import cz.zcu.fav.liks.apps.GateAppWrapper;

/**
 * Morfologický analyzátor
 * @author Ondřej Pražák
 * @version 1.00
 * @since 2.0
 */
public class LaseMorphologicalAnalyser implements MorphologicalAnalyser {

	private static final String CONFIGURATION_PATH = "data/zswi-app.xml";
	
	private GateAppWrapper wrapper;
	
	public LaseMorphologicalAnalyser()
	{
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Deprecated
	public List<MorphWord> analyse(String[] text) throws MorphologyException {
		return analyse(Arrays.asList(text));
	}

	@Override
	public List<MorphWord> analyse(List<String> text)
			throws MorphologyException{
		StringBuilder sb = new StringBuilder();
		for(String word: text)
		{
			sb.append(word+" ");
		}
		return analyse(sb.toString());
	}

	@Override
	public void close() throws IOException {
	}
	
	@Override
	public List<MorphWord> analyse(String text) throws MorphologyException {
		if(wrapper==null)
			wrapper = SharedObjects.getMorphologyWrapper();
		List<MorphWord> list = new ArrayList<MorphWord>();
		try
		{
			Map<String, AnnotationSet> as = wrapper.execute(text);
			AnnotationSet tokens = as.get("Token");
			Set<Annotation> set = new TreeSet<Annotation>(new Comparator<Annotation>() {

				@Override
				public int compare(Annotation o1, Annotation o2) {
					return o1.getStartNode().getOffset().intValue()-o2.getStartNode().getOffset().intValue();
				}
			});
			set.addAll(tokens);
			for(Annotation token: set)
			{
				String lemma = token.getFeatures().get("Lemma").toString();
				lemma = lemma.indexOf('_') != -1 ? lemma.substring(0, lemma.indexOf('_')) : lemma;
				lemma = lemma.indexOf('-') != -1 ? lemma.substring(0, lemma.indexOf('-')) : lemma;
				MorphWord mw = new MorphWord(token.getFeatures().get("Word").toString(),
						lemma,
						token.getFeatures().get("Tag").toString().charAt(0));
				list.add(mw);
			}
			// Testování NER
			
			/*AnnotationSet entities = as.get("Named Entity");
			for(Annotation entity : entities)
			{
				System.err.println("["+entity.getStartNode().getOffset() + ", " + entity.getEndNode().getOffset()+ "] - " + entity.getType());
				System.err.println(text.substring(entity.getStartNode().getOffset().intValue(),entity.getEndNode().getOffset().intValue()));
			}*/
		}
		catch (GateException e) {
		 	e.printStackTrace();
	 	}
		return list;
	}
	
	public static void main(String[] args)
	{
		LaseMorphologicalAnalyser a = new LaseMorphologicalAnalyser();
		List<String> list = new ArrayList<String>();
		list.add("Kdy");
		list.add("se");
		list.add("narodil");
		list.add("Josef");
		list.add("Čapek");
		try {
			a.analyse(list);
		} catch (MorphologyException e) {
			e.printStackTrace();
		}
	}
	/**
	 * inicializuje analyzátor, odložená inicializace kvůli velké časové náročnosti
	 */
	/*private void init()
	{
		if(!Gate.isInitialised())
		{
			Gate.runInSandbox(true);
			try {
				Gate.init();
			} catch (GateException e) {
				e.printStackTrace();
			}
		}
		try {
			wrapper = new GateAppWrapper(PathUtils.convertRelativePath(CONFIGURATION_PATH));
		} catch (ResourceInstantiationException e) {
			e.printStackTrace();
		}
	}
	
	public GateAppWrapper getWrapper()
	{
		if(wrapper==null)
			init();
		return wrapper;
	}*/

}
