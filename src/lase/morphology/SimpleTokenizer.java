package lase.morphology;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import cz.cuni.mff.ufal.nametag.Forms;

/**
 * <p>
 * Jednoduchý "offline" tokenizátor. (Offline znamená, že narozdíl od jiné
 * implementace nepoužívá k tokenizaci webovou službu morfologický analyzátoru.
 * </p>
 * 
 * <p>
 * Používá {@link StringTokenizer} s množinou oddělovačů, kterými jsou ASCII
 * znaky od <tt>' '</tt> do <tt>'~'</tt> včetně, tj. <tt>0x20</tt> až
 * <tt>0x7E</tt>, mimo všech písmen a číslic.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class SimpleTokenizer implements Tokenizer {
	
	/**
	 * Řetězec, jehož znaky mají význam oddělovačů při tokenizaci řetězce.
	 * @see SimpleTokenizer
	 */
	private static final String delimiters;
	static {
		final char lBound  = ' ';
		final char uBound  = '~';
		StringBuilder sb = new StringBuilder(uBound - lBound); 
		for(char c = lBound; c <= uBound; c++) {
			if (!Character.isLetterOrDigit(c))
			sb.append(c);
		}
		delimiters = sb.toString();
	}
	
	/**
	 * @see lase.morphology.Tokenizer#tokenize(java.lang.String)
	 * @see SimpleTokenizer
	 */
	public List<String> tokenize(String text) {
		StringTokenizer tokenizer = new StringTokenizer(text, delimiters, true);
		List<String> tokens = new ArrayList<String>();
		while(tokenizer.hasMoreTokens()) {
			String t = tokenizer.nextToken();
			if (t.length() != 1 || !Character.isWhitespace(t.charAt(0)))
				tokens.add(t);
		}
		return tokens;
	}
	
	// TODO
	public Forms forms() {
		return null;
	}
}
