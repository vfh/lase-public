package lase.morphology;

import java.util.ArrayList;
import java.util.List;

import cz.cuni.mff.ufal.nametag.Forms;
import cz.cuni.mff.ufal.nametag.Ner;
import cz.cuni.mff.ufal.nametag.TokenRanges;
import cz.cuni.mff.ufal.nametag.Tokenizer;

/**
 * Class prepared for tokenize with NameTag library
 * @author Jakub Vasta
 *
 */
public class NamedTagTokenizer implements lase.morphology.Tokenizer {
	
	private final Ner ner;
	private Forms forms = new Forms();
	private TokenRanges tokens = new TokenRanges();
	
	/**
	 * Constructor
	 * @param ner refernece to instance of Ner
	 */
	public NamedTagTokenizer(Ner ner) {
		this.ner = ner;
	}
	
	@Override
	public List<String> tokenize(String text) {
		
		Tokenizer tokenizer = ner.newTokenizer();
		tokenizer.setText(text);
		tokenizer.nextSentence(forms, tokens);
		
		return getStringsFromTokens(tokens, text);
		
	}
	
	@Override
	public Forms forms() {
		return forms;
	}
	
	/**
	 * Get list of words (tokens) from tokens
	 * @param tokens form of tokens after tokenization with Nametag
	 * @param text original text for tokenizer
	 * @return
	 */
	private List<String> getStringsFromTokens(TokenRanges tokens, String text) {
		
		List<String> tokenWords = new ArrayList<String>();
		int start, end;
		String word;
		
		for (int i = 0; i < tokens.size(); i++) {
			
			start = (int) tokens.get(i).getStart();
			end = start + (int) tokens.get(i).getLength();
			
			word = text.substring(start, end);
			
			tokenWords.add(word);
			
		}
		
		return tokenWords;
		
	}

}
