package lase.morphology;

import java.io.IOException;
import java.util.List;

import lase.template.Query;

@Deprecated
public interface NamedEntityRecognizer {
	
	public List<Query> recognize(String sentence) throws MorphologyException;
	
	public void close() throws IOException;
	
}
