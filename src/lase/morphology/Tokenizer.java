package lase.morphology;

import java.util.List;

import cz.cuni.mff.ufal.nametag.Forms;

/**
 * Tokenizátor textu. Slouží k tokenizaci textu, která se zpravidla provádí před
 * lemmatizací resp. morfologickou analýzou.
 * 
 * @author Rudolf Šíma
 * 
 * @see SimpleTokenizer
 * 
 */
public interface Tokenizer {
	
	/**
	 * Ze zadaného textu vytvoří a vrátí seznam tokenů.
	 * 
	 * @param text - Text, který má být tokenizován.
	 * 
	 * @return Vrací seznam tokenů.
	 */
	public List<String> tokenize(String text);
	
	public Forms forms();
}