package lase.morphology;

/**
 * <p>
 * Znakový filtr sloužící pro nahrazení sémanticky shodných znaků jediným
 * zástupcem dané třídy.
 * </p>
 * 
 * <p>
 * Např. všechny druhy pomlček (plus spojovní, znaménko mínus) mohou být
 * nahrazeny jednou konkrétní pomlčkou.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public abstract class CharFilter {
	
	/*
	 * Pozn.: Vytváření filtrů podle jména přes reflexe už nemá žádnou výhodu a může
	 * být nahrazeno něčím konvenčnějším.
	 */
	
	/** Poslední použítý filter. */
	private static CharFilter cache;
	
	/** Jméno posledního použitého filtru */
	private static String cacheName;
	
	/** Preferovaný filtr. */
	private static CharFilter prefered = byName("SimpleCharFilter");
	
	/**
	 * Provede nahrazení sémanticky shodných znaků jediným zástupcem dané třídy
	 * (např. všech pomlček, spojovníku a známénka mínus jedním druhem pomlčky.
	 * 
	 * @param original
	 *            - Vstupní text pro nahrazování.
	 *            
	 * @return Vrací text s nahrazenými znaky.
	 */
	public abstract String substitute(String original);

	/**
	 * Vrací filtr podle jména třídy nebo <code>null</code>, není-li filtr
	 * daného jména k dispozici.
	 * 
	 * @param name
	 *            - Jméno třídy filtru. (Pouze krátké jméno, tj. bez názvu
	 *            balíku.)
	 * 
	 * @return Vrací filtr nebo <code>null</code>, není-li filtr k dispozici.
	 */
	public static CharFilter byName(String name) {
		if (!name.equals(cacheName)) {
			final String packageName = CharFilter.class.getPackage().getName();
			try {
				Class<?> charFilterClass = Class.forName(packageName + "." + name);
				CharFilter charFilter = (CharFilter) charFilterClass.newInstance();
				cacheName = name;
				cache = charFilter;
			} catch (ClassNotFoundException e) {
				return null;
			} catch (IllegalAccessException e) {
				return null;
			} catch (InstantiationException e) {
				return null;
			}
		}
		return cache;
	}

	/**
	 * Vrací preferovaný filtr.
	 * 
	 * @return preferovaný filtr.
	 */
	public static CharFilter getPreferredFilter() {
		return prefered;
	}
}
