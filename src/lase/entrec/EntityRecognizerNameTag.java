package lase.entrec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.httpclient.HttpException;
import org.json.JSONException;
import org.xml.sax.SAXException;

import cz.cuni.mff.ufal.nametag.Ner;
import lase.entrec.NamedEntityGraph.Node;
import lase.morphology.MorphWord;
import lase.morphology.MorphologicalAnalyser;
import lase.morphology.MorphologyException;
import lase.morphology.Tokenizer;
import lase.nametagsearch.Container;
import lase.nametagsearch.Item;
import lase.nametagsearch.NameTagWebService;
import lase.nametagsearch.TimeItem;
import lase.template.NamedEntityToken;
import lase.template.NamedEntityToken.Subtype;
import lase.template.QueryToken;
import lase.template.Token;
import lase.template.WordToken;
import lase.util.DateMaker;
import lase.util.IterationUtils;
import lase.util.LaseCalendar;
import lase.util.StringUtils;
import lase.util.UniqueIterator;

/**
 * New named entity recognization class which uses NameTag web service to analyse named entities
 * 
 * @author Jakub Vašta (Rudolf Šíma - buildGraph, close) 
 *  
 */
public class EntityRecognizerNameTag implements NamedEntityRecognizer {
	
	/**
	 * Model of NamedTag 
	 */
	private static final String DATA_MODEL = "data/NamedTagModels/czech-cnec2.0-140304.ner";
	
	/**
	 * Morfologický analyzátor použitý k předzpracování textu.
	 */
	private MorphologicalAnalyser morphAnalyser;
	
	private DateMaker dm;
	
	/**
	 * Tokenizátor použitý ke zpracování textu před analýzou morfologickým
	 * analyzátorem.
	 */
	private Tokenizer tokenizer;
	
	private Ner namedEntityRecognizer;
	
	/**
	 * Vytvoří nový rozpoznávač pojmenovaných entity a nastaví nástroje, které
	 * tento rozpoznávač použije k předzpracování textu a vyhledávání.
	 * 
	 * @param tokenizer
	 *            Tokenizátor použitý ke zpracování textu před analýzou
	 *            morfologickým analyzátorem.
	 * @param morphologicalAnalyser
	 *            Morfologický analyzátor použitý k předzpracování textu.
	 */
	public EntityRecognizerNameTag(Tokenizer tokenizer, MorphologicalAnalyser morphologicalAnalyser)  {
		this.morphAnalyser = morphologicalAnalyser;
		this.tokenizer = tokenizer;
		this.dm = new DateMaker();
		
//		Prepared for Nametag as library
//		
//		System.err.print("Loading ner: ");
//		namedEntityRecognizer = Ner.load(DATA_MODEL);
//		System.err.println("done");
	}
	
	
	/**
	 * Find in lemmas list word with given position and length
	 * @param lemmas list of lemmas
	 * @param position index in list of lemmas
	 * @param length lenght of word over lemmas
	 * @return word string starting in given position and with given length
	 */
	private String getWord(List<MorphWord> lemmas, int position, int length) {
		
		String word = "";
		
		MorphWord[] temp = new MorphWord[lemmas.size()];
		lemmas.toArray(temp);
		
		word += temp[position];
		for (int i = 1; i < length; i++) {
			word += ' ' + temp[position + i].getWord();
		}
		
		return word;
		
	}

	/**
	 * Find given word in lemmas abnd find out position and length over lemmas
	 * @param lemmas list of lemmas
	 * @param text string we are trying to find out where starts in lemmas and its length
	 * @param temp all lemmas in one string without white spaces
	 * @return array - [0] - position, [1] - length
	 */
	private int[] getLemmasWidth(List<MorphWord> lemmas, String text, String temp) {
		
		int index = 0, position = 0;
		text = text.replaceAll(" ", "");
		
		text = text.toLowerCase();
		temp = temp.toLowerCase();
		
		int pos = temp.indexOf(text);
		
		int count = 0;
		for (MorphWord mw : lemmas) {
			if (count == pos) {
				position = index;
			}
			else if (count >= pos + text.length()) {
				break;
			}
			
			count += mw.getWord().length();
			
			index++;
		}
		
		return new int[]{position, index - position};
	}
	
	/**
	 * Find lemmas from given position to given position + length
	 * @param lemmas list of lemmas
	 * @param position position of first lemma in lemmas list
	 * @param length count of lemmas form start position
	 * @return string which contains all lemmas between position and position + length separate with whitespace
	 * @throws NamedEntityRecognitionException
	 */
	private String getLemmas(List<MorphWord> lemmas, int position, int length) throws NamedEntityRecognitionException {
		
		String word = "";
		
		MorphWord[] temp = new MorphWord[lemmas.size()];
		lemmas.toArray(temp);
		// System.err.println("LEMMAS SIZE: " + lemmas.size());
		word += temp[position].getLemma();
		for (int i = 1; i < length; i++) {
			word += ' ' + temp[position + i].getLemma();
		}
		
		return word;
		
	}
	
//	Prepared for NameTag as library
	
	/* (non-Javadoc)
	 * @see lase.entrec.NamedEntityRecognizer#analyse(java.lang.String)
	 * @see LaseEntityRecognizer
	 */
//	@Override
//	public NamedEntityGraph analyse(String text) throws NamedEntityRecognitionException {
//		
//		List<MorphWord> lemmas;
//		
//		NamedEntities preEntities = new NamedEntities();
//		
//		List<String> tokens = tokenizer.tokenize(text);
//		namedEntityRecognizer.recognize(tokenizer.forms(), preEntities);
//
//		try {
//			//TODO při analýze celého textu, kvůli jiné tokenizaci, funguje špatně DataMatcher
//			lemmas = morphAnalyser.analyse(tokens);
//			//lemmas = morphAnalyser.analyse(text);
//		} catch (MorphologyException e) {
//			throw new NamedEntityRecognitionException(e.getMessage(), e);
//			
//		}
//		
//		PositionedEntity pe;
//		List<PositionedEntity> entities = new ArrayList<PositionedEntity>();
//		
//		for (int i = 0; i < preEntities.size(); i++) {
//			
//			int position = (int) preEntities.get(i).getStart();
//			int length = (int) preEntities.get(i).getLength();
//			
//			String word = getWordFromTokens(tokens, position, length);
//			String lemma = getLemmas(lemmas, position, length);
//			String type = preEntities.get(i).getType();
//			
//			// TODO
//			// Convert type to Subtype
//			pe = new PositionedEntity(position, length, word, lemma, NamedEntityToken.getSubtype(type));
//			entities.add(pe);
//			
//		}
//	
//	
//		PositionedEntity.sort(entities);
//		UniqueIterator<PositionedEntity> uniqueEntityIterator = new UniqueIterator<PositionedEntity>(entities.iterator());
//		return buildGraph(lemmas, IterationUtils.newIterable(uniqueEntityIterator));
//	}

	/* (non-Javadoc)
	 * @see lase.entrec.NamedEntityRecognizer#analyse(java.lang.String)
	 * @see LaseEntityRecognizer
	 */
	@Override
	public NamedEntityGraph analyse(String text) throws NamedEntityRecognitionException {
		
		List<MorphWord> lemmas;
		
		List<Item> preEntities;

		try {
			//TODO při analýze celého textu, kvůli jiné tokenizaci, funguje špatně DataMatcher
			Container cont = NameTagWebService.getNamedEntities(text);
			
			lemmas = morphAnalyser.analyse(text);
			
			//lemmas = morphAnalyser.analyse(text);
			preEntities = cont.items;
			
			linkEntities(preEntities);
		
		} catch (MorphologyException e) {
			throw new NamedEntityRecognitionException(e.getMessage(), e);		
		} catch (IOException e) {
			throw new NamedEntityRecognitionException(e.getMessage(), e);
		} catch (JSONException e) {
			throw new NamedEntityRecognitionException(e.getMessage(), e);
		} catch (ParserConfigurationException e) {
			throw new NamedEntityRecognitionException(e.getMessage(), e);
		} catch (SAXException e) {
			throw new NamedEntityRecognitionException(e.getMessage(), e);
		}
		
		PositionedEntity pe;
		List<PositionedEntity> entities = new ArrayList<PositionedEntity>();
		
		// For getEntityWidth
		String temp = "";
		for (MorphWord mw : lemmas) {		
			temp += mw.getWord();
		}
		
		for (int i = 0; i < preEntities.size(); i++) {
						
//			int position = preEntities.get(i).getPosition();
//			int length = preEntities.get(i).getLength();
//			String word = preEntities.get(i).getWord();
			
			int[] inf = getLemmasWidth(lemmas, preEntities.get(i).getWord(), temp);
			int position = inf[0];
			int length = inf[1];
			String word = getWord(lemmas, position, length);

			Object value = getLemmas(lemmas, position, length);
			String type = preEntities.get(i).getType();
			
			if(type.equals("T")) {
				//System.err.println(preEntities.get(i).getWord() + " CREATING");
				LaseCalendar calendar = dm.createCalendar((TimeItem)preEntities.get(i),value);
				//System.err.println(calendar.toString());
//				if(calendar.isSet(0)){
				value = calendar;
				pe = new PositionedEntity(position, length, word, value, Subtype.DATE);
				entities.add(pe);
//				}
			}
			else if (type.equals("ty")) {
				//System.err.println(preEntities.get(i).getWord() + " CREATING");
				LaseCalendar calendar = dm.createCalendar(preEntities.get(i).getWord());
				//System.err.println(calendar.toString());
				value = calendar;
				pe = new PositionedEntity(position, length, word, value, Subtype.DATE);
				entities.add(pe);
			}			
			else {
				pe = new PositionedEntity(position, length, word, value, NamedEntityToken.getSubtype(type));
				entities.add(pe);
			}
			
		}
		
		entities = reduce(entities, lemmas.size());
		
//		for (PositionedEntity ent : entities) {
//			System.err.println(ent);
//		}
//		
//		for (MorphWord lemma : lemmas) {
//			System.err.println(lemma.getLemma());
//		}
		
		PositionedEntity.sort(entities);
		UniqueIterator<PositionedEntity> uniqueEntityIterator = new UniqueIterator<PositionedEntity>(entities.iterator());
		NamedEntityGraph neg = buildGraph(lemmas, IterationUtils.newIterable(uniqueEntityIterator));
		return neg;
	}

	/*Linkuje entity aby bylo možné využít potencial toho NER při stávajících šablonách
	 * funce udělána pouze pro entitu Time kde následuje rok a pro entity kde posobě následuje pf pm ps (first name second name surname)
	 */
	private void linkEntities(List<Item> preEntities) {
		Iterator<Item> it = preEntities.iterator();

		if(!it.hasNext()){
			return;
		}
		
		Item entity1 = it.next();
		
		while (it.hasNext()) {
			String type1 = entity1.getType();

			if (!it.hasNext()) {
				break;
			} else {
				Item entity2 = it.next();
				String type2 = entity2.getType();
				if (type1.equals("pf") && type2.equals("pm")) {
					entity1.setType("p");
					entity1.setLength(entity1.getLength() + entity2.getLength());
					entity1.setWord(entity1.getWord() + " " + entity2.getWord());
					it.remove();
					if (!it.hasNext()) {
						break;
					} else {
						entity2 = it.next();
						type2 = entity2.getType();
						if (type2.equals("ps")) {
							entity1.setLength(entity1.getLength() + entity2.getLength());
							entity1.setWord(entity1.getWord() + " " + entity2.getWord());
							it.remove();
						}
					}
				} else if(type1.equals("T")&& type2.equals("ty")){
					TimeItem timeEntity = (TimeItem) entity1;
					if(timeEntity.getYear()==null){
						timeEntity.setYear(entity2.getWord());
						timeEntity.setLength(timeEntity.getLength() + entity2.getLength());
						timeEntity.setWord(timeEntity.getWord() + " " + entity2.getWord());
						it.remove();
					}
				}
				else{
					entity1=entity2;
				}
			}
		}
	}


	/**
	 * Detec subsets and make list of positioned entities which are not overlaying
	 * @param entities list of positioned entities
	 * @param size count of lemmas
	 * @return list of positioned entities
	 */
	private List<PositionedEntity> reduce(List<PositionedEntity> entities, int size) {
		
		// In case there is only 1 entity, there cannot be overlaying
		if (entities.size() == 1) {
			return entities;
		}
		
		PositionedEntity[] temp = new PositionedEntity[entities.size()];
		temp = entities.toArray(temp);
		
		int pos;
		
		int[] map = new int[size];
		
		// Inicialization of map func
		for (int i = 0; i < size; i++) {
			map[i] = -1;
		}
		
		// Reduction
		for (int i = 0; i < temp.length; i++) {
			
			pos = temp[i].getPosition();
			
			// Rewrite subset with superset
			if (map[pos] == -1) {
				for (int j = 0; j < temp[i].getLength(); j++) {
						map[pos + j] = i;
				}
			}
			else {
				if (temp[map[pos]].getLength() < temp[i].getLength()) {
					for (int j = 0; j < temp[i].getLength(); j++) {
						map[pos + j] = i;
					}
				}
			}
		}
		
		List<PositionedEntity> result = new ArrayList<PositionedEntity>();
		
		int actual = -2;
		int count = 0;
		// Create list of entitties
		for (int i = 0; i < size; i++) {
			if (map[i] != actual) {
				actual = map[i];
				
				if (actual > -1) {
					result.add(temp[actual]);
					count++;
				}
			}
		}
		
		return result;
	}
	

	/**
	 * Sestaví graf pojmenovaných entit na základě morfologickým analyzátorem
	 * vráceného seznamu morfologických slov, ze kterých sestaví základní cestu,
	 * a seznamu nalezených pojmenovaných entit, který je vzestupně setříděn
	 * podle pozice, na které se v textu vyskytují (na které začínají).
	 * 
	 * @param lemmas
	 *            Seznam lemmatu.
	 * @param sortedEntities
	 *            Seznam nalezených pojmenovaných entity.
	 * 
	 * @return Vrací vytvořený graf pojmenovaých entit.
	 * 
	 * @see NamedEntityGraph
	 */
	private NamedEntityGraph buildGraph(List<MorphWord> lemmas, Iterable<PositionedEntity> sortedEntities) {
		NamedEntityGraph graph = new NamedEntityGraph();
		int len = lemmas.size();
		List<Node> basePath = new ArrayList<Node>(len + 1);
		Node node = graph.getRootNode();
		basePath.add(node);
	
		// vytvoření základní cesty
		for (MorphWord mw : lemmas) {
			QueryToken wordQueryToken = new QueryToken(new WordToken(mw.getWord()), mw.getWord(), mw.getLemma());
			node = graph.appendNode(node, wordQueryToken);
			basePath.add(node);
		}

		/*
		 * Vytvoření seznamů přípojných bodů pro každý uzel. Řeší problém více
		 * sousedících pojmenovaných entit, které nemusí oddělovat žádný uzel
		 * základní cesty. Potom se při vytváření grafu přidá do příslušného
		 * seznamu uzel, ze kterého se později vytvoří hrana. Viz. následující
		 * cyklus o kousek níž: 
		 * 
		 * for (Node fwdJoinNode: fwdJoinList.get(pos))
		 *     graph.addEdge(fwdJoinNode, newNode); }
		 */ 
		List<List<Node>> fwdJoinList = new ArrayList<List<Node>>(len + 1);
		for (int i = 0; i < len + 1; i++)
			fwdJoinList.add(new ArrayList<Node>());
		
		
		// připojení entit na základní cestu
		for (PositionedEntity pe : sortedEntities) {
			int pos = pe.getPosition();
			node = basePath.get(pos);
			Token entityToken = new NamedEntityToken(pe.getSubtype());
			String origWords = StringUtils.workaroundExtraSpaces(pe.getWords());
			//String origWords = getOriginalWords(lemmas, pe);
			QueryToken entityQueryToken = new QueryToken(entityToken, 
					origWords, pe.getValue());
			Node newNode = graph.appendNode(node, entityQueryToken);
			int jointIndex = pos + pe.getLength() + 1;
			if (jointIndex <= len) {
				node = basePath.get(jointIndex);
				graph.addEdge(newNode, node);
				fwdJoinList.get(jointIndex - 1).add(newNode);
			}
			for (Node fwdJoinNode: fwdJoinList.get(pos)) {
				graph.addEdge(fwdJoinNode, newNode);
			}
		}
		return graph;
	}

	
	/**
	 * Zavře používaný morfologický analyzátor.
	 * @see lase.entrec.NamedEntityRecognizer#close()
	 */
	@Override
	public void close() throws IOException {
		morphAnalyser.close();
	}

}

