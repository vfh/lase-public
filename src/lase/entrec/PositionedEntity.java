package lase.entrec;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import lase.template.NamedEntityToken.Subtype;

/**
 * <p>
 * Pojmenovaná entita s pozicí. Skládá se z původní formulace slova nebo
 * několika slov v textu, sémantické hodnoty (řetězec s lemmaty, kalendář),
 * určení podtypu pojmenované entity ({@link NamedEntityToken.Subtype}), pozice
 * tokenu, na kterém ve vstupním textu nalezená entita začíná a délky nalezené
 * entity (tzn. počtu tokenů, přes které se rozprostírá).
 * </p>
 * 
 * <p>
 * Seznam těchto entit je výsledkem činnosti rozpoznávače implmentujícího
 * rozhraní {@link EntityMatcher}. Z vráceného seznamu je potom sestaven graf
 * pojmenovaných entit {@link NamedEntityGraph}.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class PositionedEntity {

	/**
	 * Index tokenu, na kterém ve vstupním textu nalezená entita začíná
	 */
	private int position;
	
	/**
	 * Délka nalezené entity (tzn. počtu tokenů, přes které se rozprostírá).
	 */
	private int length;
	
	/**
	 * Původní formulace slova nebo několika slov v textu, ze kterého/kterých se
	 * nalezená entita skládá.
	 */
	private String words;
	
	/**
	 * Sémantická hodnota nalezené entity (řetězec s lemmatem/lemmaty, kalendář)
	 */
	private Object value;
	
	/**
	 * Určení podtypu pojmenované entity.
	 */
	private Subtype subtype;

	/**
	 * Vytvoří novou pojmenovanou entitu s pozicí.
	 * 
	 * @param position
	 *            Index tokenu, na kterém ve vstupním textu nalezená entita
	 *            začíná
	 * @param length
	 *            Délka nalezené entity (tzn. počtu tokenů, přes které se
	 *            rozprostírá).
	 * @param words
	 *            Původní formulace slova nebo několika slov v textu, ze
	 *            kterého/kterých se nalezená entita skládá.
	 * @param value
	 *            Sémantická hodnota nalezené entity (řetězec s
	 *            lemmatem/lemmaty, kalendář)
	 * @param subtype
	 *            Určení podtypu pojmenované entity.
	 */
	public PositionedEntity(int position, int length, String words, Object value, Subtype subtype) {
		assert position >= 0;
		this.position = position;
		this.length = length;
		this.words = words;
		this.value = value;
		this.subtype = subtype;
	}

	/**
	 * Vrací sémantickou hodnotu nalezené entity (řetězec s lemmatem/lemmaty,
	 * kalendář)
	 * 
	 * @return Vrací sémantickou hodnotu nalezené entity.
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Vrací index tokenu, na kterém ve vstupním textu nalezená entita začíná
	 * 
	 * @return Vrací pozici, na které se pojmenované entita nachází.
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * Vrací délka nalezené entity (tzn. počtu tokenů, přes které se
	 * rozprostírá).
	 * 
	 * @return Vrací délku pojmenované entity.
	 */
	public int getLength() {
		return length;
	}

	/**
	 * Vrací původní formulace slova nebo několika slov v textu, ze
	 * kterého/kterých se nalezená entita skládá.
	 * 
	 * @return Vrací původní formulaci pojmenované entity.
	 */
	public String getWords() {
		return words;
	}

	/**
	 * Vrací podtyp nalezené pojmenované entity.
	 * 
	 * @return Vrací podtyp pojmenované entity.
	 */
	public Subtype getSubtype() {
		return subtype;
	}

	/**
	 * Vrací řetězcovou reprezentaci nalezené pojmenované entity pro ladicí
	 * účely.
	 */
	public String toString() {
		return String.format("PositionedEntity[%d;%d;%s;'%s';%s]", 
				position, length, subtype, words, value);
	}

	/**
	 * Seřadí seznam pojmenovaných entit vzestupně podle indexu prvního tokenu,
	 * ze kterých se skládá.
	 * 
	 * @param entities
	 *            Seznam pojmenovaných entit.
	 * 
	 * @see PositionedEntity#getPosition()
	 */
	public static void sort(List<PositionedEntity> entities) {
		Collections.sort(entities, PositionComparator.instance);
	}

	/**
	 * Komparátor, který využívá metoda {@link PositionedEntity#sort(List)} k
	 * seřazení seznamu entit vzestupně podle pozice, na které začínají.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	private static class PositionComparator implements Comparator<PositionedEntity>, Serializable {
		private static final long serialVersionUID = 1L;
		
		/**
		 * Instance komparátoru. (Není nutné stále vytvářet nové).
		 */
		static final PositionComparator instance = new PositionComparator();

		/**
		 * Porovná dvě entity s pozicí podle jeji počátečního indexu.
		 * 
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(PositionedEntity o1, PositionedEntity o2) {
			if (o1.position > o2.position)
				return 1;
			else if (o1.position < o2.position)
				return -1;
			if (o1.length > o2.length)
				return 1;
			else if (o1.length < o2.length)
				return -1;
			return 0;
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + length;
		result = prime * result + position;
		result = prime * result + ((subtype == null) ? 0 : subtype.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + ((words == null) ? 0 : words.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PositionedEntity other = (PositionedEntity) obj;
		if (length != other.length)
			return false;
		if (position != other.position)
			return false;
		if (subtype == null) {
			if (other.subtype != null)
				return false;
		} else if (!subtype.equals(other.subtype))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (words == null) {
			if (other.words != null)
				return false;
		} else if (!words.equals(other.words))
			return false;
		return true;
	}
}
