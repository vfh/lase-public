package lase.entrec;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Queue;
import java.util.Set;

import lase.template.NamedEntityToken;
import lase.template.Query;
import lase.template.QueryToken;
import lase.template.Token;
import lase.template.WordToken;
import lase.template.Token.Type;
import lase.util.ArrayIterator;
import lase.util.IterationUtils;

/**
 * <p>
 * Graf pojmenovaných entit. Tento graf je výsledkem práce rozpoznávače
 * pojmenovaných entity ({@link NamedEntityRecognizer}). Jeho uzly obsahují
 * tokenty typu {@code WORD} a {@code NAMED_ENTITY}, tedy objekty
 * {@link WordToken} a {@link NamedEntityToken} rozšířené o původní formulaci
 * daného slova (nebo více slov odpovídajících jednomu tokenu) v textu, tj.
 * objekty typu {@link QueryToken} obsahující zmíněné tokeny.
 * </p>
 * 
 * <p>
 * Každý graf obsahuje tzv. základní cestu, tj. cestu tvořenou výhradně tokeny
 * typu {@code WORD}. Pokud není v textu nalezená žádná pojmenovaná entita, je
 * graf tvořen pouze touto cestou. Každá nalezená pojmenovaná entita je potom k
 * této cestě přidána tak, že v grafu vznikne další alternativní cesta shodná se
 * základní cestou právě s výjimkou uzlů odpovídající slovům v textu, která byla
 * rozpoznána jako tato nalezená pojmenovaná entita. Takovýmto postupem je
 * vytvořen celý graf.
 * </p>
 * 
 * <p>
 * Počáteční uzel grafu neobsahuje žádná data.
 * </p> 
 * 
 * <p>
 * Při prohledávání je možné graf obarvovat. K práci s barevnými značkami slouží
 * metody:
 * <ul>
 * <li>{@link #markNode(Node)}</li>
 * <li>{@link #isMarked(Node)}</li>
 * <li>{@link #clearMarks()}</li>
 * </ul>
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class NamedEntityGraph {
	/**
	 * Aktuální značkovací barva. Slouží k obarvení uzlu při prohledávání grafu.
	 * Při každém dalším prohledávání je inkrementována, čímž dojde k
	 * "odbarvení" grafu a je možné jej obarvovat znovu.
	 */
	private int markColor = 1;
	
	/**
	 * Počet datových uzlů grafu. (Prázdný kořenový uzel se nepočítá.)
	 */
	private int nodeCount;
	
	/**
	 * Kořenový uzel grafu. Tento uzel je přítomen vždy a nenese v sobě žádný
	 * {@link QueryToken}.
	 */
	private Node root = new Node(null);

	/**
	 * Vytvoří prázdný graf pojmenovaných entit obsahující pouze kořenový (nedatový) uzel.
	 */
	public NamedEntityGraph() {
	}

	/**
	 * Vrací kořenový uzel. Tento uzel je přítomen vždy a neobsahuje žádná data.
	 * 
	 * @return Vrací kořenový uzel grafu.
	 */
	public Node getRootNode() {
		return root;
	}
	
	/**
	 * Nastaví kořenový uzel grafu. Tento uzel musí existovat i v prázdném grafu a nesmí obsahovat data.
	 */
	public void setRootNode(Node newRoot) {
		if (newRoot == null || newRoot.content != null)
			throw new IllegalArgumentException();
		root = newRoot;
	}

	/**
	 * Přidá do grafu nový uzel a přiřadí mu jeho data v podobně objektu typu
	 * {@link QueryToken}.
	 * 
	 * @param ancestor
	 *            Předchůdce přidávaného uzlu, za který je tento připojen.
	 * @param content
	 *            {@link QueryToken}, který je přidanému uzlu přiřazen.
	 *            
	 * @return Vrací přidaný uzel.
	 */
	public Node appendNode(Node ancestor, QueryToken content) {
		nodeCount++;
		Node newNode = new Node(content);
		ancestor.addSuccessor(newNode);
		newNode.addPredecessor(ancestor);
		return newNode;
	};

	/**
	 * Vytvoří v grafu novou hranu z uzlu {@code from} do uzlu {@code to}.
	 * 
	 * @param from Počáteční uzel přidávané hrany.
	 * @param to Koncový uzen přidávané hrany.
	 */
	public void addEdge(Node from, Node to) {
		from.addSuccessor(to);
		to.addPredecessor(from);
	}

	/**
	 * Vrací počet datových uzlů grafu. (Prázdný kořenový uzel se nepočítá.)
	 * 
	 * @return Vrací počet datových uzlů grafu.
	 */
	public int getNodeCount() {
		return nodeCount;
	}

	/**
	 * <p>
	 * Označí (obarví) daný uzel grafu. Pro zachování konzistentního stavu je
	 * nutné, aby uzel náležel grafu, nad kterým je tato metoda volána, jinak
	 * výsledek není definová.
	 * </p>
	 * 
	 * <p>
	 * Obarvování grafu lze využít při prohledávání různými algoritmy.
	 * </p>
	 * 
	 * <p>
	 * Zda je uzel označen lze zjistit metodou {@link #isMarked(Node)},
	 * odbarvení celého grafu provede metoda {@link #clearMarks()}.
	 * </p>
	 * 
	 * @param node
	 *            Uzel, který bude označen.
	 */
	public void markNode(Node node) {
		node.color = markColor;
	}

	/**
	 * Zjistí, zda byl uzel označen metodou {@link #markNode(Node)}.
	 * 
	 * @param node
	 *            Zkoumaný uzel.
	 *            
	 * @return Vrací {@code true}, je-li daný uzel označen. Jinak brací {@code
	 *         false}.
	 */
	public boolean isMarked(Node node) {
		return node.color == markColor; 
	}

	/**
	 * <p>
	 * Odbarví všechny obarvené uzly grafu.
	 * </p>
	 * 
	 * <p>
	 * <strong>Poznámka:</strong> Ve skutečnosti nedochází k procházení grafu a
	 * změně atributu všech uzlů ale pouze k nastavení nové značkovací barvy.
	 * Není proto třeba se obávat vysoké ceny této operace.
	 * </p>
	 */
	public void clearMarks() {
		markColor++;
	}

	/**
	 * <p>
	 * Vytvoří seznam všech cest z počátečního uzlu do všech koncových uzlů
	 * grafu.
	 * </p>
	 * 
	 * <p>
	 * <strong>Varování:</strong> Tato operace může být z hlediska výpočetní
	 * složitosti velmi drahá v případě, že v grafu existuje velké množtví cest
	 * z počátečního do koncových uzlů. Takový graf vznikne například při
	 * rozpoznání textu "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0" rozpoznávačem
	 * pojmenovaných entit.
	 * </p>
	 * 
	 * 
	 * @return Vrací cesty z počátečního do všech koncových uzlů grafu.
	 */
	public List<Query> getAllPaths() {
		List<Query> paths = new ArrayList<Query>();
		Deque<PathBuilder> queue = new LinkedList<PathBuilder>();
		queue.offer(new PathBuilder(root));
		while(!queue.isEmpty()) {
			PathBuilder pb = queue.poll();
			Node node = pb.node;
			for (Node succ : node.next) {
				PathBuilder newPb = new PathBuilder(succ);
				newPb.path.addAll(pb.path);
				queue.offer(newPb);
			}
			if (node.next.length == 0) {
				// poslední uzel
				Query newPath = createPath(pb.path);
				paths.add(newPath);
			}
		}
		return paths;
	}

	/**
	 * <p>
	 * Zapíše schéma grafu do souboru ve formátu enginu Dot programu GraphViz. Z
	 * tohoto souboru lze snadno vygenerovat vizualizaci grafu v různých
	 * obrazových formátech.
	 * </p>
	 * 
	 * <p>
	 * Více informací na <a href="http://www.graphviz.org/">http://www.graphviz.org/</a>
	 * </p>
	 * 
	 * @param filename
	 *            Cesta k souboru, který bude vytvořen. Případně potřebné
	 *            adresáře budou automaticky vytvořeny.
	 *            
	 * @throws IOException
	 *             Vyhozena v případě, že při zápisu do souboru došlo k chybě.
	 */
	public void outputGraphViz(String filename) throws IOException {
		File file = new File(filename);
		String parentPath = file.getParent();
		if (parentPath != null) {
			File parentDirecotory = new File(parentPath);
			parentDirecotory.mkdirs();
		}
		outputGraphViz(new FileOutputStream(file));
	}

	/**
	 * <p>
	 * Zapíše schéma grafu výstupního proudu ve formátu enginu Dot programu
	 * GraphViz. Z tohoto souboru lze snadno vygenerovat vizualizaci grafu v
	 * různých obrazových formátech.
	 * </p>
	 * 
	 * <p>
	 * Více informací na <a
	 * href="http://www.graphviz.org/">http://www.graphviz.org/</a>
	 * </p>
	 * 
	 * @param os
	 *            - Proud, do kterého budou data zapsána.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při zápisu došlo k chybě.
	 */
	public void outputGraphViz(OutputStream os) throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		writer.write("digraph g {");
		writer.newLine();
		try {
			Queue<Node> queue = new ArrayDeque<Node>();
			Set<Node> markedNodes = new HashSet<Node>();
			queue.offer(root);
			writer.write("n" + root.hashCode() + " [label=\"\"]");
			writer.newLine();
			while(!queue.isEmpty()) {
				Node node = queue.poll();
				for (Node succ : node.getSuccessors()) {
					writer.write("n" + node.hashCode() + " -> n" + succ.hashCode());
					writer.newLine();
					if (!markedNodes.contains(succ)) {
						markedNodes.add(succ);
						Token token = succ.getContent().getToken();
						String bracedType = "";
						if (token.getType() != Type.WORD) {
							String type = token.getSubtype() == null ? token.getType().toString() : token.getSubtype().toString();
							bracedType = " {" + type + "}";
						}
						writer.write("n" + succ.hashCode() + " [label=\"" + succ.toString() +
								bracedType + "\"]");
						writer.newLine();
						queue.offer(succ);
					}
				}
			}
		writer.write("}");
		writer.newLine();
		} finally {
			writer.close();
		}
	}
	
	/**
	 * <p>
	 * Vytvoří "dotaz" obsahující data uzlů ze seznamu {@code nodes}.
	 * </p>
	 * 
	 * <p>
	 * Název dotaz je určitých případech zavádějící. Viz {@link Query}.
	 * </p>
	 * 
	 * @param nodes Uzly, jejichž data ({@link QueryToken}) budou k vytvoření dotazu použita.
	 * @return
	 */
	private Query createPath(List<Node> nodes) {
		int size = nodes.size();
		ListIterator<Node> it = nodes.listIterator(size);
		List<QueryToken> queryTokens = new ArrayList<QueryToken>(size);
		while(it.hasPrevious()) {
			QueryToken qt = it.previous().content;
			if (qt != null)
				queryTokens.add(qt);
		}
		return new Query(queryTokens);
	}

	/**
	 * Pomocná třída použití při sestavování seznamu všech cest v grafu.
	 * Obsahuje aktuální uzel (aktuální z hlediska stavu procházení grafu) a
	 * seznam uzlů tvořící celou historii jeho předchůdců.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	private static class PathBuilder {

		/**
		 * Vytvořní novou instanci a nastaví aktuální uzel.
		 * 
		 * @param node
		 *            Aktuální uzel z hlediska stavu procházení grafu.
		 */
		PathBuilder(Node node) {
			this.node = node;
			this.path.add(node);
		}
		
		/**
		 * Aktuální uzel z hlediska stavu procházení grafu.
		 */
		Node node;
		
		/**
		 * Historii uzlů tvořící předchůdce uzlu {@link #node}.
		 */
		LinkedList<Node> path = new LinkedList<Node>();
	}
	
	/**
	 * Uzel grafu pojmenovaných entit.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	public static class Node {
		/**
		 * Pole následovníků
		 */
		private Node[] next = new Node[0];
		
		/**
		 * Pole předchůdců
		 */
		private Node[] prev = new Node[0];
		
		/**
		 * Rozšířený token, který je datovým obsahem tohoto uzlu. 
		 */
		private QueryToken content;
		
		/**
		 * Barva, kterou je uzel označen.
		 */
		private int color;

		/**
		 * Vytvoří nový uzel a vloží do něj rozšířený token.
		 * 
		 * @param content
		 *            Rozšířený token, který je datovým obsahem tohoto uzlu.
		 */
		public Node(QueryToken content) {
			this.content = content;
		}

		/**
		 * Vrací rozšířený token, který je datovým obsahem tohoto uzlu.
		 * 
		 * @return Vrací datový obsah uzlu.
		 */
		public QueryToken getContent() {
			return content;
		}

		/**
		 * <p>
		 * Nastaví barvu uzlu.
		 * </p>
		 * 
		 * <p>
		 * <strong>Doporučení:</strong> Tato metoda není označena jako Deprecated, nicméně je
		 * doporučeno k označování uzlů využívat metody třídy
		 * {@link NamedEntityGraph}. Jsou to následující tři metody:
		 * <ul>
		 * <li>{@link #markNode(Node)}</li>
		 * <li>{@link #isMarked(Node)}</li>
		 * <li>{@link #clearMarks()}</li>
		 * </ul>
		 * </p>
		 * 
		 * @param color
		 *            Nová barva uzlu.
		 */
		public void setColor(int color) {
			this.color = color;
		}

		/**
		 * <p>
		 * Vrací barvu, kterou byl uzel ozanačen.
		 * </p>
		 * 
		 * <p>
		 * <strong>Doporučení:</strong> Tato metoda není označena jako
		 * Deprecated, nicméně je doporučeno k označování uzlů využívat metody
		 * třídy {@link NamedEntityGraph}. Jsou to následující tři metody:
		 * <ul>
		 * <li>{@link #markNode(Node)}</li>
		 * <li>{@link #isMarked(Node)}</li>
		 * <li>{@link #clearMarks()}</li>
		 * </ul>
		 * </p>
		 * 
		 * @return Vrací barvu uzlu.
		 */
		public int getColor() {
			return color;
		}

		/**
		 * Vrací i-tého následovníka tohoto uzlu.
		 * 
		 * @param index
		 *            Index následovníka v poli následovníků.
		 * 
		 * @return Vrací následovníka tohoto uzlu.
		 * 
		 * @throws ArrayIndexOutOfBoundsException
		 *             Vyhozena v případě, že následovník s takovým indexem
		 *             neexistuje.
		 */
		public Node getSuccessor(int index) {
			return next[index];
		}

		/**
		 * Vrací počet následovníků tohoto uzlu.
		 * 
		 * @return Vrací počet následovníků tohoto uzlu.
		 */
		public int getSuccessorCount() {
			return next.length;
		}

		/**
		 * Vrací read-only iterátor následovníků tohoto uzlu.
		 * 
		 * @return Vrací iterátor následovníků.
		 */
		public Iterator<Node> successorIterator() {
			return new ArrayIterator<Node>(next);
		}
		
		/**
		 * Vrací instanci rozhraní {@link Iterable} umožnůjící iteraci přes
		 * následovníky tohoto uzlu.
		 * 
		 * @return Vrací následovníky tohoto uzlu.
		 */
		public Iterable<Node> getSuccessors() {
			return IterationUtils.newIterable(successorIterator());
		}
		
		public Iterable<Node> getPredecessors() {
			return IterationUtils.newIterable(new ArrayIterator<Node>(prev));
		}

		/**
		 * Přidá následovníka do seznamu následovníků tohoto uzlu.
		 * 
		 * @param n
		 *            Následovník, který má být přidán.
		 */
		private void addSuccessor(Node n) {
			int len = next.length;
			next = Arrays.copyOf(next, len+1);
			next[len] = n;
		}
		
		private void addPredecessor(Node n) {
			int len = prev.length;
			prev = Arrays.copyOf(prev, len+1);
			prev[len] = n;
		}

		/**
		 * Vrací původní slovo (nebo několik slov), ze kterého (ze kterých) byl
		 * vytvořen token, který je datovým obsahem tohoto uzlu.
		 */
		public String toString() {
			if (content == null)
				return "[root]";
			return content.getOriginalWord();
		}
	}
}

// TODO - javadoc
