package lase.entrec;

import java.io.IOException;

/**
 * Rozpoznávač pojmenovaných entity využívaný systémem LASE. Slouží k vyhledávání
 * pojmenovaných entity ve vstupním textu a následnému vytvoření grafu pojmenovaných 
 * entit ({@link NamedEntityGraph}).
 * 
 * @author Rudolf Šíma
 *
 */
public interface NamedEntityRecognizer {

	/**
	 * Vyhledá v textu pojmenované entity a sestaví z nich graf pojmenovaných
	 * entit.
	 * 
	 * @param text
	 *            Vstupní text, ve kterém se vyhledají pojmenované entity.
	 * @return Vrací graf pojmenovaných entity (viz {@link NamedEntityGraph} .
	 * 
	 * @throws NamedEntityRecognitionException
	 *             Vyhozena v případě, že při hledání pojmenovaných entit došlo
	 *             k chybě, kterou způsobila výjimka vyhozená některým z
	 *             nástrojů pro předzpracování vstupního textu.
	 */
	public NamedEntityGraph analyse(String text) throws NamedEntityRecognitionException;

	/**
	 * Uvolní zdroje využívané rozpoznávačem pojmenovaných entit, což jsou
	 * zejména nástroje sloužící k předzpracování vstupního textu.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při uzavření rozpoznávače
	 *             pojmenovaných entit nastala chyba.
	 */
	public void close() throws IOException;

}
