package lase.entrec;

import lase.util.LaseException;


/**
 * Výjimka vyhozená v případě, že při rozpoznávání pojmenovaných entit v textu
 * nastala chyba.
 * 
 * @author Rudolf Šíma
 * 
 */
public class NamedEntityRecognitionException extends LaseException {
	private static final long serialVersionUID = 1L;

	/**
	 * Vytvoří novou výjimku bez podrobné zprávy a nastavení příčiny.
	 */
	public NamedEntityRecognitionException() {
	}

	/**
	 * Vytvoří novou výjimku, nastaví podrobnou zprávu a příčinu.
	 */
	public NamedEntityRecognitionException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Vytvoří novou výjimku a nastaví podrobnou zprávu.
	 */
	public NamedEntityRecognitionException(String message) {
		super(message);
	}

	/**
	 * Vytvoří novou výjimku a nastaví příčinu.
	 */
	public NamedEntityRecognitionException(Throwable cause) {
		super(cause);
	}
}
