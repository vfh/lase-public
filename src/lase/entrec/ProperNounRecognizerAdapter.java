package lase.entrec;

import java.util.ArrayList;
import java.util.List;

import lase.morphology.MorphWord;
import lase.morphology.ProperNounRecognizer;
import lase.morphology.ProperNounRecognizer.ProperNoun;
import lase.template.NamedEntityToken.Subtype;

/**
 * Adaptér rozpoznávače vlastních jmen aby vyhovoval rozhraní
 * {@link EntityMatcher}.
 * 
 * @author Rudolf Šíma
 * 
 * @deprecated Nahrazeno novým rozpoznávačem {@link PersonMatcher}.
 * 
 */
@Deprecated
public class ProperNounRecognizerAdapter implements EntityMatcher {
	private ProperNounRecognizer properNounRecognizer;
	
	public ProperNounRecognizerAdapter() {
		properNounRecognizer = new ProperNounRecognizer();
	}
	
	@Override
	public List<PositionedEntity> search(List<MorphWord> tokens) {
		List<ProperNoun> properNouns = properNounRecognizer.analyse(tokens);
		List<PositionedEntity> entities = new ArrayList<PositionedEntity>(properNouns.size());
		for (ProperNoun pn : properNouns) {
			String lemmas = ((String)pn.getLemmas()).toLowerCase();
			PositionedEntity newPe = new PositionedEntity(pn.getPosition(), pn.size(), 
					pn.getWords(), lemmas, Subtype.PERSON);
			entities.add(newPe);
		}
		return entities;
	}
}
