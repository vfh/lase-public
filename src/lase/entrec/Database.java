package lase.entrec;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.RandomAccess;

import lase.entrec.Trie.SubstringFlag;
import lase.morphology.MorphWord;
import lase.template.NamedEntityToken.Subtype;
import lase.util.StringUtils;

/**
 * <p>
 * Databáze (lemmat) pojmenovaných entit. Slouží k rozpoznávání pojmenovaných
 * entit v textu.
 * </p>
 * 
 * <p>
 * Je implementována strukturou trie, jejíž data se načítají z textového souboru
 * s řádky následujícího formátu:
 * 
 * <pre>
 * &lt;podtyp pojmenované entity&gt; "&lt;lemma 1&gt;" "&lt;lemma 2&gt;" ... "&lt;lemma n&gt;"
 * </pre>
 * 
 * tedy například:
 * 
 * <pre>
 * PERSON "napoleon bonaparte" "napoleon"
 * PERSON "tomáš garigue masaryk" "tomáš masaryk"
 * GEOGRAPHY "československý republika" "čsr"
 * GEOGRAPHY "spojený stát americký" "usa"
 * GEOGRAPHY "šumava"
 * </pre>
 * 
 * </p>
 * 
 * @author Rudolf Šíma
 * @see NamedEntityRecognizer
 * 
 */
public class Database implements EntityMatcher {
	/**
	 * Struktura trie, do které se ukládají data této databáze řetězců.
	 */
	private Trie<Entry> trie;

	/**
	 * Vytvoří novou databázi a naplní ji daty ze souboru {@code filename}.
	 * 
	 * @param filename
	 *            Soubor, ze kterého se načtou vytvářené databáze. Řádky tohoto
	 *            souboru mají následující formát:<br>
	 *            <code>&lt;podtyp pojmenované entity&gt; "&lt;lemma 1&gt;" 
	 *            "&lt;lemma 2&gt;" ... "&lt;lemma n&gt;"</code><br>
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při načítání souboru nastane chyba.
	 *             
	 * @see Database
	 */
	public Database(String filename) throws IOException {
		trie = loadTrieFromFile(filename);
	}

	/**
	 * V seznamu tokenů {@code} nalezne všechny pojmenované entity, které se
	 * nachází v databázi a vrátí jejich seznam.
	 * 
	 * @see PositionedEntity
	 */
	public List<PositionedEntity> search(List<MorphWord> tokens) {
		assert tokens instanceof RandomAccess;
		List<PositionedEntity> results = new ArrayList<PositionedEntity>();
		int len = tokens.size();
		for (int i = 0; i < len; i++) {
			StringBuilder searchLemma = new StringBuilder();
			int k = i;
			while (k < len) {
				if (searchLemma.length() > 0) {
					searchLemma.append(' ');
				}
				MorphWord mw = tokens.get(k++);
				searchLemma.append(mw.getLemma());
				SubstringFlag flag = Trie.createFlag();
				Entry entry = trie.get(searchLemma.toString().toLowerCase(), flag);
				if (entry != null) {
					int length = k - i;
					String originalWords = getOriginalWords(tokens, i, length);
					PositionedEntity newResult = new PositionedEntity(i, length,
							originalWords, entry.semanticValue, entry.subtype);
					results.add(newResult);
				}
				if (!flag.flag())
					break;
			}
		}
		return results;
	}
	
	private String getOriginalWords(List<MorphWord> morphWords, int from, int count) {
		StringBuilder origWordsBuilder = new StringBuilder();
		for (int i = from; i < from + count; i++) {
			origWordsBuilder.append(morphWords.get(i).getWord());
			origWordsBuilder.append(' ');
		}
		// odstranění poslední mezery
		int len = origWordsBuilder.length();
		if (len > 0) {
			origWordsBuilder.setLength(len - 1);
		}
		return StringUtils.workaroundExtraSpaces(origWordsBuilder.toString());
	}

	/**
	 * Záznam, který se uloží jako tag do uzlu trie. Viz {@link Trie}.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	private static class Entry {
		/**
		 * Podtyp pojmenované entity.
		 */
		Subtype subtype;

		/**
		 * Sémantická hodnota pojmenané entity. Tj. lemma první ze seznamu
		 * možných formulací jejího názvu, které jsou uvedeny ve zdrojovém
		 * souboru databáze.
		 * 
		 * @see Database
		 */
		String semanticValue;

		/**
		 * Vytvoří nový záznam sloužící jako tag v uzlu trie.
		 * 
		 * @param subtype
		 *            Podtyp pojmenované entity.
		 * @param semanticValue
		 *            Sémantická hodnota pojmenané entity. Tj. lemma první ze
		 *            seznamu možných formulací jejího názvu, které jsou uvedeny
		 *            ve zdrojovém souboru databáze.
		 */
		Entry(Subtype subtype, String semanticValue) {
			this.subtype = subtype;
			this.semanticValue = semanticValue;
		}
	}

	/**
	 * Načte databázi ze souboru. Popis formátu je uveden v dokumentaci třídy
	 * {@link Database}.
	 * 
	 * @param filename
	 *            Soubor, ze kterého se databáze načte.
	 * @return Vrací strukturu trie s daty načtenými ze souboru.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při načítání souboru došlo k chybě.
	 */
	private static Trie<Entry> loadTrieFromFile(String filename)
			throws IOException {
		HashMap<String, String> semValTable = new HashMap<String, String>();
		Trie<Entry> trie = new Trie<Entry>();
		FileInputStream fr = new FileInputStream(filename);
		BufferedReader br = new BufferedReader(new InputStreamReader(fr,"UTF-8"));
		try {
			String line;
			while ((line = br.readLine()) != null) {
				ParsedLine parsedLine = new ParsedLine(line);
				if (parsedLine.subtype != null) {
					for (String val : parsedLine.lemmas) {
						String semVal = parsedLine.lemmas.get(0);
						String tableVal = semValTable.get(semVal);
						if (tableVal == null) {
							semValTable.put(semVal, semVal);
							tableVal = semVal;
						}
						Entry entry = new Entry(parsedLine.subtype, tableVal);
						trie.put(val, entry);
					}
				}
			}
		} finally {
			br.close();
		}
		return trie;
	}

	/**
	 * Zpracovaný řádek vstupního souboru databáze.
	 * Parser je implementován konstruktorem této třídy.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	private static class ParsedLine {
		
		/**
		 * Seznam lemmat možných formulací na řádku uvedené pojmenované entity
		 * @see Database
		 */
		List<String> lemmas = new ArrayList<String>();
		
		/**
		 * Podtyp na řádku uvedené pojmenované entity.
		 */
		Subtype subtype;

		/**
		 * Zpracuje řádek vstupního souboru a nastaví atributy {@link #lemmas} a
		 * {@link #subtype}.
		 * 
		 * @param line
		 *            Řádek vstupního souboru.
		 */
		ParsedLine(String line) {
			int index = line.indexOf(' ');
			if (index < 0)
				index = line.indexOf('\t');
			if (index < 0)
				return; // subtype == null;
			try {
				subtype = Subtype.valueOf(line.substring(0, index));
			} catch (IllegalArgumentException e) {
				return; // subtype == null;
			}
			int prevIndex = 0;
			while ((index = line.indexOf("\"", index + 1)) > 0) {
				// String str = ;
				// List<String> tokens = tokenizer.tokenize(str);
				// str = StringUtils.concat(tokens, " ");
				if (prevIndex > 0) {
					lemmas.add(line.substring(prevIndex + 1, index));
					prevIndex = 0;
				} else {
					prevIndex = index;
				}
			}
		}
	}
}
