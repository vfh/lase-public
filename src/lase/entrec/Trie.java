package lase.entrec;

/**
 * <p>
 * Generická implementace datové struktury Trie (bez komprese), jejíž uzly kromě
 * znaku mohou obsahovat jeden objekt typu {@code T}, tzv. tag.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 * @param <T>
 *            Typ tagů uložených v uzlech trie.
 */
public class Trie<T> {
	private Node root = new Node('\0');

	/**
	 * Vytvoří novou prázdnou trie obsahující pouze kořenový uzel.
	 */
	public Trie() {
	}

	/**
	 * Přidá řetězec, tzn. jeden nebo více uzlů, do stromu trie. Poslednímu
	 * přidanému uzlu přiřadí tag daný parametrem {@code tag}.
	 * 
	 * @param str
	 *            Řetězec, který byde přidán.
	 * @param tag
	 *            Tag, který bude přidán do posledního uzlu. V případě, že byl
	 *            přidávaný řetězec v trie již obsažen, tj. všechny uzly již
	 *            existovaly, tag v posledním, tj. nejhlouběji umístěném, uzlu
	 *            bude nahrazen tímto tagem.
	 */
	public void put(CharSequence str, T tag) {
		int len = str.length();
		Node prev = root;
		for (int i = 0; i < len; i++) {
			char c = str.charAt(i);
			Node cur = findNext(c, prev);
			if (cur == null) {
				;
				Node newNode = new Node(c);
				prev.addNext(newNode);
				cur = newNode;
			}
			prev = cur;
		}
		prev.tag = tag;
	}

	/**
	 * Vyhledá ve stromu řetězec {@code str}, a, je-li řetězec nalezen, vrací
	 * tag podledního uzlu řetězce.
	 * 
	 * @param str
	 *            Hledaný řetězec.
	 * @param containsLongerString
	 *            Metoda {@code get} nastaví flag {@code containsLongerString} v
	 *            případě, že řetězec {@code str} byl nalezen, ale jeho poslední
	 *            uzel není v trie listem, tzn. trie obsahuje delší řetězec
	 *            začínající řetězcem {@code str}.
	 * 
	 * @return Vrací tag posledního uzlu řetězce {@code str}, je-li nalezen.
	 *         Vrací {@code null} v případě, že řetězec nebyl nalezen nebo měl
	 *         tag jeho posledního uzlu hodnotu {@code null}.
	 * 
	 * @see SubstringFlag
	 * @see Trie#createFlag()
	 */
	@SuppressWarnings("unchecked")
	public T get(CharSequence str, SubstringFlag containsLongerString) {
		Node cur = root;
		int len = str.length();
		for (int i = 0; i < len; i++) {
			char c = str.charAt(i);
			cur = findNext(c, cur);
			if (cur == null)
				return null;
		}
		if (cur.next.length > 0)
			containsLongerString.flag = true;
		return (T) cur.tag;
	}

	/**
	 * Nalezne přímého následovníka uzlu {@code cur} obsahující znak {@code c}.
	 * 
	 * @param c
	 *            Hledaný znak
	 * @param cur
	 *            Uzel mezi jehož následovníky se hledá.
	 * 
	 * @return Vrací nalezeného následovníka nebo {@code null}, nebyl-li
	 *         následovník nalezen.
	 */
	private Node findNext(char c, Node cur) {
		for (Node f : cur.next) {
			if (f.c == c)
				return f;
		}
		return null;
	}

	/**
	 * Vytvoří nový objekt {@code SubstringFlag} určený pro použití v metodě
	 * {@link #get(CharSequence, SubstringFlag)}.
	 * 
	 * @return Vrací vytvořený objekt {@code SubstringFlag}.
	 */
	public static SubstringFlag createFlag() {
		return new SubstringFlag();
	}

	/**
	 * Flag sloužící metodě {@link Trie#get(CharSequence, SubstringFlag)} k
	 * tomu, aby dala najevo, že nalezený řetězec je počátečním podřetězcem
	 * delšího řetězce, který se v {@link Trie} nachází.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	public static class SubstringFlag {
		private boolean flag;

		/**
		 * Privátní konstruktor. Vytvoří novou instanci {@link SubstringFlag}.
		 */
		private SubstringFlag() {
		}

		/**
		 * Vrací logickou hodnotu, kterou tento objekt reprezentuje. Hodnota
		 * {@code true} značí, že že nalezený řetězec je počátečním podřetězcem
		 * delšího řetězce, který se v {@link Trie} nachází.
		 * 
		 * @return Vrací logickou hodnotu, kterou tento objekt reprezentuje.
		 */
		public boolean flag() {
			return flag;
		}

		/**
		 * Vrací totéž co {@link Boolean#toString()}.
		 */
		public String toString() {
			return Boolean.toString(flag);
		}
	}

	/**
	 * Uzel struktury {@link Trie}.
	 * 
	 * @author Rudolf Šíma
	 * 
	 * @see Trie
	 * 
	 */
	private static class Node {

		/**
		 * Znak v tomto uzlu.
		 */
		char c;

		/**
		 * Pole uzlů následovníků
		 */
		Node[] next;

		/**
		 * Tag náležící tomuto uzlu.
		 * 
		 * @see Trie
		 */
		Object tag;

		/**
		 * Vytvoří nový uzel a přiřadí mu znak
		 * 
		 * @param c
		 *            Znak ve vytvořeném uzlu.
		 */
		Node(char c) {
			this.c = c;
			this.next = new Node[0];
		}

		/**
		 * Přidá uzel {@code newNode} mezi následovníky tohoto uzlu.
		 * 
		 * @param newNode
		 *            Přidávaný uzel následovník.
		 */
		void addNext(Node newNode) {
			int len = next.length;
			next = java.util.Arrays.copyOf(next, len + 1);
			next[len] = newNode;
		}
	}
}
