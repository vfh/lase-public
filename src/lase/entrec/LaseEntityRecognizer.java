package lase.entrec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lase.entrec.NamedEntityGraph.Node;
import lase.morphology.MorphWord;
import lase.morphology.MorphologicalAnalyser;
import lase.morphology.MorphologyException;
import lase.morphology.Tokenizer;
import lase.template.NamedEntityToken;
import lase.template.Query;
import lase.template.QueryToken;
import lase.template.Token;
import lase.template.WordToken;
import lase.util.IterationUtils;
import lase.util.StringUtils;
import lase.util.UniqueIterator;

/**
 * Rozpoznávač pojmenovaných entit systému LASE. V daném textu vyhledává
 * pojmenované entity čtyřmi různými algoritmy. Nejprve v textu nalezne čísla,
 * poté časové údaje (datum, čas, datum a čas). Déle se heuristikou založenou na
 * velkých a malých písmenech a římských řadových číslovách pokusí rozpoznat
 * jména osob. Nakonec vyhledá pojmenované entity porovnáním s databází
 * pojmenovaných entity.
 * 
 * @author Rudolf Šíma
 * 
 * 
 * @see NamedEntityToken
 * @see NamedEntityToken.Subtype
 * @see EntityMatcher
 * @see NumberMatcher
 * @see DateMatcher
 * @see PersonMatcher
 * @see Database
 * 
 */
public class LaseEntityRecognizer implements NamedEntityRecognizer {
	/**
	 * Morfologický analyzátor použitý k předzpracování textu.
	 */
	private MorphologicalAnalyser morphAnalyser;

	/**
	 * Tokenizátor použitý ke zpracování textu před analýzou morfologickým
	 * analyzátorem.
	 */
	private Tokenizer tokenizer;

	/**
	 * Rozpoznávač založený na porovnávání s databáze pojmenovaných entit.
	 */
	private EntityMatcher database;
	
	/**
	 * Rozpoznávač čísel.
	 */
	private EntityMatcher numberMatcher;
	
	
	/**
	 * Rozpoznávač časových údajů.
	 */
	private EntityMatcher dateMatcher;
	
	/**
	 * Rozpoznávač jmeno osob založený na velkých a malých písmenech a římských
	 * číslovách
	 */
	private EntityMatcher personMatcher;

	/**
	 * Vytvoří nový rozpoznávač pojmenovaných entity a nastaví nástroje, které
	 * tento rozpoznávač použije k předzpracování textu a vyhledávání.
	 * 
	 * @param tokenizer
	 *            Tokenizátor použitý ke zpracování textu před analýzou
	 *            morfologickým analyzátorem.
	 * @param morphologicalAnalyser
	 *            Morfologický analyzátor použitý k předzpracování textu.
	 * @param properNounDatabase
	 *            Databáze vlastních jmeno – rozpoznávač založený na porovnávání
	 *            s databáze pojmenovaných entit.
	 */
	public LaseEntityRecognizer(Tokenizer tokenizer, MorphologicalAnalyser morphologicalAnalyser, Database properNounDatabase)  {
		this.morphAnalyser = morphologicalAnalyser;
		this.database = properNounDatabase;
		this.personMatcher = new PersonMatcher();
		this.numberMatcher = new NumberMatcher();
		this.dateMatcher = new DateMatcher();
		this.tokenizer = tokenizer;
	}
	
	/* (non-Javadoc)
	 * @see lase.entrec.NamedEntityRecognizer#analyse(java.lang.String)
	 * @see LaseEntityRecognizer
	 */
	@Override
	public NamedEntityGraph analyse(String text) throws NamedEntityRecognitionException {
	
		List<String> tokens = tokenizer.tokenize(text);
		List<MorphWord> lemmas;
		try {
			//TODO při analýze celého textu, kvůli jiné tokenizaci, funguje špatně DataMatcher
			lemmas = morphAnalyser.analyse(tokens);
			//lemmas = morphAnalyser.analyse(text);
		} catch (MorphologyException e) {
			throw new NamedEntityRecognitionException(e.getMessage(), e);
		}
		List<PositionedEntity> entities = new ArrayList<PositionedEntity>();
		List<PositionedEntity> res;
		res = numberMatcher.search(lemmas);
		entities.addAll(res);
		res = dateMatcher.search(lemmas);
		entities.addAll(res);
		res = personMatcher.search(lemmas);
		entities.addAll(res);
		res = database.search(lemmas);		
		entities.addAll(res);
		PositionedEntity.sort(entities);
		UniqueIterator<PositionedEntity> uniqueEntityIterator = new UniqueIterator<PositionedEntity>(entities.iterator());
		return buildGraph(lemmas, IterationUtils.newIterable(uniqueEntityIterator));
	}

	/**
	 * Sestaví graf pojmenovaných entit na základě morfologickým analyzátorem
	 * vráceného seznamu morfologických slov, ze kterých sestaví základní cestu,
	 * a seznamu nalezených pojmenovaných entit, který je vzestupně setříděn
	 * podle pozice, na které se v textu vyskytují (na které začínají).
	 * 
	 * @param lemmas
	 *            Seznam lemmatu.
	 * @param sortedEntities
	 *            Seznam nalezených pojmenovaných entity.
	 * 
	 * @return Vrací vytvořený graf pojmenovaých entit.
	 * 
	 * @see NamedEntityGraph
	 */
	private NamedEntityGraph buildGraph(List<MorphWord> lemmas, Iterable<PositionedEntity> sortedEntities) {
		NamedEntityGraph graph = new NamedEntityGraph();
		int len = lemmas.size();
		List<Node> basePath = new ArrayList<Node>(len + 1);
		Node node = graph.getRootNode();
		basePath.add(node);
	
		// vytvoření základní cesty
		for (MorphWord mw : lemmas) {
			QueryToken wordQueryToken = new QueryToken(new WordToken(mw.getWord()), mw.getWord(), mw.getLemma());
			node = graph.appendNode(node, wordQueryToken);
			basePath.add(node);
		}

		/*
		 * Vytvoření seznamů přípojných bodů pro každý uzel. Řeší problém více
		 * sousedících pojmenovaných entit, které nemusí oddělovat žádný uzel
		 * základní cesty. Potom se při vytváření grafu přidá do příslušného
		 * seznamu uzel, ze kterého se později vytvoří hrana. Viz. následující
		 * cyklus o kousek níž: 
		 * 
		 * for (Node fwdJoinNode: fwdJoinList.get(pos))
		 *     graph.addEdge(fwdJoinNode, newNode); }
		 */ 
		List<List<Node>> fwdJoinList = new ArrayList<List<Node>>(len + 1);
		for (int i = 0; i < len + 1; i++)
			fwdJoinList.add(new ArrayList<Node>());
		
		
		// připojení entit na základní cestu
		for (PositionedEntity pe : sortedEntities) {
			int pos = pe.getPosition();
			node = basePath.get(pos);
			Token entityToken = new NamedEntityToken(pe.getSubtype());
			String origWords = StringUtils.workaroundExtraSpaces(pe.getWords());
			//String origWords = getOriginalWords(lemmas, pe);
			QueryToken entityQueryToken = new QueryToken(entityToken, 
					origWords, pe.getValue());
			Node newNode = graph.appendNode(node, entityQueryToken);
			int jointIndex = pos + pe.getLength() + 1;
			if (jointIndex <= len) {
				node = basePath.get(jointIndex);
				graph.addEdge(newNode, node);
				fwdJoinList.get(jointIndex - 1).add(newNode);
			}
			for (Node fwdJoinNode: fwdJoinList.get(pos)) {
				graph.addEdge(fwdJoinNode, newNode);
			}
		}
		return graph;
	}

	/**
	 * Sestaví řetězec z původních slov vyskytujících se v textu, který odpovídá
	 * dané nalezené pojmenované entitě.
	 * 
	 * @param morphWords
	 *            Seznam morfologickým analyzátorem vrácených morfologických
	 *            slov.
	 * @param pe
	 *            Nalezená pojmenovaná entita, jejíž původní formulace v textu
	 *            se hledá.
	 * 
	 * @return Vrací řetězec s původní formulací dané pojmenované entity v
	 *         textu.
	 * 
	 * @deprecated Tato metoda není potřeba, protože slova v původní formulaci
	 *             lze nyní získat metodou {@link PositionedEntity#getWords()}.
	 *             Snad to již funguje u všech implementací {@link EntityMatcher}
	 *             správně, ale pro jistotu tuto metodu ještě zachovávám.
	 */
	@Deprecated
	@SuppressWarnings("unused")
	private String getOriginalWords(List<MorphWord> morphWords, PositionedEntity pe) {
		int pos = pe.getPosition();
		int len = pe.getLength();
		StringBuilder origWordsBuilder = new StringBuilder();
		for (int i = 0; i < len; i++) {
			origWordsBuilder.append(morphWords.get(i + pos).getWord());
			if (i + 1 < len)
				origWordsBuilder.append(' ');
		}
		return StringUtils.workaroundExtraSpaces(origWordsBuilder.toString());
	}

	/**
	 * Zavře používaný morfologický analyzátor.
	 * @see lase.entrec.NamedEntityRecognizer#close()
	 */
	@Override
	public void close() throws IOException {
		morphAnalyser.close();
	}

	/**
	 * Vrací seznam všech cest v grafu pojmenovaných entity, který na základě
	 * vstupního textu vytvoří metoda {@link #analyse(String)}.
	 * 
	 * @param text
	 *            Vstupní text.
	 * 
	 * @return Vrací seznam všech cest ve vytvořeném grafu.
	 * 
	 * @throws NamedEntityRecognitionException
	 *             Vyhozena v případě, že při hledání pojmenovaných entit došlo
	 *             k chybě, kterou způsobila výjimka vyhozená některým z
	 *             nástrojů pro předzpracování vstupního textu.
	 * 
	 * @deprecated Pro některé vstupní texty (např. řetězec
	 *             "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0") může být operace
	 *             vytvoření všech cest z hlediska výpočetní složitosti neúnosně
	 *             drahá. Pokud je přesto nutné generovat všechny cesty,
	 *             preferovaným způsobem je volání metody
	 *             {@link NamedEntityGraph#getAllPaths} nad grafem, který vrátí
	 *             metoda {@link #analyse(String)}
	 */
	@Deprecated
	public List<Query> recognize(String text) throws NamedEntityRecognitionException {
		NamedEntityGraph graph = analyse(text);
		List<Query> paths = graph.getAllPaths();
		return paths;
	}

}
