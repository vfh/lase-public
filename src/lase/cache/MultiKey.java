package lase.cache;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Vícenáosbný klíč. Umožňuje použít více objektů jako klíč v
 * <code>{@link java.util.Map}</code>.
 * 
 * @author Rudolf Šíma
 * 
 * @deprecated Použít {@link lase.cache2.Cache} link lase.cache2.MultiKey}
 * 
 */
@Deprecated
public final class MultiKey implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private final Object[] values;
    private final int hashCode;
    
    public MultiKey(Object... values)
    {
    	final int prime = 31;
        this.values = values;
        hashCode = prime * Arrays.hashCode(this.values);
    }

    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final MultiKey other = (MultiKey) obj;
        return Arrays.equals(values, other.values);
    }
    
    public int hashCode()
    {
        return hashCode;
    }
}