package lase.cache;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * <p>
 * Metoda využívající při svém volání lokální persistetní cache. Při opakovaném
 * volání se stejnými parametry vrací hodnotu vypočtenou při prvním volání.
 * Cache se ukládá na disk do souboru
 * <code>./cache/&lt;jméno_třídy&gt;#&lt;méno_metody&gt;.cache</code>.
 * </p>
 * 
 * <p>
 * <strong>Příklad použití:</strong>
 * <pre>
 * class SomeClass() {
 *     public String quadrupleToString(int number) {
 *         if ((number) < 0)
 *             throw new IllegalArgumentException("Number must not be negative.");
 *         return Integer.toString(4 * number);
 *     }
 * }
 * ... 
 * SomeClass someObject = new SomeClass();
 * CachedMethod&lt;String&gt; method = new CachedMethod&lt;String&gt;(
 *                                   someObject, "quadrupleToString", 
 *                                   Integer.TYPE);
 * try {
 *     String result = method.invoke(10);
 *     System.out.println(result);
 * catch (InvocationTargetException e) {
 *     e.getCause().printStackTrace();
 * }
 * </pre>
 * </p>
 * 
 * 
 * @author Rudolf Šíma
 * 
 * @param <T>
 *            Návratový typ metody. Musí být {@link Serializable}.
 *            
 * @deprecated Používat {@link lase.cache2.Cache} 
 */
@Deprecated
public class CachedMethod<T> {
	private Object associatedObject;
	private Method method;
	private Cache cache;
	private String filename;
	private FileNotFoundException fileNotFoundException;
	private IOException ioException;
	private ClassNotFoundException classNotFoundException;
	
	private static boolean cacheEnabled = true;
	private static boolean persistent = true;
	
	public static void setMasterSwitch(boolean cacheEnabled) {
		CachedMethod.cacheEnabled = cacheEnabled; 
	}
	
	public static void setPersistent(boolean persistent) {
		CachedMethod.persistent = persistent;
	}

	public CachedMethod(Object associatedObject, String methodName, Class<?>... paramTypes) {
		try {
			Method method = associatedObject.getClass().getMethod(methodName, paramTypes);
			init(associatedObject, method);
		} catch (NoSuchMethodException e) {
			throw new NoSuchMethodRuntimeException(e.getMessage(), e);
		}
	}
	
	public CachedMethod(Object associatedObject, Method method) {
		init(associatedObject, method);
	}
	
	private void init(Object associatedObject, Method method) {
		this.associatedObject = associatedObject;
		this.method = method;
		filename = getFileName(associatedObject, method);
		cache = loadCache(filename);
		if (cache == null)
			cache = new Cache();
	}
	
	@SuppressWarnings("unchecked")
	public T invoke(Object... args) throws InvocationTargetException {
		MultiKey key = null;
		Object result = null;
		if (cacheEnabled) {
			key = new MultiKey(args);
			result = cache.get(key);
		}
		if (result == null) {
			try {
				result = (T) method.invoke(associatedObject, args);
			} catch (IllegalAccessException e) {
				throw new IllegalAccessRuntimeException(e.getMessage(), e);
			} 
			if (cacheEnabled)
				cache.put(key, result);
		}		
		return (T) result;
	}
	
	
	public FileNotFoundException getFileNotFoundException() {
		return fileNotFoundException;
	}

	public IOException getIOException() {
		return ioException;
	}

	public ClassNotFoundException getClassNotFoundException() {
		return classNotFoundException;
	}
	
	public boolean storeCache() {
		if (!cacheEnabled || !persistent)
			return true;
		if (!cache.modified())
			return true;
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(new FileOutputStream(filename));
			oos.writeObject(cache);
		} catch(IOException e) {
			this.ioException = e;
			return false;
		} finally {
			if (oos != null) {
				try {
					oos.close();
				} catch (IOException e) {
					this.ioException = e;
					return false;
				}
			}
		}
		return true;
	}

	private Cache loadCache(String filename) {
		if (!persistent)
			return null;
		ObjectInputStream ios = null;
		try {
			ios = new ObjectInputStream(new FileInputStream(filename));
			cache = (Cache) ios.readObject();
			return cache;
		} catch (FileNotFoundException e) {
			fileNotFoundException = e;
		} catch (IOException e) {
			ioException = e;
		} catch (ClassNotFoundException e) {
			classNotFoundException = e;
		} finally {
			if (ios != null) {
				try {
					ios.close();
				} catch (IOException e) {
					ioException = e;
				}
			}
		}
		return null;
	}
	
	private static String getFileName(Object associatedObject, Method method) {
		return String.format("cache/%s#%s.cache", associatedObject.getClass().getName(), method.getName());
	}
}
