package lase.cache;

/**
 * Runtime verze výjimky <code>{@link IllegalAccessException}</code>.
 * 
 * @author Rudolf Šíma
 *
 * @deprecated Použít {@link lase.cache2.Cache} 
 */
@Deprecated
public class IllegalAccessRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public IllegalAccessRuntimeException() {
	}

	public IllegalAccessRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public IllegalAccessRuntimeException(String message) {
		super(message);
	}

	public IllegalAccessRuntimeException(Throwable cause) {
		super(cause);
	}
}