package lase.cache;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Univerzální cache systému LASE.
 * 
 * @author Rudolf Šíma
 *
 * @deprecated Používat {@link lase.cache2.Cache} 
 */
@Deprecated
public class Cache implements Serializable {
	private static final long serialVersionUID = 1L;
	private boolean modified = true;
	
	private Map<MultiKey, Object> storage = new HashMap<MultiKey, Object>(0);
	
	public Cache() {
	}
	
	public void put(MultiKey key, Object value) {
		modified = true;
		storage.put(key, value);
	}
	
	public Object get(MultiKey key) {
		return storage.get(key);
	}

	public boolean modified() {
		return modified;
	}
}
