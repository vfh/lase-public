package lase.cache;

/**
 * Výjimka značící, že záznam v cache nebyl nalezen.
 * 
 * @author Rudolf Šíma
 * 
 * @deprecated Používat {@link lase.cache2.Cache}
 */
@Deprecated
public class CacheEntryNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public CacheEntryNotFoundException() {
	}

	public CacheEntryNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public CacheEntryNotFoundException(String message) {
		super(message);
	}

	public CacheEntryNotFoundException(Throwable cause) {
		super(cause);
	}
}
