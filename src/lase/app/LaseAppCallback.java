package lase.app;

import java.util.List;

import lase.answersearch.Answer;
import lase.answersearch.AnswerGroup;
import lase.template.SearchPattern;
import lase.util.Corpus;

/**
 * <p>
 * Třídu implementující toto rozhraní je možné zaregistrovat jako příjemce
 * informací o průběhu vyhledávacího procesu spuštěného voláním metody
 * {@link LaseApp#findBestAnswer(String)}.
 * </p>
 * 
 * <p>
 * Nastavení objektu pro callback {@link LaseApp} se provádí v konstruktoru
 * {@link LaseApp#LaseApp(LaseAppCallback)}.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public interface LaseAppCallback {

	/**
	 * Tato metoda je volána při zahájení vyhledávání stránek s odpověďmi
	 * webovým vyhledávačem.
	 * 
	 * @param searchStrings
	 *            - Fráze, které jsou použity k hledání na webu. Např.: Je-li
	 *            očekávána odpověď "Pes má &lt;n&gt; nohou", budou to řetězce
	 *            "Pes má" a "nohou".
	 */
	/* TODO - přidat URL */
	public void webSearchStarted(String[] searchStrings);
	
	/**
	 * Metoda je volána, když je pro určitý vyhledávací dotaz vytvořen korpus
	 * @param corpus korpus vytvořený z výsledků klasického vyhledávače
	 */
	public void corpusRetrieved(Corpus corpus, SearchPattern pattern);

	/**
	 * Tato metoda je volána při každého kandidáta na nejlepší odpověď.
	 * Kandidátem se rozumí odpověď získaná ze staženého textu, který odpovídá
	 * vyhledávacímu vzoru ({@link SearchPattern}).
	 * 
	 * @param candidate
	 *            - kandidát na nejlepší odpověď
	 */
	public void answerCandidateFound(Answer candidate);

	/**
	 * Tato metoda je volána po zařazení odpovědí do skupin sémanticky
	 * ekvivalentních odpověďí seřazení
	 * <ol>
	 * <li>skupin podle velikosti,</li>
	 * </li>odpovědí v rámci skupin podle přesnosti údajů,</li>
	 * </ol>
	 * což se provádí jako poslední činnost ve vyhledávacím procesu. Tato metoda
	 * tedy slouží k předání všech možných odpovědí, nejen té, která je
	 * vyhodnocena jako nejlepší a vrácena metodou
	 * {@link LaseApp#findBestAnswer(String)}.
	 * 
	 * @see LaseApp#sortAnswers
	 * 
	 * @param groups
	 *            - seznam skupin odpověďí
	 */
	public void answersSorted(List<AnswerGroup> groups);
}
