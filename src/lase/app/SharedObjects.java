package lase.app;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import cz.zcu.fav.liks.apps.GateAppWrapper;
import gate.Gate;
import gate.creole.ResourceInstantiationException;
import gate.util.GateException;
import lase.entrec.Database;
import lase.entrec.EntityRecognizerNameTag;
import lase.entrec.LaseEntityRecognizer;
import lase.entrec.NamedEntityRecognizer;
import lase.morphology.LaseMorphologicalAnalyser;
import lase.morphology.MorphologicalAnalyser;
import lase.morphology.SimpleTokenizer;
import lase.morphology.Tokenizer;
import lase.util.LaseException;
import lase.util.PathUtils;
import wordnet.WordNet;

/**
 * <p>
 * Sdílené objekty systému LASE. Jedná se o sadu nástrojů, jejichž instance jsou
 * vytvořeny při inicializaci systému LASE a jsou používány opakovaně v mnoha
 * různých třídách.
 * </p>
 * 
 * <p>
 * Konstruktor {@link #SharedObjects(String)} je zodpovědný za vytvoření
 * instancí nástrojů implementujících standardní rozhraní tokenizátoru,
 * morfologického analyzátoru a vyhledávače pojmenovaných entit.
 * </p>
 * 
 * <p>
 * Z důvodu snadného nastavení standardních nástrojů systému LASE na jednom
 * místě není vhodné pro sdílené objekty používat návrhový vzor Singleton.
 * </p>
 * 
 * <p>
 * Před použitím kteréhokoli z nástrojů, jejichž reference instance jsou vraceny
 * statickými gettery této třídy je třeba {@link SharedObjects} inicializovat
 * voláním konstruktoru, a to nejlépe voláním metody
 * {@link LaseApp#initializeDefaults()}.
 * <p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class SharedObjects {

	/**
	 * Instance třídy {@code SharedObjects}. {@code null} v případě, že ještě
	 * nebyla inicializována ovláním konstruktoru.
	 */
	private static volatile SharedObjects instance; 
	
	
	/**
	 * Morfologický analyzátor používaný v celém systému LASE.
	 */
	private MorphologicalAnalyser morphologicalAnalyser;
	
	/**
	 * Vyhledávač pojmenovaný entit systému LASE.
	 */
	private NamedEntityRecognizer namedEntityRecognizer;
	
	/**
	 * wrapper pro přístup k morfologické knihovně, která má na starost lemmatizaci, POS tagging a NER
	 */
	private GateAppWrapper morphologyWrapper;
	
	private static final String WRAPPER_CONFIGURATION_PATH = "data/zswi-app.xml";
	
	/**
	 * Tokenizátor sloužící k předzpracování textu určeného k morfologické
	 * analýze.
	 */
	private Tokenizer tokenizer;
	
	/**
	 * Český WordNet sloužící k získání seznamu synonym a hypernym.
	 */
	private WordNet czechWordNet;

	/**
	 * Inicializuje systém LASE. Konstruktor je nutné zavolat právě jednou.
	 * Tento způsob incicializace je použit z důvodu možnosti ošetření
	 * případných výjimek, které mohou při inicializaci nastat.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že při inicializaci některého z
	 *             používaných nástrojů byla vyhozena výjimka. Tato původní
	 *             výjimka je nastavena jako příčina výjimky
	 *             {@link LaseException} a může bý získána voláním metody
	 *             {@link LaseException#getCause().
	 */
	public SharedObjects() throws LaseException {
		if (instance != null)
			throw new IllegalStateException("SharedObjects may only be initialized once.");
		try {
			
			// Vytvoření morfologického analyzátoru.
			//morphologicalAnalyser = new LiksWebServiceMorphologicalAnalyser(Settings.liksMorphologyWebServiceUrl);
			//morphologicalAnalyser = new FakeMorphologicalAnalyser();
			morphologicalAnalyser = new LaseMorphologicalAnalyser();

			// Vytvoření tokenizátoru
			// TODO nahradit za FastTokenizer a otestovat
			tokenizer = new SimpleTokenizer();
			
			// Načtení databáze vlastních jmen pro vyhledávač pojmenovaných entit.
			String absoluteDbPath = PathUtils.convertRelativePath(Settings.properNounDatabaseFilename);
			Database properNounDatabase = new Database(absoluteDbPath);
			//asdf
			// Vytvoření vyhledávače pojmenovaných entit.
			
			//namedEntityRecognizer = new LaseEntityRecognizer(tokenizer, morphologicalAnalyser, properNounDatabase);
			namedEntityRecognizer = new EntityRecognizerNameTag(tokenizer, morphologicalAnalyser);
			
			// Vytvoření českého WordNetu.
			String absoluteCzechWordNetPath = PathUtils.convertRelativePath(Settings.czechWordNetFilename);
			InputStream fis = new FileInputStream(absoluteCzechWordNetPath);
			try {
				czechWordNet = new WordNet(fis);
			} finally {
				fis.close();
			}
		
		} catch (Exception e) {
			throw new LaseException(e.getMessage(), e);
		}
		instance = this;
	}

	/**
	 * Vrací tokenizátor společný pro celý systém LASE sloužící mimo jiné k
	 * předzpracování textu pro morfologický analyzátor.
	 * 
	 * @return Vrací tokenizátor.
	 */
	public static Tokenizer getTokenizer() {
		checkInitialized();
		return instance.tokenizer;
	}
	
	/**
	 * Vrací morfologický analyzátor společný pro celý systém LASE.
	 * 
	 * @return Vrací morfologický analyzátor.
	 */
	public static MorphologicalAnalyser getMorphologicalAnalyser() {
		checkInitialized();
		return instance.morphologicalAnalyser;
	}
	
	/**
	 * Vrací rozpoznávač pojmenovaných entit společný  pro celý systém LASE.
	 *  
	 * @return Vrací rozpoznávač pojmenovaných entit.
	 */
	public static NamedEntityRecognizer getNamedEntityRecognizer() {
		checkInitialized();
		return instance.namedEntityRecognizer;
	}

	public static WordNet getCzechWordNet() {
		checkInitialized();
		return instance.czechWordNet;
	}
	
	public static GateAppWrapper getMorphologyWrapper()
	{
		checkInitialized();
		if(instance.morphologyWrapper==null)
		{
			if(!Gate.isInitialised())
			{
				Gate.runInSandbox(true);
				try {
					Gate.init();
				} catch (GateException e) {
					e.printStackTrace();
				}
			}
			try {
				instance.morphologyWrapper = new GateAppWrapper(PathUtils.convertRelativePath(WRAPPER_CONFIGURATION_PATH));
			} catch (ResourceInstantiationException e) {
				e.printStackTrace();
			}
		}
		return instance.morphologyWrapper;
	}

	/**
	 * Uzavře všechny sdílené nástroje a zruší instanci {@link SharedObjects}.
	 * Jakékoli volání statického getteru od této chvíli způsobí vyhození
	 * výjimky {@link IllegalStateException}, pokud není provedena opakovaná
	 * inicializace voláním konstrutoru {@link SharedObjects#SharedObjects()}
	 * resp. {@link LaseApp#initializeDefaults()}.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že některý z uzavíraných sdílených
	 *             nástrojů vyhodil výjimku.
	 */
	public static void close() throws LaseException {
		checkInitialized();
		try {
			instance.morphologicalAnalyser.close();
			instance.namedEntityRecognizer.close();
			instance = null;
		} catch (IOException e) {
			throw new LaseException(e);
		}
	}

	/**
	 * Ověří, byla již byly sdílené objekty inicializovány voláním konstrutoru
	 * {@link SharedObjects#SharedObjects()} resp.
	 * {@link LaseApp#initializeDefaults()}, případně zda nebyly uzavřeny
	 * voláním {@link #close()}.
	 * 
	 * @return Vrací {@code true}, jsou-li sdílené objeky inicializovány. Jinak
	 *         vrací {@code false}.
	 */
	public static boolean initialized() {
		return instance != null;
	}

	/**
	 * Testuje, zda jsou sdílené objekty inicializovány. Pokud nejsou, vyhodí
	 * výjimku {@link IllegalStateException}.
	 */
	private static void checkInitialized() {
		if (instance == null)
			throw new IllegalStateException("SharedObjects not initialized.");
	}
}
