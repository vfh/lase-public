package lase.app;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import lase.answersearch.Answer;
import lase.answersearch.AnswerComparator;
import lase.answersearch.AnswerGroup;
import lase.answersearch.AnswerMatcher;
import lase.answersearch.BasicAnswerComparator;
import lase.answersearch.Countable;
import lase.answersearch.EntityGraphAnswerMatcher;
import lase.answersearch.SizeComparator;
import lase.cache2.Cache;
import lase.cache2.CacheResult;
import lase.entrec.NamedEntityGraph;
import lase.entrec.NamedEntityRecognitionException;
import lase.entrec.NamedEntityRecognizer;
import lase.template.Query;
import lase.template.QuestionTransformer;
import lase.template.SearchPattern;
import lase.template.Template;
import lase.template.TemplateQuestionTransformer;
import lase.util.ArrayWrapper;
import lase.util.Corpus;
import lase.util.LaseException;
import lase.util.PathUtils;
import lase.util.ProgressListener;
import lase.util.StringUtils;
import lase.websearch.BingSnippetWebSearch;
import lase.websearch.FakeWebSearch;
import lase.websearch.JyxoSnippetWebSearch;
import lase.websearch.SeznamSnippetWebSearch;
import lase.websearch.WebSearch;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * <p>
 * Hlavní třída aplikační vrstvy systému LASE. Zajišťuje vyhledání odpovědi na
 * zadanou otázku v přirozeném jazyce provedením následujících kroků.
 * 
 * <ol>
 *   <li>Vyhledání pojmenovaných entit v zadané otázce.</li>
 *   <li>Nalezení odpovídající šablony.</li>
 *   <li>Sestavený vyhledávací řetězců pro internetový vyhledávač s použitím
 *       forem odpovědi definovaných shodující se šablonou.</li>
 *   <li>Stažení snippetů resp. stránek nalezených internetovým vyhledávačem.</li>
 *   <li>Vyhledání odpovědi (odpovídající šablonou definované formě odpovědi) ve
 *       stažených textech.</li>
 *   <li>Určení sémanticky shodných odpovědí
 *       (1948 ≈ únor 1948, USA ≈ Spojené státy americké).</li>
 *   <li>Výběr nejlepší odpovědi, tj. nejkonkrétnější odpovědi ze třídy
 *       sémantické rovnosti, která se ve výsledcích objevuje nejčastěji.</li>
 * </ol>
 * </p>
 * 
 * @see #findBestAnswer(String)
 * 
 * @author Rudolf Šíma
 * 
 */
public class LaseApp {
	
	/** 
	 * Komparátor pro sestupné řazení podle velikosti. 
	 */
	private static Comparator<Countable> sizeComparator = new SizeComparator(
			true);

	/**
	 * Seznam načtených vyhledávacích šablon.
	 */
	private List<Template> templates;

	/**
	 * Modul sloužící k převdou otázky do tvaru odpovědi, která je následně
	 * hledána.
	 */
	private QuestionTransformer transformer;

	/**
	 * Rozpoznávač pojmenovaných entit (číslo, datum, osoba, geog. název, ...) v
	 * textu.
	 */
	private NamedEntityRecognizer namedEntityRecognizer;

	/**
	 *  Rozhraní webového vyhledávače. 
	 */
	private WebSearch webSearch;

	/**
	 * Objekt, který je informován o průběhu vyhledávacího procesu. 
	 */
	private LaseAppCallback callback;

	/**
	 * Flag sloužící k zastavení vyhledávání 
	 */
	private volatile boolean searchInterrupted;
	
	/**
	 * Udává, zda byla instance aplikace LASE uzavřena voláním {@link #close()}.
	 */
	private volatile boolean closed;

	/**
	 * Vytvoří novou instanci vyhledávače odpovědí bez nastavení objektu, který
	 * bude informován o průběhu vyhledávání.
	 * 
	 * @see #LaseApp(LaseAppCallback)
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že při inicLaseAppializaci některého z modulů
	 *             použitého v procesu vyhledávání dojde k vyhození výjimky.
	 */
	public LaseApp() throws LaseException {
		this(null);
	}

	/**
	 * Vytvoří novou instanci vyhledávače odpovědí a nastaví callback.
	 * 
	 * @param callback
	 *            - Objekt, který bude informován o průběhu vyhledávání
	 *            odpovědi.
	 *            
	 * @throws LaseException
	 *             Vyhozena v případě, že při inicializaci některého z modulů
	 *             použitého v procesu vyhledávání dojde k vyhození výjimky.
	 */
	public LaseApp(LaseAppCallback callback) throws LaseException {
		// nastavení callbacku, prázdného v případě null
		if (callback == null)
			this.callback = new NullCallback();
		else
			this.callback = callback;
		
		try {
			// načtení šablon
			String absoluteTemplatePath = PathUtils.convertRelativePath(Settings.templatesdirectory);
			templates = Template.loadDirectory(absoluteTemplatePath);
			// inicializace sdílených objektů
			if (!SharedObjects.initialized())
				new SharedObjects();
		} catch (Exception e) {
			throw new LaseException(e);
		}
		
		// inicializace šablonového převodníku
		transformer = new TemplateQuestionTransformer(templates);
		
		// inicializace čtečky snippetů z Jyxo
		//webSearch = new JyxoSnippetWebSearch();
		//webSearch = new FakeWebSearch();
		webSearch = new BingSnippetWebSearch();
		//webSearch = new SeznamSnippetWebSearch();
		
		// vyhledávač pojmenovaných entity
		namedEntityRecognizer = SharedObjects.getNamedEntityRecognizer();
	}

	/**
	 * Inicializuje sdílené prostředky v systému LASE standardními hodnotami.
	 * Tato metoda je určena inicializaci při testování samostatných částí
	 * systému.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že při inicializaci
	 *             {@link SharedObjects} dojde k chybě.
	 */
	public static void initializeDefaults() throws LaseException {
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.WARN);
		new SharedObjects();
	}
	
	/**
	 * <p>
	 * Spustí vyhledávání nejlepší odpovědi. Za nejlepší je považována odpověď
	 * nesoucí nejpřesnější údaj ze skupiny nejčetněji se vyskytujících
	 * sémanticky ekvivalentních odpovědí.<br/>
	 * Například při nalezení odpovědí: 'květen 1986', '1990', '15. ledna 1990',
	 * '21. května 1986' a '1986', bude jako nejlepší odpověď vrácena odpověď
	 * '21. května 1986'.
	 * </p>
	 * 
	 * <p>
	 * Byl-li konstruktorem {@link LaseApp} nastaven callback, bude v průběhu
	 * vyhledávání docházet k volání příslušných metod.
	 * </p>
	 * 
	 * @param question
	 *            - Otázka v přirozeném jazyce. Např:
	 *              "Kdy ze narodil Karel IV.?"
	 * 
	 * @return Vrací nejlepší nalezenou odpověď.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že některý z modulů použitých při
	 *             vyhledávání odpovědi způsobil vyhození výjimky. Tato výjimka
	 *             je potom obalena výjimkou LaseException.
	 * 
	 * @throws InterruptedException
	 *             Vyhozena v případě, že došlo k přerušení procesu vyhledávání
	 *             voláním metody {@link LaseApp#stop()}.
	 *             
	 * @see LaseApp
	 */
	public Answer findBestAnswer(String question) throws LaseException, InterruptedException {
		return findBestAnswer(question, new NullProgressListener());
	}

	/**
	 * <p>
	 * Spustí vyhledávání nejlepší odpovědi. Za nejlepší je považována odpověď
	 * nesoucí nejpřesnější údaj ze skupiny nejčetněji se vyskytujících
	 * sémanticky ekvivalentních odpovědí.<br/>
	 * Například při nalezení odpovědí: 'květen 1986', '1990', '15. ledna 1990',
	 * '21. května 1986' a '1986', bude jako nejlepší odpověď vrácena odpověď
	 * '21. května 1986'.
	 * </p>
	 * 
	 * <p>
	 * Byl-li konstruktorem {@link LaseApp} nastaven callback, bude v průběhu
	 * vyhledávání docházet k volání příslušných metod.
	 * </p>
	 * 
	 * @param question
	 *            - Otázka v přirozeném jazyce. Např:
	 *            "Kdy ze narodil Karel IV.?"
	 * 
	 * @param progressIndicator
	 *            - Indikátor průběhu vyhledávání sloužící možnosti informovat
	 *            uživatele o zbývajícím čase.
	 * 
	 * @return Vrací nejlepší nalezenou odpověď.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že některý z modulů použitých při
	 *             vyhledávání odpovědi způsobil vyhození výjimky. Tato výjimka
	 *             je potom obalena výjimkou LaseException.
	 * 
	 * @throws InterruptedException
	 *             Vyhozena v případě, že došlo k přerušení procesu vyhledávání
	 *             voláním metody {@link LaseApp#stop()}.
	 * 
	 * @see LaseApp
	 */
	public Answer findBestAnswer(String question, ProgressListener progressListener)throws LaseException, InterruptedException {
		// test na použití dříve uzavřené instance
		if (closed)
			throw new IllegalStateException("The instance of LaseApp was closed by a preceding call to close().");
		// zrušení zastavovací podmínky po předchozím volání stop() 
		reset();
		// vytvoření seznamu pro ukládání nalezených odpovědí
		List<Answer> answers = new ArrayList<Answer>();
		// předzpracování textu otázky – odstranění případného otazníku
		question = StringUtils.stripQuestionMarks(question);
		// vytvoření grafu pojmenovaných entit
		NamedEntityGraph namedEntityGraph;
		try {
			namedEntityGraph = namedEntityRecognizer.analyse(question);
		} catch (NamedEntityRecognitionException e) {
			throw new LaseException(e.getMessage(), e);
		}
		
		// ladicí výpis grafu pojmenovaných entit nalezených v zadané otázce
		// try {
		//     namedEntityGraph.outputGraphViz("graphviz/LastQuery.gv");
		// } catch (IOException e) {
		//     e.printStackTrace();
		// }
		
		// počet všech vyhledávacích vzorů výpočet hotové/zbývající části 
		int searchPatternsCount = 0;
		// počet již zpracovaných vyhledávacích vzorů
		int searchPatternsComplete = 0;
		
		// iterace přes všechny možné kombinace otázky s nalezenými entitami
		for (Query query : namedEntityGraph.getAllPaths()) {
			// test přerušení hledání uživatelem
			testInterrupted();
			// transformace otázky na vyhledávací vzory
			List<SearchPattern> searchPatterns = transformer.transform(query);
			searchPatternsCount += searchPatterns.size();
			// iterace přes všechny vyhledávací vzory
			for (SearchPattern pattern : searchPatterns) {
				// test přerušení hledání uživatelem
				testInterrupted();
				// vytvoření vyledávacího řetězce pro Jyxo
				String[] searchStrings = pattern.getSearchStrings();
				callback.webSearchStarted(searchStrings);		
				// vyhledání řetězců z vyhledávacího vzoru internetovým vyhledávačem
				Corpus corpus = searchWeb(searchStrings);
				callback.corpusRetrieved(corpus, pattern);
				// vytvoření listeneru průběhu prohledávání korpusu
				ProgressListener answerMatchingProgressListener = new AnswerMatchingProgressListener(
						searchPatternsCount, searchPatternsComplete, progressListener);
				// vytvoření matcheru pro hledání úseků obsahujících odpověď v korpusu
				AnswerMatcher answerMatcher = new EntityGraphAnswerMatcher(corpus, pattern, answerMatchingProgressListener);
				while (answerMatcher.hasNextAnswer()) {
					// test přerušení hledání uživatelem
					testInterrupted();
					// nalezení další shody s vyhledávacím vzorem šablony
					Answer answer = answerMatcher.nextAnswer();
					answers.add(answer);
					// oznámení nalezení odpovědi
					callback.answerCandidateFound(answer);
				}
				searchPatternsComplete++;
			}
		}
		// výběr nejpřesnější odpovědi z každé skupiny sémanticky rovných a seřazení
		// od nejčetnější po nejméně četnou.
		sortAnswers(answers);
		// vrácení nejlepší odpovědi
		if (answers.isEmpty())
			return null;
		return answers.get(0);
	}

	/**
	 * <p>
	 * Přeruší probíhající process vyhledávání. Čeká-li metoda
	 * {@link #findBestAnswer(String)} na dokočení operace (např.: morpfologické
	 * analýzy) prováděné některým z použitých modulů, dojde k přerušení až po
	 * skončení této operace.
	 * </p>
	 * 
	 * <p>
	 * Přerušení se projeví vyhozením {@link InterruptedException} metodou
	 * {@link #findBestAnswer(String)}.
	 * </p>
	 * 
	 * <p>
	 * Volání metody <code>stop()<code> ve chvíli, kdy vyhledávání neprobíhá, a
	 * její opakované volání nemá žádný vliv.
	 */
	public void stop() {
		searchInterrupted = true;
	}

	/**
	 * Uzavře instanci aplikace LASE a uvolní použité sdílené zdroje.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že některý z uzavíraných sdílených
	 *             nástrojů (např. morfologický analyzátor) vyhodil výjimku.
	 */
	public void close() throws LaseException {
		if (SharedObjects.initialized())
			SharedObjects.close();
		closed = true;
	}
	
	/**
	 * Pokusí se získat výsledek hledání z cache, případně provede hledání
	 * webovým vyhledávačem.
	 * 
	 * @param searchStrings
	 *            - Pole hledaných řetězců. Každý prvek pole představuje přesnou
	 *            frázi, která se do vyhledávače zadá v uvozovkách.
	 * 
	 * @return Vrací korpus obsahující texty získané na základě hledání na
	 *         internetu.
	 * 
	 * @throws LaseException
	 *             - Vyhozena v případě, že při hledání nastala chyba.
	 */
	private Corpus searchWeb(String[] searchStrings) throws LaseException {
		// Získání příslušné cache z mapy.
		Cache cache = Cache.getCache(WebSearch.class, "search", String[].class);
		// Vyvoření wrapperu pole řetězců. (Pole nelze použít jako argument metody 
		// s proměnným počtem parametrů.)
		ArrayWrapper<String> searchStringsWrapper = new ArrayWrapper<String>(searchStrings);
		// Hledání v cache.
		CacheResult cr = cache.get(searchStringsWrapper);
		Corpus corpus;
		if (cr.cacheHit()) {
			// Výsledky nalezeny v cache.
			corpus = (Corpus)(cr.getValue());
		} else {
			// Výsledky nenalezeny, hledání na webu.
			corpus = webSearch.search(searchStrings, null);
			// Uložení nových výsledků do cache.
			cr.updateCache(corpus);
		}
		return corpus;
	}

	/**
	 * Zruší zastavovací podmínku nastavenou voláním {@link #stop()}.  
	 */
	private void reset() {
		searchInterrupted = false;
	}

	/**
	 * Otestuje, zda byla nastavena zastavovací podmínka voláním {@link #stop()}.
	 * V případě pozitivního zjištění způsobí vyhození výjimky
	 * {@link InterruptedException}.
	 * 
	 * @throws InterruptedException
	 *             Vyhozena v případě, že je nastavena zastavovací podmínka.
	 */
	private void testInterrupted() throws InterruptedException {
		if (searchInterrupted)
			throw new InterruptedException();
	}

	
	/**
	 * <p>
	 * Seřadí odpovědi v seznamu sestupně podle:
	 * <ol>
	 *   <li>
	 *     četnosti výskytu odpovědi a sémanticky rovných 
	 *     (1948 ≈ únor 1948 ≈ únor 48),
	 *   </li>
	 *   <li>
	 *     přesnosti údaje v rámci své třídy sémantické rovnosti
	 *     ( únor 48 > 1948 ).
	 *   </li>
	 * </ol>
	 * </p>
	 * 
	 * @param answers
	 *            - seznam odpovědí
	 */
	private void sortAnswers(List<Answer> answers) {
		// vytvoření komparátoru k porovnání odpovědí podle přesnosti
		AnswerComparator comparator = new BasicAnswerComparator();
		// porovnání odpovědí podle sémantického obsahu a zařazení do skupin
		List<AnswerGroup> answerGroups = AnswerGroup.sortAnswers(answers, comparator);
		// seřazení skupin odpovědí podle počtu prvků
		Collections.sort(answerGroups, sizeComparator);
		// předání skupin posluchači
		callback.answersSorted(answerGroups);
		// vyprázdnění původního seznamu odpovědí
		answers.clear();
		// zkopírování nejlepší odpovědi každé skupiny do seznamu odpovědí
		for (AnswerGroup group : answerGroups)
			answers.add(group.getBestAnswer());
	}
	
	/**
	 * Prázdný callback použitý v případě, že při volání konstruktoru není
	 * volajícím žádný nastaven.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	public static class NullCallback implements LaseAppCallback {
		
		/* (non-Javadoc)
		 * @see lase.app.LaseAppCallback#webSearchStarted(java.lang.String[])
		 */
		@Override
		public void webSearchStarted(String[] searchStrings) {
			// žádná akce
		}
		
		/* (non-Javadoc)
		 * @see lase.app.LaseAppCallback#answerCandidateFound(lase.answersearch.Answer)
		 */
		@Override
		public void answerCandidateFound(Answer candidate) {
			// žádná akce
		}

		/* (non-Javadoc)
		 * @see lase.app.LaseAppCallback#answersSorted(java.util.List)
		 */
		@Override
		public void answersSorted(List<AnswerGroup> groups) {
			// žádná akce
		}

		/* (non-Javadoc)
		 * @see lase.app.LaseAppCallback#answersSorted(java.util.List)
		 */
		@Override
		public void corpusRetrieved(Corpus corpus, SearchPattern pattern) {
			// žádná akce
			
		}
	}
	
	private static class AnswerMatchingProgressListener implements ProgressListener {
		private int searchPatternsCount;
		private int searchPatternsComplete;
		private ProgressListener overallProgressListener;

		private AnswerMatchingProgressListener(int searchPatternsCount,
				int searchPatternsComplete,
				ProgressListener overallProgressListener) {
			this.searchPatternsCount = searchPatternsCount;
			this.searchPatternsComplete = searchPatternsComplete;
			this.overallProgressListener = overallProgressListener;
		}
		
		@Override
		public void progressChanged(double value) {
			double overalProgress = (searchPatternsComplete + value) / searchPatternsCount;
			overallProgressListener.progressChanged(overalProgress);
		}
	}
	
	/**
	 * Prázdný listener použitý v případě, že volající nepřijímá informace o průběhu
	 * hledání odpovědi.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	private static class NullProgressListener implements ProgressListener {

		/* (non-Javadoc)
		 * @see lase.util.ProgressListener#progressChanged(double)
		 */
		@Override
		public void progressChanged(double value) {
		}
	}
}
