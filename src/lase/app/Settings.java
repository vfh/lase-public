package lase.app;


/**
 * Nastavení systému LASE.
 * 
 * @author Rudolf Šíma
 *
 */
public class Settings {
	
	/** Adresa webové služby morfologického analyzátoru {@link lase.morphology.LiksWebServiceMorphologicalAnalyser}. */
	public static final String liksMorphologyWebServiceUrl = "http://liks.fav.zcu.cz:8180/axis2/services/MorphologyService";
	
	/** Cesta k souboru s databází pojmenovaných entit pro {@link lase.entrec.Database}. */
	public static final String properNounDatabaseFilename = "data/pndb/pndb";
	
	/** Cesta k adresáři s šablonami. */ 
	public static final String templatesdirectory = "data/templates";
	
	/** Cesta k souboru s daty českého WordNetu. */
	public static final String czechWordNetFilename = "data/wordnet/wordnet.cz";
	
	/** Cesta k souboru s cache */
	public static final String cachePath = "cache/cache";
	
	/* Údaje pro přístup do databáze. */
	
	/** URL databázového serveru. */
	public static final String databaseUrl = "mysql://localhost";
	
	/** Jmény MySQL databáze pro logování hodnocení odpovědí. */
	public static final String databaseName = "lase2";
	
	/** Uživatelské jméno pro přihlášení do  MySQL databáze pro logování hodnocení odpovědí. */
	public static final String databaseUserName= "root";
	
	/** Heslo jméno pro přihlášení do  MySQL databáze pro logování hodnocení odpovědí. */
	public static final String databasePassword = "";
	
	/* Nastavení pro testování */
	
	/** Soubor se vstupními otázkami a předpokládanými odpovědmi */
	public static final String testInputFile = "data/inputTest.txt";
	
	/** Složka pro ukládání výsledků testování */
	public static final String testOutputFolder = "testResults/";
}
