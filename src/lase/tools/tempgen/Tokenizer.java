package lase.tools.tempgen;

import java.io.IOException;
import java.io.Reader;
import java.util.HashSet;
import java.util.Set;

/**
 * Lexikální analyzátor zdrojového textu, ze kterého se překladačem šablon
 * generují šablony.
 * 
 * @author Rudolf Šíma
 * 
 */
class Tokenizer {
	
	/**
	 * Množina oddělovačů.
	 */
	private static final Set<Character> delimiters = new HashSet<Character>();
	static {
		delimiters.add('[');
		delimiters.add(']');
		delimiters.add('@');
		delimiters.add('$');
		delimiters.add('"');
		delimiters.add('?');
		delimiters.add(' ');
		delimiters.add('\n');
		delimiters.add('\t');
		delimiters.add(':');
		delimiters.add('\\');
	}

	
	/**
	 * Zdroj zdrojového textu.
	 */
	private Reader input;
	
	/**
	 * Oddělovač na kterém bylo ukončeno čtení předchozího literálu.
	 * Pokud není nastaven, má hodnotu 0.
	 */
	private int pendingDelimiter;

	/**
	 * Vytvoří novou instanci lexikálního analyzátoru a nastaví zdroj zdrojového
	 * textu.
	 * 
	 * @param input
	 *            Zdroj zdrojového textu.
	 */
	public Tokenizer(Reader input) {
		this.input = input;
	}

	/**
	 * Načte a vrátí další lexikální atom.
	 * 
	 * @return Vrací další lexikální atom.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při čtení ze zdroje zdrojového textu
	 *             došlo k chybě.
	 */
	public Token nextToken() throws IOException {
		String chunk;
		String prevChunk = "";
		StringBuilder builder = new StringBuilder();
		boolean braced = false;
		while ((chunk = nextChunk()) != null) {
			if (chunk.trim().isEmpty()) {
				if (chunk.isEmpty() || !braced) {
					if (!"\n".equals(chunk))
						continue;
				}
			}
			if ("\"".equals(chunk)) {
				if (!"\\".equals(prevChunk))
					braced = !braced;
			}
			
			if (braced) {
				if ("\\".equals(chunk)) {
					if ("\\".equals(prevChunk))
						builder.append(chunk);
				} else
					builder.append(chunk);
				
			} else {
				builder.append(chunk);
				String retval = builder.toString();
				boolean quotesRemoved = false;
				if (retval.startsWith("\"") && retval.endsWith("\"")) {
					retval = retval.substring(1, retval.length() - 1);
					quotesRemoved = true;
				}
				return new Token(retval, !quotesRemoved && retval.length() == 1 &&
						delimiters.contains(retval.charAt(0)));
			}
			
			if ("\\".equals(prevChunk))
				prevChunk = "";
			else
				prevChunk = chunk;
		}
		return null;
	}

	/**
	 * Načte a vrátí další lexikální atom. Narozdíl od metody {@link #nextToken()}
	 * nerozpozná řetězce v uvozovkách a escape sekvence.
	 * 
	 * @return Vrací další lexikální atom.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při čtení ze zdroje zdrojového textu
	 *             došlo k chybě.
	 */
	private String nextChunk() throws IOException {
		if (pendingDelimiter != 0) {
			char c = (char) pendingDelimiter;
			pendingDelimiter = 0;
			return String.valueOf(c);
		}
		StringBuilder builder = new StringBuilder();
		int i;
		while ((i = input.read()) != -1) {
			char c = (char) i;
			if (delimiters.contains(c)) {
				pendingDelimiter = c;
				return builder.toString();
			}
			builder.append(c);
		}
		return null;
	}
	
	/**
	 * Lexikální atom vrácený metodou {@link Tokenizer#nextToken()}.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	public static class Token {
		
		/**
		 * Řetězcová hodnota lexikálního atomu.
		 */
		private String value;
		
		/**
		 * Udává, zda je tento lexikální atom oddělovačem.
		 */
		private boolean delimiter;

		/**
		 * Vytvoří nový lexikální atom.
		 * 
		 * @param value
		 *            Řetězcová hodnota lexikálního atomu.
		 * @param delimiter
		 *            Udává, zda je tento lexikální atom oddělovačem.
		 */
		public Token(String value, boolean delimiter) {
			this.value = value;
			this.delimiter = delimiter;
		}

		/**
		 * Vrací řetězcovou hodnotu lexikálního atomu.
		 * 
		 * @return Vrací řetězcovou hodnotu lexikálního atomu.
		 */
		public String getValue() {
			return value;
		}

		/**
		 * Zjistí, zda je lexikální atom oddělovačem.
		 * 
		 * @return Vrací {@code true}, je-li lexikální atom oddělovačem. Jinak
		 *         vrací {@code false}.
		 */
		public boolean isDelimiter() {
			return delimiter;
		}

		/**
		 * Vrací znak oddělovače, je-li tento lexikální atom oddělovač, jinak
		 * vrací '\0'.
		 * 
		 * @return Vrací znak oddělovače, je-li tento lexikální atom oddělovač,
		 *         jinak vrací '\0'.
		 */
		public char getDelimiter() {
			if (!delimiter)
				return '\0';
			else
				return value.charAt(0);
		}

		/**
		 * Zjistí, zda se tento lexikální atom bez ohledu na velikost písmen
		 * shoduje s jiným lexikální atomem.
		 * 
		 * @param other
		 *            Porovnávaný lexikální atom.
		 *            
		 * @return Vrací {@code true}, pokud jsou si lexikální atomy bez ohledu
		 *         na velikost písmen rovny. Jinak vrací {@code false}.
		 */
		public boolean matches(Token other) {
			return delimiter == other.delimiter && value.equalsIgnoreCase(other.value);
		}

		/**
		 * Vrací totéž co {@link #getValue()}.
		 */
		public String toString() {
			return value;
		}
	}
}
