package lase.tools.tempgen;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.TransformerException;

import lase.template.Template;

/**
 * CLI utilita určené k překládání šablon. Generuje XML souboru z formátu
 * jednoduššího pro zápis. (viz data/templates/templates.src)
 * 
 * @author Rudolf Šíma
 * 
 */
public class TemplateCompiler implements Compiler.Callback {
	
	/**
	 * Vytvoření seznam šabon.
	 */
	private List<Template> templates = new ArrayList<Template>();
	
	/**
	 * Udává, zda při překladu nastala chyba z důvodu chyby ve stupním souboru.
	 */
	private boolean error;

	
	/**
	 * Reaguje na chybu ve vstupním souboru vypsáním hlášení na chybový výstup.
	 */
	@Override
	public void handleSyntaxError(int line, String token) {
		error = true;
		System.err.format("Error on line %d, unexpected token '%s'.", line, token).println();
	}

	/**
	 * Přidá nově nalezenou šablonu do interního seznamu šablon.
	 */
	@Override
	public void handleTemplate(Template t) {
		templates.add(t);
	}

	/**
	 * Reaguje na vyhozenou výjimku {@link IOException} vypsáním stack trace na
	 * chybový výstup.
	 */
	@Override
	public void handleIOException(IOException e) {
		error = true;
		e.printStackTrace();
	}

	/**
	 * Hlavní metoda CLI rozhraní překladče šablon
	 * 
	 * @param argv
	 *            Vstupní soubor (1. argument, ostatní ignorovány).
	 */
	public static void main(String[] argv) {
		FileReader reader;
		if (argv.length != 1) {
			System.out.println("Usage: TemplateCompiler <input file>");
			
			return;
		}
		try {
			reader = new FileReader(argv[0]);
		} catch (FileNotFoundException e) {
			System.err.println("Input file not found.");
			return;
		}
		
		TemplateCompiler callback = new TemplateCompiler();
		Compiler compiler = new Compiler(callback);
		compiler.parse(reader);
		if (!callback.error) {
			for (Template t : callback.templates) {
				try {
					t.saveToDirectory(".");
				} catch (TransformerException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
