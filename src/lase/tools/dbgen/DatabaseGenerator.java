package lase.tools.dbgen;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import lase.app.LaseApp;
import lase.morphology.CharFilter;
import lase.morphology.MorphologyException;
import lase.util.LaseException;

/**
 * Generátor souboru databáze pojmenovaných entit (CLI utilita).
 * 
 * @author Rudolf Šíma
 *
 */
public class DatabaseGenerator {
	
	/**
	 * Seznam načtených řádků určených k lemmatizaci.
	 */
	private List<String> lines = new ArrayList<String>();
	
	/**
	 * Dávkový lemmatizátor.
	 */
	private BatchLemmatizer batchLemmatizer = new BatchLemmatizer();

	/**
	 * Vytvoří nový generátor databáze pojmenovaných entit.
	 * 
	 * @param inpFile
	 *            Vstupní soubor (co řádek, to pojmenovaná entita)
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při čtení vstupního souboru dojde k
	 *             chybě.
	 */
	public DatabaseGenerator(String inpFile) throws IOException {
		CharFilter charFilter = CharFilter.getPreferredFilter();
		FileReader fr = new FileReader(inpFile);
		BufferedReader br = null;
		try {
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				line = line.trim();
				if (!line.isEmpty())
					lines.add(charFilter.substitute(line));
			}
		} finally {
			if (br != null)
				br.close();
		}
	}

	/**
	 * Zapíše do proudu {@code ps} část souboru databáze pojmenovaných entit,
	 * přičemž všechny podtyp všech entit je dán parametry {@code subtype}.
	 * 
	 * @param subtype
	 *            Podtyp přidávaných pojmenovaných entit (např. {@code
	 *            "GEOGRAPHY"}).
	 * @param ps
	 *            Proud, do kterého bude vygenerovaná část souboru zapsána.
	 * 
	 * @throws MorphologyException
	 *             Vyhozena v případě, že při lemmatizaci došlo k chybě.
	 */
	public void appendToDb(String subtype, PrintStream ps) throws MorphologyException {
		for (String line : batchLemmatizer.lemmatize(lines)) {
			String newRecord = line.toLowerCase();
			ps.println(subtype + " \"" + newRecord + "\"");				
		}
	}

	/**
	 * Uzavře generátor, což způsobí uzavření použitého dávkového lemmatizátoru.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při uzavírání dávkového lemmatizátoru
	 *             došlo k chybě.
	 */
	public void close() throws IOException {
		batchLemmatizer.close();
	}
	
	/**
	 * Vypíše návod k použití tohoto nástroje z CLI.
	 */
	private static void printUsage() {
		System.err.println("Použití: DatabaseGenerator <infile> <subtype>");
		System.err.println("  <infile> soubor řádků s entitami");
		System.err.println("  <subtype> podtyp entity (např.: GEOGRAPHY)");
	}

	/**
	 * Hlavní metoda CLI generátoru pojmenovaných entit.
	 * 
	 * @param argv
	 *            Vstupní soubor a název podtypu entit.
	 * 
	 * @throws Exception
	 *             Vyhozena v případě, že při generování databáze došlo k
	 *             jakékoli chybě.
	 */
	public static void main(String[] argv) throws Exception {
		if (argv.length != 2) {
			printUsage();
			return;
		}
		try {
			LaseApp.initializeDefaults();
		} catch (LaseException e) {
			System.err.println("Chyba při inicializaci systému LASE.");
			throw e;
		}
		DatabaseGenerator gen;
		try {
			gen = new DatabaseGenerator(argv[0]);
		} catch (FileNotFoundException e) {
			System.err.println("Vstupní soubor nenalezen.");
			return;
		} catch (IOException e) {
			System.err.println("Chyba čtení.");
			throw e;
		}
		gen.appendToDb(argv[1], System.out);
		gen.close();
	}
}
