package lase.tools.dbgen;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import lase.app.SharedObjects;
import lase.morphology.Lemmatizer;
import lase.morphology.MorphologyException;
import lase.morphology.StandardLemmatizer;
import lase.morphology.Tokenizer;

/**
 * Dávkový lemmatizátor. Částečně řeší problém pomalosti morfologického
 * analyzátoru, pokud je požadována pouze lemmatizace, nikoli kompletní
 * morfologická analýza. Dávka řetězců je spojena do jednoho řetězce, který je
 * podeslán lemmatizátoru. Z vráceného řetězce jsou poté zrekonstruovány řetězce
 * odpovídající původním řetězcům určeným k lemmatizaci.
 * 
 * @author Rudolf Šíma
 * 
 */
public class BatchLemmatizer {
	
	/**
	 * Velikost jedné dávky odeslané lemmatizátoru (počet spojených řetězců).
	 */
	private static final int batchSize = 1000;
	
	/**
	 * Použitý lemmatizátor.
	 */
	private Lemmatizer lemmatizer;
	
	/**
	 * Tokenizátor použitý k předzpracování odesílaného textu. Jen u předem tokenizováného
	 * textu lze správně zrekonstruovat původní řetězce. 
	 */
	private Tokenizer tokenizer;

	/**
	 * Vytvoří nový dávkový lemmatizátor.
	 */
	public BatchLemmatizer() {
		this.lemmatizer = new StandardLemmatizer(SharedObjects.getMorphologicalAnalyser());
		this.tokenizer = SharedObjects.getTokenizer();
	}

	/**
	 * Provede lemmatizaci dávky řetězců.
	 * 
	 * @param sentences
	 *            Seznam řetězců určených k lemmatizaci.
	 * 
	 * @return Vrací seznam lemmatizovaných řetězců stejné délky jako seznam
	 *         řetězců určených k lemmatizaci.
	 * 
	 * @throws MorphologyException
	 *             Vyhozena v případě, že při lemmatizaci došlo k chybě.
	 */
	public List<String> lemmatize(List<String> sentences) throws MorphologyException {
		List<String> results = new ArrayList<String>();
		int prevI = 0;
		for (int i = batchSize; i < sentences.size(); i+=batchSize) {
			List<String> batch = sentences.subList(prevI, i); 
			results.addAll(lemmatizeBatch(batch));
			prevI = i;
		}
		List<String> batch = sentences.subList(prevI, sentences.size()); 
		results.addAll(lemmatizeBatch(batch));
		return results;
	}

	/**
	 * Uzavře generátor, což způsobí uzavření použitého lemmatizátoru.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při uzavírání lemmatizátoru došlo k
	 *             chybě.
	 */
	public void close() throws IOException {
		lemmatizer.close();
	}
	
	/**
	 * Provede lemmatizaci dávky řetězců.
	 * 
	 * @param sentences
	 *            Seznam řetězců určených k lemmatizaci.
	 * 
	 * @return Vrací seznam lemmatizovaných řetězců stejné délky jako seznam
	 *         řetězců určených k lemmatizaci.
	 * 
	 * @throws MorphologyException
	 *             Vyhozena v případě, že při lemmatizaci došlo k chybě.
	 */
	private List<String> lemmatizeBatch(List<String> sentences) throws MorphologyException {
		List<String> batchInput = new ArrayList<String>();
		Deque<Integer> dotIndices = new ArrayDeque<Integer>(); 
		for (String sen : sentences) {
			List<String> tokens = tokenizer.tokenize(sen);
			batchInput.addAll(tokens);
			batchInput.add(".");
			dotIndices.offer(tokens.size());
		}
		List<String> batchOutput = lemmatizer.lematize(batchInput);
		List<String> sentenceOutput = new ArrayList<String>();
		int k = 0;
		while (dotIndices.peek() != null) {
			int index = dotIndices.poll();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < index; i++) {
				String s = batchOutput.get(k++);
				sb.append(s);
				if (i + 1 < index)
					sb.append(' ');
			}
			k++;
			sentenceOutput.add(sb.toString());
		}
		return sentenceOutput;
	}
}
