package lase.tools.uml;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Jednoduchá utilita sloužící pro vypsání seznamu atributů a metod při ručním
 * vytvářní UML diagramu tříd.
 * 
 * @author Rudolf Šíma
 * 
 */
public class MemberListExtractor {

	/**
	 * Třída, jejíž deklarované atributy a metody jsou sledovány.
	 */
	private Class<?> cls;

	/**
	 * Vytvoří novou instanci extraktoru atributů a metod.
	 * 
	 * @param className
	 *            Celé jméno zkoumané třídy (včetně názvu balíku).
	 * @throws ClassNotFoundException
	 *             Vyhozena v případě, že daná dřída není nalezena.
	 */
	public MemberListExtractor(String className) throws ClassNotFoundException {
		cls = Class.forName(className);
	}

	/**
	 * Vrací jednoduché jméno zkoumané třídy (bez názvu balíku).
	 * 
	 * @return
	 */
	public String getName() {
		return cls.getSimpleName();
	}

	/**
	 * Vrací názvy atributů jako řetězec. Na každém řádku jeden atribut.
	 * 
	 * @return Vrací názvy atributů
	 */
	public String getAttributes() {
		StringBuilder builder = new StringBuilder();
		Field[] fields = cls.getDeclaredFields();
		for (Field f : fields) {
			int mods = f.getModifiers();
			appendModChar(builder, mods);
			builder.append(' ');
			builder.append(f.getName() + ": " + f.getType().getSimpleName());
			if (Modifier.isStatic(mods)) {
				builder.append(" {static}");
			}
			builder.append('\n');
		}
		return builder.toString();
	}

	/**
	 * Vrací názvy metod jako řetězec. Na každém řádku jedna metoda.
	 * 
	 * @return Vrací názvy metod.
	 */
	public String getMethods() {
		StringBuilder builder = new StringBuilder();
		appendMembers(builder, cls.getDeclaredConstructors());
		appendMembers(builder, cls.getDeclaredMethods());

		return builder.toString();
	}

	/**
	 * Přidá do {@link StringBuilder}u názvy daných členů třídy.
	 * 
	 * @param builder
	 *            Builder, do kterého budou názvy přidány.
	 * @param members
	 *            Pole členů.
	 */
	private void appendMembers(StringBuilder builder, Member[] members) {
		for (Member m : members) {
			int mods = m.getModifiers();
			appendModChar(builder, mods);
			builder.append(' ');
			builder.append(m.getName());
			builder.append("(");
			Class<?>[] tvs = null;
			if (m instanceof Constructor<?>) {
				tvs = ((Constructor<?>) m).getParameterTypes();
			} else if (m instanceof Method) {
				tvs = ((Method) m).getParameterTypes();
			}
			if (tvs != null) {
				for (int i = 0; i < tvs.length; i++) {
					builder.append(tvs[i].getSimpleName());
					if (i < tvs.length - 1) {
						builder.append(", ");
					}
				}
				builder.append(")");
			}
			if (!(m instanceof Constructor<?>)) {
				Method method = (Method) m;
				builder.append(": ");
				builder.append(method.getReturnType().getSimpleName());
			}
			if (Modifier.isStatic(mods)) {
				builder.append(" {static}");
			}
			if (Modifier.isAbstract(mods)) {
				builder.append(" {abstract}");
			}
			builder.append('\n');
		}
	}

	/**
	 * Přidá do {@link StringBuilder}u znak podle modifikátoru metody nebo
	 * atributu podle standardu UML.
	 * 
	 * @param builder
	 *            Builder, do kterého bude znak přidán.
	 * @param mods
	 *            Modifikátory metody nebo atributu.
	 */
	private void appendModChar(StringBuilder builder, int mods) {
		if (Modifier.isPublic(mods)) {
			builder.append("+");
		} else if (Modifier.isPrivate(mods)) {
			builder.append("-");
		} else if (Modifier.isProtected(mods)) {
			builder.append("#");
		} else {
			builder.append("~");
		}
	}

	/**
	 * Vypíše seznam atributů a metody natrvdo zadané třídy v jednoduchém
	 * formátu.
	 * 
	 * @param args
	 *            Nepoužity.
	 *            
	 * @throws Exception
	 *             Vyhozena pokud dojde k jakékoliv chybě.
	 */
	public static void main(String[] args) throws Exception {
		String className = "lase.app.SharedObjects";

		MemberListExtractor mle = new MemberListExtractor(className);
		System.out.println(mle.getName());
		System.out.println("-------------");
		System.out.print(mle.getAttributes());
		System.out.println("-------------");
		System.out.print(mle.getMethods());
	}
}
