package lase.learning;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import lase.app.SharedObjects;
import lase.entrec.NamedEntityGraph;
import lase.entrec.NamedEntityRecognitionException;
import lase.entrec.NamedEntityRecognizer;
import lase.entrec.NamedEntityGraph.Node;
import lase.template.Query;
import lase.template.QueryToken;
import lase.template.ReferenceToken;
import lase.template.SearchPattern;
import lase.template.Token;
import lase.template.Token.Modifier;
import lase.util.ArrayIterator;
import lase.util.ArrayStack;
import lase.util.Corpus;
import lase.util.DebugGraphWriter;
import lase.util.GraphMergingTool;
import lase.util.Stack;

/**
 * 
 * @author Rudolf Šíma
 *
 */
public class LongestCommonPathLearningAlgorithm extends AbstractLearningAlgorithm {
	
	public LongestCommonPathLearningAlgorithm() {
		
	}

	@Override
	protected List<SearchPattern> retrievePatterns(LearningCorporaBundle learningCorporaBundle) throws LearningException {
		if (learningCorporaBundle.size() != 2)
			throw new IllegalArgumentException("The bundle must contain exactly two corpora.");
		
		List<MergedGraph> mergedGraphs = mergeGraphs(learningCorporaBundle);
		MergedGraph graph1 = mergedGraphs.get(0);
		MergedGraph graph2 = mergedGraphs.get(1);
		
		List<AnswerPath> graph1AnswerPaths = findAnswerPaths(graph1);
		List<AnswerPath> graph2AnswerPaths = findAnswerPaths(graph2);
		
		Set<SearchPattern> searchPatternSet = new HashSet<SearchPattern>();
		for (AnswerPath g1ap : graph1AnswerPaths) {
			for (AnswerPath g2ap : graph2AnswerPaths) {
				// common prefix path
				NodePair startingNodes = new NodePair(g1ap.getFirst(), g2ap.getFirst(), null);
				NodePair commonPrefixPath = findCommonPath(startingNodes, SearchDirection.BACKWARD);
				NodePair endingNodes = new NodePair(g1ap.getLast(), g2ap.getLast(), null);
				NodePair commonSuffixPath = findCommonPath(endingNodes, SearchDirection.FORWARD);
				
				List<NodePair> commonPath = joinCommonPaths(commonPrefixPath, commonSuffixPath, g1ap, g2ap);
				if (commonPath.size() > 0) {
					SearchPattern searchPattern = pathToSearchPattern(commonPath, graph1.getQuestion(), graph2.getQuestion());
					if (searchPattern != null)
						searchPatternSet.add(searchPattern);
				}
			}
		}
		return new ArrayList<SearchPattern>(searchPatternSet);
	}
	
	private static List<NodePair> joinCommonPaths(NodePair prefixPart, NodePair suffixPart, AnswerPath graph1AnswerPath, AnswerPath graph2AnswerPath) {
		// možno značně optimalizovat
		
		List<NodePair> result = new ArrayList<NodePair>();
		Deque<NodePair> suffixBuilder = new ArrayDeque<NodePair>();
		
		NodePair np = prefixPart;
		while(np != null) {
			if (np.node1 != graph1AnswerPath.getFirst())
				result.add(np);
			np = np.previousNodePair;
		}
		
		Iterator<Node> g1apIterator = graph1AnswerPath.iterator();
		Iterator<Node> g2apIterator = graph2AnswerPath.iterator();
		while (g1apIterator.hasNext() || g2apIterator.hasNext()) {
			Node g1n;
			if (g1apIterator.hasNext())
				g1n = g1apIterator.next();
			else 
				g1n = null;
			Node g2n;
			if (g2apIterator.hasNext())
				g2n = g2apIterator.next();
			else
				g2n = null;
			np = new NodePair(g1n, g2n, null);
			result.add(np);
		}
		
		np = suffixPart;
		while(np != null) {
			if (np.node1 != graph1AnswerPath.getLast())
				suffixBuilder.addFirst(np);
			np = np.previousNodePair;
		}
		result.addAll(suffixBuilder);
		
		return result;
	}
	
	private static SearchPattern pathToSearchPattern(List<NodePair> path, Query question1, Query question2) {
		List<QueryToken> newSearchPattern = new ArrayList<QueryToken>(path.size());
		boolean unexpressedTokenFound = false;
		for(NodePair np : path) {
			QueryToken newToken;
			QueryToken node1Content = np.node1.getContent();
			QueryToken node2Content = np.node2.getContent();
			QuestionSearchResult qsr = searchQuestionForToken(question1, node1Content);
			if (qsr != null) {
				Token t = new ReferenceToken(qsr.refIndex + 1);
				newToken = new QueryToken(t, null, null);
				Token matchingToken = qsr.matchingQueryToken.getToken();
				if ((!matchingToken.isAnswer()) && matchingToken.getText() == null)
					unexpressedTokenFound = true;;
			} else {
				if (node1Content.wordEquals(node2Content.getOriginalWord())) {
					newToken = node1Content;
				} else {
					Token t = node1Content.getToken();
					newToken = new QueryToken(t, null, null);
				}
			}
			newSearchPattern.add(newToken);
		}
		if (!unexpressedTokenFound)
			return null;
		return new SearchPattern(newSearchPattern);
	}
	
	// predelat
	private static QuestionSearchResult searchQuestionForToken(Query question, QueryToken qt1) {
		for (int i = 0; i < question.size(); i++) {
			QueryToken qt = question.get(i);
			if (qt.typeEquals(qt1) && qt.subtypeEquals(qt1) && qt.getSemanticValue().equals(qt1.getSemanticValue())) {
				return new QuestionSearchResult(i, qt); 
			}
		}
		return null;
	}
	
	private static class QuestionSearchResult {
		public QuestionSearchResult(int refIndex, QueryToken matchingQueryToken) {
			this.refIndex = refIndex;
			this.matchingQueryToken = matchingQueryToken;
		}
		int refIndex;
		QueryToken matchingQueryToken;	
	}
	
	private static NodePair findCommonPath(NodePair startingNodes, SearchDirection direction) {
		Stack<NodePair> stack = new ArrayStack<NodePair>();
		stack.push(startingNodes);
		
		NodePair longestCommonPath = startingNodes;
		
		while(!stack.isEmpty()) {
			NodePair nodePair = stack.pop();
			for (Node successor1 : direction.getNext(nodePair.node1)) {
				for (Node successor2 : direction.getNext(nodePair.node2)) {
					if (compareNodes(successor1, successor2)) {
						NodePair prevNodePair = nodePair;
						nodePair = new NodePair(successor1, successor2, prevNodePair); 
						stack.push(nodePair);
					}
					if (nodePair.pathLength > longestCommonPath.pathLength)
						longestCommonPath = nodePair;
				}
			}
		}
		return longestCommonPath;
	}
	
	private static List<MergedGraph> mergeGraphs(LearningCorporaBundle learningCorporaBundle) throws LearningException {
		NamedEntityRecognizer recognizer = SharedObjects.getNamedEntityRecognizer();
		List<MergedGraph> mergedGraphs = new ArrayList<MergedGraph>(learningCorporaBundle.size());
		for (LearningCorporaBundle.Entry bundleEntry : learningCorporaBundle) {
			GraphMergingTool graphMergingTool = new GraphMergingTool(bundleEntry.getQuestion(), bundleEntry.getAnswer());  
			for (Corpus.Entry corpusEntry : bundleEntry.getCorpus()) {
				try {
					NamedEntityGraph graph = recognizer.analyse(corpusEntry.getText());
					graphMergingTool.addGraph(graph);
				} catch (NamedEntityRecognitionException e) {
					throw new LearningException(e.getMessage(), e);
				}
			}
			mergedGraphs.add(graphMergingTool.getMergedGraph());
			DebugGraphWriter.write(graphMergingTool.getMergedGraph());
		}
		return mergedGraphs;
	}
	
	private static List<AnswerPath> findAnswerPaths(MergedGraph graph) {
		List<AnswerPath> answerPaths = new ArrayList<AnswerPath>();
		
		Queue<Node> queue = new ArrayDeque<Node>();
		queue.offer(graph.getRootNode());
		
		Query answer = graph.getAnswer();
		QueryToken firstToken = answer.get(0);
		
		while (!queue.isEmpty()) {
			Node node = queue.poll();
			
			if (compareNodeWithToken(node, firstToken)) {
				AnswerPath path = followAnswerPath(node, answer);
				if (path != null)
					answerPaths.add(path);
			}
			
			for (Node successor : node.getSuccessors()) {
				if (!graph.isMarked(successor)) {
					graph.markNode(successor);
					queue.offer(successor);
				}
			}
		}
		return answerPaths;
	}
	
	private static AnswerPath followAnswerPath(Node startingNode, Query answer) {
		addAnswerModifier(startingNode);
		
		Queue<SearchNode> queue = new ArrayDeque<SearchNode>();
		queue.offer(new SearchNode(startingNode, null));
		
		int currentAnswerTokenIndex = 0;
		int answerSize = answer.size();
		
		SearchNode sn;
		while (!queue.isEmpty() && ++currentAnswerTokenIndex < answerSize) {
			sn = queue.poll();
			
			for (Node successor : sn.node.getSuccessors()) {
				QueryToken qt = answer.get(currentAnswerTokenIndex);
				if (compareNodeWithToken(successor, qt)) {
					addAnswerModifier(successor);
					queue.offer(new SearchNode(successor, sn));
				}
			}
		}
		
		if (currentAnswerTokenIndex >= answerSize) {
			AnswerPath answerPath = new MultiNodeAnswerPath(queue.poll());
			return answerPath;
		}
		
		return null;
	}

	private static void addAnswerModifier(Node n) {
		QueryToken ansQt = n.getContent();
		if (ansQt != null) {
			Token ansT = ansQt.getToken();
			ansT.addModifier(Modifier.ANSWER);
		}
	}

	private static boolean compareNodeWithToken(Node n, QueryToken qt) {
		QueryToken qt1 = n.getContent();
		if (qt == null)
			return false;
		if (qt.typeEquals(qt1)) {
			Token t = qt.getToken();
			Token t1 = qt1.getToken();
			if (t.getSubtype() == null && t1.getSubtype() == null
					|| t.subtypeEquals(t1)) {
				if (qt.getSemanticValue().equals(qt1.getSemanticValue())) {
					return true;
				}
			}
		}
		return false;
	}
	
	private static boolean compareNodes(Node n1, Node n2) {
		QueryToken qt1 = n1.getContent();
		QueryToken qt2 = n2.getContent();
		if (qt1 == null || qt2 == null)
			return false;
		if (qt1.typeEquals(qt2))
		{
			Token t1 = qt1.getToken();
			Token t2 = qt2.getToken();
			if (t1.getType() == Token.Type.NAMED_ENTITY) {
				return t1.subtypeEquals(t2);
			} else if (t1.getType() == Token.Type.WORD) {
				return qt1.getSemanticValue().equals(qt2.getSemanticValue());
			}
		}
		return false;
	}

	// --------- vnořené statické třídy a rozhraní --------- //
		
	private static enum SearchDirection {
		FORWARD, BACKWARD;
		
		public Iterable<Node> getNext(Node n) {
			if (this == FORWARD) {
				return n.getSuccessors();
			} else {
				return n.getPredecessors();
			}
		}
	}

	private static class NodePair {
		private Node node1;
		private Node node2;
		private NodePair previousNodePair;
		private int pathLength;
		
		public NodePair(Node node1, Node node2, NodePair previousNodePair) {
			this.node1 = node1;
			this.node2 = node2;
			this.previousNodePair = previousNodePair;
			if (previousNodePair != null)
				pathLength = previousNodePair.pathLength + 1;
		}
		
		@Override
		public String toString() {
			return String.format("NodePair(%s;%s;%d)", node1, node2, pathLength);
		}
	}

	private static class SearchNode {
		private Node node;
		private SearchNode prev;
		private int length;
		public SearchNode(Node node, SearchNode prev) {
			this.node = node;
			this.prev = prev;
			if (prev !=  null)
				length = prev.length + 1;
		}
	}

	private static interface AnswerPath extends Iterable<Node> {
		public Node getFirst();
		public Node getLast();
		public int length();
	}
	
	private static class MultiNodeAnswerPath implements AnswerPath {
		public List<Node> path;
		
		public MultiNodeAnswerPath(SearchNode lastSearchNode) {
			path = new ArrayList<Node>(lastSearchNode.length);
			SearchNode sn = lastSearchNode;
			while (sn != null) {
				path.add(sn.node);
				sn = sn.prev;
			}
		}
		
		@Override
		public Node getFirst() {
			return path.get(0);
		}

		@Override
		public Node getLast() {
			return path.get(path.size()-1);
		}

		@Override
		public Iterator<Node> iterator() {
			return path.iterator();
		}
		
		@Override
		public String toString() {
			return path.toString();
		}
		
		@Override
		public int length() {
			return path.size();
		}
	}
	
	@SuppressWarnings("unused")
	private static class SingleNodeAnswerPath implements AnswerPath {
		private Node node;
		
		public SingleNodeAnswerPath(Node n) {
			this.node = n;
		}

		@Override
		public Iterator<Node> iterator() {
			return new ArrayIterator<Node>(new Node[] {node});
		}

		@Override
		public Node getFirst() {
			return node;
		}

		@Override
		public Node getLast() {
			return node;
		}
		
		@Override
		public int length() {
			return 1;
		}
	}
}
