package lase.learning;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import lase.answersearch.Countable;
import lase.template.Query;
import lase.util.Corpus;
import lase.util.IterationUtils;

/**
 * Balík učicích korpusů stažených pro několik různých otázek odpovídajích jedná
 * šabloně a příslušných odpovědí použitý pro učení jedné šablony.
 * 
 * @author Rudolf Šíma
 * 
 */
public class LearningCorporaBundle implements Countable, Iterable<LearningCorporaBundle.Entry>, Serializable {
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Záznamy balíku korpusů obsahující jednotlivé korpusy.
	 */
	private List<Entry> entries = new ArrayList<Entry>();

	/**
	 * Vytvoří nový prázdný balík korpusů, do kterého mohou být přidávány
	 * záznamy voláním metody {@link #add(Corpus, Query)}
	 */
	public LearningCorporaBundle() {
	}
	
	/**
	 * Přidá záznam do balíku učicích korpusů.
	 * 
	 * @param corpus
	 *            Učicí korpus, který bude v záznamu obsažen.
	 * @param answer
	 *            Správná odpověď zadaná uživetelem při učení, na základě které
	 *            byl korpus {#code corpus} nalezen.
	 */
	public void add(Corpus corpus, Query question, Query answer) {
		entries.add(new Entry(corpus, question, answer));
	}
	
	/**
	 * Záznam balíků učicích korpusů obsahující jeden korpus a odpověď zadanou
	 * uživatelem při učení, na základě které byl příslušný korpus nalezen.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	public static class Entry implements Serializable {
		private static final long serialVersionUID = 1L;


		/**
		 * Učicí korpus stažený pro určitou otázku a příslušnou správnou
		 * odpověď.
		 */
		private Corpus corpus;
		
		// TODO - comment
		private Query question;
		
		/**
		 * Správná odpověď, pro kterou byl nalezen korpus {@link #corpus}.
		 */
		private Query answer;

		/**
		 * Vytvoří nový záznam balíku korpusů, nastaví korpus a příslušnou
		 * správnou odpověď, pro kterou byl korpus nalezen.
		 * 
		 * @param corpus
		 *            Učicí korpus, který bude v záznamu obsažen.
		 * @param answer
		 *            Správná odpověď zadaná uživetelem při učení, na základě
		 *            které byl korpus {#code corpus} nalezen.
		 */
		public Entry(Corpus corpus, Query question, Query answer) {
			this.corpus = corpus;
			this.question = question;
			this.answer = answer;
		}

		/**
		 * Vrací učicí korpus stažený pro určitou otázku a příslušnou správnou
		 * odpověď.
		 * 
		 * @return Vrací učicí korpus stažený pro určitou otázku a příslušnou správnou
		 * odpověď.
		 */
		public Corpus getCorpus() {
			return corpus;
		}

		/**
		 * Správná odpověď, pro kterou byl nalezen učicí korpus, který vrací
		 * getter {@link #getCorpus()}.
		 * 
		 * @return Správná odpověď, pro kterou byl nalezen učicí korpus.
		 */
		public Query getAnswer() {
			return answer;
		}

		// TODO - comment
		public Query getQuestion() {
			return question;
		}
	}

	/**
	 * Vrací read-only iterátor záznamů balíku učicích korpusů. Tyto záznamy
	 * tedy obsahují jednotlivé korpusy.
	 */
	@Override
	public Iterator<Entry> iterator() {
		return IterationUtils.readOnlyIterator(entries);
	}

	@Override
	public int size() {
		return entries.size();
	}
}
