package lase.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lase.template.Query;
import lase.template.QueryToken;
import lase.template.SearchPattern;
import lase.util.Corpus;
import lase.util.LaseException;
import lase.websearch.BingSnippetWebSearch;
import lase.websearch.JyxoSnippetWebSearch;
import lase.websearch.SeznamSnippetWebSearch;
import lase.websearch.WebSearch;

/**
 * Abstraktní předek učicích algoritmů zodpovědný za získání učicího korpusu
 * sérií hledání webovým vyhledávačem.
 * 
 * @author Rudolf Šíma
 * 
 * @see LearningAlgorithm
 * 
 */
public abstract class AbstractLearningAlgorithm implements LearningAlgorithm {

	/**
	 * Množina slov, která jsou v otázce ignorována při hledání učicího korpusu
	 * na webu.
	 */
	private static final Set<String> stopList;
	static {
		String[] words = {"kdy", "kde", "kdo", "co", "jak", "proč", "?"};
		stopList = new HashSet<String>(Arrays.asList(words));
	}
	
	/**
	 * Rozhraní webového vyhledávače.
	 */
	private WebSearch webSearch = new SeznamSnippetWebSearch();

	/**
	 * Spustí učicí algoritmus nad balíkem stažených učicích korpusů. Tento
	 * algoritmu je zodpovědný za vytvoření vyhledávacích vzorů pro šablonu,
	 * které odpovídají otázky použité pro vytvoření balíku učicích korpusů.
	 * 
	 * @param learningCorporaBundle
	 *            Balík učicích korpusů vytvořený pro seznam párů
	 *            otázka–odpověď, kde otázky odpovídají vzoru otázky jedné již
	 *            existujícíc šablony.
	 * 
	 * @return Vrací seznam učicím algoritmem vytvořených vyhledávacích vzorů.
	 * 
	 * @throws LearningException
	 *             Vyhozena v případě, že při učení došlo k jakékoli chybě.
	 *             Pokud bylo její způsobeno jinou výjimkou, je tato výjimka
	 *             nastavena jako její příčina a může být zjištěna metodou
	 *             {@link LearningException#getCause()}.
	 * 
	 * @see lase.learning.LearningAlgorithm#learn(java.util.List)
	 */
	protected abstract List<SearchPattern> retrievePatterns(LearningCorporaBundle learningCorporaBundle) throws LearningException;
	
	/* (non-Javadoc)
	 * @see lase.learning.LearningAlgorithm#learn(java.util.List)
	 */
	@Override
	public List<SearchPattern> learn(List<QuestionAnswerPair> learningPairs) throws LaseException {
		LearningCorporaBundle learningCorporaBundle = new LearningCorporaBundle();
		for (QuestionAnswerPair pair : learningPairs) {
			Corpus corpus = getCorpus(pair);
			learningCorporaBundle.add(corpus, pair.getQuestion(), pair.getAnswer());
		}
		return retrievePatterns(learningCorporaBundle);
	}

	/**
	 * Pro daný pár otázka–odpověď získá učicí korpus z výsledků webového
	 * hledání.
	 * 
	 * @param learningPair
	 *            Pár otázka odpověď ze kterého se vytvoří řetězec, který bude
	 *            zadán do webového vyhledávače.
	 * 
	 * @return Vrací stažený učicí korpus.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že při použití webového vyhledávače došlo
	 *             k chybě.
	 */
	public Corpus getCorpus(QuestionAnswerPair learningPair)throws LaseException {
		Query question = learningPair.getQuestion();
		Query answer = learningPair.getAnswer();
		List<String> questionSearchWords = new ArrayList<String>(question.size());

		// odstranění slov podle stop listu
		for (QueryToken qt : question) {
			Object semVal = qt.getSemanticValue();
			if (semVal instanceof String) {
				if (!stopList.contains(semVal))
				questionSearchWords.add(qt.getOriginalWord());
			}
		}
		
		// stažení korpusu
		String[] exactPhrases = new String[] { answer.toString() };
		String[] otherWords = questionSearchWords.toArray(new String[0]);
		return webSearch.search(exactPhrases, otherWords);
	}
}
