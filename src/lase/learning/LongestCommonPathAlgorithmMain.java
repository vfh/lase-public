package lase.learning;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.TransformerException;

import lase.app.LaseApp;
import lase.app.Settings;
import lase.app.SharedObjects;
import lase.entrec.NamedEntityGraph;
import lase.entrec.NamedEntityRecognitionException;
import lase.entrec.NamedEntityRecognizer;
import lase.template.BadTemplateException;
import lase.template.Query;
import lase.template.QueryToken;
import lase.template.Template;
import lase.template.Token;
import lase.template.NamedEntityToken.Subtype;
import lase.template.Token.Type;
import lase.util.LaseException;

import org.xml.sax.SAXException;

public class LongestCommonPathAlgorithmMain {
	
	private static final LearningAlgorithm algorithm = new LongestCommonPathLearningAlgorithm();
	private static final String learningDir = new File(Settings.templatesdirectory, "learning").getPath();
	private static final String generatedDir = new File(learningDir, "generated").getPath();

	private List<QuestionAnswerPair> qaPairList;
	private LearningManager manager;

	public LongestCommonPathAlgorithmMain(InputStream inputStream) throws IOException, BadTemplateException, SAXException {
		List<Template> templates = Template.loadDirectory(learningDir);
		manager = new LearningManager(algorithm, templates);
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		try {
			String line;
			int cnt = 0;
			QuestionAnswerPair pair = new QuestionAnswerPair();
			qaPairList = new  ArrayList<QuestionAnswerPair>();
			while ((line = br.readLine()) != null) {
				if (line.trim().isEmpty())
					continue;
				if (cnt++ % 2 == 0) {
					pair.question = line;
				}
				else {
					pair.answer = line;
					qaPairList.add(new QuestionAnswerPair(pair));
				}
			}
		} finally {
			try {
				br.close();
			} catch (IOException e) {}
		}
	}
	
	public void learn() throws LaseException, TransformerException {
		int cnt = 0;
		for (QuestionAnswerPair pair : qaPairList) {
			Query answer = createAnswer(pair.answer);
			manager.addLearningPair(pair.question, answer);
			
			if (cnt++ % 2 == 1) {
				Template newTemplate = manager.learn();
				newTemplate.saveToDirectory(generatedDir);
				// TODO - naložit si newTemplate;
				manager.clearLearningPairs();
			}
		}
	}
	
	private Query createAnswer(String text) throws NamedEntityRecognitionException {
		NamedEntityRecognizer recognizer = SharedObjects.getNamedEntityRecognizer();
		NamedEntityGraph graph = recognizer.analyse(text);
		List<Query> allPaths = graph.getAllPaths();
		int bestScore = Integer.MIN_VALUE;
		Query bestAnswer = null;
		for (Query answer : allPaths) {
			int score = 0;
			for (QueryToken qt : answer) {
				Token t = qt.getToken();
				int points = 0;
				if (t.getType() == Type.NAMED_ENTITY) {
					Subtype subtype = (Subtype) t.getSubtype();
					switch(subtype) {
					case NUMBER:
						points = 1;
						break;
					case DATE:
					case DATE_TIME:
					case TIME:
						points = 2;
						break;
					case PERSON:
						points = 2;
						break;
					case GEOGRAPHY:
						points = 4;
					}
				}
				score += points;
			}
			score -= answer.size();
			if (score > bestScore) {
				bestScore = score;
				bestAnswer = answer;
			}
		}
		return bestAnswer;
	}
	
	private static class QuestionAnswerPair {
		public QuestionAnswerPair(){
			
		}
		
		public QuestionAnswerPair(QuestionAnswerPair pair) {
			this.question = pair.question;
			this.answer = pair.answer;
		}
		
		String question;
		String answer;
	}
	
	public static void main(String[] args) throws LaseException, IOException, SAXException, TransformerException {
		LaseApp.initializeDefaults();
		String learningInput = 
			"kdy se narodil Karel IV.\n" +
			"14. 5. 1316\n" +
			"\n" +
			"kdy se narodil Josef Čapek\n" +
			"23. 3. 1887\n";
		/*String learningInput = 
			"čím se platí v USA\n" +
			"Dolarem\n" +
			"\n" +
			"čím se platí ve Španělsku\n" +
			"Eurem\n";*/
		InputStream inputStream = new ByteArrayInputStream(learningInput.getBytes());
		LongestCommonPathAlgorithmMain main = new LongestCommonPathAlgorithmMain(inputStream);
		main.learn();
	}
}
