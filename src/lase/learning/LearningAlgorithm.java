package lase.learning;

import java.util.List;

import lase.template.SearchPattern;
import lase.util.LaseException;

/**
 * <p>
 * Konkrétní učicí algoritmus sloužicí k poloautomatickému vytváření
 * vyhledávacích vzorů pro šablony systému LASE.
 * </p>
 * 
 * <p>
 * Implementace algoritmu je zodpovědná za získání balíku učicích korpusů na
 * základě otázek a příslušných správných odpovědí zadaných učitelem a následné
 * vytvoření nových vyhledávacích vzorů pro šablonu, které odpovídají zadané
 * otázky.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public interface LearningAlgorithm {

	/**
	 * Spustí učení. Nejprve se získá balíku učicích korpusů základě otázek a
	 * příslušných správných odpovědí zadaných učitelem, a na základě těchto
	 * korpusů se vytvoří nové vyhledávací vzory, které odpovídají zadané
	 * otázky.
	 * 
	 * @param learningPairs
	 *            Seznam párů otázka–odpověď, na základě kterých se získá učicí
	 *            korpus a následně se vytvoří nové vyhledávací vzory.
	 *            <em><strong>Důležité:</strong> Otázky všech párů musí odpovídat
	 *            stejnému již existujícímu (ručně vytvořenému) vzoru otázky
	 *            některé šablony!</em>
	 * 
	 * @return Vrací seznam učicím algoritmem vytvořených vyhledávacích vzorů
	 *         náležících šabloně, které odpovídají otázky učicích párů.
	 * 
	 * @throws LearningException
	 *             Vyhozena v případě, že při učení nastala chyba způsobená
	 *             některou modulem učení. Výjimka {@link LearningException} je
	 *             potomkem výjimky {@link LaseException}, která může být také
	 *             vyhozena touto metodou. Mohou být tedy ošetřeny v jednom
	 *             {@code catch} bloku, pokud není potřeba je rozlišit.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že při učení nastala chyba, která má
	 *             svoji příčinu mimo modul učení resp. byla způsobena některým
	 *             z použitých nástrojů systému LASE.
	 */
	public List<SearchPattern> learn(List<QuestionAnswerPair> learningPairs) throws LearningException, LaseException;
	
}
