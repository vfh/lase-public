package lase.learning;

import lase.entrec.NamedEntityGraph;
import lase.template.Query;

/**
 * Pár otázka–odpověď použitý při poloautomatickém učení šablon. Jedná se o
 * předzpracovanou otázku a odpověď, ve kterých jsou již nalezeny pojmenované
 * entity a je vybrána jedna konkrétní kombinace rozpoznaných pojmenovaných
 * entit. Jinými slovy jak otázka tak odpověď jsou jednou konkrétní cestou v
 * grafu pojmenovaných entity {@link NamedEntityGraph}.
 * 
 * @author Rudolf Šíma
 * 
 */
public class QuestionAnswerPair {
	
	/**
	 * Předzpracovaná otázka zadaná uživatelem při učení.
	 */
	private Query question;
	
	/**
	 * Předzpracovaná odpověď zadaná uživatelem při učení.
	 */
	private Query answer;

	/**
	 * Vytvoří nový pát otázka–odpověď použitý při poloautomatickém učení
	 * šablon.
	 * 
	 * @param question
	 *            Předzpracovaná otázka zadaná uživatelem při učení.
	 * @param answer
	 *            Předzpracovaná odpověď zadaná uživatelem při učení.
	 */
	public QuestionAnswerPair(Query question, Query answer) {
		this.question = question;
		this.answer = answer;
	}

	/**
	 * Vrací předzpracovanou otázku zadanou uživatelem při učení.
	 * 
	 * @return Vrací předzpracovanou otázku zadanou uživatelem při učení.
	 */
	public Query getQuestion() {
		return question;
	}

	/**
	 * Vrací předzpracovanou odpověď zadanou uživatelem při učení.
	 * 
	 * @return Vrací předzpracovanou odpověď zadanou uživatelem při učení.
	 */
	public Query getAnswer() {
		return answer;
	}
}
