package lase.learning;

import java.util.ArrayList;
import java.util.List;

import lase.app.SharedObjects;
import lase.entrec.NamedEntityGraph;
import lase.entrec.NamedEntityRecognitionException;
import lase.entrec.NamedEntityRecognizer;
import lase.template.Query;
import lase.template.QueryToken;
import lase.template.SearchPattern;
import lase.template.Template;
import lase.template.TemplateCreator;
import lase.template.TemplateMatchResult;
import lase.template.Token;
import lase.util.DebugGraphWriter;
import lase.util.LaseException;

/**
 * Manažer poloautomatického učení šablon systému LASE.
 * 
 * @author Rudolf Šíma
 *
 */
// TODO - comments
public class LearningManager {
	
	/**
	 * Použitý učicí algoritmus
	 */
	private LearningAlgorithm algorithm;
	
	/**
	 * Rozpoznávač pojmenovaných entit.
	 */
	private NamedEntityRecognizer namedEntityRecognizer;
	
	/**
	 * Načtený seznam šablon, mezi kterých se hledá šablona shodující
	 * se se zadanou otázkou. 
	 */
	private List<Template> templates;

	/**
	 * Seznam párů otázka–odpověď vytvářený metodou
	 * {@link #addLearningPair(String, Query)}, na základě kterých se stáhne
	 * balík učicích korpusů a učicím algoritmem vytvoří nové vyhledávací vzory.
	 */
	private List<QuestionAnswerPair> learningPairs = new ArrayList<QuestionAnswerPair>();
	
	private Template matchingTemplate;

	/**
	 * Vytvoří novou instanci učicího manažeru, nastaví učicí algoritmus, který
	 * bude použit, a seznam šablon, mezi kterými se bude hledat šablona
	 * odpovítající učitelem zadaným otázkám.
	 * 
	 * @param algorithm
	 *            Učicí algoritmu.
	 * @param templates
	 *            Seznam šablon, mezi kterými se bude hledat šablona
	 *            odpovítající učitelem zadaným otázkám.
	 */
	public LearningManager(LearningAlgorithm algorithm, List<Template> templates) {
		this.algorithm = algorithm;
		this.templates = templates;
		namedEntityRecognizer = SharedObjects.getNamedEntityRecognizer();
	}

	/**
	 * Přidá pár jeden otázka–odpověď do seznamu párů, které budou použity ke
	 * stažení učicího korpusus a vytvoření vyhledávacích vzorů učicím
	 * algoritmem při volání metody {@link #learn()}.
	 * 
	 * @param question
	 *            Otázka zadaná učitelem.
	 *            <em><strong>Důležité:</strong> Otázky všech párů musí odpovídat
	 *            stejnému již existujícímu (ručně vytvořenému) vzoru otázky
	 *            některé šablony!</em>
	 * @param answer
	 *            Správná odpověď na zadanou otázku. Jedná se předzpracovanou
	 *            odpověď s konkrétní kombinací nalezených pojmenovaných entity,
	 *            tedy o jednu konkrétní cestu v grafu pojmenovaných entit z
	 *            počátečního do některého koncového uzlu.
	 * 
	 * @throws NamedEntityRecognitionException
	 *             Vyhozena v případě, že došlo k chybě při rozpoznávání
	 *             pojmenovaných entity v otázce.
	 * 
	 * @throws LearningException
	 *             Vyhozena v případě, že otázka přidávaného páru odpovídá jiné
	 *             šabloně, než které odpovídaly otázky předchozích přidaných
	 *             párů.
	 */
	public void addLearningPair(String question, Query answer) throws NamedEntityRecognitionException, LearningException {
		NamedEntityGraph graph = namedEntityRecognizer.analyse(question);
		
		// TODO optimalizovat - poslední trefená šablona jako první
		
		DebugGraphWriter.write(graph);
		
		for (Query query : graph.getAllPaths()) {
			for (Template template : templates) {
				TemplateMatchResult matchResult = template.matchQuery(query);
				if (matchResult.matches()) {
					if (matchingTemplate != null & matchingTemplate != template)
						throw new LearningException("All questions must match the same template.");
					matchingTemplate = template;
					QuestionAnswerPair pair = new QuestionAnswerPair(query, answer);
					learningPairs.add(pair);
					return;
				}
			}
		}
		
		throw new LearningException("The question '" + question + "' does not match any template.");
	}

	/**
	 * Smaže všechny páry otázka–odpověď přidané metodou
	 * {@link #addLearningPair(String, Query)}.
	 */
	public void clearLearningPairs() {
		matchingTemplate = null;
		learningPairs.clear();
	}

	/**
	 * Spustí učení. Nejprve se získá balíku učicích korpusů základě otázek a
	 * příslušných správných odpovědí zadaných učitelem, a na základě těchto
	 * korpusů se vytvoří nové vyhledávací vzory, které odpovídají zadané
	 * otázky.
	 * 
	 * @return Vrací seznam učicím algoritmem vytvořených vyhledávacích vzorů
	 *         náležících šabloně, které odpovídají otázky učicích párů.
	 * 
	 * @throws LearningException
	 *             Vyhozena v případě, že při učení nastala chyba způsobená
	 *             některou modulem učení. Výjimka {@link LearningException} je
	 *             potomkem výjimky {@link LaseException}, která může být také
	 *             vyhozena touto metodou. Mohou být tedy ošetřeny v jednom
	 *             {@code catch} bloku, pokud není potřeba je rozlišit.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že při učení nastala chyba, která má
	 *             svoji příčinu mimo modul učení resp. byla způsobena některým
	 *             z použitých nástrojů systému LASE.
	 *             
	 * TODO - upravit komentáře
	 */
	public Template learn() throws LearningException, LaseException {
		TemplateCreator creator = new TemplateCreator();
		List<SearchPattern> patterns = algorithm.learn(learningPairs);
		for (SearchPattern sp : patterns) {
			creator.newAnswerPattern();
			for (QueryToken qt : sp) {
				Token t = qt.getToken();
				creator.addToken(t);
			}
		}
		return creator.getTemplate(matchingTemplate);
	}
}
