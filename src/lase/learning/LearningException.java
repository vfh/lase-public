package lase.learning;

import lase.util.LaseException;

/**
 * Výjimka zapouzdřující jakoukoli chybu, která nastala při poloautomatickém
 * učení šablon v systému LASE. Její vyhození může být způsobeno vyhozením jiné
 * výjimky, které je v tom případě udána jako příčina výjimky
 * {@link LearningException} a může být zjištěna voláním {@link #getCause()}.
 * 
 * @author Rudolf Šíma
 * 
 */
public class LearningException extends LaseException {
	private static final long serialVersionUID = 1L;

	/**
	 * Vytvoří novou výjimku bez podrobné zprávy a nastavení příčiny.
	 */
	public LearningException() {
	}

	/**
	 * Vytvoří novou výjimku a nastaví podrobnou zprávu.
	 */	
	public LearningException(String message) {
		super(message);
	}

	/**
	 * Vytvoří novou výjimku a nastaví příčinu.
	 */
	public LearningException(Throwable cause) {
		super(cause);
	}

	/**
	 * Vytvoří novou výjimku, nastaví podrobnou zprávu a příčinu.
	 */
	public LearningException(String message, Throwable cause) {
		super(message, cause);
	}

}
