package lase.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import lase.answersearch.Countable;
import lase.answersearch.SizeComparator;
import lase.app.LaseApp;
import lase.app.SharedObjects;
import lase.entrec.NamedEntityGraph;
import lase.entrec.NamedEntityRecognitionException;
import lase.entrec.NamedEntityRecognizer;
import lase.template.Query;
import lase.template.QueryToken;
import lase.template.Token;
import lase.util.CustomTraversalPolicy;
import lase.util.LaseException;
import lase.util.StringUtils;

/**
 * GUI pro zadávání dat k poloautomatickémnu učení šablon.
 * 
 * @author Rudolf Šíma
 *
 *
 * @deprecated Neodpovídá změnám v LearningManageru.
 */
@Deprecated
public class LearningGui extends JFrame implements FocusListener, ActionListener {

	private static final long serialVersionUID = 1L;
	private static final Comparator<Countable> sizeComparator = new SizeComparator(false);
	
	private JTextField tfQuestion;
	private JTextField tfAnswer;
	private JButton btStartLearning;
	//private JButton btAddPair;

	private JTextArea taOutput;

	private RadioPanel rpAnswerOptions;	
	@SuppressWarnings("unused")
	private JScrollPane spAnswerOptions;
	
	private List<Query> answerPaths;
	
	public LearningGui() {
		super("LASE – automatické učení šablon");
		initializeControls();
		redirectStdOut(taOutput);
		setSize(700, 500);
		setLocationRelativeTo(null);
		
		tfQuestion.setText("Kdy se narodil Karel IV.?");
		tfAnswer.setText("14. května 1316");
	}
	
	private void initializeControls() {
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		Container contentPane = getContentPane();
		contentPane.setLayout(layout);
		Component newComponent;
		
		newComponent = tfQuestion = new JTextField();
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;
		layout.setConstraints(newComponent, constraints);
		contentPane.add(newComponent);
		
		JLabel lbQuestion;
		newComponent = lbQuestion = new JLabel("Otázka:");
		lbQuestion.setDisplayedMnemonic('O');
		lbQuestion.setLabelFor(tfQuestion);
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		layout.setConstraints(newComponent, constraints);
		contentPane.add(newComponent);
		
		newComponent = tfAnswer = new JTextField();
		tfAnswer.addFocusListener(this);
		constraints.gridx = 1;
		constraints.gridy = 1;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;
		layout.setConstraints(newComponent, constraints);
		contentPane.add(newComponent);
		
		JLabel lbAnswer;
		newComponent = lbAnswer = new JLabel("Odpověď:");
		lbAnswer.setDisplayedMnemonic('d');
		lbAnswer.setLabelFor(tfAnswer);
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		layout.setConstraints(newComponent, constraints);
		contentPane.add(newComponent);
		
//		newComponent = btAddPair = new JButton("Přidat");
//		btAddPair.setMnemonic('P');
//		btAddPair.addActionListener(this);

		newComponent = btStartLearning = new JButton("Spustit učení");
		btStartLearning.setMnemonic('S');
		btStartLearning.addActionListener(this);
		btStartLearning.setEnabled(false);
		constraints.gridx = 2;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 2;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		layout.setConstraints(newComponent, constraints);
		contentPane.add(newComponent);
		
		rpAnswerOptions = new RadioPanel();
		newComponent = spAnswerOptions = new JScrollPane(rpAnswerOptions,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		constraints.gridx = 0;
		constraints.gridy = 2;
		constraints.gridwidth = 3;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		layout.setConstraints(newComponent, constraints);
		contentPane.add(newComponent);
		
		taOutput = new JTextArea();
		taOutput.setBackground(Color.BLACK);
		taOutput.setForeground(Color.GREEN);
		taOutput.setFont(Font.decode(Font.MONOSPACED));
		newComponent = new JScrollPane(taOutput,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		constraints.gridx = 0;
		constraints.gridy = 3;
		constraints.gridwidth = 3;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		layout.setConstraints(newComponent, constraints);
		contentPane.add(newComponent);
		
		setupTraversalPolicy();
	}
	
	private void setupTraversalPolicy() {
		CustomTraversalPolicy newPolicy = new CustomTraversalPolicy();
		newPolicy.addComponent(tfQuestion);
		newPolicy.addComponent(tfAnswer);
		newPolicy.addComponent(rpAnswerOptions);
		newPolicy.addComponent(btStartLearning);
		setFocusTraversalPolicy(newPolicy);
	}

	private void redirectStdOut(JTextArea textArea) {
		OutputStream newOutputStream = new TextAreaOutputStream(textArea);
		PrintStream newPrintStream = new PrintStream(newOutputStream, true);
		System.setOut(newPrintStream);
	}

	private void showAnswerOptions() {
		NamedEntityRecognizer recognizer = SharedObjects.getNamedEntityRecognizer();
		String answerText = tfAnswer.getText().trim();
		answerPaths = null;
		rpAnswerOptions.clear();
		btStartLearning.setEnabled(false);
		if (answerText.isEmpty())
			return;
		NamedEntityGraph graph;
		try {
			graph = recognizer.analyse(answerText);
		} catch (NamedEntityRecognitionException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(),
					"Chyba rozpoznávače pojmenovaných entit",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		try {
			graph.outputGraphViz("graphviz/Answer.gv");
		} catch (IOException e) {
			e.printStackTrace();
		}
		answerPaths = graph.getAllPaths();
		Collections.sort(answerPaths , sizeComparator);
		for (Query query : answerPaths) {
			rpAnswerOptions.addOption(formatQuery(query));
		}
		rpAnswerOptions.showControls();
		btStartLearning.setEnabled(true);
	}
	
	private String formatQuery(Query query) {
		int size = query.size();
		if (size <= 0)
			return "";
		StringBuilder builder = new StringBuilder();
		int cnt = 0;
		for (QueryToken qt : query) {
			builder.append(formatQueryToken(qt));
			if (++cnt != size)
				builder.append(" ");
		}
		return StringUtils.workaroundExtraSpaces(builder.toString());
	}
	
	private String formatQueryToken(QueryToken qt) {
		Token token = qt.getToken();
		Token.Type type = token.getType();
		String origWord = qt.getOriginalWord();
		if (type == Token.Type.WORD) {
			return origWord;
		} else if (type == Token.Type.NAMED_ENTITY) {
			return String.format("{%s: %s}", token.getSubtype(), origWord);   
		} else {
			throw new IllegalArgumentException();
		}
	}

//	private void startLearning() {
//		int selectedIndex = rpAnswerOptions.getSelectedIndex();
//		assert selectedIndex >= 0;
//		assert answerPaths != null;
//		Query chosenAnswer = answerPaths.get(selectedIndex);
//		
//		List<Template> templates = Collections.emptyList();
//		try {
//			templates = Template.loadDirectory(Settings.templatesdirectory);
//		} catch (SAXException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (BadTemplateException e) {
//			e.printStackTrace();
//		}
//		TestingLearningAlgorithm algorithm = new SuffixTreeLearningAlgorithm();  
//		
//		LearningManager manager = new LearningManager(algorithm, templates);
//		@SuppressWarnings("unused")
//		List<SearchPattern> newPatternList = Collections.emptyList();
//		try {
//			newPatternList = manager.learn(tfQuestion.getText(), chosenAnswer);
//		} catch (MorphologyException e) {
//			e.printStackTrace();
//		} catch (LaseException e) {
//			e.printStackTrace();
//		}
//	}

	@Override
	public void focusGained(FocusEvent e) {
	}

	@Override
	public void focusLost(FocusEvent e) {
		Object source = e.getSource();
		if (source == tfAnswer) {
			showAnswerOptions();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == btStartLearning) {
			System.out.println("startLearning() is commented out!");
			//startLearning();
		}
	}

	private static class RadioPanel extends JPanel {
		private static final long serialVersionUID = 1L;
		private List<JRadioButton> radios = new ArrayList<JRadioButton>();
		private ButtonGroup radioGroup;
		
		public RadioPanel() {
			LayoutManager layout = new BoxLayout(this, BoxLayout.Y_AXIS);
			setLayout(layout);
		}
		
		public void addOption(String text) {
			JRadioButton newRadioButton = new JRadioButton(text);
			radios.add(newRadioButton);
		}
		
		public void showControls() {
			radioGroup = new ButtonGroup();
			for(JRadioButton rb : radios) {
				radioGroup.add(rb);
				add(rb);
			}
			if (radios.size() > 0)
				radios.get(0).setSelected(true);
			repaint();
			revalidate();
		}
		
		public void clear() {
			removeAll();
			radios.clear();
		}
		
		@SuppressWarnings("unused")
		public int getSelectedIndex() {
			int count = radios.size();
			for (int i = 0; i < count; i++) {
				if (radios.get(i).isSelected())
					return i;
			}
			return -1;
		}
	}

	public static void main(String[] args) {
		try {
			LaseApp.initializeDefaults();
			LearningGui gui = new LearningGui();
			gui.setDefaultCloseOperation(EXIT_ON_CLOSE);
			gui.setVisible(true);
		} catch (LaseException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(),
					"Inicializace aplikace LASE se nezdařila.",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
