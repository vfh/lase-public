package lase.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import lase.answersearch.Answer;
import lase.answersearch.AnswerGroup;
import lase.app.LaseApp;
import lase.app.LaseAppCallback;
import lase.app.Settings;
import lase.cache2.Cache;
import lase.template.SearchPattern;
import lase.util.Corpus;
import lase.util.LaseException;
import lase.util.StringUtils;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Grafické rozhraní sloužící k testování systému LASE.
 * 
 * @author Rudolf Šíma
 *
 */
public class LaseGui extends JFrame implements LaseAppCallback, KeyListener, FocusListener, ActionListener {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Nastavení tohoto GUI. Serializuje se při ukončení a načítá se při
	 * vytvoření instance.
	 */
	private LaseGuiSettings laseGuiSettings;
	
	/**
	 * Vlákno, ve kterém se spouští vyhledávání odpovědí systémem LASE.
	 */
	private transient Thread appThread;
	
	/**
	 * Aplikace LASE.
	 */
	private transient LaseApp app;
	
	/**
	 * Tlačítko "Hledat".
	 */
	private JButton searchBt;
	
	/**
	 * Tlačítko "Přerušit".
	 */
	private JButton interruptBt;
	
	/**
	 * Textové pole, do kterého se zadává otázka.
	 */
	private JTextField searchTf;
	
	/**
	 * Textová oblast, do které se přesměruje standardní výstup aplikace.
	 */
	private JTextArea consoleTa;
	
	/**
	 * Textová oblast, ve které se zobrazí dlouhá forma nejlepší nalezené odpovědi.
	 */
	private JTextArea longAnswerTa;
	
	/**
	 * Textové pole, ve kterém se zobrazá krátká forma nejlepší nalezené odpovědi.
	 */
	private JTextField shortAnswerTf;

	/**
	 * Vytvoří novou instanci formuláře GUI pro LASE, inicializuje aplikaci
	 * LASE, deserializuje dříve uložené nastavení (existuje-li daný souboru) a
	 * aplikuje jej.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že při inicializaci aplikace LASE došlo k
	 *             chybě.
	 */
	public LaseGui() throws LaseException {
		
		super("Systém zodpovídání otázek LASE");
		app = new LaseApp(this);
		initControls();
		
		OutputStream textAreaOutputStream = new TextAreaOutputStream(consoleTa);
		PrintStream textAreaPrintStream = new PrintStream(textAreaOutputStream,
				true);
		System.setOut(textAreaPrintStream);

		setSize(700, 500);
		setLocationRelativeTo(null);

		try {
			laseGuiSettings = LaseGuiSettings.loadSettings();
			searchTf.setText(laseGuiSettings.getLastQuestion());
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Při načítání nastavení došlo k chybě.", "Chyba", JOptionPane.ERROR_MESSAGE);
		}
		
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent event) {
				try {
					laseGuiSettings.saveSettings();
				} catch (IOException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Při ukládání nastavení došlo k chybě.", "Chyba", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}

	/**
	 * Spustí hledání odpovědi po kliknutí na tlačítko "Hledat".
	 * 
	 * @param question
	 *            Otázka, kterou se systém LASE pokuzí zodpovědět.
	 */
	private void findAnswer(final String question) {
		laseGuiSettings.setLastQuestion(question);

		longAnswerTa.setText("");
		shortAnswerTf.setText("");
		consoleTa.setText("");
		System.out.format("Hledání odpovědi na otázku: %s\n", question);
		final Runnable r = new Runnable() {
			@Override
			public void run() {
				searchTf.setEnabled(false);
				searchBt.setVisible(false);
				interruptBt.setVisible(true);
				
				try {
					Answer bestAnswer = app.findBestAnswer(question);
					if (bestAnswer != null) {
						shortAnswerTf.setText(bestAnswer.toString());
						String context = bestAnswer.getContext();
						if (context != null)
							longAnswerTa.setText(bestAnswer.getContext());
					}
				} catch (LaseException e) {
					JOptionPane.showMessageDialog(LaseGui.this, e.getMessage(),
							"Při hledání odpovědi nastala chyba",
							JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} catch (InterruptedException e) {
					interruptBt.setEnabled(true);
				} finally {
					searchTf.setEnabled(true);
					interruptBt.setVisible(false);
					searchBt.setVisible(true);
				}
			}
		};
		appThread = new Thread(r);
		appThread.start();
	}

	/**
	 * Přeruší vyhledávání. Zastaví aplikaci LASE a znepřístupní tlačítko
	 * "Přerušit". Volání {@link LaseApp#stop()} je neblokující, ale než ke
	 * skutečnému nastavení dojde může chvíli trvat. Teprve potom GUI opět
	 * připraveno pro nové hledání.
	 */
	private void interruptSearch() {
		app.stop();
		interruptBt.setEnabled(false);
	}

	/**
	 * Vypíše informace o vyhledávacím dotazu ve chvíli, kdy systém LASE zahájí
	 * vyhledávaní na webu.
	 */
	@Override
	public void webSearchStarted(String[] searchStrings) {
		System.out.println("Vyhledávací dotaz: "
				+ String.format("\"%s\"", StringUtils.concat(searchStrings,
						"\" \"")));
	}

	/**
	 * Vypíše informaci o nalezení kandidáta na odpověď.
	 */
	@Override
	public void answerCandidateFound(Answer candidate) {
		System.out.format("Nalezen kandidát: %s\n", candidate);
	}

	/**
	 * Vypíše seznam odpovědí seřazený podle pravděpodobnosti jejich správnosti.
	 * Tato metoda je volána systémem LASE po dokončení vyhledávání a seřazení
	 * odpovědí.
	 */
	@Override
	public void answersSorted(List<AnswerGroup> groups) {
		System.out.println("Odpovědi podle pravděpodobnosti:");
		for (AnswerGroup ag : groups) {
			System.out.format("  %-40s %3d\n", ag.getBestAnswer(), ag.size());
		}
	}
	
	@Override
	public void corpusRetrieved(Corpus corpus, SearchPattern pattern) {
		// žádná akce
		
	}

	/**
	 * Vytvoří grafické komponenty a rozmístí ja na formuláři.
	 */
	private void initControls() {
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		getContentPane().setLayout(layout);

		JLabel searchLb = new JLabel("Zadejte otázku:");
		searchLb.setDisplayedMnemonic(KeyEvent.VK_O);
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(10, 10, 5, 5);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		layout.setConstraints(searchLb, constraints);
		getContentPane().add(searchLb);

		searchTf = new JTextField();
		searchLb.setLabelFor(searchTf);
		searchTf.addKeyListener(this);
		searchTf.addFocusListener(this);
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(10, 5, 5, 5);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;
		layout.setConstraints(searchTf, constraints);
		getContentPane().add(searchTf);

		searchBt = new JButton("Hledat");
		searchBt.setMnemonic(KeyEvent.VK_H);
		searchBt.addActionListener(this);
		constraints.gridx = 2;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(10, 5, 5, 10);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		layout.setConstraints(searchBt, constraints);
		getContentPane().add(searchBt);

		interruptBt = new JButton("Přerušit");
		interruptBt.setMnemonic(KeyEvent.VK_P);
		interruptBt.addActionListener(this);
		constraints.gridx = 2;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(10, 5, 5, 10);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		layout.setConstraints(interruptBt, constraints);
		getContentPane().add(interruptBt);

		GridBagLayout answerPnLayout = new GridBagLayout();
		JPanel answerPn = new JPanel(answerPnLayout);
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.gridwidth = 3;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(10, 5, 5, 10);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		layout.setConstraints(answerPn, constraints);
		getContentPane().add(answerPn);

		JLabel shortAnswerLb = new JLabel("Krátká odpověď:");
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(10, 5, 5, 10);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		answerPnLayout.setConstraints(shortAnswerLb, constraints);
		answerPn.add(shortAnswerLb);

		shortAnswerTf = new JTextField();
		shortAnswerTf.setEditable(false);
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(0, 5, 5, 5);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 5.0;
		constraints.weighty = 0.0;
		answerPnLayout.setConstraints(shortAnswerTf, constraints);
		answerPn.add(shortAnswerTf);

		JLabel longAnswerLb = new JLabel("Kontextová odpověď:");
		constraints.gridx = 0;
		constraints.gridy = 2;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(10, 5, 5, 10);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		answerPnLayout.setConstraints(longAnswerLb, constraints);
		answerPn.add(longAnswerLb);

		longAnswerTa = new JTextArea();
		longAnswerTa.setEditable(false);
		longAnswerTa.setRows(3);
		longAnswerTa.setBorder(shortAnswerTf.getBorder());
		longAnswerTa.setBackground(shortAnswerTf.getBackground());
		constraints.gridx = 0;
		constraints.gridy = 3;
		constraints.gridwidth = 4;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(0, 5, 5, 5);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;
		answerPnLayout.setConstraints(longAnswerTa, constraints);
		answerPn.add(longAnswerTa);

		consoleTa = new JTextArea();
		consoleTa.setBackground(Color.BLACK);
		consoleTa.setForeground(Color.GREEN);
		consoleTa.setFont(Font.decode(Font.MONOSPACED));
		JScrollPane consoleSp = new JScrollPane(consoleTa,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		constraints.gridx = 0;
		constraints.gridy = 2;
		constraints.gridwidth = 3;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(10, 5, 5, 5);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 0.0;
		constraints.weighty = 1.0;
		layout.setConstraints(consoleSp, constraints);
		getContentPane().add(consoleSp);

	}

	/**
	 * Neprovádí žádnou akci.
	 */
	@Override
	public void keyPressed(KeyEvent e) {
	}

	/**
	 * Neprovádí žádnou akci.
	 */
	@Override
	public void keyReleased(KeyEvent e) {
	}

	/**
	 * Neprovádí žádnou akci.
	 */
	@Override
	public void keyTyped(KeyEvent e) {
	}

	/**
	 * Neprovádí žádnou akci.
	 */
	@Override
	public void focusGained(FocusEvent e) {
	}

	/**
	 * Neprovádí žádnou akci.
	 */
	@Override
	public void focusLost(FocusEvent e) {
	}

	/**
	 * Obsluhy kliknutí na tlačítka "Hledat" a "Přerušit".
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == searchBt) {
			String question = searchTf.getText().trim();
			if (!question.isEmpty()) {
				findAnswer(question);
			}
		} else if (source == interruptBt) {
			interruptSearch();
		}
	}

	/**
	 * Hlavní metoda desktopové verze uživatelského rozhraní LASE. Zajistí
	 * načtení cache ze souboru, vytvoří shutdown hook pro uložení cache při
	 * ukončení, vytvoří a zobrazí GUI LASE.
	 * 
	 * @param argv
	 *            Nepoužity.
	 */
	public static void main(String[] argv) {
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.INFO);
		
		// Načtení cache
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {
			File f = new File(Settings.cachePath);
			if (f.exists()) {
				fis = new FileInputStream(f);
				ois = new ObjectInputStream(fis);
				Cache.setCacheSerializationObject(ois.readObject());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ois != null)
					ois.close();
				else if (fis != null)
					fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// Shutdown hook pro uložení cache
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				
				FileOutputStream fos = null;
				ObjectOutputStream oos = null;
				try {
					fos = new FileOutputStream(Settings.cachePath);
					oos = new ObjectOutputStream(fos);
					oos.writeObject(Cache.getCacheSerializationObject());
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (oos != null)
							oos.close();
						else if (fos != null)
							fos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
		try {
			LaseGui gui = new LaseGui();
			gui.setVisible(true);
			gui.setDefaultCloseOperation(EXIT_ON_CLOSE);
		} catch (LaseException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(),
					"Inicializace aplikace LASE se nezdařila.",
					JOptionPane.ERROR_MESSAGE);
		}
	}

}
