package lase.servlet;

import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import lase.answersearch.Answer;
import lase.answersearch.AnswerGroup;
import lase.app.LaseApp;
import lase.app.LaseAppCallback;
import lase.cache2.Cache;
import lase.template.SearchPattern;
import lase.util.Corpus;
import lase.util.IterationUtils;
import lase.util.LaseException;
import lase.util.ProgressListener;

import org.apache.log4j.Logger;

/**
 * Fronta, do které se řadí požadavky na nalezení odpovědi při použití webového
 * rozhraní systému LASE.
 * 
 * @author Rudolf Šíma
 * 
 */
public class RequestQueue {

	/**
	 * Čas v milisekundách, po které je dokončený požadavek vyřazen z mapy
	 * požadavků. Vyřazení v praxi znamená, že pokud uživatel ručně obnoví
	 * stránku s nalezenou odpovědí, nebude zobrazena ta samá stránky, ale jen
	 * výchozí vyhledávací stránka.
	 */
	private static final long requestMapTimeoutMilis = 120000;
	
	/**
	 * Jediná instance třídy {@code RequestQueue}.
	 */
	private static RequestQueue instance;
	
	/**
	 * První nepoužité číslo požadavku, které bude přiděleno
	 * příštímu požadavku.
	 */
	private static int nextSeqNo;

	/**
	 * Aplikace LASE
	 */
	private LaseApp app;

	/**
	 * Objekt sloužící ke shromažďování nalezených odpovědí, které LASE průběžně
	 * oznamuje zpětným voláním.
	 */
	private CallbackCollector collector;
	
	/**
	 * Fronta požadavků na vyhledání otázky.
	 */
	private BlockingQueue<Request> queue;

	/**
	 * Vlákno, ve kterém se spouští vyhledávání otázky systémem LASE.
	 */
	private SearchThread searchThread;
	
	/**
	 * Mapa požadavků na vyhledání otázky podle jejich ID.
	 */
	private Map<Integer, Request> requestsById;
	
	/**
	 * Počet požadavků čekajících na vyřízení.
	 */
	private int waitingCount;

	/**
	 * Fronta záznamů odpovídajících vyřízeným požadavků, které mají být
	 * vyřazeny z mapy po vypršení časového limitu.
	 */
	private Queue<MapEntry> mapTimeoutQueue;

	/**
	 * V případě prvníh volání vytvoří, a pak už jen vrátí, instanci jedinou
	 * třídy {@link RequestQueue}.
	 * 
	 * @return Vrací jedinou instanci třídy {@link RequestQueue}.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že při vytvoření instance došlo k chybě.
	 */
	public static RequestQueue getInstance() throws LaseException {
		if (instance == null) {
			instance = new RequestQueue();
		}
		return instance;
	}
	
	/**
	 * Ukončí případné probíhající vyhledávání a zruší instanci třídy
	 * {@link RequestQueue}.
	 */
	public static void close() {
		instance.searchThread.close();
		instance = null;
	}

	/**
	 * Zjistí velikost velikost fronty, tj. počet požadavků čekajících na
	 * vyřízení.
	 * 
	 * @return Vrací velikost fronty.
	 */
	public int getQueueSize() {
		return queue.size();
	}

	/**
	 * Privátní konstruktor. Při prvním volání {@link #getInstance()} vytvoří
	 * jedinou instanci fronty požadavků na vyhledání otázky.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že při vytváření fronty došlo k chybě.
	 */
	private RequestQueue() throws LaseException {
		collector = new CallbackCollector();
		app = new LaseApp(collector);
		queue = new LinkedBlockingQueue<Request>();
		waitingCount = queue.size();
		requestsById = new TreeMap<Integer, Request>();
		mapTimeoutQueue = new ArrayDeque<MapEntry>();
		searchThread = new SearchThread();
		searchThread.start();
	}

	/**
	 * Vytvoří nový požadavek na vyhledání otázky {@code question} a zařadí jej
	 * do fronty požadavků.
	 * 
	 * @param question
	 *            Otázka, které má být systémem LASE zodpovězena.
	 *            
	 * @return Vytvořený požadavek, který byl zařazen do fronty.
	 */
	public Request newRequest(String question) {
		Request newReq = new Request(question);
		queue.add(newReq);
		synchronized (instance) {
			waitingCount++;
		}
		requestsById.put(newReq.getId(), newReq);
		return newReq;
	}

	/**
	 * Vrací požadavek podle jeho ID z mapy požadavků čekajících na vyřízení
	 * nebo vyřízených, které ještě nebyly z mapy odstraněny z důvodu vypršení
	 * časového limitu dvou minut.
	 * 
	 * @param id
	 *            ID hledaného požadavku.
	 *            
	 * @return Vrací požadavek z mapy požadavků podle jeho ID.
	 */
	public Request getRequestById(int id) {
		synchronized (instance) {
			
			// Před vrácením požadavku jsou z mapy vyřazeny všechny požadavky, které
			// již vytikaly.
			while (!mapTimeoutQueue.isEmpty() && 
					mapTimeoutQueue.peek().timestamp +
					requestMapTimeoutMilis < System.currentTimeMillis()) {
				int reqId = mapTimeoutQueue.poll().requestId;
				requestsById.remove(reqId);
			}
			
			// Vrací požadavek, o který volající stál.
			return requestsById.get(id);
		}
	}

	/**
	 * Vlákno, ve kterém se provádí vlastní vyhledávání odpovědi systémem LASE.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	private class SearchThread extends Thread {
		
		/**
		 * Zastavovací podmínka, kterou volání {@link #close()} nastaví na
		 * {@code false}.
		 */
		private volatile boolean running = true;

		/**
		 * Spustí vyřizování požadavků na vyhledávání odpovědi. Vybírá požadavky
		 * z blokující fronty {@link #queue} postupně je vyřizuje. Vyřízen-
		 * požadavky ukládá do fronty {@link #mapTimeoutQueue} a dekrementuje
		 * {@link #waitingCount}.
		 */
		@Override
		public void run() {
			while (running) {
				Request curReq = null;
				try {
					curReq = queue.take();
					
					synchronized (Cache.class) {
						// načtení cache 
//						try {
//							DatabaseLogger logger = DatabaseLogger.getInstance();
//							Object cache = logger.readCache();
//							if (cache != null) {
//								Cache.setCacheSerializationObject(cache);
//								Logger.getLogger(getClass()).info("Cache read from the database.");
//							} else {
//								Logger.getLogger(getClass()).info("No cache found in the database.");
//							}
//						} catch (SQLException e) {
//							Logger.getLogger(getClass()).warn("Error reading cache from the database.",e);
//						}
						
						// vyhledání odpovědi
						app.findBestAnswer(curReq.question, curReq.progressListener);
						
						// uložení cache
//						if (Cache.modified()) {
//							try {
//								DatabaseLogger logger = DatabaseLogger.getInstance();
//								logger.storeCache(Cache.getCacheSerializationObject());
//								Cache.setModified(false);
//								Logger.getLogger(getClass()).info("Cache stored into the database.");
//							} catch (SQLException e) {
//								Logger.getLogger(getClass()).warn("Error writing cache to database.", e);
//							}
//						} else {
//							Logger.getLogger(getClass()).info("Cache not modified.");
//						}
					}

					curReq.searchResult = new LaseSearchResult(collector.readAnswerGroups(),
							curReq.question);
					
					synchronized(instance) {
						waitingCount--;
						mapTimeoutQueue.offer(new MapEntry(curReq.getId()));
					}
				} catch (LaseException e) {
					curReq.setException(e);
					continue;
				} catch (InterruptedException e) {
					// vyhledávání přerušeno
				}
			}
		}
		
		/**
		 * Ukončí vyhledávání nastavením zastavovací podmínky a přerušení
		 * vyhledávacího vlákna
		 */
		public void close() {
			running = false;
			searchThread.interrupt();
		}
	}

	/**
	 * Požadavek na vyhedání odpovědi systémem LASE. Před vyhledáním odpovědi
	 * tyto požadavky slouží ke zjištění stavu čekání (pořadí ve frontě, průběhu
	 * nalezených kandidátů), po nalezení požadavku slouží k získání výsledků
	 * hledání.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	public class Request {
		
		
		/**
		 * Otázka, kterou se systém LASE snaží zodpovědět.
		 */
		private String question;

		/**
		 * Výsledky hledání. {@code null, pokud ještě nebylo vyhledávání
		 * dokončeno.}
		 */
		private LaseSearchResult searchResult;

		/**
		 * Případná výjimka, které mohla při hledání nastat. Je uchováná v této
		 * proměnné a znovu vyhozena v případě volání metod
		 * {@link #searchFinished()} nebo {@link #getResult()}.
		 */
		private LaseException exception;
		
		/**
		 * Pořadové číslo tohoto požadavku plnící úlohu ID požadavku.
		 */
		private int seqNo;

		/**
		 * Průběh vyhledávání od 0.0 do 1.0 průběžně se měnící během vyhledávání
		 * odpovědi. Před započetím vyhledávání, když požadavek čeká ve frontě,
		 * je stále 0.0.
		 */
		private double progress;

		/**
		 * Posluchač průběhu, kterém se zjišťuje velikost dokončené části
		 * vyhledávání odpovědi systémem LASE.
		 */
		private ProgressListener progressListener = new ProgressListener() {
			
			/* (non-Javadoc)
			 * @see lase.util.ProgressListener#progressChanged(double)
			 */
			@Override
			public void progressChanged(double value) {
				progress = value; 
			}
		};

		/**
		 * Privátní konstruktor. Je volán metodou
		 * {@link RequestQueue#newRequest(String)}. Vytvoří nový požadavek na
		 * zodpovězení dané otázky systémem LASE.
		 * 
		 * @param question
		 *            Otázka, která má být zodpovězena.
		 */
		private Request(String question) {
			this.question = question;
			synchronized (instance) {
				this.seqNo = nextSeqNo++;
			}
		}

		/**
		 * Vrací pořadí ve frontě požadavků. Hodnota 0 znamená, že je požadavek
		 * právě vyřizován. Záporná hodnota značí, že byl již vyřízen.
		 * 
		 * @return Vrací pořadí ve frontě požadavků, nulu pro vyřizující se
		 *         požadavek a zápornou hodnotu pro již vyřízený požadavek.
		 */
		public int getSequenceNumber() {
			synchronized (instance) {
				return waitingCount - nextSeqNo + seqNo;
			}
		}

		/**
		 * Vrací průběh vyhledávání vyjádřený číslem v intervalu 0.0 až 1.0. řed
		 * započetím vyhledávání, když požadavek čeká ve frontě, je stále 0.0,
		 * po dokončení vyhledávání má hodnotu 1.0.
		 * 
		 * @return Vrací průběh vyhledávání vyjádřený číslem v intervalu 0.0 až 1.0.
		 */
		public double getProgress() {
			return progress;
		}

		/**
		 * Vrací ID požadavku.
		 * 
		 * @return Vrací ID požadavku.
		 */
		public int getId() {
			return seqNo;
		}

		/**
		 * Zjistí, zda již bylo vyhledávání dokončeno a je možné získat výsledek
		 * voláním {@link #getResult()}.
		 * 
		 * @return Vrací {@code true}, bylo-li již hledání dokončeno. Jinak
		 *         vrací {@code false}.
		 * 
		 * @throws LaseException
		 *             Vyhozena v případě, že během vyhledávání došlo k vyhození
		 *             výjimky systémem LASE. Tato případná vyjímka je uchována
		 *             a znovu vyhozena při volání této metody.
		 */
		public synchronized boolean searchFinished() throws LaseException {
			checkException();
			return searchResult != null;
		}

		/**
		 * Vrací výsledek hledání odpovědi systémem LASE. Před voláním této
		 * metody je nutné se přesvědčit, že již bylo hledání dokončeno voláním
		 * metody {@link #searchFinished()}.
		 * 
		 * @return Vrací výsledek hledání.
		 * 
		 * @throws LaseException
		 *             Vyhozena v případě, že během vyhledávání došlo k vyhození
		 *             výjimky systémem LASE. Tato případná vyjímka je uchována
		 *             a znovu vyhozena při volání této metody.
		 * @throws IllegalStateException
		 *             Vyhozena v případě volání této metody ve chvíli, kdy
		 *             ještě nebylo dokončeno vyhledávání, tzn. požadavek čeka
		 *             ve froně nebo je právě vyřizován.
		 */
		public synchronized LaseSearchResult getResult() throws LaseException {
			checkException();
			if (searchResult == null)
				throw new IllegalStateException("Search not finished yet.");
			return searchResult;
		}

		/**
		 * Zjistí, zda je nastavena systémem LASE dříve vyhozená výjimka. Pokud
		 * ano, vyhodí ji znovu.
		 * 
		 * @throws LaseException
		 *             Vyhozena v případě, že je v proměnné {@link #exception}
		 *             uložena dříve vyhozená výjimka.
		 */
		private void checkException() throws LaseException {
			if (exception != null)
				throw exception;
		}

		/**
		 * Nastaví výjimku, která bude vyhozena při příštím volání metody
		 * {@link #searchFinished()} nebo {@link #getResult()}.
		 * 
		 * @param e
		 *            Výjimka, která bude připravena pro vhození.
		 */
		private void setException(LaseException e) {
			if (exception == null)
				exception = e;
		}
	}
	
	/**
	 * Výsledek hledání odpovědi systémem LASE.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	public static class LaseSearchResult implements Iterable<AnswerGroup> {
		
		/**
		 * Seznam nalezených skupin sémanticky shodných odpovědí žazený od
		 * nejlepší k nejhoší.
		 */
		private List<AnswerGroup> groups;
		
		/**
		 * Otázka, na kterou byla hledána odpověď.
		 */
		private String question;

		/**
		 * Vytvoří nový výsledek hledání odpovědi systémem LASE a nastaví
		 * výsledky.
		 * 
		 * @param groups
		 *            Seznam nalezených skupin sémanticky shodných odpovědí
		 *            žazený od nejlepší k nejhoší.
		 * @param question
		 *            Otázka, na kterou byla hledána odpověď.
		 */
		public LaseSearchResult(List<AnswerGroup> groups, String question) {
			if (groups == null)
				throw new NullPointerException();
			this.groups = groups; 
			this.question = question;
		}

		/**
		 * Zjistí, zda odpověď <strong>ne</strong>byla nalezena.
		 * 
		 * @return Vrací {@code true}, <strong>ne</strong>byla-li odpověd
		 *         nalezena. Byla-li odpověď nalezena, vrací {@code false}.
		 */
		public boolean isNotFound() {
			return groups.isEmpty();
		}

		/**
		 * Vrací otázku, na kterou byla hledána odpověď.
		 * 
		 * @return Vrací otázku, na kterou byla hledána odpověď.
		 */
		public String getQuestion() {
			return question;
		}

		/**
		 * Vrací nejlepší nalezenou odpověď. Před voláním této metody je třeba
		 * se přesvědčit, zda nějaká odpověď vůbec nalezena byla voláním
		 * {@link #isNotFound()}.
		 * 
		 * @return Vrací nejlepší nalezenou odpověď.
		 * 
		 * @throws IllegalStateException
		 *             vyhozena v případ, že nebyla nalezena žádná odpověď.
		 */
		public Answer getBestAnswer() {
			if (groups.isEmpty())
				throw new IllegalStateException("No answers were found.");
			return groups.get(0).getBestAnswer();
		}

		/**
		 * Vrací nalezených skupin odpověďí seřazených od nejlepší k nejhoší.
		 */
		@Override
		public Iterator<AnswerGroup> iterator() {
			return IterationUtils.readOnlyIterator(groups.iterator());
		}
	}

	/**
	 * Záznam v fronty vyřízených požadavků čekajících na vypršení časového limitu
	 * sloužící k určení záznamu v mapě požadavků, který bude po vypršení tohoto
	 * limitu odstraněn.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	private static class MapEntry {

		/**
		 * Časové razítko okamžiku zařazení tohoto záznamu do fronty požadavků
		 * čekajících na vypršení časového limitu.
		 */
		long timestamp;
		
		/**
		 * ID požadavku, které bude po vypršení časového limitu z fronty odstraněn.
		 */
		int requestId;

		/**
		 * Vytvoří nový záznam fronty požadavků čekajících na odstranění z mapy
		 * požadavků a nastaví aktuální časové razítky.
		 * 
		 * @param requestId
		 *            ID požadavku, které bude po vypršení časového limitu
		 *            odstraněn.
		 */
		public MapEntry(int requestId) {
			this.timestamp = System.currentTimeMillis();
			this.requestId = requestId;
		}
	}

	/**
	 * Implementace rozhraní {@link LaseAppCallback} sloužící ke shromáždění
	 * všech odpovědí nalezených systémem LASE, které jsou jsou předávány
	 * zpětným voláním.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	private static class CallbackCollector implements LaseAppCallback {
		
		/**
		 * Seznam shormážděných nalezených odpovědí.
		 */
		private List<AnswerGroup> answerGroups;

		/**
		 * Neprovádí žádnou činnost. Tato událost je v případě webové aplikace
		 * ignorována.
		 */
		@Override
		public void webSearchStarted(String[] searchStrings) {
		}
		
		/**
		 * Neprovádí žádnou činnost. Tato událost je v případě webové aplikace
		 * ignorována.
		 */
		@Override
		public void answerCandidateFound(Answer candidate) {
		}

		/**
		 * Neprovádí žádnou činnost. Tato událost je v případě webové aplikace
		 * ignorována.
		 */
		@Override
		public void answersSorted(List<AnswerGroup> groups) {
			answerGroups = groups;
		}

		/**
		 * Přečte a smaže seznam shromážděných odpovědí.
		 * 
		 * @return Vrací seznam shromážděných odpovědí.
		 */
		public List<AnswerGroup> readAnswerGroups() {
			List<AnswerGroup> groups = answerGroups;
			answerGroups = null;
			return groups;
		}

		/**
		 * Neprovádí žádnou činnost. Tato událost je v případě webové aplikace
		 * ignorována.
		 */
		@Override
		public void corpusRetrieved(Corpus corpus, SearchPattern pattern) {
			// TODO Auto-generated method stub
			
		}
	}
}
