package lase.servlet;

import java.io.Serializable;

/**
 * JavaBean s odpovědí nalezenou systémem LASE určený pro zobrazení odpovědi na
 * webové stránce.
 * 
 * @author Rudolf Šíma
 * 
 */
public class AnswerBean implements Serializable {
	private static final long serialVersionUID = 7590379439616421151L;
	
	
	/**
	 * Krátká forma nalezené odpovědi.
	 */
	private String shortAnswer;
	
	/**
	 * Dlouhá forma nalezení odpovědi.
	 */
	private String longAnswer;
	
	/**
	 * URL stránky, ze které nalezená odpověď pochází.
	 */
	private String sourceUrl;
	
	/**
	 * Titulek stránky, ze které nalezená odpověď pochází.
	 */
	private String sourceTitle;
	
	/**
	 * Udává, zda je obsah této třídy platný.
	 */
	private boolean valid;
	
	/**
	 * Udává, že odpověď nebyla nalezena.
	 */
	private boolean notFound;
	
	/**
	 * Vytvoří novou instancí {@code AnswerBean}.
	 */
	public AnswerBean() {
	}

	/**
	 * Vrací {@code true} nebyla-li odpověď nalezena. Byla-li nalezena, vrací
	 * {@code false}.
	 * 
	 * @return Vrací {@code true} nebyla-li odpověď nalezena. Byla-li nalezena,
	 *         vrací {@code false}.
	 */
	public boolean isNotFound() {
		return notFound;
	}

	/**
	 * Nastaví hodnotu udávající, zda odpověď NEbyla nalezena ({@code true}
	 * znamená, že NEbyla).
	 * 
	 * @param notFound
	 *            Hodnota udávající, zda odpověď NEbyla nalezena.
	 */
	public void setNotFound(boolean notFound) {
		this.notFound = notFound;
	}

	/**
	 * Vrací krátkou formu nalezené odpovědi.
	 * 
	 * @return Vrací krátkou formu nalezené odpovědi.
	 */
	public String getShortAnswer() {
		return shortAnswer;
	}

	/**
	 * Nastaví krátkou formu odpovědi.
	 * 
	 * @param shortAnswer
	 *            Nová krátká forma odpovědi.
	 */
	public void setShortAnswer(String shortAnswer) {
		this.shortAnswer = shortAnswer;
	}

	/**
	 * Vrací dlouhou formu nalezené odpovědi.
	 * 
	 * @return Vrací formu odpovědi.
	 */
	public String getLongAnswer() {
		return longAnswer;
	}

	/**
	 * Nastaví dlouhou formu nalezené odpovědi.
	 * 
	 * @param longAnswer
	 *            Nová dlouhá forma nalezené odpovědi.
	 */
	public void setLongAnswer(String longAnswer) {
		this.longAnswer = longAnswer;
	}

	/**
	 * Vrací URL stránky, na které byla odpověď nalezena.
	 * 
	 * @return Vrací URL stránky, na které byla odpověď nalezena.
	 */
	public String getSourceUrl() {
		return sourceUrl;
	}

	/**
	 * Nastaví URL stránky, na které byla odpověď nalezena.
	 * 
	 * @param sourceUrl
	 *            URL stránky na které byla odpověď nalezena.
	 * 
	 */
	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	/**
	 * Vrací titulek stránky na které byla odpověď nalezena.
	 * 
	 * @return Vrací titulek stránky na které byla odpověď nalezena.
	 */
	public String getSourceTitle() {
		return sourceTitle;
	}

	/**
	 * Nastaví titulek stránky, na které byla odpověď nalezena.
	 * 
	 * @param sourceTitle
	 *            Nový titulek stránky, na které byla odpověď nalezena.
	 */
	public void setSourceTitle(String sourceTitle) {
		this.sourceTitle = sourceTitle;
	}

	/** Zjistí, zda je obsah této třídy platný.
	 * 
	 * @return Vrací {@code true}, je-li obsah této třídy platný. Jinak vrací {@code false}.
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * Nastaví hodnotu udávající, zda je obsah této třídy platný.
	 * 
	 * @param valid
	 *            Hodnota udávající, zda je obsah této třídy platný.
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}
}
