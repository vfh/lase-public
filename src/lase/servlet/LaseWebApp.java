package lase.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lase.servlet.RequestQueue.Request;
import lase.util.LaseException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Hlavní servlet webové aplikace LASE.
 * 
 * @author Rudolf Šíma
 *
 */
 public class LaseWebApp extends HttpServlet  {
	private static final long serialVersionUID = 1L;
	
	static {
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.INFO);
	}
	
	/**
	 * Vytvoří novou instanci servletu.
	 */
	public LaseWebApp() {
	}

	/**
	 * Je-li v {@code httpRequest} zadán parametr s otázkou, provede vyhledání
	 * odpovědi a odešle data stránce k zobrazení výsledků resp. čekání na
	 * výsledky. Jinak zobrazí výchozí vyhledávací stránku.
	 */
	@Override
	protected void doGet(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws ServletException, IOException {
		ServletUtils.setUnicode(httpRequest, httpResponse);
		ServletUtils.chdirToWebContent(this);
		
		RequestQueue requestQueue;
		try {
			requestQueue = RequestQueue.getInstance();
		} catch (LaseException e) {
			ServletUtils.throwUp(e);
			return;
		}
		
		String question = httpRequest.getParameter("q");
		if (question != null) {
			Request request = requestQueue.newRequest(question);
			ServletUtils.dispatch("/results?request=" + request.getId(),
					this, httpRequest, httpResponse);
			return;
		}
		
		ServletUtils.dispatch("/index.jsp", this, httpRequest, httpResponse);
	}

	/**
	 * Alias metody {@link #doGet(HttpServletRequest, HttpServletResponse)}.
	 * 
	 * @see #doGet(HttpServletRequest, HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}