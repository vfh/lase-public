package lase.servlet;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import lase.answersearch.Answer;
import lase.answersearch.AnswerGroup;
import lase.servlet.RequestQueue.LaseSearchResult;
import lase.util.Corpus.SourcePage;

/**
 * JavaBean obsahující výsledky hledání, tj. seznam všech nalezených odpovědí seřazený podle kvality, 
 * a zadanou otázku.
 *   
 * @author Rudolf Šíma
 *
 */
public class ResultsBean extends AbstractCollection<AnswerBeanList> {

	/**
	 * Seznam nalezených odpovědí seřazený podle kvality, tzn. první odpověď
	 * byla systémem LASE vyhodnocena jako nejlepší.
	 */
	public List<AnswerBeanList> answers;
	
	/**
	 * Otázka, kterou se systém pokoušel zodpovědět.
	 */
	private String question;

	/**
	 * Udává, zda tento bean obsahuje platné údaje, které mohou být zobrazeny.
	 */
	private boolean valid;
	
	/**
	 * Vytvoří novou prázdnou instanci výsledků hledání.
	 */
	public ResultsBean() {
		answers = new ArrayList<AnswerBeanList>(0);
		question = "";
		valid = false;
	}

	/**
	 * Vytvoří novou instanci a nastaví výsledky.
	 * 
	 * @param searchResult
	 *            Výsledk hledání.
	 */
	public ResultsBean(LaseSearchResult searchResult) {
		answers = new ArrayList<AnswerBeanList>(0);
		for (AnswerGroup ag : searchResult) {
			List<AnswerBean> beanList = new ArrayList<AnswerBean>();
			for (Answer a : ag.getAnswers()) {
				AnswerBean bean = createAnswerBean(a); 
				beanList.add(bean);
			}
			answers.add(new AnswerBeanList(beanList));
		}
		question = searchResult.getQuestion();
		valid = true;
	}

	/**
	 * Vrací {@code true} obsahují-li tyto výsledky platné údaje, které mohou
	 * být zobrazeny, jinak brací {@code false}.
	 * 
	 * @return Vrací {@code true} obsahují-li tyto výsledky platné údaje, které
	 *         mohou být zobrazeny, jinak brací {@code false}.
	 */
	public boolean isValid() {
		return valid;
	}
	
	/* (non-Javadoc)
	 * @see java.util.AbstractCollection#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return answers.isEmpty();
	}

	/**
	 * Vrací otázkou, kterou se systém pokoušel zodpovědět.
	 * 
	 * @return Vrací zodpovídanou otázku.
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * Vrací nejlepší skupinu odpovědí, tj. seznam všech nalezených shodných
	 * odpovědí, které byly vyhodnoceny jako nejlepší. (Odpovědi v seznamu se
	 * liší pouze svojí přesností.)
	 * 
	 * @return Vrací nejlepší skupinu odpovědí.
	 */
	public AnswerBeanList getBestGroup() {
		if (answers.isEmpty())
			throw new NoSuchElementException();
		return answers.get(0);
	}

	/**
	 * Vrací nejpravdivější a nejpřesnější nalezenou odpověď.
	 * 
	 * @return Vrací nejlepší odpověď.
	 */
	public AnswerBean getBestAnswer() {
		return getBestGroup().getBestAnswer();
	}

	/**
	 * Vytvoří {@link AnswerBean} na základě odpovědi v instanci třídy
	 * {@link Answer}, kterou vrátí systém LASE jako výsledek hledání.
	 * 
	 * @param answer
	 *            Výsledek hledání odpovědi systémem LASE.
	 * 
	 * @return Vrací odpověď nalezenou systémem laze jako instanci třídy
	 *         {@link AnswerBean}.
	 */
	private static AnswerBean createAnswerBean(Answer answer) {
		AnswerBean answerBean = new AnswerBean();
		if (answer == null) {
			answerBean.setNotFound(true);
		} else {
			SourcePage source = answer.getSource();
			answerBean.setSourceUrl(source.getUrl().toString());
			answerBean.setSourceTitle(source.getTitle());
			answerBean.setShortAnswer(answer.toString());
			answerBean.setLongAnswer(answer.getContext());
			answerBean.setValid(true);
		}
		return answerBean;
	}

	/**
	 * Vrací iterátor skupin sémanticky shodných odpovědí.
	 */
	@Override
	public Iterator<AnswerBeanList> iterator() {
		return answers.iterator();
		//return IterationUtils.readOnlyIterator(answers.iterator()); 
	}

	/**
	 * Vrací počet obsažených skupin sémanticky shodných shodných odpovědí.
	 */
	@Override
	public int size() {
		return answers.size();
	}
}
