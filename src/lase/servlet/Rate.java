package lase.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lase.servlet.DatabaseLogger.AnswerRating;

import org.apache.log4j.Logger;

/**
 * Servlet provádějící uložení výsledků uživatelského hodnocení do databáze.
 * 
 * @author Rudolf Šíma
 * 
 */
public class Rate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Mapa možných hodnoty parametru {@code r} udávajícího, jaké hodnocení
	 * uživatel zadan na hodnoty výčtu, které se zapisují do databáze.
	 */
	private static final Map<String, AnswerRating> ratings;
	static {
		ratings = new HashMap<String, DatabaseLogger.AnswerRating>();
		ratings.put("T", AnswerRating.TRUE);
		ratings.put("F", AnswerRating.FALSE);
		ratings.put("0", AnswerRating.DONTKNOW);
		ratings.put("OT", AnswerRating.OTHERTRUE);
	}
	
    /**
     * Vytvoří novou instanci servletu.
     */
    public Rate() {
    }

    /**
     * Provede zapsání hodnocení do databáze prostřednictvím třídy {@link DatabaseLogger}.
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletUtils.setUnicode(request, response);
		
		String question = request.getParameter("q");
		String answer = request.getParameter("a");
		AnswerRating rating = ratings.get(request.getParameter("r"));

		if (question != null && answer != null && rating != null) {
			try {
				DatabaseLogger logger = DatabaseLogger.getInstance();
				logger.addRecord(question, answer, rating);
			} catch (SQLException e) {
				Logger.getLogger(getClass()).error("The database logger failed to perform a database operation.", e);
			}
		}
		
		response.sendRedirect("./");
	}

	/**
	 * Alias metody {@link #doGet(HttpServletRequest, HttpServletResponse).
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
