package lase.servlet;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import lase.util.IterationUtils;

/**
 * Seznam nalezených odpovědí ve formě objektů {@link AnswerBean}.
 * 
 * @author Rudolf Šíma
 * 
 */
public class AnswerBeanList extends AbstractCollection<AnswerBean> {
	private List<AnswerBean> list;
	
	/**
	 * Vytvoří nový prázdný seznam odpovědí.
	 */
	public AnswerBeanList() {
		list = new ArrayList<AnswerBean>(0);
	}

	/**
	 * Vytvoří nový seznam odpovědí a nastaví jeho obsah.
	 * 
	 * @param list
	 *            Prvky seznamu odpovědí (nekopírují se) seřazeny podle
	 *            "kvality", tzn. první je nejlepší.
	 */
	public AnswerBeanList(List<AnswerBean> list) {
		this.list = list;		
	}

	/**
	 * Vrací nejlepší odpověď, tj. první ze seznamu odpovědí.
	 * 
	 * @return Vrací nejlepší odpověď.
	 */
	public AnswerBean getBestAnswer() {
		if (list.isEmpty())
			throw new NoSuchElementException();
		return list.get(0);
	}	
	
	/* (non-Javadoc)
	 * @see java.util.AbstractCollection#isEmpty()
	 */
	public boolean isEmpty() {
		return list.isEmpty();
	}

	/* (non-Javadoc)
	 * @see java.util.AbstractCollection#iterator()
	 */
	@Override
	public Iterator<AnswerBean> iterator() {
		return IterationUtils.readOnlyIterator(list.iterator());
	}

	/* (non-Javadoc)
	 * @see java.util.AbstractCollection#size()
	 */
	@Override
	public int size() {
		return list.size();
	}
}
