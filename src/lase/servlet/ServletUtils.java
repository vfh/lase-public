package lase.servlet;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lase.util.PathUtils;

/**
 * Utility pro řešení často se opakujících problémů v servletech.
 * 
 * @author Rudolf Šíma
 * 
 */
public class ServletUtils {

	/**
	 * Nastaví kódování UTF-8 pro request i response.
	 * 
	 * @param request
	 *            - parametr "request" z {@link HttpServlet#doGet} resp.
	 *            {@link HttpServlet#doPost}.
	 * @param response
	 *            - parametr "response" z {@link HttpServlet#doGet} resp.
	 *            {@link HttpServlet#doPost}.
	 */
	public static void setUnicode(HttpServletRequest request, HttpServletResponse response) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new AssertionError();
		}
		response.setCharacterEncoding("UTF-8");
	}

	/**
	 * Změní kódování parametru z ISO-8859-1 na UTF-8.
	 * 
	 * @param parameter
	 *            - Řetězec v kódování ISO-8859-1.
	 *            
	 * @return Vrací řetězec v kódování UTF-8.
	 */
	@Deprecated
	public static String tomcat55UnicodeWorkaround(String parameter) {
		if (parameter == null)
			return null;
		try {
			return new String(parameter.getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new AssertionError();
		}
	}
	
	/**
	 * Zavolá
	 * {@link RequestDispatcher#forward(javax.servlet.ServletRequest, javax.servlet.ServletResponse)}
	 * .
	 * 
	 * @param dispatchToUrl
	 *            - URL na kterou se request a response předá.
	 * @param servlet
	 *            - Volající servlet.
	 * @param request
	 *            - parametr "request" z {@link HttpServlet#doGet} resp.
	 *            {@link HttpServlet#doPost}.
	 * @param response
	 *            - parametr "response" z {@link HttpServlet#doGet} resp.
	 *            {@link HttpServlet#doPost}.
	 *            
	 * @throws ServletException
	 *             Pokud byla ServletException vyhozena při volání
	 *             <code>forward()</code>.
	 * @throws IOException
	 *             Pokud byla IOException vyhozena při volání
	 *             <code>forward()</code>.
	 */
	public static void dispatch(String dispatchToUrl, HttpServlet servlet, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext context = servlet.getServletConfig().getServletContext();
		RequestDispatcher dispatcher = context.getRequestDispatcher(dispatchToUrl);
		dispatcher.forward(request, response);
	}

	/**
	 * Nastaví aktuální adresář v {@link PathUtils} na adresář, do kterého se
	 * spuštění webové aplikace umístí obsah adresáře WebContent.
	 * 
	 * @param servlet
	 *            - Servlet, který tuto metodu volá.
	 * 
	 * @see PathUtils
	 */
	public static void chdirToWebContent(HttpServlet servlet) {
		String realPath = servlet.getServletContext().getRealPath("/");
		PathUtils.setParentDirectory(realPath);
	}

	/**
	 * Vytvoří výjimku {@link ServletException}, do které zapouzdří výjimku e, a
	 * vyhodí ji.
	 * 
	 * @param e
	 *            - Výjimka, který bude vložena do nové výjimky
	 *            {@link ServletException} jako její příčina.
	 *            
	 * @throws ServletException
	 *             Vyhozena vždy.
	 */
	public static void throwUp(Exception e) throws ServletException {
		throw new ServletException(e.getMessage(), e.getCause());
	}
}
