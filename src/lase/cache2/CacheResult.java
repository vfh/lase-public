package lase.cache2;

import java.io.Serializable;
import java.util.NoSuchElementException;

/**
 * <p>
 * Výsledek hledání v {@link Cache} vrácený metodou {@link Cache#get(Object...)}.
 * <p>
 * 
 * <p>
 * Deklaruje metody {@link #cacheHit()}, jejíž výsledek udává, zda byl
 * požadovaný záznam v cache nalezen. V případě, že vrací {@code true}, je možné
 * objekt z cache získat voláním metody {@link #getValue()}.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class CacheResult implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Data získaná hledáním v cache. Jsou-li {@code null}, je nebyl požadovaný
	 * záznam v cache nalezen.
	 */
	private Object data;

	/**
	 * Vícenásobný klíč, podle kterého bylo v cache hledáno. Je uložen pro
	 * případnou pozdější aktualizaci cache.
	 */
	private MultiKey key;
	
	/**
	 * Instance {@link Cache}, ze které byla data získána, resp. ve které nebyla
	 * nalezena.
	 */
	private Cache dataSource;

	/**
	 * Vytvoří nový výsledek hledání v Cache.
	 * 
	 * @param data
	 *            Objekt, který byl z cache získán. {@code null} v případě, že
	 *            požadovaný záznam nebyl nalezen.
	 * @param key
	 *            Vícenásobný klíč, na základě kterého bylo v cache hledáno.
	 * @param dataSource
	 *            Instance {@link Cache}, ze které byla data získána, resp. ve
	 *            které nebyla nalezena.
	 */
	CacheResult(Object data, MultiKey key, Cache dataSource) {
		this.dataSource = dataSource;
		this.data = data; 
		this.key = key;
	}

	/**
	 * Zjistí, zda byl požadovaný záznam v cache nalezen. Touto je vždy nutné
	 * ověřit, za bylo hledání úspěšné, než je možné volat {@link #getValue()},
	 * která by v případě neúspěšného hledání vyhodila
	 * {@link NoSuchElementException}.
	 * 
	 * @return Vrací {@code true}, byl-li požadovaný záznam v cache nalezen a je
	 *         tedy možné získat požadovaný objekt metodou {@link #getValue()}.
	 *         Jinak vrací {@code false}.
	 */
	public boolean cacheHit() {
		return data != null;
	}

	/**
	 * <p>
	 * Aktualizuje záznam v cache odpovídající klíči, který byl použit k hledání
	 * v cache voláním metody {@link Cache#get(Object...)}, jejímž výsledkem byl
	 * tento {@link CacheResult}.
	 * </p>
	 * 
	 * <p>
	 * V případě, že {@code cacheHit() == true} nahradí starou hodnotu novou
	 * hodnotou {@code newResult}. V opačném případě, kdy {@code cacheHit() ==
	 * false} je se do cache přidá nový záznam.
	 * </p>
	 * 
	 * @param newResult
	 */
	public void updateCache(Object newResult) {
		dataSource.updateCache(key, newResult);
	}

	/**
	 * <p>
	 * Vrací objekt nalezený v cache na základě identifikace metody v argumentu
	 * {@link Cache#get(Object...)}.
	 * </p>
	 * 
	 * <p>
	 * Před voláním této metody je vždy nutné ověřit, že bylo hledání v cache
	 * úspěšné.
	 * </p>
	 * 
	 * @return Vrací objekt, který je výsledkem hledání v cache.
	 * 
	 * @throws NoSuchElementException
	 *             Vyhozena v případě, že hledání v cache nebylo úspěšné, tedy
	 *             že {@code cacheHit() == false}.
	 */
	public Object getValue() {
		if (data == null)
			throw new NoSuchElementException("Requested cache entry was not found.");
		return data;
	}
}
