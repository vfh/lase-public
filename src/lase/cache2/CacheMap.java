package lase.cache2;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Hash mapa s vícenásobným klíčem ({@link MultiKey}) sloužící jako cache.
 * Narozdíl od {@link HashMap} umožňuje zjistit, zda byl její obsah od vytvoření
 * instance (včetně deserializace) změněn.
 * 
 * @author Rudolf Šíma
 * 
 */
public class CacheMap implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Udává, zda byl obsah mapy od vytvoření instance (včetně deserializace) změněn.
	 * 
	 * XXX Proč je inicializována na true? Má to důvod?
	 */
	transient private boolean modified = true;

	/**
	 * Interní mapa, do které se ukládají objekty vkládané do cache.
	 */
	private Map<MultiKey, Object> storage;
	
	/**
	 * Vytvoří novou prázdnou mapu.
	 */
	public CacheMap() {
		storage = new HashMap<MultiKey, Object>();
	}

	/**
	 * Vloží objekt do cache a označí cache za modifikovanou (viz
	 * {@link #modified()}.
	 * 
	 * @param key
	 *            Klíč, pod kterým bude hodnota v cache uložena.
	 * @param value
	 *            Objek, který bude v cache uložen.
	 */
	public void put(MultiKey key, Object value) {
		modified = true;
		storage.put(key, value);
	}

	/**
	 * Hledá v cache hodnotu podle klíče.
	 * 
	 * @param key
	 *            Klíč, podle kterého se v cache vyhledává.
	 * 
	 * @return Vrací objekt uložený v cache, je-li nalezen, jinak vrací {@code
	 *         null}.
	 */
	public Object get(MultiKey key) {
		return storage.get(key);
	}

	/**
	 * Zjistí, zda byla cache od vytvoření (včetně deserializace) modifikována.
	 * 
	 * @return Vrací {@code true}, byla-li cache modifikována. Jinak vrací
	 *         {@code false}.
	 */
	public boolean modified() {
		return modified;
	}
}
