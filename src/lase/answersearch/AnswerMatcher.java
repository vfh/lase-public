package lase.answersearch;

import java.util.NoSuchElementException;

/**
 * Rozhraní, které implementují třídy sloužící k nalezení odpovědi v korpusu
 * staženém z internetu.
 * 
 * @author Rudolf Šíma
 * 
 */
public interface AnswerMatcher {
	
	/**
	 * Zjistí, zda v korpusu ještě zbývá odpověď, která nebyla vrácena, nebo byl
	 * již zpracován celý.
	 * 
	 * @return Vrací <code>true</code>, pokud je korpus ještě obsahuje odpověď,
	 *         který nebyla vrácena. Byl-li zpracován celý korpus, vrací
	 *         <code>false</code>
	 */
	public boolean hasNextAnswer();
	
	/**
	 * Vrací další odpověď nalezenou v korpusu.
	 * 
	 * @throws NoSuchElementException
	 *             Vyhozena v případě, že již není k dispozici žádná odpověď,
	 *             která ještě nebyla vrácena, tj. {@link #hasNextAnswer()} vrací
	 *             false.
	 * 
	 * @return Vrací další nalezenou odpověĎ
	 */
	public Answer nextAnswer();
}
