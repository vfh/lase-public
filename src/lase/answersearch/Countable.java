 package lase.answersearch;

/**
 * Kontejner obsahující určitý počet prvků.
 * 
 * @author Rudolf Šíma
 * 
 * @see AnswerGroup
 * 
 */
public interface Countable {

	/**
	 * Vrací počet prvků obsažených v kontejneru.
	 * 
	 * @return Vrací počet prvků obsažených v objektu implementujícím rozhraní
	 *         {@link Countable}.
	 */
	public int size();
	
}
