package lase.answersearch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import lase.app.SharedObjects;
import lase.entrec.NamedEntityGraph;
import lase.entrec.NamedEntityRecognitionException;
import lase.entrec.NamedEntityRecognizer;
import lase.template.Query;
import lase.template.QueryToken;
import lase.template.SearchPattern;
import lase.template.Token;
import lase.util.Corpus;

@Deprecated
public class EntityBasedAnswerMatcher extends AbstractAnswerMatcher {

	private Iterator<Corpus.Entry> corpusIterator;
	private SearchPattern pattern;
	private NamedEntityRecognizer namedEntityRecognizer;
	
	private Corpus tokenizeCorpus(Corpus originalCorpus) {
		CorpusTokenizer corpusTokenizer = new SimpleCorpusTokenizer(originalCorpus);
		Corpus newCorpus = new Corpus(originalCorpus.size());
		while(corpusTokenizer.hasNext()) {
			List<String> tokens = corpusTokenizer.next();
			StringBuilder entryTextBuilder = new StringBuilder();
			for (String token : tokens) {
				entryTextBuilder.append(token);
				entryTextBuilder.append(" ");
			}			
			String text = entryTextBuilder.toString();
			newCorpus.addEntry(text, "", "");
		}
		return newCorpus;
	}
	
	public EntityBasedAnswerMatcher(Corpus corpus, SearchPattern pattern) {
		this.corpusIterator = tokenizeCorpus(corpus).iterator();
		this.pattern = pattern;		
		this.namedEntityRecognizer = SharedObjects.getNamedEntityRecognizer();
	}
	
	@Override
	protected Answer findNextAnswer() {

		while(corpusIterator.hasNext()) {
			Corpus.Entry corpusEntry = corpusIterator.next();
			String text = corpusEntry.getText();
	
			NamedEntityGraph namedEntityGraph;
			try {
				namedEntityGraph = namedEntityRecognizer.analyse(text);
			} catch (NamedEntityRecognitionException e) {
				e.printStackTrace();
				continue;
			}
			
			for (Query query : namedEntityGraph.getAllPaths()) {
				Answer answer;
				try {
					answer = matchEntities(query);
				} catch (Exception e) {
					answer = null;
				}
				if (answer != null)
					return answer;
			}
		}
		
		return null;
	}
	
	private Answer matchEntities(Query query) {
		
		List<AnswerToken> answerTokens = new ArrayList<AnswerToken>();
		
		int offset = 0;
		int index = 0;
		int skip = 0;
		while(index + offset < query.size() && index + skip < pattern.size()) {
			QueryToken patternToken = pattern.get(index + skip);
			QueryToken queryToken = query.get(index + offset);
			Token qT = queryToken.getToken();
			Token pT = patternToken.getToken();
			
			if (pT.isAnswer() && pT.subtypeEquals(qT)) {
				AnswerToken newAnswerToken = AnswerToken.fromQueryToken(queryToken);
				answerTokens.add(newAnswerToken);
				index++;
			} else if (tokensMatch(queryToken, patternToken)) {
				index++;
			} else {
				if (pT.isOptional()) {
					skip++; 
				} else {
					answerTokens.clear();
					index = 0;
					skip = 0;
					offset++;
				}
			}
		}
		
		if (index + skip != pattern.size())
			return null;
		
		return new Answer(answerTokens, Corpus.SourcePage.NONE);
	}
	
//	private Answer matchEntities(Query query) {
//		
//		List<AnswerToken> answerTokens = new ArrayList<AnswerToken>();
//		
//		int offset = 0;
//		int index = 0;
//		boolean atLeastOneMatchable = false;
//		while(index + offset < query.size() && index < pattern.size()) {
//			QueryToken patternToken = pattern.get(index);
//			QueryToken queryToken = query.get(index + offset);
//			Token pT = patternToken.getToken();
//			
//			if (!isMatchable(queryToken) || !isMatchable(patternToken) || tokensMatch(queryToken, patternToken)) {
//				index++;
//				if (isMatchable(queryToken)) {
//					atLeastOneMatchable = true;
//				}
//				if (pT.isAnswer()) {
//					AnswerToken newAnswerToken = AnswerToken.fromQueryToken(queryToken);
//					answerTokens.add(newAnswerToken);
//				}
//			} else {
//				answerTokens.clear();
//				index = 0;
//				offset++;
//			}
//			
//		}
//		
//		if (index == pattern.size() && atLeastOneMatchable) {
//			System.err.println(query);
//			return new Answer(answerTokens, Corpus.SourcePage.NONE);
//		}
//		
//		return null;
//	}

}
