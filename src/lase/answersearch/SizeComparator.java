package lase.answersearch;

import java.io.Serializable;
import java.util.Comparator;

/**
 * <p>
 * Comparator velikosti pro instance rozhraní Countable. Je využit např. při
 * řazení skupin odpovědí ({@link AnswerGroup}) podle velikosti (počtu odpovědí
 * v jednotlivých skupinách).
 * </p>
 * 
 * <p>
 * Umožňuje standardní vzestupné i sestupné řazení.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class SizeComparator implements Comparator<Countable>, Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Opačné (sestupné) řazení.
	 */
	private boolean reverse;

	
	/**
	 * Vytvoří nový vzestupně řadící komparátor velikosti. 
	 */
	public SizeComparator() {
		this(false);
	}
	
	/**
	 * Vytvoří nový komparátor velikosti s daným směrem řazení. Výhozí řazení je
	 * vzestupné.
	 * 
	 * @param reverse
	 *            - je-li <code>true</code>, obrátí se směr řazení na sestupné.
	 */
	public SizeComparator(boolean reverse) {
		this.reverse = reverse;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Countable o1, Countable o2) {
		int s1 = o1.size();
		int s2 = o2.size();
		int result;
		if (s1 > s2)
			result = 1;
		else if (s1 < s2)
			result = -1;
		else
			result = 0;
		return reverse ? -result : result;
	}

}
