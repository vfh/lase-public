package lase.answersearch;

import java.io.Serializable;

import lase.util.PrecisionComparator;

/**
 * Komparátor odpovědí podle přesnosti. Funkce je popsána v dokumentaci třídy
 * {@link PrecisionComparator}.
 * 
 * @author Rudolf Šíma
 * 
 * @see PrecisionComparator
 * 
 */
public interface AnswerComparator extends PrecisionComparator<Answer>, Serializable { }
