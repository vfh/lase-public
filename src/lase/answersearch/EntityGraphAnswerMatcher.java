package lase.answersearch;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

import org.apache.log4j.Logger;

import lase.app.SharedObjects;
import lase.entrec.NamedEntityGraph;
import lase.entrec.NamedEntityRecognitionException;
import lase.entrec.NamedEntityRecognizer;
import lase.entrec.NamedEntityGraph.Node;
import lase.morphology.CharFilter;
import lase.template.NamedEntityToken;
import lase.template.QueryToken;
import lase.template.SearchPattern;
import lase.template.Token;
import lase.template.Token.Type;
import lase.util.Corpus;
import lase.util.ProgressListener;
import lase.util.Corpus.Entry;

/**
 * <p>
 * Vyhledávač odpovědí v korpusu založený na prohledávání grafu pojmenovaných
 * entity modifikovanými algoritmy prohledávání do šířky. Podrobnosti k
 * použitému algoritmu lze najít v diplomové práci Rudolfa Šímy v sekci 2.10. 
 * </p>
 * 
 * <p>
 * Implementovaný způsob vyhledávání odpovědi je narozdíl od dříve vyzkoušených
 * přístupů dostatečně rychlý, a navíc umožňuje při vyhledávání přeskočit daný
 * počet tokenů, které nejsou ve vyhledávacím vzoru uvedeny – do má ovšem určitý
 * dopad na výkon. Přeskakování tokenů je v současné verzi vypnuto.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class EntityGraphAnswerMatcher extends AbstractAnswerMatcher {
	
	/**
	 * Je-li tato konstanta nastavena na <code>true</code>, vypisují se všechny
	 * zpracovávané grafy do souboru v adresáří <tt>./graphviz/</tt>.
	 */
	private static final boolean DEBUG = false;
	
	/**
	 * Čítač uložených souborů využitý v případě, že je konstanta {@link #DEBUG}
	 * nastavena na <code>true</code>.
	 */
	private static int filenum = 0;
	
	/**
	 * Korpus, ve kterém se odpovědi vyhledávají.
	 */
	private Corpus corpus;
	
	/**
	 * Iterátor korpusu, ve kterém se vyhledávají odpovědi.
	 */
	private Iterator<Entry> corpusIterator;
	
	/**
	 * Rozpoznávač pojmenovaných entit.
	 */
	private NamedEntityRecognizer namedEntityRecognizer;
	
	/**
	 * Vzor odpovědi, která je hledána.
	 */
	private SearchPattern pattern;
	
	/**
	 * Znakový filtr, který slouží k nahrazení sémanticky shodných znaků, např.
	 * všech pomlček a spojovníku, jedním zástupcem dané třídy.
	 * 
	 * @see CharFilter
	 */
	private CharFilter charFilter = CharFilter.getPreferredFilter();
	
	/**
	 * Listener, kterému je oznamován postup ve vyhledávání.
	 */
	private ProgressListener progressListener;

	/**
	 * Čítač procházených záznamů korpusu sloužící k výpočtu velikosti
	 * procentuálního podílu již zpracované části.
	 */
	private int currentCorpusEntryIndex = 0;


	/**
	 * Vytvoří a inicializuje nový prohledávač korpusu.
	 * 
	 * @param corpus
	 *            - Korpus, který bude prohledáván.
	 * @param pattern
	 *            - Vyhledávací vzor určující tvar odpovědi, která je hledána.
	 * @param progressListener
	 *            - Posluchač postupu zpracování korpusu.
	 */
	public EntityGraphAnswerMatcher(Corpus corpus, SearchPattern pattern, ProgressListener progressListener) {
		this.corpus = corpus;
		this.pattern = pattern;
		this.corpusIterator = corpus.iterator();
		this.namedEntityRecognizer = SharedObjects.getNamedEntityRecognizer();
		this.progressListener = progressListener;
	}
	
	/* (non-Javadoc)
	 * @see lase.answersearch.AbstractAnswerMatcher#findNextAnswer()
	 */
	@Override
	protected Answer findNextAnswer() {
		while(corpusIterator.hasNext()) {
			Corpus.Entry corpusEntry = corpusIterator.next();
			
			// Workaround kvůli adaptéru pro Bing, který občas vytvoří chybný záznam v korpusu.
			if (corpusEntry.getText() == null) {
				Logger.getLogger(getClass()).warn("null corpus entry");
				continue;
			}
			
			String snippetText = charFilter.substitute(corpusEntry.getText());

			NamedEntityGraph snippetGraph;
			try {
				snippetGraph = namedEntityRecognizer.analyse(snippetText);
			} catch (NamedEntityRecognitionException e) {
				e.printStackTrace();
				continue;
			}
			
			if (DEBUG) {
				try {
					snippetGraph.outputGraphViz("graphviz/" + filenum++ + ".gv");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			double progress = (double)(++currentCorpusEntryIndex) / (double)corpus.size();
			progressListener.progressChanged(progress);
			
			AnswerPath answerPath = matchPattern(pattern, snippetGraph);
			if (answerPath != null) {
				return createAnswer(answerPath, corpusEntry);
			}
		}
		return null;
	}

	/**
	 * Sestaví odpověď na základě cesty v grafu pojmenovaných entit, která
	 * obsahuje hledanou odpověď.
	 * 
	 * @param path
	 *            - Cesta v grafu, ze které se sestaví odpověď.
	 * 
	 * @param corpusEntry
	 *            - Čast korpusu (např. snippet), ve kterém byla odpověď
	 *            nalezena.
	 * 
	 * @return Vrací vytvořenou odpověď.
	 */
	private static Answer createAnswer(AnswerPath path, Corpus.Entry corpusEntry) {
		List<AnswerToken> tokens = new ArrayList<AnswerToken>(path.nodes.length);
		for (AnswerNode an : path.nodes) {
			if (!an.partOfShortAnswer)
				continue;
			QueryToken qt = an.node.getContent();
			Token t = qt.getToken();
			AnswerToken newAnswerToken;
			if (t.getType() == Type.NAMED_ENTITY) {
				NamedEntityToken net = (NamedEntityToken) t;
				newAnswerToken = new NamedEntityAnswerToken(net.getSubtype(), qt.getOriginalWord(), qt.getSemanticValue());
			} else {
				newAnswerToken = new TextAnswerToken(qt.getOriginalWord());
			}
			tokens.add(newAnswerToken);
		}
		return new Answer(tokens, corpusEntry.getSource(), corpusEntry.getText());
	}
	
	/**
	 * Prohledá graf, a snaží se nalézt cestu odpovídající danému vyhledávacímu
	 * vzoru. Vysvětlení algoritmu hledejte, prosím, v textu diplomové práce
	 * Rudolf Šíma. ;-)
	 * 
	 * @param pattern
	 *            - Vyhledávací vzor který určuje tvar hledané odpovědi.
	 * @param graph
	 *            - Graf pojmenovaných entit, ve kterém se hledá cesta s
	 *            odpovědí.
	 * 
	 * @return Vrací cestu skládající se z uzlů obsahujících odpověď.
	 */
	private static AnswerPath matchPattern(SearchPattern pattern, NamedEntityGraph graph) {
		
		int markColor = 0;
		Node rootNode = graph.getRootNode();
		
		Deque<StackRecord> stack = new ArrayDeque<StackRecord>();
		stack.push(new StackRecord(new AnswerNode(rootNode, false), -1, new AnswerPath()));
		
		while(!stack.isEmpty()) {
			StackRecord rec = stack.pop();
		
			if (rec.answerNode.node != rootNode) {
				rec.history = new AnswerPath(rec.history, rec.answerNode);
			}
			
			if (rec.patternIndex + 1>= pattern.size()) {
				return rec.history;
			}
			
			int followUBound, maxDepth;
			if (rec.patternIndex < 0) {
				followUBound = 1;
				maxDepth = Integer.MAX_VALUE;
			} else {
				followUBound = pattern.getFollowSetUBound(rec.patternIndex);
				maxDepth = 1;
			}
			
			for (int i = rec.patternIndex + 1; i < followUBound; i++) {
				QueryToken qt = pattern.get(i);
				boolean answer = qt.getToken().isAnswer();
				
				List<Node> followCandidates = searchGraph(rec.answerNode.node, maxDepth, qt, ++markColor);
								
				for (Node successor : followCandidates) {
					stack.push(new StackRecord(new AnswerNode(successor, answer), i, rec.history));
				} 	
			}
		}
		return null;
	}

	/**
	 * <p>
	 * Provádí prohledávání grafu. Používá se modifikovaný algoritmus
	 * prohledávání do šířky. Cílem hledání je nají daný rozšířený token (resp.
	 * shodující se token – viz {@link #tokensMatch(QueryToken, QueryToken)}) v
	 * určité maximální hloubce od daného uzlu předka.
	 * </p>
	 * 
	 * <h3>Poznámka k optimalizaci</h3>
	 * <p>
	 * Tato metoda je volána velmi často a její rychlost významně ovlivňuje
	 * výkon celého vyhledávacího procesu.
	 * </p>
	 * 
	 * @param root
	 *            - Kořen resp. předek, od kterého prohledávání začíná.
	 * @param maxDepth
	 *            - Maximální hloubka hledání.
	 * @param searchFor
	 *            - Vzor rozšířeného tokenu, který se hledá.
	 * @param markColor
	 *            - Značkovací barva. Je třeba použít barvu (číslo), která se v
	 *            grafu ještě nevyskytuje. Rozumné je při každém volání této
	 *            metody použí barvu o 1 vyšší.
	 * 
	 * @return Vrací seznam uzlů odpovídající vzoru, které byly nalezeny při
	 *         prohledávání grafu (tzn maximálně v hloubce <code>maxDepth</code>
	 *         od výchozího uzlu.
	 * 
	 */
	private static List<Node> searchGraph(Node root, int maxDepth, QueryToken searchFor, int markColor) {
		
		List<Node> foundNodes = new ArrayList<Node>();
		
		Queue<QueueRecord> queue = new ArrayDeque<QueueRecord>();
		queue.offer(new QueueRecord(root, 0));
		
		while(!queue.isEmpty()) {
			QueueRecord rec = queue.poll();
			
			QueryToken corpusToken = rec.node.getContent();
			if (corpusToken != null) {
				if (tokensMatch(corpusToken, searchFor)) {
					foundNodes.add(rec.node);
				}
			}
			
			if (rec.depth < maxDepth) {
				int count = rec.node.getSuccessorCount();
				for (int i = 0; i < count; i++) {
					Node successor = rec.node.getSuccessor(i);
					if (successor.getColor() != markColor) {
						successor.setColor(markColor);
						queue.offer(new QueueRecord(successor, rec.depth + 1));
					}
				}
			}
		}
		
		return foundNodes;
	}
	
	/**
	 * <p>
	 * Uzly, ze kterých se skládá cesta v grafu, která obsahuje odpověď.
	 * </p>
	 * 
	 * <h3>Poznámka k optimalizaci</h3>
	 * <p>
	 * Tato třída je optimalizována tak, aby alokovala co nejméně paměti na úkor
	 * rychlosti. Její instance je totiž vytvářena velmi čast, ale jen zřídkakdy
	 * jsou do ní ukládána nějaká data.
	 * </p>
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	private static class AnswerPath {
		
		/**
		 * Pole obsahující grafu s odpovědí. 
		 */
		AnswerNode[] nodes;
		
		/**
		 * Vytvoří novou prázdnou cestu.
		 */
		public AnswerPath() {
			nodes = new AnswerNode[0];
		}

		/**
		 * Vytvoří novou cestu skládající se ze staré cesty na jejíž konec se
		 * přidá uzel typu {@link AnswerNode}.
		 * 
		 * @param oldPath
		 *            - Stará cesta.
		 * @param newAnswerNode
		 *            - Nový uzel, který se přidá na konec cesty.
		 */
		AnswerPath(AnswerPath oldPath, AnswerNode newAnswerNode) {
			this(oldPath, newAnswerNode.node, newAnswerNode.partOfShortAnswer);
		}

		/**
		 * 
		 * Vytvoří novou cestu skládající se ze staré cesty na jejíž konec se
		 * přidá uzel.
		 * 
		 * @param oldPath
		 *            - Stará cesta.
		 * @param newNode
		 *            - Nový uzel, který se přidá na konec cesty.
		 * @param isPartOfAnswer
		 *            - Udává, zda je nový uzel součástí odpovědi, tj. nalezený
		 *            token odpovídá tokenu, který byl ve vyhledávacím vzoru
		 *            šablony označen jako token odpověďi.
		 */
		AnswerPath(AnswerPath oldPath, Node newNode, boolean isPartOfAnswer) {
			int oldLen = oldPath.nodes.length;
			nodes = Arrays.copyOf(oldPath.nodes, oldLen + 1);
			nodes[oldLen] = new AnswerNode(newNode, isPartOfAnswer);
		}
	}
	
	/**
	 * Uzel v cestě odpovědi. Skládá se z uzlu grafu a logické proměnné udávající, že
	 * tento uzel odpovídá tokenu, který byl ve vyhledávacím vzoru šablony označen
	 * jako token odpovědi.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	private static class AnswerNode {
		
		/**
		 * Udává, zda je token součástí odpovědi.
		 */
		boolean partOfShortAnswer;
		
		/**
		 * Token grafu pojmenovaných entit.
		 */
		Node node;

		/**
		 * Vytvoří a inicializuje nový AnswerNode.
		 * 
		 * @param node
		 *            - Token grafu pojmenovaných entit.
		 * @param partOfShortAnswer
		 *            - Udává, zda tento uzel odpovídá tokenu, který byl ve
		 *            vyhledávacím vzoru šablony označen jako token odpovědi.
		 */
		AnswerNode(Node node, boolean partOfShortAnswer) {
			this.partOfShortAnswer = partOfShortAnswer;
			this.node = node;
		}
	}
	
	/**
	 * Záznam zásobníku používaného při prohledávání grafu pojmenovaných entit do hloubky.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	private static class StackRecord {
		
		/**
		 * Uzel grafu, který "byl právě objeven" resp. byl objeven v "čase", kdy
		 * se tento záznam ukladá do zásobníku.
		 */
		AnswerNode answerNode;
		
		/**
		 * Index tokenu ve vyhledávacím vzoru šablony, který se shoduje s
		 * tokenem v uzlu, který byl právě objeven, tj. uzel {@link #answerNode}.
		 */
		int patternIndex;
		
		/**
		 * Historie hledání uzlu, tj. seznam uzlů, které byly v grafu nalezeny a odpovídaly
		 * vyhledávacímu vzoru šablony. 
		 */
		AnswerPath history;

		/**
		 * Vytvoří nový záznam zásobníku.
		 * 
		 * @param node
		 *            - Uzel grafu, který "byl právě objeven" resp. byl objeven
		 *            v "čase", kdy se tento záznam ukladá do zásobníku.
		 * @param patternIndex
		 *            - Index tokenu ve vyhledávacím vzoru šablony, který se
		 *            shoduje s tokenem v uzlu, který byl právě objeven, tj.
		 *            uzel <code>node</code>.
		 * @param history
		 *            - Historie hledání uzlu, tj. seznam uzlů, které byly v
		 *            grafu nalezeny a odpovídaly vyhledávacímu vzoru šablony.
		 */
		public StackRecord(AnswerNode node, int patternIndex, AnswerPath history) {
			this.answerNode = node;
			this.patternIndex = patternIndex;
			this.history = history;
		}
	}
	
	/**
	 * Záznam fronty používané při prohledávání grafu pojmenovaných entit do
	 * šířky. Slouší k uchování uzlu a hloubky, ve které byl nalezen.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	private static class QueueRecord {
		
		/**
		 * Uzel grafu pojmenovaných entit.
		 */
		Node node;
		
		
		/**
		 * Hloubka, ve které byl uzel nalezen.
		 */
		int depth;

		/**
		 * Vytvoří a inicializuje nový záznam.
		 * 
		 * @param node
		 *            - Uzel grafu pojmenovaných entit.
		 * @param depth
		 *            - Hloubka, ve které byl uzel nalezen.
		 */
		QueueRecord(Node node, int depth) {
			this.node = node;
			this.depth = depth;
		}
	}
}
