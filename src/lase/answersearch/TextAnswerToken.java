package lase.answersearch;

/**
 * Token odpovědi s textovým obsahem, zpravidla jedním slovem.
 * 
 * @author Rudolf Šíma
 * 
 */
public class TextAnswerToken extends AnswerToken {

	/**
	 * Vytvoří a inicializuje token odpovědi s textovým obsahem.
	 * 
	 * @param value
	 *            - Textový obsah tokenu odpovědi.
	 */
	public TextAnswerToken(String value) {
		super(Type.TEXT, value);
	}

}
