package lase.answersearch;

import lase.template.NamedEntityToken;
import lase.template.QueryToken;
import lase.template.Token;
import lase.util.GenericEnumsWorkaround;

/**
 * Token odpovědi nalezené systémem LASE. Je určen typem token ({@link Type}) a
 * textovým obsahem. Tato třída je abstraktní a je využita jako předek jedné z
 * dále uvedených implementací.
 * 
 * <h3>Implementace:<h3>
 * <ul>
 * <li>{@link TextAnswerToken}</li>
 * <li>{@link NamedEntityAnswerToken}</li>
 * </ul>
 * 
 * @author Rudolf Šíma
 * 
 * @see Answer
 * 
 */
public abstract class AnswerToken {
	
	/**
	 * Typ tokenu odpovědi.
	 */
	private Type type;
	
	/**
	 * Textový obsah tokenu odpovědi.
	 */
	private String text;
	
	/**
	 * Vytvoří a inicializuje nový AnswerToken.
	 * 
	 * @param type
	 *            - Typ tokenu odpovědi.
	 * @param text
	 *            - Textový obsah tokenu odpovědi.
	 */
	public AnswerToken(Type type, String text) {
		this.type = type;
		this.text = text;
	}

	/**
	 * Vrací textový obsah tokenu odpovědi.
	 * 
	 * @return Vrací textový obsah tokenu odpovědi.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Vrací typ tokenu odpovědi.
	 * 
	 * @return Vrací typ tokenu odpovědi.
	 */
	public Type getType() {
		return type;
	}
	
	/**
	 * Typ tokenu odpovědi.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	public enum Type {
		/** Token nesoucí pouze textový obsah, zpravidla jedno slovo. */
		TEXT,
		/** Token obsahující pojmenovanou entitu, např. datum. */
		NAMED_ENTITY
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnswerToken other = (AnswerToken) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (type == Type.NAMED_ENTITY) 
		{
			return ((NamedEntityAnswerToken)this).getSubtype() ==
			       ((NamedEntityAnswerToken)obj).getSubtype();
		}
		return true;
	}

	/**
	 * <p>
	 * Vytvoří nový AnswerToken odpovídajícího typu z tokenu typu
	 * {@link QueryToken}.
	 * </p>
	 * 
	 * <p>
	 * Pokud je <code>queryToken</code> tokenem typu pojmenovaná entity, vytvoří
	 * se token odpovědi také typu pojmenovaná entity, tj.
	 * {@link NamedEntityAnswerToken}. Ve všech ostatních případech se vytvoří
	 * token nesoucí pouze textový obsah, tedy {@link TextAnswerToken}.
	 * </p>
	 * 
	 * @param queryToken
	 *            - QueryToken, ze kterého se vytvoří nový AnswerToken.
	 * 
	 * @return Vrací vytvořený token odpovědi.
	 */
	public static AnswerToken fromQueryToken(QueryToken queryToken) {
		Token.Type type = queryToken.getToken().getType();
		AnswerToken answerToken;
		
		switch(type) {
		case NAMED_ENTITY:
			/* Workaround kvůli chybě v některých překladačích.
			 * @see lase.util.GenericEnumsWorkaround */
			NamedEntityToken.Subtype subtype = GenericEnumsWorkaround.cast(queryToken.getToken().getSubtype(), NamedEntityToken.Subtype.class); 
			answerToken = new NamedEntityAnswerToken(subtype, queryToken.getOriginalWord(), queryToken.getSemanticValue());
			break;
		default:
			answerToken = new TextAnswerToken(queryToken.getOriginalWord());
		}
		
		return answerToken;
	}
}
