package lase.answersearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import lase.app.SharedObjects;
import lase.morphology.MorphWord;
import lase.morphology.MorphologicalAnalyser;
import lase.morphology.MorphologyException;
import lase.util.Corpus;

@Deprecated
public class MorphWordCorpusTokenizer implements CorpusTokenizer {
	
	private Iterator<Corpus.Entry> corpusIterator;
	private MorphologicalAnalyser morphologicAnalyser;

	public MorphWordCorpusTokenizer(Corpus corpus) {
		this.corpusIterator = corpus.iterator();
		this.morphologicAnalyser = SharedObjects.getMorphologicalAnalyser();
	}
	
	public boolean hasNext() {
		return corpusIterator.hasNext();
	}

	public List<String> next() {
		Corpus.Entry entry = corpusIterator.next();
		String text = entry.getText();
		List<MorphWord> morphWordList;
		try {
			morphWordList = morphologicAnalyser.analyse(Arrays.asList(text));
		} catch (MorphologyException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
		
		List<String> tokenList = new ArrayList<String>(morphWordList.size());
		for (MorphWord mw : morphWordList) {
			tokenList.add(mw.getWord());
		}
		
		return tokenList;
	}
	
	@SuppressWarnings("unused")
	private static class StringDerivator implements Iterator<String> {

		private Iterator<MorphWord> morpWordIterator;

		StringDerivator(Iterator<MorphWord> morphWordIterator) {
			this.morpWordIterator = morphWordIterator;
		}
		
		@Override
		public boolean hasNext() {
			return morpWordIterator.hasNext();
		}

		@Override
		public String next() {
			MorphWord mw = morpWordIterator.next();
			return mw.getWord();
		}

		@Override
		public void remove() {
			morpWordIterator.remove();
		}
	}
}
