package lase.answersearch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import lase.util.IterationUtils;
import lase.util.PrecisionComparator.Precision;

/**
 * <p>
 * Skupina odpovědí, které jsou si sémanticky rovny. Slouží k zařazení stejných
 * odpovědí do skupin v rámci kterých se odpovědi seřadí podle přesnosti.
 * Skupiny potom lze porovnat podle četnosti velikosti (podle počtu odpovědí.).
 * </p>
 * 
 * <p>
 * Obsahuje statickou metodu {@link #sortAnswers(List, AnswerComparator)}, která
 * provede výše uvedené oprerace a vrací seřazený seznam skupin odpovědí.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class AnswerGroup implements Countable, Iterable<Answer> {
	
	/**
	 * Řazená množina odpovědí implemenovaná stromem. Odpovědi jsou řazeny podle
	 * přesnosti (viz {@link AnswerComparator}).
	 */
	private SortedSet<Answer> answerTree;
	
	/**
	 *	Neřazený seznam odpovědí ve skupině.
	 */
	private List<Answer> answers;
	
	/**
	 * Počet odpovědí ve skupině. 
	 * <small>pozn.: Má nějaký jiný význam než answers.size()?</small>
	 */
	private int size;
	
	/**
	 * Vytvoří novou prázdnou skupinu odpovědí využívající daný komparátor
	 * odpovědí (podle přesnosti).
	 * 
	 * @param comparator
	 *            - komparátor odpovědí.
	 *            
	 * @see AnswerComparator
	 */
	public AnswerGroup(AnswerComparator comparator) {
		Comparator<Answer> answerTreeComparator = new AnswerTreeComparator(comparator);
		this.answerTree = new TreeSet<Answer>(answerTreeComparator);
		this.answers = new ArrayList<Answer>();
	}
	
	/**
	 * Přidá odpověď do skupiny. Ta je automaticky zařazena na správné místo podle přesnosi. 
	 * @param answer
	 */
	public void addAnswer(Answer answer) {
		answerTree.add(answer);
		answers.add(answer);
		size++;
	}
	
	/**
	 * Vrací nejlepší odpověď ve skupině, tj kořen stromu odpověďí.
	 * 
	 * @return Vrací nejlepší odpověď ve skupině.
	 */
	public Answer getBestAnswer() {
		if (answerTree.isEmpty())
			return null;
		return answerTree.first();
	}

	/**
	 * Vrací počet odpovědí ve skupině.
	 * 
	 * @return Vrací počet odpovědí.
	 */
	@Override
	public int size() {
		return size;
	}
	
	public static List<AnswerGroup> sortAnswers(List<Answer> answers, AnswerComparator comparator) {
		List<AnswerGroup> groups = new ArrayList<AnswerGroup>();
	
		for (Answer answer : answers) {
			boolean addNewGroup = true;
			for (AnswerGroup group : groups) {
				Answer bestAnswerInGroup = group.getBestAnswer();
				Precision precision = comparator.compare(answer, bestAnswerInGroup);
				if (precision != Precision.INCOMPARABLE) {
					group.addAnswer(answer);
					addNewGroup = false;
					break;
				}
			}
			if (addNewGroup) {
				AnswerGroup newGroup = new AnswerGroup(comparator);
				newGroup.addAnswer(answer);
				groups.add(newGroup);
			}
		}
		return groups;
	}
	
	/**
	 * Comparator, který slouží k sestavení stromu odpovědí (obecně řazené
	 * množiny odpovědí). Je to adaptér komparátoru přesnosti (
	 * {@link AnswerComparator}), který odstraňuje možnost
	 * {@link Precision#INCOMPARABLE}, a umožňuje tak implementovat rozhraní
	 * {@link Comparator}.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	private static class AnswerTreeComparator implements Comparator<Answer>, Serializable {
		private static final long serialVersionUID = 1L;
		
		/**
		 *	Komparátor přesnosti odpovědí, který je využit pro sestavení stromu odpovědí.
		 */
		private AnswerComparator answerComparator;

		/**
		 * Vytvoří nový komparátor pro vytvoření stormu odpovědí.
		 * 
		 * @param answerComparator
		 *            - komparátor přesnosti, který je využit pro sestavení
		 *            stromu odpovědí.
		 */
		public AnswerTreeComparator(AnswerComparator answerComparator) {
			this.answerComparator = answerComparator;
		}

		/**
		 * Provede porovnání odpovědí podle přesnosti.
		 * 
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 * 
		 * @throws IncomparableException
		 *             Vyhozena v případě, že komparátor, který zajišťuje
		 *             porovnání, vrátí výsledek {@link Precision#INCOMPARABLE}.
		 */
		@Override
		public int compare(Answer o1, Answer o2) {
			Precision precision = answerComparator.compare(o1, o2);
			switch (precision) {
			case LESS_PRECISE:
				return 1;
			case MORE_PRECISE:
				return -1;
			case EQUAL:
				return 0;
			}
			throw new IncomparableException();
		}
		
	}
	
	/**
	 * Výjimka signalizující že volání nějaké metody snaží porovnat podle
	 * přesnosti pojmenované entity, které jsou neporovnatelné (např. datumy
	 * označující dva různé dny).
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	public static class IncomparableException extends RuntimeException {
		
		private static final long serialVersionUID = 1L;

		/**
		 * {@inheritDoc}
		 */
		public IncomparableException() {
		}

		/**
		 * {@inheritDoc}
		 */
		public IncomparableException(String message, Throwable cause) {
			super(message, cause);
		}
		
		/**
		 * {@inheritDoc}
		 */	
		public IncomparableException(String message) {
			super(message);
		}
		
		/**
		 * {@inheritDoc}
		 */
		public IncomparableException(Throwable cause) {
			super(cause);
		}
		
	}
	
	public SortedSet<Answer> getAnswers(){
		return this.answerTree;
	}
	
	/**
	 * Vrací řetězec obsahující texty tokenů odpovědí ve správném pořadí.
	 * Pouze pro účely ladění.
	 */
	public String toString() {
		return answerTree.toString();
	}

	/**
	 * Vrací iterátor (pouze pro čtení) odpovědí ve skupině. Pořadí, ve kterém
	 * iterátor vrací odpovědi, není specifikováno.
	 */
	@Override
	public Iterator<Answer> iterator() {
		return IterationUtils.readOnlyIterator(answers.iterator());
	}
}
