package lase.answersearch;

import lase.template.QueryToken;
import lase.template.Token;

/**
 * Abstraktní předek třídy sloužící k nalezení odpovědi v korpusu získaném z
 * internetu. Obsahuje statickou metodu pro porovnání rozšířeného tokenu
 * korpusu s rozšířeným tokenem otázky, a dále zjednodušuje implementaci, která
 * vyhovuje návrhovému vzoru z rozhraní {@link AnswerMatcher}.
 * 
 * @author Rudolf Šíma
 * 
 * @see AnswerMatcher
 */
public abstract class AbstractAnswerMatcher implements AnswerMatcher {

	/**
	 * Nalezená odpověď připravená k vrácení.
	 */
	private Answer next;

	/**
	 * Prohledává korpus od místa, kde bylo poslední volání přerušeno a vrátí
	 * nalezenou odpověď, nebo vrátí <code>null</code>, byl-li prohledán celý
	 * korpus a žádná odpověď už není k dispozici.
	 * 
	 * @return Vrací nalezenou odpověď nebo <code>null</code>, pokud již žádná
	 *         není k dispozici.
	 */
	protected abstract Answer findNextAnswer();

	/**
	 * <p>
	 * Zjistí, zda se rozšířený token pocházející korpusu (zpracovaného
	 * morfologickým analyzátorem) shoduje s rozšířeným tokenem z otázky.
	 * </p>
	 * 
	 * <p>
	 * K dosažení shody je zapotřebí shoda typů a případných podtypů rozšířených
	 * tokenů, dále textová rovnost bez ohledu na velikost písmen v případě
	 * rozšířených tokenů s textovým obsahem nebo rovnost sémantických hodnot v
	 * případě pojmenovaných entit.
	 * </p>
	 * 
	 * @param corpusToken
	 *            - Token z korpusu.
	 * @param templateToken
	 *            - Token z otázky.
	 * 
	 * @return Vrací <code>true</code>, pokud se tokeny shodují, jinak vrací
	 *         <code>false</code>.
	 */
	protected static boolean tokensMatch(QueryToken corpusToken, QueryToken templateToken) {
		Token tT = templateToken.getToken();
		Token cT = corpusToken.getToken();
		Token.Type corpusTokenType = cT.getType();
		Token.Type templateTokenType = tT.getType();
		
		if (!tT.matches(corpusToken))
			return false;
		
		if (tT.isAnswer())
			return true;
		
		/*
		 * Nemá smysl implementovat porovnávání jiných tokenů jiných typů než
		 * WORD a NAMED_ENTITY, dokud není k dispozici vyhledávač, který by
		 * byl schopen pracovat s lemmaty, slovními druhy, synonymy a hypernymy.
		 */
		assert corpusTokenType == Token.Type.WORD || corpusTokenType == Token.Type.NAMED_ENTITY;
		assert templateTokenType != Token.Type.REFERENCE;
		
		switch(templateTokenType) {
		case WORD:
			return corpusToken.getOriginalWord().equalsIgnoreCase(templateToken.getToken().getText());
		case NAMED_ENTITY:
			assert corpusToken.subtypeEquals(templateToken);
			if (templateToken.getOriginalWord() == null) {
				//Šablona požadovala libovolnou pojmenovanou entitu daného podtypu.
				return true; 
			}
			return corpusToken.getSemanticValue().equals(templateToken.getSemanticValue());
		case LEMMA:
			throw new UnsupportedOperationException();
		case POS:
			throw new UnsupportedOperationException();
		case SEMANTIC_CATEGORY:
			throw new UnsupportedOperationException();
		}
		
		return false;
	}
	
	/* (non-Javadoc)
	 * @see lase.answersearch.AnswerMatcher#hasNextAnswer()
	 */
	@Override
	public boolean hasNextAnswer() {
		if (next != null)
			return true;
		next = findNextAnswer();
		return next != null; 
	}

	/* (non-Javadoc)
	 * @see lase.answersearch.AnswerMatcher#nextAnswer()
	 */
	@Override
	public Answer nextAnswer() {
		Answer result;
		if (next != null) {
			result = next;
			next = findNextAnswer();
		} else {
			next = findNextAnswer();
			result = next;
		}
		return result;
	}
}
