package lase.testing;

import gate.AnnotationSet;

public interface NEWriteCondition {
	
	boolean evaluate(AnnotationSet entities);
}
