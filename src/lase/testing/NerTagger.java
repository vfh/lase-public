package lase.testing;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.stream.XMLStreamException;
import gate.Annotation;
import gate.AnnotationSet;
import gate.Gate;
import gate.creole.ResourceInstantiationException;
import gate.util.GateException;
import lase.app.SharedObjects;
import lase.util.PathUtils;
import cz.zcu.fav.liks.apps.GateAppWrapper;

/**
 * určuje pojmenované entity v textu
 * @author Ondřej Pražák
 *
 */
public class NerTagger {

	private static final String CONFIGURATION_PATH = "data/zswi-app.xml";
	
	public static final NEWriteCondition writeAlways = new NEWriteCondition() {
		
		@Override
		public boolean evaluate(AnnotationSet entities) {
			return true;
		}
	};
	
	public static final NEWriteCondition writeLongOtherEntities = new NEWriteCondition() {
		
		@Override
		public boolean evaluate(AnnotationSet entities) {
			for(Annotation entity : entities)
			{
				if(entity.getType().equals("O") && entity.getEndNode().getOffset()-entity.getStartNode().getOffset()>15)
				{
					return true;
				}
			}
			return false;
		}
	};
	
	
	private GateAppWrapper wrapper;
	private NerXMLWriter xml;
	
	public NerTagger(NerXMLWriter xml) {
		wrapper = SharedObjects.getMorphologyWrapper();
		this.xml = xml;
	}
	
	/**
	 * určí pojmenované entity a vytváří z těch špatně určených XML soubor
	 * @param text analyzovaný text
	 * @param obsahuje-li text entitu splňující podmínku,
	 * je tento text po otagování zapsán do XML
	 * @return parametr text
	 */
	public String analyze(String text, NEWriteCondition condition)
	{
		StringBuilder resultBuilder = new StringBuilder(text);
		if(wrapper==null)
			init();
		try {
			Map<String, AnnotationSet> as = wrapper.execute(text);
			AnnotationSet entities = as.get("Named Entity");
			
			if(condition.evaluate(entities))
			{
				Set<Annotation> set = new TreeSet<Annotation>(new Comparator<Annotation>() {

					@Override
					public int compare(Annotation o1, Annotation o2) {
						return o1.getStartNode().getOffset().intValue()-o2.getStartNode().getOffset().intValue();
					}
				});
				set.addAll(entities);
				char[] array = text.toCharArray();
				try {
					xml.soubor.writeStartElement("sentence");
					xml.soubor.writeAttribute("id", ""+(++xml.id));
				} catch (XMLStreamException e1) {
					e1.printStackTrace();
				}
				
				int lastIndex = 0;
				for(Annotation entity2 : set)
				{
					int start = entity2.getStartNode().getOffset().intValue();
					int end = entity2.getEndNode().getOffset().intValue();
					try {
						xml.soubor.writeCharacters(array, lastIndex, start-lastIndex);
						xml.soubor.writeStartElement("ne");
						xml.soubor.writeAttribute("type", entity2.getType());
						xml.soubor.writeCharacters(array,start, end-start);
						lastIndex = end + 1;
						xml.soubor.writeEndElement();
					} catch (XMLStreamException e) {
						e.printStackTrace();
					}
				}
				try {
					xml.soubor.writeCharacters(array, lastIndex, array.length-lastIndex);
					xml.soubor.writeEndElement();
					xml.soubor.writeCharacters("\n");
				} catch (XMLStreamException e) {
					e.printStackTrace();
				}
			}
			
			
		} catch (GateException e) {
			e.printStackTrace();
		}
		
		return resultBuilder.toString();// + System.getProperty("line.separator") + errorBuilder.toString();
	}
	
	/**
	 * určí pojmenované entity a přidá tagy, tagy nedodržují striktně XML formát
	 * @param text analyzovaný text
	 * kandidáta na špatnou odpověď, tedy entitu tzpu Other další než 15 znaků
	 * @return otagovaný text
	 */
	public String analyze(String text)
	{
		StringBuilder resultBuilder = new StringBuilder(text);
		if(wrapper==null)
			init();
		try {
			Map<String, AnnotationSet> as = wrapper.execute(text);
			AnnotationSet entities = as.get("Named Entity");
			Set<Annotation> set = new TreeSet<Annotation>(new Comparator<Annotation>() {

				@Override
				public int compare(Annotation o1, Annotation o2) {
					return o1.getStartNode().getOffset().intValue()-o2.getStartNode().getOffset().intValue();
				}
			});
			set.addAll(entities);
			int inserted = 0;
			boolean error = false;
			for(Annotation entity : set)
			{
				String insertedStringBefore = "<ne type=\""+entity.getType()+"\">";
				String insertedStringAfter = "</ne>";
				resultBuilder.insert(entity.getEndNode().getOffset().intValue()+inserted, insertedStringAfter);
				resultBuilder.insert(entity.getStartNode().getOffset().intValue()+inserted, insertedStringBefore);
				inserted += insertedStringBefore.length()+insertedStringAfter.length();
				
				if(entity.getType().equals("O") && entity.getEndNode().getOffset()-entity.getStartNode().getOffset()>15)
				{
					error = true;
				}
			}
			
			if(error)
			{
				xml.writeSentence(resultBuilder.toString());
			}
		} catch (GateException e) {
			e.printStackTrace();
		}
		
		return resultBuilder.toString();// + System.getProperty("line.separator") + errorBuilder.toString();
	}
	
	/**
	 * inicializuje analyzátor
	 */
	private void init()
	{
		if(!Gate.isInitialised())
		{
			Gate.runInSandbox(true);
			try {
				Gate.init();
			} catch (GateException e) {
				e.printStackTrace();
			}
		}
		try {
			wrapper = new GateAppWrapper(PathUtils.convertRelativePath(CONFIGURATION_PATH));
		} catch (ResourceInstantiationException e) {
			e.printStackTrace();
		}
	}

}
