package lase.testing;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import lase.app.Settings;

public class NerXMLWriter{

	public XMLStreamWriter soubor;
	
	private PrintWriter pw;
	
	public int id = 0;
	
	public NerXMLWriter(String fileName)
	{
		XMLOutputFactory output = XMLOutputFactory.newInstance();
		try{
			//vytvoreni souboru
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd(HH.mm)");
			Calendar cal = Calendar.getInstance();
			String date = dateFormat.format(cal.getTime());
			fileName = fileName + date;
			File file = new File(fileName + ".xml");										
			file.createNewFile();
				//priprava souboru pro zapis xml
		    soubor = output.createXMLStreamWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF-8"));
			//pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF-8"));
		}
		//osetreni vyskytu chyby
		catch (Exception e){															
		                System.err.println("Chyba při zápisu: " + e.getMessage());		
		}
		
		//zapsani hlavicky xml	
		
			
			try {
				soubor.writeStartDocument();
				soubor.writeStartElement("doc");
			} catch (XMLStreamException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	
	public void writeSentence(String sentence)
	{
		try {
			soubor.writeStartElement("sentence");
			soubor.writeAttribute("id", ""+(++id));
			soubor.writeCharacters(sentence);
			soubor.writeEndElement();
			soubor.writeCharacters("\n");
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void closeWriter()
	{
		try {
			soubor.writeEndElement();
			soubor.writeEndDocument();
			soubor.close();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
