package lase.testing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import lase.answersearch.Answer;
import lase.answersearch.AnswerGroup;
import lase.app.LaseApp;
import lase.app.LaseAppCallback;
import lase.app.Settings;
import lase.template.SearchPattern;
import lase.util.Corpus;
import lase.util.LaseException;

/**
 * Hlavní třída funkčního testu
 * @author Gabriela Hessová, Ondřej Pražák
 * @version 1.00
 * @since 2.0
 */
public class OverallTest implements TestCore {
	
	/**
	 * cesta k XML souboru, do kterého se ukládají výsledky testu
	 */
	private String outputFile = null;
	
	/**
	 * četnost nejčastější odpovědi na aktuálně spracovávanou otázku
	 */
	private int count = -1;
	
	/**
	 * informace zda byla k aktuální otázce nalezena šablona
	 */
	private boolean templateFound;
	
	
	/**
	 * pro přesměrování standartního a chybového výstupu
	 */
	private PrintStream sysOut = System.out;
	private PrintStream sysErr = System.err;
	private PrintStream newOut;
	private PrintStream newErr;
	
	/**
	 * Načte seznam otázek a očekávaných odpovědí ze vstupního souboru, 
	 * tyto otázky předá LASE a najde k nim odpovědi, 
	 * které následně předloží testerovi k ověření správnosti.
	 * 
	 * 	název souboru, ze kterého jsou načítána vstupní data, 
	 * soubor je strukturován tak, že na jednom řádku je vždy otázka a 
	 * na následujícím řádku je očekávaní odpověď
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		TestCore test = new OverallTest();
		
		Entry[] entries = test.readData(Settings.testInputFile);
		//test.setOut();
		test.findAnswers(entries);
		//test.resetOut();
		TestInterface answerEvaluation = new AnswerEvaluationGUI(test, entries);
		//TestInterface answerEvaluation = new ComandLineInterface(entries);
		entries = answerEvaluation.getResults();
		String outputFile = test.getOutputFileName();
		test.writeDownResults(outputFile, entries);
	}
	
	/* (non-Javadoc)
	 * @see lase.testing.TestCore#findAnswers(lase.testing.Entry[])
	 */
	@Override
	public void findAnswers(Entry[] entries) {
		
		
		LaseApp lase;
		
		try {
			lase = new LaseApp(new Callback());
			
			for (int i = 0; i < entries.length; i++) {
				entries[i].setAnswer(lase.findBestAnswer(entries[i].question));
				entries[i].setAnswerCount(count);
				count = -1;
				entries[i].setTemplateFound(templateFound);
				templateFound = false;
			}
		} catch (LaseException e1) {
			e1.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
	}
	
	/* (non-Javadoc)
	 * @see lase.testing.TestCore#readData(java.lang.String)
	 */
	@Override
	public Entry[] readData(String inputFile) {
		
		Entry[] pairs; 
		List<String> questions = new ArrayList<String>();
		List<String> expectedAnswers = new ArrayList<String>();
		
		BufferedReader bfr;
		questions = new ArrayList<String>();
		boolean question = true;
		
		try {
			bfr = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile),"UTF-8"));
			String line;
			while ((line = bfr.readLine()) != null) {
				if (question) {
					questions.add(line);
					question = false;
				}
				else {
					expectedAnswers.add(line);
					question = true;
				}				
			}
			
			bfr.close();

		} catch (IOException e) {
			e.printStackTrace();
		}	
		
		pairs = new Entry[questions.size()];
		
		for (int i = 0; i < pairs.length; i++) {
			pairs[i] = new Entry(questions.get(i), expectedAnswers.get(i));
		}
		
		return pairs;
	}
	
	/* (non-Javadoc)
	 * @see lase.testing.TestCore#writeDownResults(java.lang.String, lase.testing.Entry[])
	 */
	@Override
	public void writeDownResults(String outputFile, Entry[] entries) {		
			
			//PrintStream ps;
			
			
			//zapíše výsledky to textového souboru
			/*try {			
				ps = new PrintStream(outputFile + ".txt");
				for (int i = 0; i < entries.length; i++) {
					
					ps.print(i + 1 + ") " + entries[i].question);
					ps.print(entries[i].getAnswer() != null ? "\n    " + entries[i].getAnswer().toString() + ", zdroj: " + entries[i].getAnswer().getSource().getUrl().toString() : "\n    " + "Odpověď nenalezena." + ", zdroj: " +"Odpověď nenalezena.");
					ps.print(", hodnocení: " + entries[i].getRating() + "%\n");
				}	
			} catch (IOException e) {
				e.printStackTrace();
			}*/
			
			XMLOutputFactory output = XMLOutputFactory.newInstance();
			XMLStreamWriter soubor = null;
			try{
					//vytvoreni souboru
					File file = new File(outputFile + ".xml");										
					file.createNewFile();
					//priprava souboru pro zapis xml
			        soubor = output.createXMLStreamWriter(new FileWriter(file));			
			}
			//osetreni vyskytu chyby
			catch (Exception e){															
			                System.err.println("Chyba při zápisu: " + e.getMessage());		
			}
			
			//zapsani hlavicky xml	
			try {
				soubor.writeStartDocument();
				//odradkovani - vyuzivano jen pro prehlednost
				//soubor.writeCharacters(System.getProperty("line.separator"));					
				//zapsani korenoveho adresare Root
				soubor.writeStartElement("TestResults");
				soubor.writeAttribute("overallRating", String.format("%.2f", this.getOverallRating(entries)) + "%");
				soubor.writeAttribute("count", ""+entries.length);
				//soubor.writeCharacters(System.getProperty("line.separator"));
				
				for (int i = 0; i < entries.length; i++) {
					soubor.writeStartElement("Result");
					//soubor.writeCharacters(System.getProperty("line.separator"));
					soubor.writeAttribute("templateFound", ""+entries[i].isTemplateFound());
					soubor.writeStartElement("Question");
					soubor.writeCharacters(entries[i].question);
					soubor.writeEndElement();
					//soubor.writeCharacters(System.getProperty("line.separator"));
					
					soubor.writeStartElement("Answer");
					soubor.writeAttribute("count", entries[i].getAnswerCount() + "");
					//soubor.writeCharacters(System.getProperty("line.separator"));
					
					soubor.writeStartElement("ShortAnswer");
					soubor.writeCharacters(entries[i].getAnswer() != null ? entries[i].getAnswer().toString() : "Odpověď nenalezena.");
					soubor.writeEndElement();
					//soubor.writeCharacters(System.getProperty("line.separator"));
										
					soubor.writeStartElement("Context");
					soubor.writeCharacters(entries[i].getAnswer() != null ? entries[i].getAnswer().getContext() : "Odpověď nenalezena.");
					soubor.writeEndElement();
					//soubor.writeCharacters(System.getProperty("line.separator"));
					
					soubor.writeStartElement("URL");
					soubor.writeCharacters(entries[i].getAnswer() != null ? entries[i].getAnswer().getSource().getUrl().toString() : "Odpověď nenalezena.");
					soubor.writeEndElement();
					//soubor.writeCharacters(System.getProperty("line.separator"));
					soubor.writeEndElement();
					//soubor.writeCharacters(System.getProperty("line.separator"));
					
					soubor.writeStartElement("Rating");
					soubor.writeCharacters("" + entries[i].getRating());
					soubor.writeEndElement();
					//soubor.writeCharacters(System.getProperty("line.separator"));
					
					soubor.writeEndElement();					
					
				}
				
				soubor.writeEndElement();
				soubor.writeEndDocument();														
				soubor.close();
				
			} catch (XMLStreamException e) {
				e.printStackTrace();
			}																									
						 
	}
	
	/**
	 * Vypočítá průměrné hodnocení celého testu.
	 * @return
	 */
	public double getOverallRating(Entry[] entries) {
		int sum = 0;
		for (int i = 0; i < entries.length; i++) {
			sum += entries[i].getRating();
		}
		double overallRating = (double)sum / entries.length;
		return overallRating;
	}
	
	/**
	 * Zjistí jednotlivé četnosti daných procentuálních hodnocení.
	 * @return
	 */
	public int[] getRatingFrequencies(Entry[] entries) {
		
		int[] frequencies = new int[5];		
		for (int i = 0; i < entries.length; i++) {			
			frequencies[entries[i].getRating() / 25]++;			
		}		
		return frequencies;
	}
	
	/* (non-Javadoc)
	 * @see lase.testing.TestCore#getOutputFileName()
	 */
	@Override
	public String getOutputFileName() {
		if(outputFile==null)
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd(HH.mm)");
			Calendar cal = Calendar.getInstance();
			String date = dateFormat.format(cal.getTime());
			outputFile = Settings.testOutputFolder+"laseTest" + date;
		}
		
		return outputFile;
	}
	
	@Override
	public void setOut()
	{
		try {
			newOut = new PrintStream(getOutputFileName()+"-out.txt");
			newErr = new PrintStream(getOutputFileName()+"-err.txt");
			System.setErr(newErr);
			System.setOut(newOut);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void resetOut()
	{
		System.setErr(sysErr);
		System.setOut(sysOut);
		newOut.close();
		newErr.close();
	}
	
	/**
	 * získává ze systému dodatečné informace o průběhu hledání odpovědi
	 * @author Ondřej Pražák
	 *
	 */
	private class Callback extends LaseApp.NullCallback {
		
		
		@Override
		public void webSearchStarted(String[] searchStrings) {
			templateFound = true;
			
		}


		@Override
		public void answersSorted(List<AnswerGroup> groups) {
			if(!groups.isEmpty())
				count = groups.get(0).size();
			else
				count = 0;
		}
		
	}
	
}

