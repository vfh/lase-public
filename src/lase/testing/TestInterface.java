package lase.testing;

/**
 * {@code interface} pro uživatelské rozhraní testu
 * @author Ondřej Pražák
 * @version 1.00
 * @since 2.0
 */
public interface TestInterface {

	/**
	 * 
	 * @return pole {@code Entry} s ohodnocenými odpověďmi
	 */
	public abstract Entry[] getResults();

}