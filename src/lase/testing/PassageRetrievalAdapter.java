package lase.testing;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lase.app.LaseApp;
import lase.template.SearchPattern;
import lase.util.Corpus;
import lase.util.LaseException;

/**
 * Třída využívaná testem NER, slouží ke stažení testovacích korpusů a jejich případné serializaci nebo deserializaci
 * @author Ondřej Pražák
 *
 */
public class PassageRetrievalAdapter {

	private Map<String, List<Corpus>> corporaMap;
	private String question;
	private LaseApp lase;
	
	public PassageRetrievalAdapter() {
		corporaMap = new HashMap<String,List<Corpus>>();
		try {
			lase = new LaseApp(new Callback());
		} catch (LaseException e) {
			e.printStackTrace();
			System.err.println("nepodařilo se inicializovat jádro aplikace LASE");
		}	
	}
	
	/**
	 * spustí systém LASE a požádá ho o nalezení odpovědí na zadané otázky
	 * @param questionsList seznam otázek
	 * @return Vrací mapu seznamu korpusů, kde klíčem je otázka, ke které dané korpusy náleží
	 */
	public Map<String,List<Corpus>> loadCorpora(List<String> questionsList)
	{
		corporaMap.clear();
		for(String question:questionsList)
		{
			this.question = question;
			try {
				lase.findBestAnswer(question);
			} catch (LaseException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return corporaMap;
	}
	
	/**
	 * uloží seznam mapu korpusů do zadaného souboru
	 * @param corporaMap Mapa seznamů korpusů pod klíčem otázek
	 * @param file soubor do kterého mají být data uložena
	 */
	public void serializeCorpora(Map<String,List<Corpus>> corporaMap, File file)
	{
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(new FileOutputStream(file));
			try {
				oos.writeObject(corporaMap);
			} catch (IOException e) {
				System.err.println("Chyba serializace korpusů");
				e.printStackTrace();
			}
			oos.close();
			
		} catch (FileNotFoundException e) {
			System.err.println("Chyba serializace korpusů");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Chyba serializace korpusů");
			e.printStackTrace();
		}
		
	
		
	}
	
	/**
	 * Načte mapu Korpusů ze zadaného souboru 
	 * @param file vstupní soubor
	 * @return Vrací mapu seznamů korpusů pod klíčem otázek
	 */
	public Map<String,List<Corpus>> deserializeCorpora(File file)
	{
		Map<String,List<Corpus>> corporaMap = null;
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(file));
			Object read = ois.readObject();
			if(read instanceof Map<?,?>)
				corporaMap = (Map<String,List<Corpus>>)read;
		} catch (FileNotFoundException e) {
			System.err.println("Chyba deserializace korpusů");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Chyba deserializace korpusů");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("zadaný soubor neobsahuje mapu korpusů");
			e.printStackTrace();
		}
		try {
			ois.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return corporaMap;
	}

	/**
	 * registruje se hlavní aplikaci a ukládá zpracovávané korpusy
	 * @author Ondřej Pražák
	 *
	 */
	private class Callback extends LaseApp.NullCallback
	{

		@Override
		public void corpusRetrieved(Corpus corpus, SearchPattern pattern) {
			if(!corporaMap.containsKey(question))
			{
				corporaMap.put(question, new ArrayList<Corpus>());
			}
			corporaMap.get(question).add(corpus);
			
		}
		
	}
}
