package lase.testing;

import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import lase.app.LaseApp;
import lase.app.Settings;
import lase.app.SharedObjects;
import lase.entrec.NamedEntityGraph;
import lase.entrec.NamedEntityRecognizer;
import lase.learning.LearningAlgorithm;
import lase.learning.LearningManager;
import lase.learning.LongestCommonPathLearningAlgorithm;
import lase.morphology.MorphWord;
import lase.morphology.MorphologicalAnalyser;
import lase.template.NamedEntityToken;
import lase.template.Query;
import lase.template.QueryToken;
import lase.template.Template;
import lase.template.NamedEntityToken.Subtype;
import lase.template.Token.Type;
import lase.util.LaseException;

/**
 * Třída sloužící k vytváření jednoduchých testů při ladění jednotlivých modulů
 * systému LASE. Spouští se metody s anotací {@code @Run} v pořadí, ve kterém
 * jsou deklarovány. Nejprve je zajištěna inicializace sdílených objektů systému
 * LASE (např. morfologického analyzátoru).
 * 
 * @author Rudolf Šíma
 * 
 * @see SharedObjects
 * 
 */
public class LaseTest {
	
	private int countNamedEntities(Query q) {
		int count = 0;
		for (QueryToken qt : q) {
			if (qt.getToken().getType() == Type.NAMED_ENTITY)
				count++;
		}
		return count;
	}
	
	private int countDates(Query q) {
		int count = 0;
		for (QueryToken qt : q) {
			if (qt.getToken().getType() == Type.NAMED_ENTITY && ((NamedEntityToken)qt.getToken()).getSubtype() != Subtype.NUMBER)
				count++;
		}
		return count;
	}
	
	private Query getShortestPath(String answer) throws Exception {
		NamedEntityRecognizer rec = SharedObjects.getNamedEntityRecognizer();
		NamedEntityGraph graph = rec.analyse(answer);
		List<Query> allPaths = graph.getAllPaths();
		Query shortest = allPaths.iterator().next();
		for (Query path : allPaths) {
			if (path.size() < shortest.size()) {
				shortest = path;
			} else if (path.size() == shortest.size()) {
				if (countNamedEntities(path) > countNamedEntities(shortest)) {
					shortest = path;
				} else if (countDates(path) > countDates(shortest)) {
					shortest = path;
				}
			}
		}
		return shortest;
	}
	
	
	
	@Run
	void learningTest() throws Exception {
		String path = new File(Settings.templatesdirectory, "learning/").getPath();
		List<Template> questionPatterns = Template.loadDirectory(path);
		LearningAlgorithm algorithm = new LongestCommonPathLearningAlgorithm();
		LearningManager manager = new LearningManager(algorithm, questionPatterns);
		
		//manager.addLearningPair("Kde se narodil Karel IV.", getShortestPath("v Praze"));
		//manager.addLearningPair("Kde se narodil Petr Parléř", getShortestPath("v Únehlích"));
		manager.addLearningPair("Kdy se narodil Karel IV.", getShortestPath("14.5.1316"));
		manager.addLearningPair("Kdy se narodil Petr Parléř", getShortestPath("1332"));
		
//		List<SearchPattern> searchPatterns = manager.learn();
//		Template matchingTemplate = manager.getMatchingTemplate();
//		TemplateCreator creator = new TemplateCreator();
//		for (SearchPattern sp : searchPatterns) {
//			creator.newAnswerPattern();
//			for (QueryToken qt : sp) {
//				Token t = qt.getToken();
//				creator.addToken(t);
//			}
//		}
//		Template newTemplate = creator.getTemplate(matchingTemplate);
//		newTemplate.saveToXmlFile("data/templates/learning/generated/test.xml");
	}
	
	void test2() throws Exception {
		MorphologicalAnalyser analyser = SharedObjects.getMorphologicalAnalyser();
		List<String> tokens = SharedObjects.getTokenizer().tokenize("Narodil se v Praze.");
		List<MorphWord> morphWords = analyser.analyse(tokens);
		for (MorphWord mw : morphWords) {
			System.out.println(mw.getLemma());
		}
	}
	
	// ------------------------------------------------------ //
	
	public static void main(String[] argv) throws Throwable {
		LaseTest laseTest = new LaseTest();
		for (Method method : LaseTest.class.getDeclaredMethods()) {
			if (method.getAnnotation(Run.class) != null) {
				try {
					method.invoke(laseTest);
				} catch (InvocationTargetException e) {
					throw e.getCause();
				}
			}
		}
	}
	
	private LaseTest() throws LaseException {
		LaseApp.initializeDefaults();
	}

	@Retention(RetentionPolicy.RUNTIME)
	private @interface Run {
	}
}
