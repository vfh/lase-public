package lase.nametagsearch;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

/**
 * Comunication with NameTag web service
 * @author Jakub Vasta
 *
 */
public class NameTagWebService {

	/** URL of web service */
	private static final String URL = "http://lindat.mff.cuni.cz/services/nametag/api/recognize?data=";

	/**
	 * Get JSON result from web service NameTag
	 * @param text text for named entity rocognization
	 * @return text response in JSOn format from web service
	 * @throws HttpException
	 * @throws IOException
	 */
	private static String getResponse(String text) throws HttpException, IOException {
		
		
		HttpClient client = new HttpClient();
		text = urlEncodeUtf8(text);
		HttpMethod method = new GetMethod(URL + text + "?");
		
		// System.err.print(URL + text);
		
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
		
		client.executeMethod(method);

		// response is endoded in UTF-8
		byte[] input = method.getResponseBody();
		String responseBodyAsString = new String(input, "UTF-8");
		
		return responseBodyAsString;
		
	}
	
	/**
	 * Main method for this class, execute all parts which are needed for getting list of named entities 
	 * @param text string we need to analyse with NameTag
	 * @return
	 * @throws HttpException
	 * @throws IOException
	 * @throws JSONException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public static Container getNamedEntities(String text) throws HttpException, IOException, JSONException, ParserConfigurationException, SAXException {
		
		String response = getResponse(text);
		
		// In case service is temporary unavailable
		while (response.charAt(0) != '{') {
			Logger.getRootLogger().error("Waiting for response");
			// TODO - we can let the thread wait for a while before sending request again
			response = getResponse(text);
		}
		
		String result = getResultFromJSON(response);
				
		return ResultXMLParser.parse(result);
	}

	/**
	 * Encode given string to UTF-8
	 * @param str string to encode
	 * @return encoded string
	 */
	private static String urlEncodeUtf8(String str) {
		try {
			return URLEncoder.encode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new AssertionError();
		}
	}
	
	/**
	 * 
	 * @param text
	 * @return
	 * @throws JSONException
	 */
	private static String getResultFromJSON(String text) throws JSONException {
		
		String result;
//		System.err.println(text);
		JSONObject o = new JSONObject(text);
		result = o.getString("result");
		
		result = "<root>" + result + "</root>";
		return result;
	}
	
}
