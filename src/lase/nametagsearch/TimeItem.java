package lase.nametagsearch;

public class TimeItem extends Item {
	
	private String year;
	private String month;
	private String day;
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	
	public void addTimeInfo(String type, String info) {
		info = info.replaceAll("\\D+","");
		
		if (type.equals("ty")) {
			year = info;
		}
		else if (type.equals("tm")) {
			month = info;
		}
		else if (type.equals("td")) {
			day = info;
		}
	}
	
	@Override
	public String toString() {
		return "word: " + super.getWord() + " type: " + super.getType() + " length: " + super.getLength() + " position: " + super.getPosition() + " year: " + year + " month: " + month + " day: " + day;
	}

}
