package lase.nametagsearch;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Handler for XML parser with SAX
 * @author Jakub Vasta
 *
 */
public class NameTagHandler extends DefaultHandler {
	
	private Stack<Item> stack = new Stack<Item>();
	
    private List<Item> entList = new ArrayList<Item>();
    
    private List<String> tokens = new ArrayList<String>();

    public List<Item> getEntList() {
        return entList;
    }

    public List<String> getTokens() {
        return tokens;
    }
    
    boolean bToken = false;
    
    int tokensCount = 0;
    StringBuilder buf = new StringBuilder();
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

    	if (qName.equalsIgnoreCase("ne")) {
           
        	
    		String type = attributes.getValue("type");
        	
        	Item ent;
        	
        	if (type.equals("T")) {
        		ent = new TimeItem();
        	}
        	else {
        		ent = new Item();
        	}
        	
        	ent.setType(type);
        	ent.setPosition(tokensCount);
        	stack.push(ent);
            
        } 
    	else if (qName.equalsIgnoreCase("token")) {
            bToken = true;
        } 
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("ne")) {
            
        	Item it = stack.pop();
        	
        	if (!stack.isEmpty() && stack.peek().getType().equals("T")) {
        		TimeItem ti = (TimeItem) stack.peek();
        		ti.addTimeInfo(it.getType(), it.getWord());
        	}
        	else {
        		entList.add(it);
        	}
        }
        if (qName.equalsIgnoreCase("token")) {
        	
        	String word = buf.toString();
        	
        	tokens.add(word);
        	
        	for (Item ne : stack) {
        		ne.addWord(word);
        		ne.incrementLength();
        	}
        	
        	bToken = false;	
        	buf = new StringBuilder();
        	
        	tokensCount++;
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {


		if (bToken) {

			for (int i = start; i < start + length; i++) {
				buf.append(ch[i]);
			}
		}
  
    }
}

