package lase.nametagsearch;

/**
 * Stores data about entity recieved from NammeTag
 * @author Jakub Vasta
 *
 */
public class Item {

	/** Word(s) from which entity consists */
	private String word;
	
	/** Type of entity */
	private String type;
	
	/** Length of entity over NameTag tokens */
	private int length;
	/** Start of entity in tokens */
	private int position;
	
	/**
	 * Constructor
	 */
	public Item() {
		word = "";
		length = 0;
	}
	
	/**
	 * Constructor
	 * @param word word(s) from which the entity consists
	 * @param type type of entity
	 * @param length length of entity over NameTag tokens 
	 */
	public Item(String word, String type, int length) {
		this.word = word;
		this.type = type;
		this.length = length;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	public void setPosition(int position) {
		this.position = position;
	}
	
	public void incrementLength() {
		length++;
	}

	/**
	 * Apeend word to attribute words
	 * @param word string to append
	 */
	public void addWord(String word) {
		
		if (this.word == "") {
			setWord(word);
		}
		else {
			this.word += " " + word;
		}
	}
	
	@Override
	public String toString() {
		return "word: " + word + " type: " + type + " length: " + length + " postion: " + position;
	}

	public String getWord() {
		return word;
	}

	public String getType() {
		return type;
	}

	public int getLength() {
		return length;
	}

	public int getPosition() {
		return position;
	}
	
	
}
