package lase.nametagsearch;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Class for parsing XML result
 * @author Jakub
 *
 */
public class ResultXMLParser {
	 
	/**
	 * Parse XML formated string
	 * @param xml string in XML format to be parsed
	 * @return Container with list of items and tokens
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static Container parse(String xml) throws ParserConfigurationException, SAXException, IOException {
		
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
  
		// System.err.println(xml);
    	
        SAXParser saxParser = saxParserFactory.newSAXParser();
        NameTagHandler handler = new NameTagHandler();
        
        saxParser.parse(new InputSource(new StringReader(xml)), handler);
    
        List<Item> entList = handler.getEntList();
        List<String> tokens = handler.getTokens();
        
        return new Container(entList, tokens);
     
	}
	
}
