package lase.nametagsearch;

import java.util.List;

/**
 * Container for list of tokens and list of items
 * @author Jakub Vasta
 *
 */
public class Container {
	
	/** List of items */
	public final List<Item> items;
	/** List of tokens */
	public final List<String> tokens;
	
	/**
	 * Constructor
	 * @param items list of items
	 * @param tokens list of tokens
	 */
	public Container(List<Item> items, List<String> tokens) {
		this.items = items;
		this.tokens = tokens;
	}

	public List<Item> getItems() {
		return items;
	}

	public List<String> getTokens() {
		return tokens;
	}
	
	

}
