package lase.template;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

/**
 * Výsledek testování shody uživatelem zadané otázky s danou šablonou metodou
 * {@link Template#matchQuery(Query)}. Obsahuje metody {@link #matches()},
 * kterou lze zjistit, zda byla nalezena shoda. Pokud shoda nalezena byla,
 * umožňuje získat seznam vyhledávacích vzorů vytvořených na základě doplnění
 * referencí šablonou definovaných vzorů odpovědi podle zadané otázky.
 * 
 * @author Rudolf Šíma
 * 
 */
public class TemplateMatchResult implements Iterable<SearchPattern> {

	/**
	 * Konstanta odkazující na sdílenou instanci {@link TemplateMatchResult}
	 * označující žádnou shodu. Není nutné vytvářet novou instanci pro každý
	 * takový případ, ale je vhodnější použít tuto konstantu.
	 */
	public static final TemplateMatchResult NOMATCH = new TemplateMatchResult();

	/**
	 * Seznam vyhledávacích vzorů vytvořený doplněním referencí šablonou
	 * definovaných vzorů odpovědi podle zadané otázky.
	 */
	private List<SearchPattern> searchPatterns;
	
	/**
	 * Privátní konstrukor. Vytvoří instanci označující žádnou shodu.
	 */
	private TemplateMatchResult() {
	}

	/**
	 * Vytvoří novou instanci a nastaví seznam vytvořených vyhledávacích vzorů.
	 * Je-li nastaven seznam vyhledávacích vzorů (třeba i prázdný), udává tento
	 * vysledek testování shody, že ke shodě došlo. Metoda {@link #matches()}
	 * tedy vrací {@code true}.
	 * 
	 * @param searchPatterns
	 *            Seznam vytovořených vyhledávacích vzorů pro pozitivní výsledek
	 *            shody nebo {@code null} pro negativní výsledek. V tomto
	 *            případě je ovšem lepší použí konstantu {@link #NOMATCH}.
	 */
	public TemplateMatchResult(List<SearchPattern> searchPatterns) {
		if (searchPatterns != null) {
			if (searchPatterns instanceof RandomAccess)
				this.searchPatterns = Collections.unmodifiableList(searchPatterns);
			else 
				this.searchPatterns = new ArrayList<SearchPattern>(searchPatterns);
		}
	}

	/**
	 * Vrací vyhledávací vzor podle jeho indexu.
	 * 
	 * @param index
	 *            Index požadovaného vyhledávacího vzoru.
	 * 
	 * @return Vrací vyhledávací vzor.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             Vyhozena v případě, že není zadán platný index vyhledávacího
	 *             vzoru.
	 */
	public SearchPattern getSearchPattern(int index) {
		checkState();
		return searchPatterns.get(index);
	}

	/**
	 * Vrací počet vyhledávacích vzorů obsažených v tomto výsledku testování
	 * shody s šablonou.
	 * 
	 * @return Vrací počet vyhledávacích vzorů.
	 */
	public int getSearchPatternCount() {
		checkState();
		return searchPatterns.size();
	}
	
	/**
	 * Vrací seznam vyhledávacích vzorů vytvořených při testování shody otázky s
	 * šablonou.
	 * 
	 * @return Vrací seznam vyhledávacích vzorů.
	 */
	public List<SearchPattern> getSearchPatterns() {
		return searchPatterns;
	}

	/**
	 * Zjistí, zda při testování shody otázky s šablonou došlo ke shodě či
	 * nikoli.
	 * 
	 * @return Vrací {@code true}, shoduje-li se testovaná uživatelem zadaná
	 *         otázka s danou šablonou. V opačné případě vrací {@code false}.
	 */
	public boolean matches() {
		return searchPatterns != null;
	}

	/**
	 * Vrací iterátor vyhledávacích vzorů vytvořených při testování shody otázky
	 * s šablonou.
	 */
	@Override
	public Iterator<SearchPattern> iterator() {
		checkState();
		return searchPatterns.iterator();
	}

	/**
	 * Otestuje, zda byl výsledek testování shody pozitivní. V opačném případě
	 * vyhodí výjimku {@link IllegalStateException}.
	 * 
	 * @throws IllegalStateException
	 *             Vyhozena v případě, že výsledek testování shody nebyl
	 *             pozitivní, metoda {@link #matches()} tedy vrací {@code false}.
	 */
	private void checkState() {
		if (searchPatterns == null)
			throw new IllegalStateException("The query did not match the template.");
	}
}
