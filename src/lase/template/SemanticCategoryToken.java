package lase.template;

import lase.app.SharedObjects;
import wordnet.Iteration;
import wordnet.LinkType;
import wordnet.SynSet;
import wordnet.SynSetList;
import wordnet.WordNet;

/**
 * <p>
 * Token typu {@link Token.Type#SEMANTIC_CATEGORY}. Obsahuje lemma nějakého
 * slova a definuje jazykový vztah zastupovaných slov k tomuto slovu, kterým
 * může být {@link Subtype#HYPERNYM} nebo {@link Subtype#SYNONYM}.
 * {@link SemanticCategoryToken} tedy v šabloně zastupuje všechna
 * <strong>hyponyma</strong> resp. všechna synonyma nějakého slova.
 * </p>
 * 
 * <p>
 * <strong>Upozornění:</strong> Pokud je token označen jako hypernymum, ke shodě
 * v textu dojde v případě, že je nalezeno jeho hyponymum, nikoli hypernymum.
 * Pokud je tedy v tomto tokenu uvedeno lemma <em>zvíře</em>, dojde ke shodě,
 * pokud je v textu na příslušném místě slovo <em>pes</em>.
 * </p>
 * 
 * <p>
 * Ke vyhledávání vztahů mezi slovy slouží Czech WordNet (viz {@link WordNet}).
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class SemanticCategoryToken extends Token {
	
	/**
	 * Lemma zástupce skupiny synonym nebo společné hypernymum zastupovaných slov.
	 */
	public String lemma;
	
	
	/**
	 * Podtyp, který určuje, zda jde o synonymum nebo hypernymum zastupovaných slov.
	 */
	public Subtype subtype;

	/**
	 * Vytvoří nový token, nastaví lemma zástupce skupiny slov a vztah k
	 * zastupavaoné skupině slov. ID tohoto tokenu je nastaveno na hodnotu 0.
	 * 
	 * @param lemma
	 *            Lemma zástupce skupiny synonym nebo společné hypernymum
	 *            zastupovaných slov.
	 * @param subtype
	 *            Podtyp, který určuje, zda jde o synonymum nebo hypernymum
	 *            zastupovaných slov.
	 */
	public SemanticCategoryToken(String lemma, Subtype subtype) {
		this(0, lemma, subtype);
	}

	/**
	 * Vytvoří nový token, nastaví jeho ID, lemma zástupce skupiny slov a vztah
	 * k zastupované skupině slov.
	 * 
	 * @param lemma
	 *            Lemma zástupce skupiny synonym nebo společné hypernymum
	 *            zastupovaných slov.
	 * @param subtype
	 *            Podtyp, který určuje, zda jde o synonymum nebo hypernymum
	 *            zastupovaných slov.
	 */
	public SemanticCategoryToken(int id, String lemma, Subtype subtype) {
		super(id, Token.Type.SEMANTIC_CATEGORY);
		this.lemma = lemma;
		this.subtype = subtype;
	}

	/**
	 * Vrací lemma zástupce skupiny synonym nebo společné hypernymum
	 * zastupovaných slov.
	 * 
	 * @return Vrací lemma tohoto tokenu.
	 */
	public String getLemma() {
		return lemma;
	}

	/**
	 * Vrací podtyp, který určuje, zda jde o synonymum nebo hypernymum
	 * zastupovaných slov.
	 * 
	 * @return Vrací podtyp tohoto tokenu.
	 */
	public Subtype getSubtype() {
		return subtype;
	}

	/**
	 * Podtyp tokenu typu {@link Token.Type#SEMANTIC_CATEGORY}, který určuje,
	 * zda je lemma tohoto tokenu synonymem nebo hypernymem slov, které
	 * zastupuje.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	public enum Subtype {
		/**
		 * Podtyp vyjadřující, že tokenem typu
		 * {@link Token.Type#SEMANTIC_CATEGORY} zastupovaná slova je synonymy
		 * lemmatem daného slova.
		 */
		SYNONYM,
		
		/**
		 * Podtyp vyjadřující, že tokenem typu
		 * {@link Token.Type#SEMANTIC_CATEGORY} zastupovaná slova jsou hyponymy
		 * lemmatem daného slova.
		 */
		HYPERNYM
	}

	/**
	 * Vrací lemma které společné s podtypem určuje skupinu slov zastupovanou
	 * tímto tokenem.
	 */
	public String getText() {
		return lemma;
	}

	/**
	 * <p>
	 * Zjistí shodu tohoto tokenu se slovem definovaným daným rozšířeným
	 * tokenem. Tímto tokenem typu {@link Token.Type#SEMANTIC_CATEGORY}
	 * zastupované skupina slov je definována lemmatem tohoto tokenu a daným
	 * jazykovým vztahem.
	 * </p>
	 * 
	 * <p>
	 * <ul>
	 * <li>V případě podtypu {@link Subtype#HYPERNYM} dojde ke shodě tehdy,
	 * je-li testována slova se shodem, které je hyponymem slova určeného
	 * lemmatem tohoto tokenu.</li>
	 * <li>V případě podtypu {@link Subtype#SYNONYM} dojde ke shodě tehdy, je-li
	 * testována shoda se slovem, které je synonymem slova určeného lemmatem
	 * tohoto tokenu.</li>
	 * </ul>
	 * </p>
	 * 
	 * 
	 */
	@Override
	public boolean matches(QueryToken queryToken) {
		// TODO - Zamyslet se nad velikostí písmen
		Object semanticValue = queryToken.getSemanticValue();
		if (!(semanticValue instanceof String))
			return false;
		String queryTokenLemma = (String) semanticValue;
		WordNet wordNet = SharedObjects.getCzechWordNet();
		SynSetList synSets = wordNet.findSynSets(lemma);
		switch(subtype) {
		case SYNONYM:
			for (String synonym : synSets.getWords()) {
				if (queryTokenLemma.equalsIgnoreCase(synonym))
					return true;
			}
			break;
		case HYPERNYM:
				for (SynSet hyponyms : Iteration.searchTree(synSets, LinkType.HYPERNYM, true, 100)) {
					for (String hyponym : hyponyms.getWords()) {
						if (queryTokenLemma.equalsIgnoreCase(hyponym))
							return true;
					}
				}
			break;
		default:
			assert false;
		}
		return false;
	}
}
