package lase.template;

/**
 * <p>
 * Token šablony sloužící k určení polohy pojmenovaný entity (jedno- i
 * víceslovné) v otázce nebo staženém textu.
 * </p>
 * 
 * <p>
 * Pro nalezení shody s pojmenovanou entitou v otázce nebo staženém
 * textu je požadována rovnost podtypu entity {@link NamedEntityToken.Subtype}.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class NamedEntityToken extends Token {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Podtyp tokenu.
	 * @see NamedEntityToken.Subtype
	 */
	private Subtype subtype;

	/**
	 * Vytvoří a inicializuje nový {@link NamedEntityToken}. Číselné ID nastavní
	 * na hodnotu 0.
	 * 
	 * @param subtype
	 *            - podtyp tokenu (viz {@link NamedEntityToken.Subtype}).
	 */
	public NamedEntityToken(Subtype subtype) {
		this(0, subtype);
	}

	/**
	 * Vytvoří a inicializuje nový {@link NamedEntityToken}. Číselné ID nastavní
	 * na hodnotu 0.
	 * 
	 * @param id
	 *            - číselné ID tokenu.
	 * 
	 * @param subtype
	 *            - podtyp tokenu (viz {@link NamedEntityToken.Subtype}).
	 */
	public NamedEntityToken(int id, Subtype subtype) {
		super(id, Token.Type.NAMED_ENTITY);
		this.subtype = subtype;
	}

	
	/**
	 * Vrací podtyp tokenu.
	 * @see NamedEntityToken.Subtype
	 */
	public Subtype getSubtype() {
		return subtype;
	}
	
	/**
	 * <p>
	 * Podtyp tokenu typu NAMED_ENTITY.
	 * </p>
	 * 
	 * <strong>Tabulka podtypů</strong>
	 * <table border="1" cellspacing="0" cellpadding="5">
	 * <tr>
	 * <th>Podtyp</th><th>Význam</th><th>Příklady</th>
	 * </tr><tr>
	 * <td>DATE</td><td>Datum</td><td>"1.1.1970", "květen 09", "0"</td>
	 * </tr><tr>
	 * <td>TIME</td><td>Čas</td><td>"0:00", "10 hodin"</td>
	 * </tr><tr>
	 * <td>DATE_TIME</td><td>Datum a čas</td><td>"1. ledna 1970 0:00", "roku 0 v 10 hodin"</td>
	 * </tr><tr>
	 * <td>NUMBER</td><td>Číslo</td><td>"44", "čtyřicátý čtvrtý"</td>
	 * </tr><tr>
	 * <td>PERSON</td><td>Jméno osoby</td><td>"Bedřich Smetana", "Karel IV."</td>
	 * </tr><tr>
	 * <td>GEOGRAPHY</td><td>Geografický název</td><td>"Česká republika", "Prahou", "Malše", "Šumavě", "USA"</td>
	 * </tr>
	 * </table>
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	public enum Subtype {
		DATE,
		TIME,
		DATE_TIME,
		NUMBER,
		PERSON,
		GEOGRAPHY,
		ADDRESS,
		INSTITUTIONS,
		MEDIA,
		ARTIFICILA_NAMES,
		OTHER
	}

	
	/**
	 * Vždy vrací <code>null</code>. {@link NamedEntityToken} neobsahuje žádnou
	 * smysluplnou textovou informaci. Určuje pouze podtyp pojmenované entity,
	 * na základě kterého je testována shoda.
	 */
	public String getText() {
		return null;
	}

	/**
	 * Vrací, <code>true</code>, shoduje-li se podtyp pojmenované entity
	 * nalezené v otázce nebo ve staženém textu (je-li vůbec daný
	 * {@link QueryToken} pojmenovanou entitou) s podtypem tohoto
	 * {@link NamedEntityToken}u. V ostatních případech vrací <code>false</code>
	 */
	@Override
	public boolean matches(QueryToken queryToken) {
		return equals(queryToken.getToken());
	}
	
	public static Subtype getSubtype(String type) {
		
		switch(type.toLowerCase().charAt(0)) {
		
		case 'a' : return Subtype.ADDRESS;
		case 'g' : return Subtype.GEOGRAPHY;
		case 'i' : return Subtype.INSTITUTIONS;
		case 'm' : return Subtype.MEDIA;
		case 'n' : return Subtype.NUMBER;
		case 'o' : return Subtype.ARTIFICILA_NAMES;
		case 'p' : return Subtype.PERSON;
		default : return Subtype.OTHER;
		
		}
	}
}
