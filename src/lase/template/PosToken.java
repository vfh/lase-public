package lase.template;

import java.util.Arrays;
import java.util.List;

import lase.app.SharedObjects;
import lase.morphology.MorphWord;
import lase.morphology.MorphologicalAnalyser;
import lase.morphology.MorphologyException;

/**
 * <p>
 * Token šablony, u kterého je při porovnání se slovem otázky nebo staženého
 * textu požadovna shoda slovního druhu.
 * </p>
 * 
 * <p>
 * Jediným atributem tohoto tokenu je značka slovního druhu, kterou také
 * vrací metoda {@link #getText()}.
 * </p>
 * 
 * @author Rudolf Šíma
 *
 */
public class PosToken extends Token {
	
	/**
	 * Značka slovního druhu.
	 * 
	 * @see MorphWord
	 */
	private char posTag;

	/**
	 * Vytvoří a inicializuje novou instanci {@link PosToken}u. Číselné ID je
	 * nastaveno na hodnotu 0.
	 * 
	 * @param posTag
	 *            - značka slovního druhu (viz {@link MorphWord}).
	 * 
	 * @throws IllegalArgumentException
	 *             - vyhozena v případě, že argument určující slovní druh není
	 *             platnou značkou slovního druhu.
	 */
	public PosToken(char posTag) {
		this(0, posTag);
	}

	/**
	 * Vytvoří a inicializuje novou instanci {@link PosToken}u.
	 * 
	 * @param id
	 *            - číselné ID.
	 * @param posTag
	 *            - značka slovního druhu (viz {@link MorphWord}).
	 * 
	 * @throws IllegalArgumentException
	 *             - vyhozena v případě, že argument určující slovní druh není
	 *             platnou značkou slovního druhu.
	 */
	public PosToken(int id, char posTag) {
		super(id, Token.Type.POS);
		this.posTag = Character.toUpperCase(posTag);
		checkTag(this.posTag);
	}

	/**
	 * Vrací značku slovního druhu (viz {@link MorphWord}).
	 * 
	 * @return Vrací značku slovního druhu.
	 */
	public char getPosTag() {
		return posTag;
	}
	
	/**
	 * Vrací řetězec o délce jeden znak obsahující značku slovního druhu (viz
	 * {@link MorphWord}).
	 * 
	 * @return Vrací řetězec se značkou slovního druhu.
	 */
	public String getText() {
		return String.valueOf(posTag);
	}

	/**
	 * Vždy vrací <code>null</code>. Token typu POS nemá definovány
	 * žádné podtypy.
	 */
	public Enum<?> getSubtype() {
		return null;
	}

	/**
	 * Vrací <code>true</code>, shoduje-li se slovní druh tohoto
	 * {@link PosToken}u se slovním druhem testovaného slova v otázce nebo
	 * staženém textu. V ostatních případech vrací <code>false</code>.
	 */
	@Override
	public boolean matches(QueryToken queryToken) {
		/* Analýza by se možná vůbec neměla provádět tady, ale analyzátor */
		/* používá cache, takže to není velký problém.                    */
		/* TODO - QueryToken by měl obsahovat MorphWord.                  */
		MorphologicalAnalyser analyser = SharedObjects.getMorphologicalAnalyser();
		try {
			List<MorphWord> morphWords = analyser.analyse(Arrays.asList(queryToken.getOriginalWord()));
			if (morphWords.size() != 1) {
				/* Toto může nastat v případě víceslovné pojmenované entity, 
				 * u které stejně nelze slovní druh určit. */
				return false;
			}
			char queryWordPos = Character.toUpperCase(morphWords.get(0).getPosTag());
			return posTag == queryWordPos;
		} catch (MorphologyException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Provede kontrolu správnosti tagu. V případě, že argument není platnou
	 * značkou slovního druhu, vyhodí {@link IllegalArgumentException}.
	 * 
	 * @throws IllegalArgumentException
	 *             - vyhozena v případě, že argument určující slovní druh není
	 *             platnou značkou slovního druhu.
	 * 
	 * @param posTag
	 *            - značka slovního druhu (viz {@link MorphWord}).
	 */
	private static void checkTag(char posTag) {
		switch(posTag) {
		case 'N':
		case 'V':
		case 'B':
		case 'P':
		case 'A':
		case 'C':
			break;
		default:
			throw new IllegalArgumentException(Character.toString(posTag));
		}
	}
}
