package lase.template;

/**
 * <p>
 * Chyba poukazující na pokus o použití špatně vytvořené šablony. K vyhození
 * této chyby by nemělo nikdy dojít, pokud jsou šablony správně zkontrolovány
 * při načítání.
 * </p>
 * 
 * <p>
 * Příkladem špatně vytvořené šablony může být šablona, kde vyhledávací vzor
 * obsahuje referenci na token, který ve vzoru otázky neexistuje.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 * @see Template
 * 
 */
public class BadTemplateAssertionError extends AssertionError {

	private static final long serialVersionUID = 1L;

	/**
	 * Vytvoří novou instanci chyby bez podrobné zprávy.
	 */
	public BadTemplateAssertionError() {
	}

	/**
	 * Vytvoří novou instanci chyby a nastaví podrobnou zprávu.
	 * 
	 * @param message
	 *            Podrobná zpráva.
	 */
	public BadTemplateAssertionError(String message) {
		super(message);
	}
}
