package lase.template;

import lase.util.LaseException;

/**
 * <p>
 * Výjimka poukazující na pokus o použití (např. načtení) špatně vytvořené šablony.
 * </p>
 * 
 * <p>
 * Příkladem špatně vytvořené šablony může být šablona, kde vyhledávací vzor obsahuje
 * referenci na token, který ve vzoru otázky neexistuje.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 * @see Template
 *
 */
public class BadTemplateException extends LaseException {

	private static final long serialVersionUID = 1L;

	/**
	 * Vytvoří novou výjimku bez podrobné zprávy a nastavení příčiny.
	 */
	public BadTemplateException() {
	}

	/**
	 * Vytvoří novou výjimku, nastaví podrobnou zprávu a příčinu.
	 */
	public BadTemplateException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Vytvoří novou výjimku a nastaví podrobnou zprávu.
	 */
	public BadTemplateException(String message) {
		super(message);
	}

	/**
	 * Vytvoří novou výjimku a nastaví příčinu.
	 */
	public BadTemplateException(Throwable cause) {
		super(cause);
	}
}
