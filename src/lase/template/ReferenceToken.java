package lase.template;

/**
 * <p>
 * Token typu {@link Token.Type#REFERENCE}. Smysluplné použití tohoto tokenu je
 * pouze ve vyhledávacím vzoru šablony, kde slouží jako odkaz na tokeny vzoru
 * otázky, ať už vyjádřené nebo nevyjádřené. Je tedy možné (a vhodné) se ve
 * vzoru odpověďi odkazovat na token, jehož konkrétní podoba bude známa až po
 * zadání otázky uživatelem.
 * </p>
 * 
 * <p>
 * <h3>Příklad šablony:</h3>
 * 
 * <pre>
 * &lt;template&gt;
 *    &lt;query&gt;
 *       &lt;token id="1"&gt;kdy&lt;/token&gt;
 *       &lt;token id="2"&gt;se&lt;/token&gt;
 *       &lt;token id="3" type="lemma"&gt;narodit&lt;/token&gt;
 *       &lt;token id="4" type="person"/&gt;
 *    &lt;/query&gt;
 *    &lt;pattern&gt;
 *       &lt;token id="1" ref="4"/&gt;
 *       &lt;token id="2" ref="2"/&gt;
 *       &lt;token id="3" ref="3"/&gt;
 *       &lt;token id="4" mod="answer" type="date"/&gt;
 *    &lt;/pattern&gt;
 * &lt;/template&gt;
 * </pre>
 * 
 * V tomto příkladu tokeny vyhledávacího vzoru 1, 2 a 3 odkazují na tokeny vzoru
 * otázky po řadě 4, 2 a 3. Token vyhledávacího vzoru číslo 1 se odkazuje na
 * nevyjádřený token otázky, jehož konkrétní podoba bude známa až ve chvíli
 * zadání otázky uživatelem.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class ReferenceToken extends Token {
	
	/**
	 * ID tokenu vzoru otázky, na který tento token odkazuje.
	 */
	int refId;

	/**
	 * Vytvoří nový token typu reference a nastaví ID tokenu vzoru otázky, na
	 * který odkazuje. ID nově vytvářeného tokenu bude nastaveno na hodnotu 0.
	 * 
	 * @param refId
	 *            ID tokenu vzoru otázky, na který tento token odkazuje.
	 */
	public ReferenceToken(int refId) {
		this(0, refId);
	}

	/**
	 * Vytvoří nový token typu reference, nastaví ID tohoto vytvářeného tokenu a
	 * ID tokenu vzoru otázky, na který odkazuje.
	 * 
	 * @param id
	 *            ID tohoto vytvářeného tokenu.
	 * @param refId
	 *            ID tokenu vzoru otázky, na který tento token odkazuje.
	 */
	public ReferenceToken(int id, int refId) {
		super(id, Token.Type.REFERENCE);
		this.refId = refId;
	}

	/**
	 * Vrací ID tokenu vzoru otázky, na který tento token odkazuje
	 * 
	 * @return Vrací ID odkazovaného tokenu.
	 */
	public int getRefId() {
		return refId;
	}

	/**
	 * Tato metoda je uvedena proto, že ji abstraktní předek {@link Token}
	 * vyžaduje. Token typu {@link Token.Type#REFERENCE} nikdy nemá platný
	 * textový obsah, a proto tato metody vždy vrací {@code null}.
	 */
	public String getText() {
		return null;
	}

	/**
	 * Tato metoda je uvedena proto, že ji abstraktní předek {@link Token}
	 * vyžaduje. Token typu {@link Token.Type#REFERENCE} nemá definovány žádné
	 * podtypy, a tato metody vždy vrací {@code null}.
	 */
	public Enum<?> getSubtype() {
		return null;
	}

	/**
	 * Tato metody je definována rozhraním {@link QueryTokenMatcher}, kterou
	 * implmentuje abstraktní předek {@link Token}. Token typu
	 * {@link Token.Type#REFERENCE} by nikdy neměl být testován na shodu s
	 * rozšířeným tokenem. Tato metody vždy vrací {@code false}.
	 */
	@Override
	public boolean matches(QueryToken queryToken) {
		assert false; // NEKLEPAT!
		return false;
	}
}
