package lase.template;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

import lase.answersearch.Countable;
import lase.util.StringUtils;

/**
 * <p>
 * Seznam rozšířených tokenů sloužící jako datová struktura vyhledávačem
 * pojmenovaných entit předzpracované uživatelem zadané otázky. Jinými slovy
 * jde o jednu cestu v grafu pojmenovaných entit z počátečního do některého
 * koncového uzlu.
 * </p>
 * 
 * <p>
 * Tato datová struktura je ovšem používána obecně jako seznam rozšířených
 * tokenů resp. jakákoli cesta v grafu pojmenovaných entit, tedy i v případě
 * cesty v grafu pojmenovaných entit sestaveného podle snippetů z korpusu
 * nalezeného na internetu. V tomto případě je ovšem název {@code Query} poněkud
 * zavádějící.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 * @see QueryToken
 * 
 */
public class Query implements RandomAccess, Iterable<QueryToken>, Countable {
	
	/**
	 * Seznam v této struktuře obsažených rozšířených tokenů.
	 */
	private List<QueryToken> queryTokens;

	/**
	 * Cache metody {@link #toString()}. Tato metoda může být volána velmi často
	 * a cache je proto použita, aby se zamezilo procházení seznamu a vytáření
	 * řetězce, který bude vrácen.
	 */
	private String toStringCache;

	/**
	 * Vytvoří nový seznam seznam rozšířených tokenů z daného seznamu tokenů.
	 * 
	 * @param queryTokens
	 *            Zdrojový seznam rozšířených tokenů. Tento seznam se nekopíruje
	 *            a musí být instancí třídy, která implementuje rozhraní
	 *            {@link RandomAccess}.
	 */
	public Query(List<QueryToken> queryTokens) {
		this(queryTokens, false);
	}

	/**
	 * Vytvoří nový seznam seznam rozšířených tokenů z daného seznamu tokenů.
	 * 
	 * @param queryTokens
	 *            Zdrojový seznam rozšířených tokenů. Tento seznam se může
	 *            kopírovat v závislosti na parametru {@code copy}.
	 * 
	 * @param copy
	 *            Udává, zda se má seznam {@code queryTokens} kopírovat do
	 *            nového interního seznamu s náhodným přístupem. Pokud má tento
	 *            argument hodnotu {@code false}, je požadováno, aby byl
	 *            argument {@code queryTokens} instancí třídy implementující
	 *            rozhraní {@link RandomAccess}, např. {@link ArrayList}.
	 */
	public Query(List<QueryToken> queryTokens, boolean copy) {
		if (copy)
			this.queryTokens = Collections.unmodifiableList(new ArrayList<QueryToken>(queryTokens));
		else {
			if (!(queryTokens instanceof RandomAccess))
				throw new IllegalArgumentException("The list of QueryTokens must be an instance of RandomAcces, i.e. an ArrayList.");
			this.queryTokens = Collections.unmodifiableList(queryTokens);
		}
	}

	/**
	 * Vrací rozšířený token podle zadaného indexu.
	 * 
	 * @param index
	 *            Index rozšířeného tokenu v seznamu.
	 *            
	 * @return Vrací rozšířený token ze seznamu daný indexem.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             Vyhozena v případě, že daný index není platný.
	 */
	public QueryToken get(int index) {
		return queryTokens.get(index);
	}

	/**
	 * Vrací velikost tohoto seznamu rozšířených tokenů, tj. počet rozšířených
	 * tokenů v tomto seznamu obsažených.
	 * 
	 * @return Vrací velikost tohoto seznamu.
	 */
	public int size() {
		return queryTokens.size();
	}

	/**
	 * Vrací read-only iterátor tohoto seznamu rozšířených tokenů.
	 */
	@Override
	public Iterator<QueryToken> iterator() {
		return queryTokens.iterator();
	}

	/**
	 * Vrací řetězec vzniklý spojením původních slov všech rozšířených tokenů
	 * obsažených v tomto seznamu, mezi který jsou vloženy mezery.
	 * 
	 * @see QueryToken
	 */
	public String toString() {
		if (toStringCache == null) {
			toStringCache = StringUtils.workaroundExtraSpaces(
					StringUtils.concat(this, " "));
		}
		return toStringCache;
	}

	/**
	 * Zjistí, zda se původní slovo (původní formulace z textu) rozšířeného
	 * tokenu s určitým indexem shoduje s daným slovem (řetězcem).
	 * 
	 * @param index
	 *            Index rozšířeného tokenu v seznamu, jehož původní formulace je
	 *            testována na rovnost.
	 * @param word
	 *            Slovo, které bude porovnáno s původní formulací indexem
	 *            určeného rozšířeného tokenu.
	 * 
	 * @return Vrací {@code true}, shodují-li se porovnávané řetězce (s ohledem
	 *         na velikost písmen). Jinak vrací {@code false}.
	 */
	public boolean wordEquals(int index, String word) {
		return queryTokens.get(index).wordEquals(word);
	}

	/**
	 * Vytvoří z tohoto seznamu rozšířených tokenů podseznam dané počátečním a
	 * koncovým indexem.
	 * 
	 * @param fromIndex
	 *            Počáteční index podseznamu, tj. index prvního prvku, který
	 *            bude zkopírovnán do nového seznamu.
	 * @param toIndex
	 *            Koncový index podseznamu, tj. index prvního prvku, který již
	 *            <strong>nebude</strong> zkopírován do nového seznamu.
	 *            
	 * @return Vrací vytvořený podseznam rozšířených tokenů.
	 */
	public Query subQuery(int fromIndex, int toIndex) {
		List<QueryToken> newTokenList = queryTokens.subList(fromIndex, toIndex);
		return new Query(newTokenList);
	}

	/**
	 * <p>
	 * Tato metoda používána při hledání řetězce v textu, který odpovídá vzoru
	 * definovanému tímto seznamem rozšířených tokenů.
	 * </p>
	 * 
	 * <p>
	 * Pro indexem určený token zjistí, který token s nejvyšším indexem po něm
	 * může následovat, pokud budou se nevyskytne žádný z dobrovolných tokenů.
	 * Situace je vysvětlena na následujícím příkladu:<br>
	 * Nechť seznam obsahuje rozšířené tokeny znázorněné písmeny a, b, c, d, e,
	 * f
	 * 
	 * <pre>
	 * ( a, b, [c], [d], e, f )
	 * </pre>
	 * 
	 * Tokeny {@code c} a {@code d} jsou volitelné. V textu se mohou a nemusí
	 * vyskytovat. Výsledky, který vrátí tato metody pro různé indexy uvedeného
	 * seznamy jsou uvedeny v následující tabulce:
	 * <table border="1">
	 * <tr>
	 * <th>Index</th>
	 * <th>Indexu odpovídající token</th>
	 * <th>Výsledek getFollowSetUBound</th>
	 * <th>Výsledku odpovídající token</th>
	 * </tr>
	 * <tr>
	 * <td align="center">0</td>
	 * <td align="center">a</td>
	 * <td align="center">1</td>
	 * <td align="center">b</td>
	 * </tr>
	 * <tr>
	 * <td align="center">1</td>
	 * <td align="center">b</td>
	 * <td align="center">4</td>
	 * <td align="center">e</td>
	 * </tr>
	 * <tr>
	 * <td align="center">2</td>
	 * <td align="center">c</td>
	 * <td align="center">4</td>
	 * <td align="center">e</td>
	 * </tr>
	 * <tr>
	 * <td align="center">3</td>
	 * <td align="center">d</td>
	 * <td align="center">4</td>
	 * <td align="center">e</td>
	 * </tr>
	 * <tr>
	 * <td align="center">4</td>
	 * <td align="center">e</td>
	 * <td align="center">5</td>
	 * <td align="center">f</td>
	 * </tr>
	 * <tr>
	 * <td align="center">5</td>
	 * <td align="center">f</td>
	 * <td align="center">6</td>
	 * <td align="center">[konec seznamu]</td>
	 * </tr>
	 * 
	 * @param index
	 *            Index tokenu, pro který má být zjištěn maximální index
	 *            následujícího tokenu.
	 * 
	 * @return Vrací maximální index následujícího tokenu.
	 */
	public int getFollowSetUBound(int index) {
		int size = size();
		int uBound;
		for (uBound = index + 1; uBound < size; uBound++) {
			if (!queryTokens.get(uBound).getToken().isOptional())
				return uBound + 1;
		}
		return size;
	}
	
	// TODO - comments
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((queryTokens == null) ? 0 : queryTokens.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Query other = (Query) obj;
		if (queryTokens == null) {
			if (other.queryTokens != null)
				return false;
		} else if (!queryTokens.equals(other.queryTokens))
			return false;
		return true;
	}
}
