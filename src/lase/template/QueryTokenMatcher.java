package lase.template;

/**
 * <p>
 * Instance tříd implementující toto rozhraní jsou objekty, u kterých má smysl
 * zjišťovat shodu s rozšířeným tokenem ({@link QueryToken}).
 * </p>
 * 
 * <p>
 * Třídy implementující toto rozhraní jsou zpravidla potomci abstraktní třídy
 * {@link Token}.
 * </p>
 * 
 * <p>
 * <strong>Příklad:</strong> Existuje-li rozšířený token skládající se tokenu
 * typu WORD a původního slova "zemřel" může být porovnán s tokenem typu LEMMA s
 * textovou hodnotou "zemřít" a výsledkem tohoto porovnání by měla být shoda.
 * Jinými slovy bude nad tokenem typu LEMMA zavolána metoda
 * {@link #matches(QueryToken)}, které se jako argument předá zmíněný rozšířený
 * token.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public interface QueryTokenMatcher {

	/**
	 * Zjístí shodu dat objektu implementujícího toto rozhraní s daným
	 * rozšířeným tokenem. Viz {@link QueryTokenMatcher}.
	 * 
	 * @param queryToken
	 *            - Rozšířený token, se kterým se provede porovnání.
	 * 
	 * @return Vrací <code>true</code>, pokud byla zjištěna shoda, jinak vrací
	 *         <code>false</code>.
	 * 
	 * @see QueryTokenMatcher
	 */
	public boolean matches(QueryToken queryToken);
	
}
