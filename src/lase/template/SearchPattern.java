package lase.template;

import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

/**
 * <p>
 * Vyhledávací vzor vytvořený modulem převádějícím otázku na pahýl odpovědi (
 * {@link QuestionTransformer}).
 * </p>
 * 
 * <p>
 * Je implementován seznamem rozšířených tokenů stejně jako jeho předek
 * {@link Query}. Navíc přidává metodu {@link #getSearchStrings()} sloužící k
 * sestavení vyhledávacích řetězců, které jsou zadány do webového vyhledávače za
 * účelem stažení korpusu, ve kterém se vyhledává odpověď.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class SearchPattern extends Query  {

	/**
	 * Cache vyhledávacích řetězců. Metoda {@link #getSearchStrings()} může být
	 * volána pro jeden vyhledávací vzor opakovaně a cache předchází nutnosti
	 * znovu sestavovat vyhledávací řetězec.
	 */
	private String[] searchStringsCache;

	/**
	 * Vytvoří nový vyhledávací vzor ze seznamu rozšířených tokenů.
	 * 
	 * @param queryTokens
	 *            Zdrojový seznam rozšířených tokenů. Tento seznam se nekopíruje
	 *            a musí být instancí třídy, která implementuje rozhraní
	 *            {@link RandomAccess}.
	 */
	public SearchPattern(List<QueryToken> queryTokens) {
		super(queryTokens);
		if (queryTokens == null)
			throw new NullPointerException();
	}

	/**
	 * <p>
	 * Vytvoří seznam vyhleávacích řetězců pro tento vyhledávací vzor, které
	 * jsou určeny pro zadání do webového vyhledávače. Prvky tohoto seznamu jsou
	 * přesné fráze, určené k hledání stránek, které obsahují všechny z nich
	 * zároveň.
	 * </p>
	 * 
	 * <p>
	 * Vytvoření vyhledávacích řetězců se provede spojením původních slov
	 * (formulací v původním textu) rozšířených tokenů obsažených v tomto
	 * vyhledávacím vzoru s vynecháním volitelných tokenů, které se v hledané
	 * odpovědi nemusí vyskytovat.
	 * </p>
	 * 
	 * @return Vrací pole řetězců obsahující přesné fráze určené k zadání do
	 *         webového vyhledávače tak, aby byly nalezeny stránky obsahující
	 *         všechny fráze zároveň.
	 */
	public String[] getSearchStrings() {
		if (searchStringsCache == null) {
			List<String> searchStrings = new ArrayList<String>();
			StringBuilder builder = new StringBuilder();
			for (QueryToken qt : this) {
				Token t = qt.getToken();
				String orig = qt.getOriginalWord();
				if (t.isOptional())
					continue;
				if (orig == null) {
					if (!t.isOptional()) {
						if (builder.length() > 0) {
							searchStrings.add(builder.toString());
							builder.setLength(0);
						}
					}
				} else {
					if (builder.length() > 0)
						builder.append(" ");
					builder.append(orig);
				}
			}
			if (builder.length() > 0)
				searchStrings.add(builder.toString());
			searchStringsCache = searchStrings.toArray(new String[searchStrings.size()]);
		}
		return searchStringsCache;
	}
	
	/* (non-Javadoc)
	 * @see lase.template.Query#hashCode()
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	/* (non-Javadoc)
	 * @see lase.template.Query#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
}