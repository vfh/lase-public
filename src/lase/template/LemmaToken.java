package lase.template;

/**
 * Token šablony, u kterého je při porovnání požadována shoda lemmatu slova z
 * šablony nebo staženého textu s atribute {@link #lemma} tohoto
 * <code>LemmaToken</code>u.
 * 
 * <code>LemmaToken</code> je vždy vyjádřený. Pro informace o vyjádřenosti viz
 * {@link WordToken}.
 * 
 * @author Rudolf Šíma
 * 
 */
public class LemmaToken extends Token {
	
	/**
	 * Slovníkový tvar slova požadovaného pro nalezení shody s tímto tokenem
	 * v šabloně.
	 */
	private String lemma;
	
	
	/**
	 * Vytvoří a inicializuje nový {@link LemmaToken}. Číslené ID je nastaveno
	 * na hodnotu 0.
	 * 
	 * @param lemma
	 *            - slovníkový tvar slova požadovaného pro nalezení shody s
	 *            tímto tokenem v šabloně.
	 */
	public LemmaToken(String lemma) {
		this(0, lemma);
	}
	
	/**
	 * Vytvoří a inicializuje nový {@link LemmaToken}.
	 * 
	 * @param id - číselné ID tohoto tokenu.
	 * 
	 * @param lemma
	 *            - slovníkový tvar slova požadovaného pro nalezení shody s
	 */
	public LemmaToken(int id, String lemma) {
		super(id, Token.Type.LEMMA);
		this.lemma = lemma;
	}
	
	/**
	 * Vrací hodnotu atributu {@link #lemma} tohoto {@link LemmaToken}u.
	 * 
	 * @return Vrací hodnotu atributu {@link #lemma} tohoto {@link LemmaToken}u.
	 */
	public String getLemma() {
		return lemma;
	}

	/**
	 * Vrací stejný řetězec jako metody {@link #getLemma()}. Uvedena z důvodu
	 * dědění od abstraktní třídy {@link Token}.
	 */
	public String getText() {
		return lemma;
	}
	
	/**
	 * Vždy vrací <code>null</code>. Token typu WORD nemá definovány žádné
	 * podtypy.
	 */
	public Enum<?> getSubtype() {
		return null;
	}

	/**
	 * Vrací <code>true</code>, je-li nalezena shoda tohoto tokenu s daným
	 * rozšířeným tokenem z otázky nebo staženého textu, tj. slovníkový tvar
	 * původního slova rozšířeného tokenu je textově roven atributu
	 * {@link #lemma} tohoto {@link LemmaToken}u. V ostatních případech vrací
	 * <code>false</code>.
	 */
	@Override
	public boolean matches(QueryToken queryToken) {
		Object semanticValue = queryToken.getSemanticValue();
		if (!(semanticValue instanceof String))
			return false;
		return ((String)semanticValue).equalsIgnoreCase(lemma);
	}
}
