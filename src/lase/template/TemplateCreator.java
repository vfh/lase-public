package lase.template;

import java.util.ArrayList;
import java.util.List;

/**
 * Nástroj pro vytváření šablon ({@link Template}).
 * 
 * <p>
 * <strong>Příklad vytvoření jednoduché šabony</strong>
 * <pre>
 * TemplateCreator creator = new TemplateCreator();
 * 
 * // vytvoření vzoru otázky
 * creator.addToken(new WordToken(&quot;kdy&quot;));
 * creator.addToken(new WordToken(&quot;se&quot;));
 * creator.addToken(new LemmaToken(&quot;narodit&quot;));
 * creator.addToken(new NamedEntityToken(NamedEntityToken.Subtype.PERSON));
 * 
 * // přidání prvního vzoru odpovědi
 * creator.newAnswerPattern();
 * 
 * // vytvoření vzoru odpovědi
 * creator.addToken(new ReferenceToken(4));
 * creator.addToken(new ReferenceToken(2));
 * creator.addToken(new ReferenceToken(3));
 * creator.addToken(new NamedEntityToken(NamedEntityToken.Subtype.DATE));
 * 
 * // přidání a vytvoření dalších vzorů odpovědi ...
 *
 * // získání vytvořené šablony
 * Template createdTemplate = creator.getTemplate();
 * </pre>
 * 
 * @author Rudolf Šíma
 * 
 */
public final class TemplateCreator {
	
	/**
	 * Seznam tokenů vzoru otázky.
	 */
	private List<Token> queryPattern;
	
	/**
	 * Seznam tokenů právě vytvářeného vzoru odpovědi.
	 */
	private List<Token> currentAnswerPattern;
	
	/**
	 * Seznam vzorů odpovědí (předloh vyhledávacích vzorů).
	 */
	private List<TokenList> answerPatterns;

	/**
	 * Počítadlo tokenů, na základě jehož hodnoty se přidávaným tokenům
	 * přiřazuje ID.
	 */
	private int idCounter;
	
	/**
	 * Vytvoří novou instanci nástroje pro vytváření šablon.
	 */
	public TemplateCreator() {
		queryPattern = new ArrayList<Token>();
		answerPatterns = new ArrayList<TokenList>();
	}

	/**
	 * Přidá token do vytvářeného seznamu tokenů. Před prvním voláním metody
	 * {@link #newAnswerPattern()} je to seznam tokenů vzoru otázky, po jejím
	 * volání je to seznam tokenů posledního přidaného vzoru odpovědi.
	 * 
	 * @param t
	 *            Token, které bude přidán.
	 */
	public void addToken(Token t) {
		List<Token> destination;
		if (currentAnswerPattern == null)
			destination = queryPattern;
		else
			destination = currentAnswerPattern;
		t.setId(++idCounter);		
		destination.add(t);
	}

	/**
	 * Založí nový prázdný vzor odpovědi a přidá jej do seznam vzorů odpovědí.
	 * Od této chvíle bude metoda {@link #addToken(Token)} přidávat token do
	 * tohoto nového vzoru odpovědi.
	 */
	public void newAnswerPattern() {
		flushPatterns();
		currentAnswerPattern = new ArrayList<Token>();
	}

	/**
	 * Vrací šablonu vytvořenou tímto nástrojem pro vytváření šablon.
	 * 
	 * @return Vrací vytvořenou šablonu.
	 */
	public Template getTemplate() {
		flushPatterns();
		return new Template(this);
	}
	
	// TODO - comment
	public Template getTemplate(Template questionPattern) {
		flushPatterns();
		return new Template(this, questionPattern);
	}

	/**
	 * Vrací seznam tokenů vzoru otázky. Tato metoda by měla být volána výhradně
	 * metodami třídy {@link Template}.
	 * 
	 * @return Vrací seznam tokenů vzoru otázky.
	 */
	TokenList getQueryPattern() {
		return new TokenList(queryPattern);
	}

	/**
	 * Vrací seznam seznamů tokenů vzorů odpovědí. Tato metoda by měla být
	 * volána výhradně metodami třídy {@link Template}.
	 * 
	 * @return Vrací seznam vzorů odpovědí.
	 */
	List<TokenList> getAnswerPatterns() {
		return answerPatterns;
	}

	/**
	 * Vytvoří {@link TokenList} ze seznamu tokenů aktuálního vzoru odpovědi a
	 * vzniklý vzor odpovědi přidá do seznamu vzorů odpovědí.
	 */
	private void flushPatterns() {
		copyTokenList(currentAnswerPattern, answerPatterns);
		currentAnswerPattern = null;
		idCounter = 0;
	}

	/**
	 * Vytvoří {@link TokenList} ze seznamu tokenů a vzniklý vzor odpovědi přidá
	 * do seznamu seznamu {@code destination}.
	 * 
	 * @param list
	 *            Zdrojový seznam tokenů, ze kterého bude vytvořen nový
	 *            {@link TokenList}.
	 * @param destination
	 *            Cílový seznam, do kterého bude vytvoření {@link TokenList}
	 *            přidán
	 */
	private void copyTokenList(List<Token> list, List<TokenList> destination) {
		if (list != null && !list.isEmpty()) {
			TokenList copy = new TokenList(list);
			destination.add(copy);
		}
	}
}
