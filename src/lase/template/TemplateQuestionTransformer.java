package lase.template;

import java.util.ArrayList;
import java.util.List;

import lase.app.LaseApp;

/**
 * Třída implementující předvod otázky na pahýly odpovědí (tj. vyhledávací
 * vzory) podle předem vytvořených šablon (viz {@link Template}).
 * 
 * @author Rudolf Šíma
 * 
 * @see QuestionTransformer
 * @see Template
 * @see LaseApp
 * 
 */
public class TemplateQuestionTransformer implements QuestionTransformer {

	/**
	 * Seznam šablon, ve kterých se hledá shoda se zadanou otázkou, a dojde-li
	 * ke shodě, na základě kterých se vytvoří vyhledávací vzory.
	 */
	private List<Template> templates;

	/**
	 * Vytvoří nový převodník otázky na vyhledávací vzory založený na
	 * vyhledávání podle šablon a nastaví seznam používaných šablon.
	 * 
	 * @param templates
	 *            Seznam šablon, ve kterých se hledá shoda se zadanou otázkou, a
	 *            dojde-li ke shodě, na základě kterých se vytvoří vyhledávací
	 *            vzory.
	 */
	public TemplateQuestionTransformer(List<Template> templates) {
		this.templates = templates;
	}

	/* (non-Javadoc)
	 * @see lase.template.QuestionTransformer#transform(lase.template.Query)
	 */
	public List<SearchPattern> transform(Query query) {
		List<SearchPattern> result = new ArrayList<SearchPattern>();
		for (Template template : templates) {
			TemplateMatchResult matchResult = template.matchQuery(query);
			if (matchResult.matches()) {
				result.addAll(matchResult.getSearchPatterns());
			}
		}
		return result;
	}
}
