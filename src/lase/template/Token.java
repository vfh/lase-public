package lase.template;

import java.util.Set;
import java.util.TreeSet;

import lase.util.GenericEnumsWorkaround;
import lase.util.StringUtils;

/**
 * <p>
 * Abstraktní předek všech tokenů, které se vyskytují v šablonách.
 * </p>
 * 
 * <p>
 * Typy tokenů (viz {@link Type}):
 * <table cellpadding="3">
 * <tr>
 * <th align="left">Typ</th>
 * <th align="left">Implementace</th>
 * <th align="left">Význam</th>
 * <th align="left">Podmínka shody</th>
 * </tr>
 * <tr>
 * <td>WORD</td>
 * <td>WordToken</td>
 * <td>slovo</td>
 * <td>přesná shoda (case insensitive)</td>
 * </tr>
 * <tr>
 * <td>LEMMA</td>
 * <td>LemmaToken</td>
 * <td>lemma</td>
 * <td>shoda s lemmatem, tj. slovníkovým tvarem</td>
 * </tr>
 * <tr>
 * <td>POS</td>
 * <td>PosToken</td>
 * <td>slovní druh</td>
 * <td>shoda slovního druhu</td>
 * </tr>
 * <tr>
 * <td>NAMED_ENTITY</td>
 * <td>NamedEntityToken</td>
 * <td>pojmenovaná entita</td>
 * <td>shoda podtypu (viz {@link NamedEntityToken})</td>
 * </tr>
 * <tr>
 * <td>SEMANTIC_CATEGORY</td>
 * <td>SemanticCategoryToken</td>
 * <td>sémantická kategorie</td>
 * <td>podtypem definovaný vztah (viz {@link SemanticCategoryToken}).</td>
 * </tr>
 * <tr>
 * <td>REFERENCE</td>
 * <td>ReferenceToken</td>
 * <td>reference na jiný token</td>
 * <td>nemá smysl</td>
 * </tr>
 * </table>
 * 
 * Sloupec <em>Podmínka shody</em> udává podmínku, při jejímž splnění (a jen při
 * jejímž splnění) metoda {@link #matches(QueryToken)} vrací <code>true</code>.
 * 
 * @author Rudolf Šíma
 * @version 2.00 edit 28.4.2014 Ondřej Pražák
 * 
 * @see WordToken
 * @see LemmaToken
 * @see PosToken
 * @see NamedEntityToken
 * @see SemanticCategoryToken
 * @see ReferenceToken
 */
public abstract class Token implements QueryTokenMatcher {
	
	/**
	 * Číselné ID tokenu. Slouží k odkazování na token.
	 */
	private int id;
	
	/**
	 * Typ tokenu.
	 * @see Type
	 */
	private Type type;
	
	/**
	 * Množina modifikátorů tokenu.
	 * @see Modifier
	 */
	private Set<Modifier> modifiers = new TreeSet<Modifier>();
	
	/**
	 * Logická hodnota udávající, zda je token neměnný, tzn. nelze změnit
	 * nastavení voláním setteru.
	 * @see #makeImmutable()
	 */
	private boolean immutable;
	
	/**
	 * Udává kolik slov minimálně musí token obsahovat
	 */
	private int minRepeat;
	
	/**
	 * Udává kolik slov maximálně může token obsahovat
	 */
	private int maxRepeat;
	
	protected Token(int id, Type type, int minRepeat, int maxRepeat) {
		this.id = id;
		this.type = type;
		this.minRepeat = minRepeat;
		this.maxRepeat = maxRepeat;
	}
	
	protected Token(int id, Type type) {
		this(id,type,1,1);
	}
	
	/**
	 * Voláním této metody se token stane neměnným. Následné pokusy o změnu
	 * nastavení tokenu voláním setteru zopůsobí vyhození výjimky
	 * {@link IllegalStateException}. Je-li neměnitelnost tokenu jednou
	 * nastavena, nelze již tento stav změnit.
	 */
	public void makeImmutable() {
		this.immutable = true;
	}
	
	/**
	 * Udává, zda lze měnit nastavení tokenu.
	 * 
	 * @see #makeImmutable()
	 * 
	 * @return Vrací <code>true</code>, je-li možné měnit nastavení tokenu. V
	 *         opačném případě, tedy byla-li již volána metoda
	 *         {@link #makeImmutable()}, vrací <code>false</code>.
	 */
	public boolean isMutable() {
		return !immutable;
	}

	/**
	 * Udává, zda je token výskyt tokenu volitelný. Tato existuje z důvodu
	 * pohodlného použití, vrací stejný výsledek jako
	 * <code>hasModifier(Modifier.OPT)</code>.
	 * 
	 * @return Vrací <code>true</code>, je-li token volitelný, jinak vrací
	 *         <code>false</code>
	 */
	public boolean isOptional() {
		return hasModifier(Modifier.OPT);
	}

	/**
	 * Udává, zda má token význam očekávané odpovědi. Tato existuje z důvodu
	 * pohodlného použití, vrací stejný výsledek jako
	 * <code>hasModifier(Modifier.ANSWER)</code>.
	 * 
	 * @return Vrací <code>true</code>, má-li token význam očekávané odpovědi,
	 *         jinak vrací <code>false</code>.
	 */
	public boolean isAnswer() {
		return hasModifier(Modifier.ANSWER);
	}

	/**
	 * Vrací číselné ID tokenu, které slouží k odkazování na něj tokenem typu
	 * <code>REFERENCE</code>.
	 * 
	 * @return Vrací číslené ID tokenu.
	 */
	public int getId() {
		return id;
	}

	/**
	 * <p>
	 * Nastaví číselné ID tokenu, které slouží k odkazování na něj tokenem typu
	 * <code>REFERENCE</Code>.
	 * </p>
	 * 
	 * @throws IllegalStateException
	 *             v případě, že byla dříve nad tímto objektem zavolána metoda
	 *             {@link #makeImmutable()}.
	 *             
	 * @param id
	 *            - nové číselné ID tokenu
	 */
	public void setId(int id) {
		checkMutability();
		this.id = id;
	}

	/**
	 * Vrací typ tokenu.
	 * 
	 * @see Type
	 * 
	 * @return Vrací typ tokenu.
	 */
	public Type getType() {
		return type;
	}
	
	/**
	 * vrací minimální počet opakování
	 * @return minimální počet opakování
	 */
	public int getMinRepeat() {
		return minRepeat;
	}

	/**
	 * nastaví minimální počet opakování
	 * 
	 * @throws IllegalStateException
	 *             v případě, že byla dříve nad tímto objektem zavolána metoda
	 *             {@link #makeImmutable()}.
	 *             
	 * @param minRepeat
	 *            minimální počet opakování
	 */
	public void setMinRepeat(int minRepeat) {
		checkMutability();
		if(minRepeat>this.maxRepeat)
			throw new IllegalArgumentException("maximální počet opakování nemůže být menší než minimální");
		this.minRepeat = minRepeat;
	}

	/**
	 * vrací maximální počet opakování
	 * @return maximální počet opakování
	 */
	public int getMaxRepeat() {
		return maxRepeat;
	}

	/**
	 * nastaví maximální počet opakování
	 * 
	 * @throws IllegalStateException
	 *             v případě, že byla dříve nad tímto objektem zavolána metoda
	 *             {@link #makeImmutable()}.
	 *             
	 * @param maxRepeat
	 *            maximální počet opakování
	 */
	public void setMaxRepeat(int maxRepeat) {
		checkMutability();
		if(maxRepeat<this.minRepeat)
			throw new IllegalArgumentException("maximální počet opakování nemůže být menší než minimální");
		this.maxRepeat = maxRepeat;
	}

	/**
	 * Nastaví tokenu nový modifikátor. Pokud již token tento modifikátor
	 * nastaven má, nemá volání této metody žádný vliv.
	 * 
	 * @throws IllegalStateException
	 *             v případě, že byla dříve nad tímto objektem zavolána metoda
	 *             {@link #makeImmutable()}.
	 * 
	 * @param m
	 *            - nový modifikátor
	 */
	public void addModifier(Modifier m) {
		checkMutability();
		modifiers.add(m);
	}
	
	/**
	 * Zruší nastavený modifikátor tokenu. Není-li daný modifikátor nastaven,
	 * nemá volání této metody žádný vliv.
	 * 
	 * @throws IllegalStateException
	 *             v případě, že byla dříve nad tímto objektem zavolána metoda
	 *             {@link #makeImmutable()}.
	 * 
	 * @param m
	 *            - Modifikátor, jehož nastavení má být zrušeno.
	 */
	public void removeModifier(Modifier m) {
		checkMutability();
		modifiers.remove(m);
	}
	
	/**
	 * Udává, zda je pro tento token nastaven daný modifikátor.
	 * 
	 * @param m
	 *            - modifikátor, jehož nastavení je testováno.
	 *            
	 * @return Vrací <code>true</code>, je-li pro tento token nastaven
	 *         modifikátor <code>m</code>, jinak vrací </code>false</code>.
	 */
	public boolean hasModifier(Modifier m) {
		return modifiers.contains(m);
	}

	/**
	 * <p>
	 * Vrací podtyp tokenu, nebo <code>null</code> v případě, že daný typ tokenu
	 * žádný podtyp nemá.
	 * </p>
	 * 
	 * <p>
	 * <strong>Poznámka:</strong> Překladač <code>javac</code>, který
	 * <em>není</em> distribuován s vývojovým prostředím Eclipse, alespoň do
	 * verze 1.6.0, obsahuje chybu, která způsobuje chybu překladu při
	 * přetypování generického výčtu nebo při jeho porovnání operátorem '=='
	 * resp. '!='. K řešení tohoto problému použijte statické metody třídy
	 * {@link GenericEnumsWorkaround}.
	 * </p>
	 * 
	 * @return Vrací podtyp tokenu, nebo <code>null</code> v případě, že daný
	 *         typ tokenu žádný podtyp nemá.
	 * 
	 * @see NamedEntityToken
	 * @see SemanticCategoryToken
	 * @see GenericEnumsWorkaround
	 */
	public abstract Enum<?> getSubtype();

	/**
	 * <p>
	 * Vrací textový obsah tokenu nebo <code>null</code> v případě, že daný
	 * token žádný textový obsah nemá.
	 * </p>
	 * 
	 * <p>
	 * Textový obsah je závislý na typu tokenu. Token nemusí definovat žádný
	 * textový obsah. V tom případě tato metoda vrací <code>null</code>. Tokeny,
	 * které typů, které textový obsah definován mají, nikdy nesmí vracet
	 * <code>null</code>, mohou však vrace prázdný řetězec.
	 * </p>
	 * 
	 * @return Vrací podtyp tokenu, nebo <code>null</code> v případě, že daný
	 *         typ tokenu žádný podtyp nemá.
	 *         
	 * @see WordToken#getText()
	 * @see LemmaToken#getText()
	 * @see PosToken#getText()
	 * @see NamedEntityToken#getText()
	 * @see SemanticCategoryToken#getText()
	 * @see ReferenceToken#getText()
	 */
	public abstract String getText();

	/**
	 * <p>
	 * Typ tokenu.
	 * </p>
	 * 
	 * <p>
	 * <strong>Poznámka:</strong> Tokeny některých typů mají definován podtyp
	 * (viz {@link Token#getSubtype()}).
	 * </p>
	 * 
	 * <p>
	 * <strong>Tabulka typů</strong>
	 * <table cellpadding="3">
	 * <tr>
	 * <th align="left">Typ</th>
	 * <th align="left">Implementace</th>
	 * <th align="left">Význam</th>
	 * <th align="left">Podmínka shody</th>
	 * </tr>
	 * <tr>
	 * <td>WORD</td>
	 * <td>WordToken</td>
	 * <td>slovo</td>
	 * <td>přesná shoda (case insensitive)</td>
	 * </tr>
	 * <tr>
	 * <td>LEMMA</td>
	 * <td>LemmaToken</td>
	 * <td>lemma</td>
	 * <td>shoda s lemmatem, tj. slovníkovým tvarem</td>
	 * </tr>
	 * <tr>
	 * <td>POS</td>
	 * <td>PosToken</td>
	 * <td>slovní druh</td>
	 * <td>shoda slovního druhu</td>
	 * </tr>
	 * <tr>
	 * <td>NAMED_ENTITY</td>
	 * <td>NamedEntityToken</td>
	 * <td>pojmenovaná entita</td>
	 * <td>shoda podtypu (viz {@link NamedEntityToken})</td>
	 * </tr>
	 * <tr>
	 * <td>SEMANTIC_CATEGORY</td>
	 * <td>SemanticCategoryToken</td>
	 * <td>sémantická kategorie</td>
	 * <td>podtypem definovaný vztah (viz {@link SemanticCategoryToken}).</td>
	 * </tr>
	 * <tr>
	 * <td>REFERENCE</td>
	 * <td>ReferenceToken</td>
	 * <td>reference jiný token</td>
	 * <td>nemá smysl</td>
	 * </tr>
	 * </table>
	 * 
	 * Sloupec <em>Podmínka shody</em> udává podmínku, při jejímž splnění (a jen
	 * při jejímž splnění) metoda {@link #matches(QueryToken)} vrací
	 * <code>true</code>.
	 * 
	 * @author Rudolf Šíma
	 * 
	 * @see WordToken
	 * @see LemmaToken
	 * @see PosToken
	 * @see NamedEntityToken
	 * @see SemanticCategoryToken
	 * @see ReferenceToken
	 */
	public enum Type {
		/** @see WordToken */
		WORD,
		/** @see LemmaToken */
		LEMMA,
		/** @see PosToken */
		POS,
		/** @see NamedEntityToken */
		NAMED_ENTITY,
		/** @see SemanticCategoryToken */
		SEMANTIC_CATEGORY,
		/** @see ReferenceToken */
		REFERENCE
	}

	/**
	 * Modifikátor tokenu. Udává vlastnost tokenu, která není závislá na
	 * konkrétním typu.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	public enum Modifier {
		/** Volitelně se vystytující token. */
		OPT,             
		/** Token značící pozici a typ očekávané odpovědi */
		ANSWER            
	}

	/**
	 * <p>
	 * Vytvoří nový token na základě číselného ID, názvu podtypu a textové
	 * hodnoty. V případě tokenu takového typu, který nemá podtyp (např. WORD),
	 * se použije název typu.
	 * </p>
	 * 
	 * <p>
	 * Tato metoda nalezne uplatnění především při načítání tokenů ze souboru
	 * a při ladění.
	 * </p>
	 * 
	 * <p>
	 * <strong>Příklady:</strong>
	 * 
	 * <pre>
	 * <code>
	 * // Vytvoření tokenu typu WORD s ID 10 a hodnotou "pes"
	 * Token t = Token.newToken(10, "WORD", "pes");
	 * 
	 * // Vytvoření tokenu typu NAMED_ENTITY s podtypem PERSON s ID 10 
	 * // a žádnou hodnotou (jejíž zadání by u tohoto typu nemělo smysl).
	 * Token t = Token.newToken(10, "PERSON", "");
	 * 
	 * // Vytvoření tokenu typu SEMANTIC_CATEGORY s podtypem SYNONYM s ID 10
	 * // a hodnotou "zemřít".
	 * Token t = Token.newToken(10, "SYNONYM", "zemřít")
	 * </code>
	 * </pre>
	 * 
	 * </p>
	 * 
	 * @param id
	 *            - Číselné ID tokenu.
	 * 
	 * @param subtype
	 *            - Název podtypu tokenu. V případě typu, který nemá podtyp, se
	 *            použije název typu. Název je case insensitive.
	 * 
	 * @param value
	 *            - Textová hodnota tokenu.
	 * @return
	 * @throws BadTemplateException
	 */
	public static Token newToken(int id, String subtype, String value) throws BadTemplateException {
		String subtypeUpper = subtype.toUpperCase();
		// Pokus použít parametr "subtype" jako název typu (nikoli podtypu).
		Token.Type type = valueOfEnum(Token.Type.class, subtypeUpper);
		if (type != null) {
			switch(type) {
			case WORD:
				return new WordToken(id, value);
			case LEMMA:
				return new LemmaToken(id, value);
			case POS:
				if (value.length() != 1)
					throw new BadTemplateException("POS tag must contain exactly one character.");
				return new PosToken(id, value.charAt(0));
			default:
				assert false;
				return null;
			}
	
		}
		// Pokus použí parametr "subtype" jako podtyp typu NAMED_ENTITY.
		NamedEntityToken.Subtype neSubtype = valueOfEnum(NamedEntityToken.Subtype.class, subtypeUpper);
		if (neSubtype != null)
			return new NamedEntityToken(id, neSubtype);
		// Pokus použí parametr "subtype" jako podtyp typu SEMANTIC_CATEGORY.
		SemanticCategoryToken.Subtype scSubtype = valueOfEnum(SemanticCategoryToken.Subtype.class, subtypeUpper);
		if (scSubtype != null)
			return new SemanticCategoryToken(id, value, scSubtype);
		// Neznámý podtyp.
		throw new BadTemplateException("'" + subtypeUpper + "' is not a valid value for the 'type' attribute.");
	}
	
	/**
	 * "Ověří" možnost měnit nastavení tokenu, tj. zda dosud nebyly změny
	 * zablokovány voláním {@link #makeImmutable()}. V případě, že jsou změny
	 * povoleny, neprovede žádnou akci, v opačném případě vyhodí výjimku.
	 * 
	 * @throws IllegalStateException
	 *             Vyhozena v případě, že je token zamčen proti změnám, tj. byla
	 *             dříve nad tímto objektem volána metody
	 *             {@link #makeImmutable()}.
	 */
	private void checkMutability() {
		if (immutable)
			throw new IllegalStateException("This token has been made immutable.");
	}

	/**
	 * Vrací prvek daného výčtového typu na základě jeho názvu. V případě
	 * neplatného názvu nikdy nevyhodí výjimku, ale vrátí <code>null</code>.
	 * 
	 * @param enumType
	 *            - Třída výčtováho typu, jehož prvek je hledán.
	 *            
	 * @param name
	 *            - Název prvku výčtového typu (case sensitive).
	 *            
	 * @return Vrací prvek výčtového typu nebo <code>null</code>, není-li jeho
	 *         název platný.
	 */
	private static <T extends Enum<T>> T valueOfEnum(Class<T> enumType, String name) {
		try {
			return Enum.valueOf(enumType, name);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Token))
			return false;
		Token other = (Token) obj;
		if (type != other.type)
			return false;
		if (getSubtype() != other.getSubtype())
			return false;  
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result
				+ ((modifiers == null) ? 0 : modifiers.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		Enum<?> subtype = getSubtype();
		String subtypeStr = subtype == null ? "" : String.format(",Subtype:%s", subtype);
		String value = getText();
		String valueStr = value == null ? "" : String.format(",Value:'%s'", value);
		String modStr = modifiers.isEmpty() ? "" : String.format(",Mod:{%s}", StringUtils.concat(modifiers, ","));		
		return String.format("Token[Type:%s%s%s%s]", type, subtypeStr, valueStr, modStr);
	}

	/**
	 * Udává, zda má token prázdnou hodnotu, tzn. jeho textová hodnota je buď
	 * <code>null</code> v případě tokenů, které nikdy nemají přiřazenu textovou
	 * hodnotu (např. {@link NamedEntityToken}), nebo je prázdný řetězec v ostatních
	 * případech.
	 * 
	 * @return Vrací <code>true</code>, nemá-li tento token přiřazen žádnou
	 *         textovou hodnotu nebo je tato hodnota prázdný řetězec. V opačném
	 *         případě vrací <code>false</code>.
	 */
	public boolean voidValue() {
		String text = getText();
		return text == null || "".equals(text);
	}

	/**
	 * Udává, zda se <em>typ</em> (nikoly podtyp) tohoto tokenu shoduje s
	 * <em>typem</em> jiného tokenu. Tato metoda existuje z důvodu jednoduchého
	 * zápisu a častého výskytu takového porovnání.
	 * 
	 * @param other
	 *            - Token, jehož typ je porovnán s typem tohoto tokenu.
	 * 
	 * @return Vrací <code>true</code>, shoduje-li se typ tohoto tokenu s typem
	 *         daného tokenu, jinak vrací <code>false</code>.
	 */
	public boolean typeEquals(Token other) {
		return type.equals(other.type);
	}

	/**
	 * Udává, zda se <em>podtyp</em> tohoto tokenu shoduje s podtypem jiného
	 * tokenu. (Tedy i typy se musí rovnat.) Tato metoda existuje z důvodu
	 * jednoduchého zápisu a častého výskytu takového porovnání.
	 * 
	 * @param other
	 *            - Token, jehož podtyp je porovnán s podtypem tohoto tokenu.
	 * 
	 * @return Vrací <code>true</code>, shoduje-li se podtyp tohoto tokenu s
	 *         podtypem daného tokenu, jinak vrací <code>false</code>.
	 */
	public boolean subtypeEquals(Token other) {
		if (!typeEquals(other))
			return false;
		Enum<?> subtype = getSubtype();
		if (subtype == null)
			return other.getSubtype() == null;
		return subtype.equals(other.getSubtype());
	}
}
