package lase.template;

import lase.util.LaseCalendar;

/**
 * <p>
 * Rozšířený token neboli token uživatelem zadané otázky nebo textu staženého z
 * internetu. Skádá ze standardního {@link Token}u, původního slova/sousloví
 * obsažného v textu a sémantické hodnoty, která se liší v závislosit na tokenu.
 * </p>
 * 
 * <p>
 * <strong>Příklad:</strong> Je-li v otázce "Kdy se narodil Karel IV.?" při
 * porovnávání s šablonou "kdy se &lt;lemma:narodit&gt; &lt;osoba&gt;" nalezeno
 * slovo "narodil" při porovnání s lemmatem "narodit", odpovídající
 * <code>QueryToken</code> se bude skládat z původního tokenu šablony (instance
 * třídy {@link LemmaToken}), původní slovo z textu bude mít hodnotu "narodil".
 * Sémantická hodnota bude v tomto případě instance třídy String a bude mít
 * hodnotu "narodit", neboť jde o lemma.
 * </p>
 * 
 * 
 * @author Rudolf Šíma
 * 
 */
public class QueryToken {
	private static final long serialVersionUID = 1L;

	/**
	 * Token pocházející ze šablony. Udává vlastnosti tohoto rozšířeného tokenu.
	 */
	private Token token;

	/**
	 * Původní slovo (nebo spojení slov), které bylo při porovnávání
	 * se šablonou vyhodnoceno jako odpovídající tokenu v šabloně.
	 */
	private String originalWord;

	/**
	 * Sémantická hodnota tokenu. Závisí na typu tokenu. Její datový typ by měl
	 * být pro stejný typ resp. podtyp tokenu vždy stejný. Například pro token
	 * typu LEMMA to bude vždy instance třídy String se slovníkovým tvarem
	 * původního slova, pro typ NAMED_ENTITY podtyp DATE, to bude instance třídy
	 * {@link LaseCalendar} s daným datem.
	 */
	private Object semanticValue;

	/**
	 * Vytvoří novou instanci rozšířeného tokenu a nastaví všechny tři jeho
	 * atributy na dané hodnoty.
	 * 
	 * @param token
	 *            - Token pocházející ze šablony. Udává vlastnosti tohoto
	 *            tokenu.
	 * 
	 * @param originalWord
	 *            - Původní slovo (nebo spojení slov), které bylo při
	 *            porovnávání se šablonou vyhodnoceno jako odpovídající tokenu v
	 *            šabloně.
	 * 
	 * @param semanticValue
	 *            - Sémantická hodnota tokenu. Závisí na typu tokenu. Její
	 *            datový typ by měl být pro stejný typ resp. podtyp tokenu vždy
	 *            stejný. Například pro token typu LEMMA to bude vždy instance
	 *            třídy String se slovníkovým tvarem původního slova, pro typ
	 *            NAMED_ENTITY podtyp DATE, to bude instance třídy
	 *            {@link LaseCalendar} s daným datem
	 */
	public QueryToken(Token token, String originalWord, Object semanticValue) {
		this.token = token;
		this.originalWord = originalWord;
		this.semanticValue = semanticValue;
	}

	/**
	 * <p>
	 * Vytvoří novou instanci rozšířeného tokenu a nastaví všechny tři jeho
	 * atributy pouze na základě původního slova. Pro atribut tokenu ze šablony
	 * ({@link #token}) je vytvořena nová instance třídy {@link WordToken},
	 * jejíž textová hodnota parametr <code>wordToken</code>. Atributy původné
	 * slova i sémantické hodnoty jsou nastaveny na hodnotu parametru
	 * <code>wordToken</code>
	 * </p>
	 * 
	 * <p>
	 * Tento konstruktor existuje z důvodu pohodlného vytvoření rozšířeného
	 * tokenu při testování.
	 * </p>
	 * 
	 * @see #QueryToken(Token, String, Object)
	 * 
	 * @param wordToken
	 *            - Slovo, na základě kterého je vytvořen rozšířený token.
	 */
	public QueryToken(String wordToken) {
		this(new WordToken(wordToken));
		semanticValue = wordToken.toLowerCase();
	}

	/**
	 * Vytvoří novou instanci rozšířeného tokenu a nastaví všechny tři jeho
	 * atributy pouze na základě tokenu šablony, kterým je parametr
	 * <code>wordToken</code>. Atribut původního slova i sémantické hodnoty je
	 * nastaven na hodnotu, kterou vrací metoda {@link WordToken#getWord()}.
	 * 
	 * <p>
	 * Tento konstruktor existuje z důvodu pohodlného vytvoření rozšířeného
	 * tokenu při testování.
	 * </p>
	 * 
	 * @param wordToken
	 *            - Token šablona, na základě kterého se vytvoří nový rozšířený
	 *            token.
	 */
	public QueryToken(WordToken wordToken) {
		this.token = wordToken;
		this.originalWord = wordToken.getWord();
		this.semanticValue = this.originalWord.toLowerCase();
	}

	/**
	 * Vrací token šablony, který udává vlastnosti tohoto rozšířeného tokenu.
	 * Vrácený token šablony je zpravidla ten token, na základě kterého bylo
	 * původní slovo ({@link #originalWord}) rozpoznáno jako vyhovující šabloně.
	 * 
	 * @return Vrací token šablony.
	 */
	public Token getToken() {
		return token;
	}


	/**
	 * Vrací původní slovo z textu otázky nebo staženého textu, ve kterém se
	 * vyhledává odpověď. Toto slovo bylo při porovnání s šablonou vyhodnoceno
	 * jako shodující se s {@link #token}em, který vrací getter
	 * {@link #getToken()}.
	 * 
	 * @return Vrací původní slovo z textu.
	 */
	public String getOriginalWord() {
		return originalWord;
	}


	/**
	 * Vrací sémantickou hodnotu původního slova z otázky resp. staženého textu,
	 * na základě něhož byl vytvořen tento rozšířený token. Datový typ záleží
	 * na typu tokenu.
	 * 
	 * @see #getToken()
	 * 
	 * @return Vrací sémantickou hodnotu původního slova.
	 */
	public Object getSemanticValue() {
		return semanticValue;
	}


	/**
	 * Zjistí, zda se typ tokenu ze šablony, který je jedním z atributů tohoto
	 * rozšířeného tokenu, shoduje s typem tokenu jiného rozšířeného tokenu.
	 * 
	 * @param other
	 *            - Jiný rozšířený token, se kterým bude tento porovnán.
	 *            
	 * @return Vrací <code>true</code>, pokud se typy shodují, jinak vrací
	 *         <code>false</code>.
	 */
	public boolean typeEquals(QueryToken other) {
		if (other == null)
			return false;
		return token.typeEquals(other.token);
	}
	
	/**
	 * Zjistí, zda se podtyp tokenu ze šablony, který je jedním z atributů
	 * tohoto rozšířeného tokenu, shoduje s podtypem tokenu jiného rozšířeného
	 * tokenu.
	 * 
	 * @param other
	 *            - Jiný rozšířený token, se kterým bude tento porovnán.
	 * 
	 * @return Vrací <code>true</code>, pokud se podtypy shodují, jinak vrací
	 *         <code>false</code>.
	 */
	public boolean subtypeEquals(QueryToken other) {
		return token.subtypeEquals(other.token);
	}
	
	/**
	 * Zjistí, zda se dané slovo, kterým je paramety <code>word</code> rovná
	 * atributu původního slova tohoto rozšířeného tokenu.
	 * 
	 * @param word
	 *            - Slovo, se kterým bude porovnáno původní slovo z textu otázky
	 *            nebo staženého textu, které je atributem tohoto rozšířeného
	 *            tokenu.
	 *            
	 * @return Vrací <code>true</code>, pokud se slova rovnají, jinak vrací
	 *         <code>false</code>.
	 */
	public boolean wordEquals(String word) {
		return word.equals(originalWord);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return originalWord;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((originalWord == null) ? 0 : originalWord.hashCode());
		result = prime * result
				+ ((semanticValue == null) ? 0 : semanticValue.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QueryToken other = (QueryToken) obj;
		if (originalWord == null) {
			if (other.originalWord != null)
				return false;
		} else if (!originalWord.equals(other.originalWord))
			return false;
		if (semanticValue == null) {
			if (other.semanticValue != null)
				return false;
		} else if (!semanticValue.equals(other.semanticValue))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}
}
