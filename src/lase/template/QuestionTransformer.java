package lase.template;

import java.util.List;

/**
 * <p>
 * Rozhraní tříd implementujících převod uživatelem otázky na pahýl opovědi, z
 * nějž se později sestaví vyhledávací řetězec pro webový vyhledávač, ze kterého
 * se získá korpus s odpověďmi.
 * </p>
 * 
 * <p>
 * Jednodnoduchý algoritmus může být založen na prosté změně pořadí slov ve
 * větě, takže dojde k převodu otázky jako v násleujícím příkladu:<br>
 * <strong>Otázka:</strong> <em>Kdy se narodil Karel IV.?</em><br>
 * <strong>Odpověď:</strong> <em>Karel IV. se narodil &lt;datum&gt;.</em><br>
 * Protože otázka začínala klíčovým slovem <em>kdy</em>, umístí se na konec
 * pahýlu odpovědi token typu {@code NAMED_ENTITY} označující časový údaj. V
 * tomto příkladu to bude podtyp {@code DATE}. Dále by bylo vhodné vytvořit
 * další dva vzory odpovědi, na jejichž konci by byly očkávány pojmenované
 * entity podtypů {@code TIME} a {@code DATE_TIME}, protože oba případy vyhovují
 * otázce začínající slovem <em>Kdy</em>.
 * </p>
 * 
 * <p>
 * Složitější algoritmy používají k převodu otázky na pahýl odpovědi předem
 * vytvořené šablony otázek a příslušných vyhledávacích vzorů. Viz
 * {@link TemplateQuestionTransformer}.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public interface QuestionTransformer {

	/**
	 * Podle uživatelem zadané otázky vytvoří seznam možných vyhledávacích
	 * vzorů, na základě kterých lze webovým vyhledávačem získat korpus, ve
	 * kterém se podle těchto vzorů hledá správná odpověď.
	 * 
	 * @param question
	 *            Předzpracovaná uživatelem zadané otázka ve formě cesty v grafu
	 *            pojmenovaných entit.
	 * 
	 * @return Vrací seznam vyhledávacích vzorů.
	 */
	public List<SearchPattern> transform(Query question);
}
