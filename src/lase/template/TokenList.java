package lase.template;

import java.util.List;

import lase.util.ReadOnlyVector;
import lase.util.StringUtils;

/**
 * <p>
 * Read-only seznam tokenů šablon ({@link Token}).
 * </p>
 * 
 * <p>
 * Implementuje vzor otázky nebo vyhledávací vzor, tj. vzor hledané odpovědi, v šablonách.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 * @see Template
 *
 */
public class TokenList extends ReadOnlyVector<Token> {
	private static final long serialVersionUID = 1L;

	/**
	 * Vytvoří nový prázdný seznam tokenů.
	 */
	public TokenList() {
	}

	/**
	 * Vytvoří seznam tokenů, do kterého zkopíruje tokeny ze seznamu {@code
	 * list}.
	 * 
	 * @param list
	 *            Seznam tokenů, které se zkopírují.
	 */
	public TokenList(List<Token> list) {
		super(list);
	}
	
	/**
	 * Vrací řetězcovou reprezentaci seznamu pro ladicí účely.
	 */
	public String toString() {
		return String.format("TokenList[%s]", StringUtils.concat(this, ","));
	}
}
