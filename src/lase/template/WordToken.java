package lase.template;

/**
 * <p>
 * Token šablony, u kterého je při porovnání požadována přesná textová shoda
 * konkrétního tvaru slova bez ohledu na velikost písmen.
 * </p>
 * 
 * <p>
 * <code>WordToken</code> může být <em>vyjádřený</em> i <em>nevyjádřený</em>.
 * Je-li token nevyjádřený, znamená to, že hodnota atributu <code>word</code> je
 * prázdný řetězec nebo <code>null</code> (jinými slovy, metoda
 * {@link Token#voidValue()} vrací <code>true</code>. Nevyjádřený
 * <code>WordToken</code> může zastupovat libovolné slovo v zadané otázce nebo
 * staženém textu.
 * </p>
 * 
 * <p>
 * V případě <code>WordToken</code> je testována textová shoda slova z šablony
 * nebo staženého textu s atributem <code>word</code> (bez ohledu na velikost
 * písmen).
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class WordToken extends Token {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Atribut <code>word</code> udává slovo, s nímž se při vyhledávání shody s
	 * šablonou testuje přesná textová shoda bez ohledu na velikost písmen.
	 */
	private String word;

	/**
	 * Vytvoří a inicializuje nový <code>WordToken</code>. Číselné ID je
	 * nastaveno na hodnotu 0.
	 * 
	 * @param word
	 *            - udává slovo, s nímž se při vyhledávání shody s šablonou
	 *            testuje přesná textová shoda bez ohledu na velikost písmen.
	 */
	public WordToken(String word) {
		this(0, word);
	}

	/**
	 * Vytvoří a inicializuje nový <code>WordToken</code>.
	 * 
	 * @param id
	 *            - číselné ID tokenu (sloužící k odkazování na něj).
	 * 
	 * @param word
	 *            - udává slovo, s nímž se při vyhledávání shody s šablonou
	 *            testuje přesná textová shoda bez ohledu na velikost písmen.
	 */
	public WordToken(int id, String word) {
		super(id, Token.Type.WORD);
		this.word = word;
	}

	/**
	 * Vrací hodnotu atributu <code>word</code> tohoto {@link WordToken}u, která
	 * je využita při testování shody s šablonou.
	 * 
	 * @return Vrací hodnotu atributu <code>word</code> tohoto {@link WordToken}
	 *         u, která je využita při testování shody s šablonou.
	 */
	public String getWord() {
		return word;
	}

	/**
	 * Vrací stejný řetězec jako metoda {@link #getWord()}. Uvedena z důvodu
	 * dědění abstraktní třídy {@link Token}.
	 */
	@Override
	public String getText() {
		return word;
	}

	/**
	 * Vždy vrací <code>null</code>. Token typu WORD nemá definovány žádné
	 * podtypy.
	 */
	@Override
	public Enum<?> getSubtype() {
		return null;
	}
	
	/* (non-Javadoc)
	 * @see lase.template.Token#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	/**
	 * Vrací <code>true</code>, je-li nalezena shoda tohoto tokenu s tokenem
	 * jiným tokenem, tj. atributy word těchto dvou tokenů se rovnají bez ohledu
	 * na velikost písmen. Jinak vrací <code>false</code>.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WordToken other = (WordToken) obj;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equalsIgnoreCase(other.word))
			return false;
		return true;
	}

	/**
	 * Vrací <code>true</code>, platí-li, že {@link #voidValue()} pro tento
	 * {@link Token} vrací <code>true</code> (protože s nevyjádřeným
	 * {@link WordToken}em se shoduje každé slovo) nebo pokud slovo tohoto
	 * tokenu shoduje textově shoduje se slovem z otázky nebo staženého textu
	 * bez ohledu na velikost písmen. V ostatních případech vrací
	 * <code>false</code>.
	 */
	@Override
	public boolean matches(QueryToken queryToken) {
		return voidValue() || equals( queryToken.getToken());
	}
}
