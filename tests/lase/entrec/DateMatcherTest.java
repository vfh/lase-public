package lase.entrec;

import static org.junit.Assert.assertEquals;

import java.util.List;

import lase.app.LaseApp;
import lase.app.SharedObjects;
import lase.morphology.MorphWord;
import lase.morphology.MorphologicalAnalyser;
import lase.morphology.Tokenizer;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DateMatcherTest {
	private EntityMatcher dateMatcher = new DateMatcher();
	private MorphologicalAnalyser analyser;
	private Tokenizer tokenizer;

	@BeforeClass
	public static void init() throws Exception {
		LaseApp.initializeDefaults();
	}
	
	@Before
	public void setUp() throws Exception {
		analyser = SharedObjects.getMorphologicalAnalyser();
		tokenizer = SharedObjects.getTokenizer();
	}

	@Test
	public void testSearch() throws Exception {
		String testSnippet = "prezidenti – Tomáš Garigue Masaryk (1918-1935) a Edvard Beneš (1935-1938) • demokratický stát s vyspělým .....";
		List<String> tokens = tokenizer.tokenize(testSnippet);
		List<MorphWord> morphWords = analyser.analyse(tokens);
		List<PositionedEntity> dates = dateMatcher.search(morphWords);
		assertEquals(4, dates.size());
//		for (PositionedEntity date : dates) {
//			System.out.println(date);
//		}
	}

}
