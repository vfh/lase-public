package lase.template;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import lase.app.Settings;
import lase.app.SharedObjects;
import lase.util.LaseException;
import lase.util.PathUtils;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

// TODO test
public class TemplateTest {

	private static List<Template> templates;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try {
			// načtení šablon
			String absoluteTemplatePath = PathUtils.convertRelativePath(Settings.templatesdirectory);
			templates = Template.loadDirectory(absoluteTemplatePath);
			// inicializace sdílených objektů
			if (!SharedObjects.initialized())
				new SharedObjects();
		} catch (Exception e) {
			throw new LaseException(e);
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMatchQueryLong() {
		List<QueryToken> tokenList = new ArrayList<QueryToken>();
		tokenList.add(new QueryToken(new WordToken("kdo"),"kdo","kdo"));
		tokenList.add(new QueryToken(new WordToken("napsal"),"napsal","napsat"));
		tokenList.add(new QueryToken(new WordToken("knihu"),"knihu","kniha"));
		tokenList.add(new QueryToken(new WordToken("Harry"),"Harry","Harry"));
		tokenList.add(new QueryToken(new WordToken("Potter"),"Potter","Potter"));
		Query query = new Query(tokenList);
		Template template = templates.get(23);
		TemplateMatchResult result = template.matchQuery(query);
		assertTrue("dotaz nevyhovuje šabloně",result.matches());
		
	}
	
	@Test
	public void testMatchQueryShort() {
		List<QueryToken> tokenList = new ArrayList<QueryToken>();
		tokenList.add(new QueryToken(new WordToken("kdo"),"kdo","kdo"));
		tokenList.add(new QueryToken(new WordToken("napsal"),"napsal","napsat"));
		tokenList.add(new QueryToken(new WordToken("knihu"),"knihu","kniha"));
		tokenList.add(new QueryToken(new WordToken("Eragon"),"Eragon","eragon"));
		Query query = new Query(tokenList);
		Template template = templates.get(23);
		TemplateMatchResult result = template.matchQuery(query);
		assertTrue("dotaz nevyhovuje šabloně",result.matches());
		
	}

}
