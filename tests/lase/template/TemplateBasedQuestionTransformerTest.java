package lase.template;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Test převodu otázky na začátek odpovědi.
 * 
 * @author Rudolf Šíma
 *
 */
@RunWith(value = Parameterized.class)
public class TemplateBasedQuestionTransformerTest {
//	Lemmatizer lemmatizer;
//	QuestionTransformer transformer;
//	String expectedOutput;
//	String input;
//
//	public QuestionTransformerTest(String input, String expectedOutput) {
//		this.expectedOutput = expectedOutput;
//		this.input = input;
//	}
//
//	@BeforeClass
//	public static void setUpBeforeClass() throws Exception {
//		BasicConfigurator.configure();
//		Logger.getRootLogger().setLevel(Level.WARN);
//	}
//
//	@AfterClass
//	public static void tearDownAfterClass() throws Exception {
//	}
//
//	@Before
//	public void setUp() throws Exception {
//		MorphologicalAnalyser analyser = Common.getMorphologicalAnalyser();
//		lemmatizer = new StandardLemmatizer(analyser);
//		transformer = new SimpleQuestionTransformer();
//	}
//
//	@After
//	public void tearDown() throws Exception {
//		transformer.close();
//	}
//
//	@Parameters
//	public static Collection<Object[]> parameters() {
//		return Arrays .asList(new Object[][] {
//		{ "Kdy se narodil Karel IV?", "Karel IV se narodil" },
//		{ "Kudy se jde ke škole?", "Ke škole se jde" },
//		{ "Jak se dostanu z Plzně do Prahy?", "Z Plzně do Prahy se dostanete" },
//		{ "Kdo z plzeňských hokejistů je vrahem?", "Vrahem je" },
//		{ "Jak se píše diplomka?", "Diplomka se píše" },
//		{ "Přes jakou vesnici se dá ještě jet z Plzně do Prahy", "Z Plzně do Prahy se dá ještě jet přes" },
//		{ "Kde je O2 arena?", "O2 arena je" },
//		{ "Kolik obyvatel má San Marino?", "San Marino má" }
//		});
//	}
//
//	@Test
//	public void test() throws Exception {
//		TransformerResult result = transformer.transform(input);
//		String output = result.getAnswerOutset().toLowerCase();
//		Assert.assertEquals(expectedOutput.toLowerCase(), output);
//	}
}
