package lase.morphology;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class LaseMorphologicalAnalyserTest {

	private static MorphologicalAnalyser analyser;
	
	private static final String sentence = "Karel Čapek se narodil 9. ledna 1890 v Malých Svatoňovicích";
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		analyser = new LaseMorphologicalAnalyser();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAnalyseListOfString() {
	}

	@Test
	public void testAnalyseStringWord() throws MorphologyException {
		List<MorphWord> list = analyser.analyse(sentence);
		String text = "";
		for(MorphWord mw:list)
		{
			text += mw.getWord()+" ";
		}
		text = text.trim();
		assertEquals(sentence, text);
	}
	
	@Test
	public void testAnalyseStringLemma() throws MorphologyException {
		List<MorphWord> list = analyser.analyse(sentence);
		String text = "";
		for(MorphWord mw:list)
		{
			text += mw.getLemma()+" ";
		}
		text = text.trim();
		assertEquals("Karel Čapek se narodit 9. leden 1890 v malý Svatoňovice", text);
	}
	
	@Test
	public void testAnalyseStringPOSTag() throws MorphologyException {
		List<MorphWord> list = analyser.analyse(sentence);
		assertEquals('N',list.get(0).getPosTag());
		assertEquals('N',list.get(1).getPosTag());
		assertEquals('V',list.get(3).getPosTag());
		assertEquals('N',list.get(5).getPosTag());
		assertEquals('A',list.get(8).getPosTag());
	}

}
